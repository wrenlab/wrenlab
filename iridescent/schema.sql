CREATE TABLE term (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    birth_date INTEGER NOT NULL,
    frequency INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE 'synonym' (
    id INTEGER PRIMARY KEY NOT NULL,
    term_id INTEGER NOT NULL,

    text VARCHAR NOT NULL,
    source VARCHAR NOT NULL,
    birth_date INTEGER NOT NULL,
    category VARCHAR NOT NULL, -- ObjectType
    frequency INTEGER NOT NULL DEFAULT 0,
    case_sensitive BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (term_id) REFERENCES term (id)
);

CREATE TABLE edge (
    e1 INTEGER NOT NULL,
    e2 INTEGER NOT NULL,
    s_count INTEGER NOT NULL DEFAULT 0,
    a_count INTEGER NOT NULL DEFAULT 0,
    birth_date INTEGER NOT NULL,
    MI FLOAT,

    FOREIGN KEY (e1) REFERENCES term (id),
    FOREIGN KEY (e2) REFERENCES term (id)
);

CREATE TABLE xref (
    term_id INTEGER NOT NULL,
    xref_prefix VARCHAR NOT NULL,
    xref_id INTEGER NOT NULL
);

CREATE TABLE metadata (
    key VARCHAR,
    value BLOB
);

CREATE TABLE posting (
    document_id INTEGER NOT NULL,
    term_id INTEGER NOT NULL,

    FOREIGN KEY (term_id) REFERENCES term (id)
);

--CREATE TABLE section (
--    id INTEGER PRIMARY KEY NOT NULL,
--    name VARCHAR UNIQUE NOT NULL,
--);

--CREATE TABLE synonym_posting (
--    synonym_id INTEGER,
--    document_id INTEGER
--);

--CREATE TABLE term_posting (
--  term_id INTEGER,
--  document_id INTEGER
--);

--    section_id INTEGER,
--    sentence_index INTEGER,

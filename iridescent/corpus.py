import os

import numpy as np
import gensim.corpora
import gensim.models
import gensim.similarities
import nltk.tokenize

import wrenlab.ncbi.medline

import iridescent.config

# FIXME: map gensim document ID <-> PMID

def preprocess_text(text):
    return [w.lower() for w in nltk.tokenize.word_tokenize(text)]

class MEDLINE(object):
    def __init__(self):
        self._directory = os.path.join(iridescent.config.ROOT, "corpus", "MEDLINE")
        os.makedirs(self._directory, exist_ok=True)
        if not os.path.exists(self._get_path("corpus.mm")):
            self.initialize()

        self.dictionary = gensim.corpora.Dictionary.load(self._get_path("dictionary.pkl"))
        self.corpus = gensim.corpora.MmCorpus(self._get_path("corpus.mm"))
        self.external_id = np.load(self._get_path("external_id.npy"))
        self.LSI = gensim.models.LsiModel.load(self._get_path("LSI"))
        self.similarity = gensim.similarities.Similarity.load(self._get_path("similarity.0"))

    def reset(self):
        for fname in os.listdir(self._directory):
            path = os.path.join(self._directory, fname)
            os.unlink(path)

    def _get_path(self, name):
        return os.path.join(self._directory, name)

    def initialize(self):
        self.reset()

        dictionary_path = self._get_path("dictionary.pkl")
        corpus_path = self._get_path("corpus.mm")

        self.dictionary = gensim.corpora.Dictionary((x[1] for x in self._text()))
        self.dictionary.save(dictionary_path)

        external_id = []
        def bag_of_words():
            for id, text in self._text():
                external_id.append(id)
                yield self.dictionary.doc2bow(text)

        gensim.corpora.MmCorpus.serialize(corpus_path, bag_of_words())
        self.corpus = gensim.corpora.MmCorpus(corpus_path)

        external_id = np.array(external_id, dtype=int)
        np.save(self._get_path("external_id"), external_id)

        self.LSI = gensim.models.LsiModel(self.corpus, id2word=self.dictionary)
        self.LSI.save(self._get_path("LSI"))

        self.similarity = gensim.similarities.Similarity(self._get_path("similarity"),
                self.LSI[self.corpus], num_features=len(self.dictionary))

    def similar_documents(self, text):
        vector = self.dictionary.doc2bow(preprocess_text(text))
        dx = self.similarity[vector]
        return dx

    def _text(self):
        cx = wrenlab.ncbi.medline.MEDLINE()

        import itertools
        #for article in itertools.islice(cx.articles, 1000):
        for article in cx.articles:
            if not article.title:
                continue
            text = " ".join([article.title, article.abstract or ""])
            yield article.id, preprocess_text(text)

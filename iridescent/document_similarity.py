import os

import gensim
import wrenlab.ncbi.medline

class Search(object):
    def __init__(self):

    def corpus(self):
        def tokenize(text):
            return [w.lower() for w in nltk.tokenize.word_tokenize(text)]

        import itertools
        for article in itertools.islice(self._cx.articles, 1000):
            text = " ".join([article.title, article.text or ""])
            yield tokenize(text)

    def train(self):
        self._dictionary = gensim.corpora.Dictionary(self.corpus())
        it = (self._dictionary.doc2bow(text) for text in self.corpus())

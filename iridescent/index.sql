--CREATE INDEX ix_xref_term_id ON xref (term_id);
--CREATE INDEX ix_xref_xref_prefix ON xref (xref_prefix);
--CREATE INDEX ix_xref_xref_id ON xref (xref_id);

--CREATE INDEX ix_synonym_posting_synonym_id ON synonym_posting (synonym_id);
--CREATE INDEX ix_synonym_posting_document_id ON synonym_posting (document_id);

--CREATE INDEX ix_term_posting_term_id ON term_posting (term_id);
--CREATE INDEX ix_term_posting_document_id ON term_posting (document_id);

CREATE INDEX ix_synonym_term_id ON 'synonym' (term_id);
CREATE INDEX ix_synonym_category ON 'synonym' (category);
CREATE INDEX ix_synonym_source ON 'synonym' (source);

CREATE INDEX ix_edge_e1 ON edge (e1);
CREATE INDEX ix_edge_e2 ON edge (e2);

CREATE INDEX ix_posting_term_id ON posting (term_id);
CREATE INDEX ix_posting_document_id ON posting (document_id);

import contextlib
import collections
import os
import sqlite3
import logging
import math
import gzip
import pickle

import pandas as pd
import numpy as np
import scipy.sparse
import gensim
import sklearn.metrics

import mdbread
import wrenlab.outlier

logging.basicConfig()
LOG = logging.getLogger("iridescent")
LOG.setLevel(logging.INFO)

sqlite3.register_adapter(np.int64, lambda val: int(val))

class Ragged2DArray(object):
    """
    Compactly store a list of lists of elements with the same numpy dtype (just integers for now).
    """
    def __init__(self, index, data):
        self._index = index
        self._data = data

    @staticmethod
    def from_arrays(index, data):
        # constructor for when index array is IDs (same length as data, as opposed to slice offsets)
        # FIXME: sort by index if needed
        index = np.concatenate([np.where(index[:-1] != index[1:])[0], np.zeros(1, dtype=int)])
        index[-1] = data.shape[0]
        return Ragged2DArray(index, data)

    @staticmethod
    def from_iterator(iterator):
        index = []
        data = np.zeros(16, dtype=int)

        position = 0
        for row in iterator:
            N = len(row)
            index.append(position)
            if (position + N) > data.shape[0]:
                # grow data buffer logarithmically
                data = np.concatenate([data, np.zeros(len(data), dtype=int)])
            data[position:(position+N)] = row
            position += N

        data = data[:position].copy()
        index.append(position)
        index = np.array(index, dtype=int)
        return Ragged2DArray(index, data)

    def __getitem__(self, i):
        ix = self._index[i]
        return self._data[ix:self._index[i+1]]

    def __len__(self):
        return len(self._index) - 1

    def __iter__(self):
        for i in range(len(self._index) - 1):
            yield self.__getitem__(i)


class PostingsCorpus():
    def __init__(self, path):
        self.path = path
        self.size = 0
        for _ in self:
            self.size += 1

    def __len__(self):
        return self.size

    def __iter__(self):
        prev = None
        terms = set()

        with gzip.open(self.path, "rt") as h:
            for line in h:
                document_id, term_id = line.strip().split("\t")
                terms.add(str(term_id))
                if (prev is not None) and (document_id != prev):
                    yield list(terms)
                    terms = set()
                prev = document_id
                terms.add(term_id)

class HasConnection(object):
    def cursor(self):
        return contextlib.closing(self._cx.cursor())

class HasMetadataTable(HasConnection):
    @property
    def metadata(self):
        return Metadata(self._cx)

class Metadata(HasConnection):
    def __init__(self, cx):
        self._cx = cx

    def __setitem__(self):
        with self.cursor() as c:
            pass

class Postings(Ragged2DArray):
    def __init__(self, iterator):
        array = Ragged2DArray.from_iterator(iterator)
        super().__init__(array._index, array._data)

    def __iter__(self):
        for row in super().__iter__():
            yield list(map(str, row))

    def frequencies(self, min_count=5):
        assert min_count >= 1
        o = np.bincount(self._data)
        ix = np.where(o >= min_count)[0]
        return {str(k):v for k,v in zip(ix,o[ix])}

class ClassicAlgorithms(object):
    def __init__(self, database):
        self._db = database

    def direct(self, term_id):
        if isinstance(term_id, str):
            term_id = self._db.synonym_id[term_id]
        o = {}
        with self._db.cursor() as c:
            c.execute("""
            SELECT e2, s_count + a_count, MI
                FROM edge
                WHERE e1=?
            UNION
            SELECT e1, s_count + a_count, MI
                FROM edge
                WHERE e2=?
            """, (term_id, term_id))
            o = pd.DataFrame.from_records(c, columns=["TermID", "EdgeFrequency", "MI"])\
                    .set_index(["TermID"])
        return self._db.terms.join(o, how="inner")\
                .sort_values("MI", ascending=False)

    def implicit(self, term_id):
        if isinstance(term_id, str):
            term_id = self._db.synonym_id[term_id]
        with self._db.cursor() as c:
            c.execute("""
            SELECT AB.e2 AS B, BC.e2 AS C, 
                    AB.s_count + AB.a_count AS AB_Score, AB.MI AS AB_MI,
                    BC.s_count + BC.a_count AS BC_Score, BC.MI AS BC_MI
                FROM edge AB
            INNER JOIN
                edge BC
                ON BC.e1=AB.e2
            WHERE AB.e1=? AND BC.e2!=?
            """, (term_id,term_id))
            df = pd.DataFrame.from_records(c, columns=["B", "C", "AB_Score", "AB_MI", "BC_Score", "BC_MI"])


        o = []
        for C,subset in df.groupby("C"):
            Strength = np.minimum(subset.AB_Score, subset.BC_Score).sum()
            MI = np.minimum(subset.AB_MI, subset.BC_MI).mean()
            o.append((C, Strength, MI))
        o = pd.DataFrame.from_records(o, columns=["TermID", "IStrength", "IMI"])\
                .set_index(["TermID"])

        direct = self.direct(term_id).loc[:,["EdgeFrequency", "MI"]]
        direct.columns = ["DStrength", "DMI"]
        o = direct.join(o, how="right")

        return self._db.terms.join(o, how="inner")\
                .sort_values("IMI", ascending=False)\
                .round(3)


class Word2Vec(object):
    def __init__(self, db, model):
        self._db = db
        self._model = model

    def _get_id(self, key):
        terms = self._db.terms
        query_id = int(terms.loc[terms.Name == key,:].index[0])
        return str(query_id)

    def __getitem__(self, key):
        return self._model.wv.get_vector(self._get_id(key))

    def _format_most_similar(self, iterator):
        terms = self._db.terms
        o = []
        for k,score in iterator:
            term_id = int(k)
            name = terms.Name.loc[term_id]
            count = terms.NodeFrequency.loc[term_id]
            o.append((term_id, name, count, score))
        o = pd.DataFrame\
                .from_records(o, columns=["TermID", "Name", "Count", "Distance"])\
                .set_index(["TermID"])\
                .query("Count >= 10")\
                .sort_values("Distance")
        return o

    def most_similar(self, key):
        if not isinstance(key, list):
            key = [key]

        terms = self._db.terms

        for i,k in enumerate(key):
            query_id = int(terms.loc[terms.Name == k,:].index[0])
            key[i] = str(query_id)

        iterator = self._model.wv.most_similar(topn=100, positive=key)
        return self._format_most_similar(iterator)

    def distance(self, key, category=None):
        terms = self._db.terms
        query_id = terms.loc[terms.Name == key,:].index[0]
        o = self._model.wv.distances(str(query_id))
        ix = list(map(int, self._model.wv.index2word))
        o = self._format_most_similar(zip(ix, o))
        if category is not None:
            o = o.loc[o.index.isin(self._db.category[category]),:]
        return o

    def _id_to_index(self):
        return {int(key):i for i,key in  enumerate(self._model.wv.index2word)}

    def category(self, category):
        """
        Return vector data for each element in the given category.
        """
        M = self._id_to_index()
        keys = self._db.category[category]
        keys = list(set(M) & set(keys))
        X = self._model.wv.vectors[[M[k] for k in keys],:]
        o = pd.DataFrame(X, index=keys)
        return o.loc[o.index.sort_values(),:]

    def gene_category_distance_matrix(self, category):
        Xg = self.category("Gh")
        M = self._db.term_gene()
        ix = list(set(Xg.index) & set(M))
        Xg = Xg.loc[ix,:]
        Xg.index = [M[k] for k in ix]

        Xc = self.category(category)

        o = pd.DataFrame(sklearn.metrics.pairwise.cosine_distances(Xg, Xc),
                index=Xg.index, columns=Xc.index)
        o.index.name = "GeneID"
        o.columns.name = "TermID"
        return o

    def gene_term(self):
        ok = set(self._model.wv.index2word)
        return {v:str(k) for k,v in self._db.term_gene().items() if str(k) in ok}

    def gene_enrichment(self, query, offset=5):
        if isinstance(query, pd.Series):
            if not query.dtype.name == "bool":
                query = query < query.quantile(0.05)
            query = list(query.index[query])
        # now it is a sequence of ints

        gene_term = self.gene_term()
        ok = set(gene_term) & set(query)
        # FIXME: actually not right background 
        nok = set(gene_term) - set(query)
        positive = [gene_term[k] for k in ok]
        negative = [gene_term[k] for k in nok]
        #negative = [gene_term[k] for k in query.index[~query]]

        def remove_outliers(terms):
            X = pd.DataFrame({k:self._model.wv.get_vector(k) for k in terms})
            X = wrenlab.outlier.remove(X.T, iterations=20)
            LOG.info("{}% of query terms removed as outliers, {} remaining".format(
                int(100 * X.shape[0] / len(terms)), X.shape[0]))
            return list(X.index)

        positive = remove_outliers(positive)
        o = self._model.wv.most_similar(positive=positive, topn=1000)
        o = self._format_most_similar(o)

        if True:
            categories = ["O", "CP"]
            ok = set()
            for c in categories:
                ok |= set(self._db.category[c])
            ok &= set(o.index)
            o = o.loc[list(ok),:].sort_values("Distance")
        return o

    def category_distance(self, key):
        Xg = self.category("Gh")
        Xc = self.category(key)
        import wrenlab.ncbi.gene

        dx = self.distance(key)
        M = self._db.term_gene()
        ix = list(set(dx.index) & set(M))
        o = dx.loc[ix]
        o.index = [M[k] for k in o.index]
        o.name = "Distance"
        return wrenlab.ncbi.gene.info(9606)\
                .join(o.to_frame(), how="inner")\
                .sort_values("Distance")


class Database(HasConnection):
    def __init__(self, path="~/.cache/iridescent", read_only=True, initialize=True):
        self._directory = os.path.abspath(os.path.expanduser(path))
        os.makedirs(self._directory, exist_ok=True)
        path = os.path.join(self._directory, "database.sqlite")
        if read_only:
            uri = "file:{}?mode=ro".format(path)
            self._cx = sqlite3.connect(uri, uri=True)
        else:
            self._cx = sqlite3.connect(path)

        self._postings = None
        self._term_gene = {}

        if initialize:
            self._initialize()

    @property
    def classic(self):
        return ClassicAlgorithms(self)

    @property
    def postings(self):
        def fn():
            with self.cursor() as c:
                c.execute("""
                    SELECT document_id, term_id FROM posting
                    ORDER BY document_id;""")
                terms = set()
                prev = None
                for document_id, term_id in c:
                    if (prev is not None) and (document_id != prev):
                        if len(terms) >= 2:
                            yield list(terms)
                        terms = set()
                    prev = document_id
                    terms.add(term_id)

        if self._postings is None:
            with self.cursor() as c:
                self._postings = Postings(fn())
        return self._postings

    def load_postings(self, path, min_count_per_document=2):
        def fn():
            prev = None
            terms = set()

            with gzip.open(path, mode="rt") as h:
                for line in h:
                    document_id, term_id = line.strip().split("\t")
                    document_id = int(document_id)
                    terms.add(int(term_id))
                    if (prev is not None) and (document_id != prev):
                        if len(terms) >= min_count_per_document:
                            for term_id in terms:
                                yield document_id, term_id
                        terms = set()
                    prev = document_id

        with self.cursor() as c:
            c.executemany("INSERT INTO posting (document_id, term_id) VALUES (?,?)",
                    fn())
            self._cx.commit()

    @property
    def w2v(self):
        if not hasattr(self, "_w2v"):
            path = os.path.join(self._directory, "word2vec.model")
            if not os.path.exists(path):
                LOG.info("Reading postings ...")
                corpus = self.postings
                LOG.info("Initializing word2vec model ...")
                model = gensim.models.Word2Vec(size=256, window=5, 
                        workers=8, sorted_vocab=True)
                LOG.info("Building word2vec model vocabulary ...")
                model.build_vocab_from_freq(corpus.frequencies())
                LOG.info("Training word2vec model ...")
                model.train(corpus, 
                        total_examples=len(corpus), 
                        total_words=int(corpus._data.shape[0]),
                        report_delay=10.0,
                        epochs=model.iter)
                model.save(path)
            self._w2v = gensim.models.word2vec.Word2Vec.load(path)
        return Word2Vec(self, self._w2v)

    def _initialize(self):
        with self.cursor() as c:
            c.execute("""
                SELECT text, term_id
                FROM 'synonym'
                """)
            self.synonym_id = dict(c)

        with self.cursor() as c:
            c.execute("""
                SELECT term_id, category
                FROM 'synonym';
                """)
            o = collections.defaultdict(set)
            for term_id, category in c:
                o[category].add(term_id)

            self.category = {k:np.array(list(ix)) for k,ix in o.items() if len(ix) > 100}

        self.terms = []
        with self.cursor() as c:
            c.execute("""
                SELECT id, name, frequency
                FROM term;""")
            self.terms = pd.DataFrame.from_records(c, 
                    columns=["TermID", "Name", "NodeFrequency"])\
                            .set_index(["TermID"])

    def xref(self, term_id):
        with self.cursor() as c:
            c.execute("""
                SELECT source
                    FROM synonym
                    WHERE term_id=?
                """, (term_id,))
            return [kv for kv in c if ":" in kv]

    def by_xref(self, term_id):
        with self.cursor() as c:
            c.execute("""
                SELECT term_id
                    FROM synonym
                    WHERE source=?
                    LIMIT 1
            """, (term_id,))
            try:
                return next(c)[0]
            except:
                return
                

    def term_gene(self, taxon_id=9606):
        assert taxon_id == 9606
        if not taxon_id in self._term_gene:
            o = {}
            with self.cursor() as c:
                c.execute("""SELECT term_id, source 
                    FROM synonym
                    WHERE category='Gh';""")
                for term_id, source_id in c:
                    if not source_id.startswith("LL:"):
                        continue
                    gene_id = int(source_id[3:])
                    o[term_id] = gene_id
            self._term_gene[taxon_id] = o
        return self._term_gene[taxon_id]

    @property
    def synonyms(self):
        if not hasattr(self, "_synonyms"):
            with self.cursor() as c:
                c.execute("""
                    SELECT id, term_id, category, text, case_sensitive
                    FROM synonym;
                """)
                o = pd.DataFrame.from_records(c,
                        columns=["SynonymID", "TermID", "type", "text", "case_sensitive"])\
                                .set_index(["SynonymID"])
                o.case_sensitive = o.case_sensitive.astype(bool)
                self._synonyms = o
        return self._synonyms

    def matrix(self):
        return self._matrix

    def cursor(self):
        return contextlib.closing(self._cx.cursor())

    def trie(self):
        import wrenlab.text.ahocorasick

        trie = wrenlab.text.ahocorasick.MixedCaseSensitivityTrie(
            allow_overlaps=False,
            boundary_characters=" ."
        )
        for id, term_id, category, text, case_sensitive in self.synonyms.to_records():
            trie.add(text, key=term_id, case_sensitive=bool(case_sensitive))
        trie.build()
        return trie

    def _execute_schema(self):
        with self.cursor() as c:
            path = os.path.join(os.path.dirname(__file__), "schema.sql")
            with open(path) as h:
                c.executescript(h.read())

    def _insert_terms(self, df):
        with self.cursor() as c:
            LOG.info("Inserting rows: `term` ...")
            c.executemany("""
                INSERT INTO 
                    term (id, name, birth_date, frequency)
                VALUES
                    (?,?,0,?)""",
                    df.groupby("RecordID").first().loc[:,["Objectname","Net_Freq"]].to_records())

            LOG.info("Inserting rows: `synonym` ...")
            df.CAPS_flag = df.CAPS_flag.astype(int)
            c.executemany("""
                INSERT INTO 
                    'synonym' (term_id, text, source, birth_date, category, frequency, case_sensitive) 
                VALUES (?,?,?,?,?,?,?)""",
                    df.loc[:,["RecordID", "Objectsynonym", "SourceID", 
                                "Birth_Date", "ObjectType", "Occurances", "CAPS_flag"]]\
                            .to_records(index=False))

            LOG.info("Updating term birth dates ...")
            c.executemany("UPDATE term SET birth_date=? WHERE id=?",
                    ((int(birth_date), int(id)) for id, birth_date in
                        df.query("Birth_Date > 0").groupby("RecordID")\
                                .Birth_Date.min().to_frame().to_records()))
            self._cx.commit()

    def _insert_metadata(self, df):
        LOG.info("Inserting rows: `metadata` ...")
        with self.cursor() as c:
            row = df.iloc[0,:]
            x = row["Abstracts_processed"]
            c.execute("INSERT INTO metadata (key, value) VALUES (?,?)",
                ("N_ABSTRACTS", str(row["Abstracts_processed"])))
            self._cx.commit()
        
    def _insert_edges(self, rows):
        LOG.info("Inserting rows: `edge` ...")
        N_ABSTRACTS = self.metadata("N_ABSTRACTS")
        with self.cursor() as c:
            c.execute("SELECT id, frequency, birth_date FROM term;")
            F = {}
            BD = {}
            p = {}
            for id, frequency, birth_date in c:
                F[id] = frequency
                BD[id] = birth_date
                if birth_date < N_ABSTRACTS:
                    p[id] = frequency / (N_ABSTRACTS - birth_date)
                
        def fn():
            for _, e1, e2, s_count, a_count, birth_date in rows:
                if birth_date >= N_ABSTRACTS:
                    continue
                pA = p.get(e1)
                pB = p.get(e2)
                if not (pA and pB):
                    continue
                possible_BD = max(BD[e1], BD[e2])
                if possible_BD >= N_ABSTRACTS:
                    continue
                pAB = (s_count + a_count) / possible_BD
                MI = math.log2(pAB / (pA * pB))
                yield e1, e2, s_count, a_count, birth_date, MI
                yield e2, e1, s_count, a_count, birth_date, MI

        with self.cursor() as c:
            c.executemany("""
                INSERT INTO 
                    edge (e1,e2,s_count,a_count,birth_date,MI)
                VALUES (?,?,?,?,?,?)""", fn())
            self._cx.commit()

    def _build_indices(self):
        LOG.info("Building indices ...")
        with self.cursor() as c:
            path = os.path.join(os.path.dirname(__file__), "index.sql")
            with open(path) as h:
                c.executescript(h.read())
        self._cx.commit()

    @staticmethod
    def new(SORD_path):
        db = Database(initialize=False, read_only=False)
        db._execute_schema()

        mdb = mdbread.MDB(SORD_path)
        terms = mdb["tblObjectSynonyms"].to_data_frame()
        db._insert_terms(terms)

        metadata = mdb["Database_Build_Information"]
        db._insert_metadata(metadata.to_data_frame())

        edges = mdb["tblCo_mention"]
        db._insert_edges(edges.records())

        db._build_indices()

        db._initialize()

        return db

    def metadata(self, key):
        with self.cursor() as c:
            c.execute("SELECT value FROM metadata WHERE key='N_ABSTRACTS'")
            o = next(c)[0]
            try:
                return int(o)
            except:
                return o

if __name__ == "__main__":
    Database.new("/home/gilesc/collab/perz/mnemonic/data/SORD_Master_v45.mdb")
    pass

#include <armadillo>

#include <wrenlab/matrix/io.hpp>
#include <wrenlab/util.hpp>

#include <iostream>

namespace wrenlab {

SeriesReader::SeriesReader(std::string path) {
    input.open(path.c_str());
    std::string header;
    getline(input, header);
    std::vector<std::string> labels = wrenlab::split(header);
    labels.erase(labels.begin());

    p_index = std::make_shared<SeriesIndex>();
    p_index->initialize(labels);

    read_next();
}

void
SeriesReader::read_next() {
    std::string line;
    if (!getline(input, line)) {
        _eof = true;
        return;
    }
    std::vector<std::string> fields = wrenlab::split(line);

    std::vector<double> elements;
    for (int i=1; i<fields.size(); i++) {
        elements.push_back(atof(fields[i].c_str()));
    }
    wrenlab::Series next(fields[0], p_index, arma::vec(elements));
    current = next;
}

Series
SeriesReader::next() {
    assert(!eof());
    Series o = current;
    read_next();
    return o;
}

bool
SeriesReader::eof() {
    return _eof;
}

SeriesReader::~SeriesReader() {}

};

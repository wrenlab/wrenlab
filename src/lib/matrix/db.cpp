#include <wrenlab/matrix/db.hpp>

namespace wrenlab {

std::string
MatrixDB::get_schema() {
    return std::string(SCHEMA);
}

void
MatrixDB::insert_rows(std::vector<Series>& rows) {
    execute("BEGIN TRANSACTION;");
    PreparedStatement psr = 
        prepare_statement("INSERT INTO data VALUES (?,?);");
    for (Series& row : rows) {
        assert(row.data.size() == _columns.size());

        psr.bind_text(0, row.key);
        std::string data_string((const char*) row.data.memptr(), 
                row.data.size() * sizeof(double));
        std::string data_compressed = compress_string(data_string);
        //std::cerr << data_string.size() << '\t' << data_compressed.size() << std::endl;
        psr.bind_blob(1, (const void*) &data_compressed[0], data_compressed.size());
        psr.step(true);
    }
    psr.commit();
    execute("END TRANSACTION;");
}

void 
MatrixDB::load_indices() {
    PreparedStatement pc = prepare_statement("SELECT key FROM columns;");
    pc.step();
    while (pc.has_next()) {
        _columns.push_back(pc.get_text(0));
        pc.step();
    }
    pc.close();

    PreparedStatement pr = prepare_statement("SELECT key FROM data;");
    pr.step();
    while (pr.has_next()) {
        _index.push_back(pr.get_text(0));
        pr.step();
    }
    pr.close();

    c_ix = std::make_shared<SeriesIndex>();
    c_ix->initialize(_columns);

    std::cerr 
        << "Indexes loaded.\n* Rows:" 
        << _index.size() 
        << "\n* Columns: "
        << _columns.size()
        << std::endl;
}

void
MatrixDB::initialize(SeriesReader& reader) {
    /*
     * Import the data from a MatrixReader object into this database.
     */
    assert(!read_only);

    std::string schema = get_schema();
    execute(schema);
    //execute("PRAGMA cache_size = 10000;");

    //std::vector<std::string> columns = reader.columns();
    std::vector<std::string> columns = reader.p_index->labels;
    _columns = columns;
    
    std::cerr << "Inserting " << columns.size() << " column labels...\n";
    execute("BEGIN TRANSACTION;");
    PreparedStatement ps = 
        prepare_statement("INSERT INTO columns VALUES (?,?);");
    for (size_t i=0; i<columns.size(); i++) {
        std::string c = columns[i];
        //std::cerr << c << std::endl;
        ps.bind_integer(0, i);
        ps.bind_text(1, c);
        ps.step(true);
    }
    execute("END TRANSACTION;");
    ps.commit();

    std::cerr << "Inserting row data...\n";
    std::vector<Series> rows;
    Series* row;

    size_t i = 0;
    size_t BLOCK_SIZE = 100;
    while (!reader.eof()) {
        i += 1;
        rows.push_back(reader.next());
        if (rows.size() == 100) {
            insert_rows(rows);
            rows.clear();
        }
        if ((i > 0) & (i % BLOCK_SIZE == 0)) {
            std::cerr << "* Rows " << i - BLOCK_SIZE << "-" << i << " inserted.\n";
        }
    }
    if (rows.size() > 0) {
        insert_rows(rows);
    }

    std::cerr << "Indexing row keys ..." << std::endl;
    execute("CREATE INDEX ix_data_key ON data (key);");
}

const std::vector<std::string>&
MatrixDB::columns() {
    return _columns;
}

const std::vector<std::string>&
MatrixDB::index() {
    return _index;
}

};

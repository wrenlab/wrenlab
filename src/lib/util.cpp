#include <algorithm>
#include <fstream>
#include <streambuf>
#include <sys/stat.h>
#include <cassert>
#include <cmath>

#include <iostream>

#include <zlib.h>

#include <wrenlab/util.hpp>

using namespace std;

namespace wrenlab {

std::vector<std::string> 
split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    int start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

std::string 
lowercase(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    return s;
}

std::string 
uppercase(string s) {
    std::transform(s.begin(), s.end(), s.begin(), ::toupper);
    return s;
}

string 
slurp (const string path) {
    /*
     * Read an entire file into a string.
     */
    std::ifstream h(path);
    std::string o(
            (std::istreambuf_iterator<char>(h)),
             std::istreambuf_iterator<char>());
    return o;
}

string
compress_string(const string& s) {
    string o;
    size_t ratio = 2;

    z_stream z;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * 2;
        o.resize(size);

        z.avail_in = s.length();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = size;
        z.next_out = (unsigned char*) &o[0]; 

        assert (deflateInit(&z, Z_BEST_SPEED) == Z_OK);
        deflate(&z, Z_FINISH);
        deflateEnd(&z);

        //ratio *= round(s.length() / z.avail_in) + 1;
        ratio *= 2;
        o.resize(size - z.avail_out);
    } while (z.avail_in != 0);

    return o;
}

string
decompress_string(const string& s) {
    size_t ratio = 1;

    string o;
    size_t avail_in;

    /* FIXME: should use a buffer instead of this inefficient method... */
    do {
        z_stream z;
        z.zalloc = Z_NULL;
        z.zfree = Z_NULL;
        z.opaque = Z_NULL;

        size_t size = s.length() * ratio;
        o.resize(size);

        z.avail_in = s.size();
        z.next_in = (unsigned char*) &s[0];
        z.avail_out = o.size();
        z.next_out = (unsigned char*) &o[0];
         
        assert(inflateInit(&z) == Z_OK);
        inflate(&z, Z_NO_FLUSH);
        inflateEnd(&z);

        //std::cerr << s.length() << "\t" << z.avail_in << std::endl;
        //ratio *= round(s.length() / z.avail_in) + 1;
        //std::cerr << "factor: " << round(s.length() / z.avail_in) + 1 << std::endl;
        ratio *= 2;
        o.resize(size - z.avail_out);	
        avail_in = z.avail_in;
    } while (avail_in != 0);

    return o;
};

bool 
file_exists(const string& path) {
    struct stat buffer;
    return (stat (path.c_str(), &buffer) == 0); 
}

};

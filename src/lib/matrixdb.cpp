#include <string>
#include <iterator>
#include <cstring>

#include <wrenlab/matrix/db.hpp>
#include <matrixdb.h>

using namespace wrenlab;

/* C API */

typedef const std::vector<std::string> Index;

extern "C" {

matrixdb* 
matrixdb_open(const char* _path, bool read_only) {
    std::string path(_path);
    MatrixDB* db = new MatrixDB(path, read_only);
    return reinterpret_cast<matrixdb*>(db);
}

void
matrixdb_close(matrixdb* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    _db->close();
}

/* Row functions */

matrixdb_row*
matrixdb_get_row(matrixdb* db, const char* _key) {
    std::string key(_key);
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    Row* row = _db->row(key);
    return reinterpret_cast<matrixdb_row*>(row);
}

const char*
matrixdb_row_key(matrixdb_row* row) {
    Row* r = reinterpret_cast<Row*>(row);
    return &r->key[0];
}

size_t
matrixdb_row_size(matrixdb_row* row) {
    Row* r = reinterpret_cast<Row*>(row);
    return r->data.size();
}

double*
matrixdb_row_data(matrixdb_row* row) {
    Row* r = reinterpret_cast<Row*>(row);
    return &r->data[0];
}

void
matrixdb_row_delete(matrixdb_row* row) {
    Row* r = reinterpret_cast<Row*>(row);
    delete r;
}

/* Index functions */

matrixdb_index*
matrixdb_get_row_index(matrixdb* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    return reinterpret_cast<matrixdb_index*>(&_db->index());
}

matrixdb_index*
matrixdb_get_column_index(matrixdb* db) {
    MatrixDB* _db = reinterpret_cast<MatrixDB*>(db);
    return reinterpret_cast<matrixdb_index*>(&_db->columns());
}

size_t
matrixdb_index_size(matrixdb_index* ix) {
    Index* _ix = reinterpret_cast<Index*>(ix);
    return _ix->size();
}

void
matrixdb_index_element(matrixdb_index* ix, size_t i, char* result) {
    Index* _ix = reinterpret_cast<Index*>(ix);
    const std::string& s = (*_ix)[i];
    strcpy(result, s.c_str());
}

}

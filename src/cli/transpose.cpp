#include <stdio.h>
#include <unistd.h>
#include <cassert>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <sys/resource.h>
#include <thread>
#include <omp.h>
#include <cmath>

#include <wrenlab.hpp>

/*
 * TODO: 
 * - Use TMPDIR (or TEMPDIR?) environment variable before /tmp if NOS 
 * - It is not actually necessary to transpose the data ever, just
 *      write it out looping j over i
 * - Leaking memory? (Seems to occur if: lots of processors running with big
 *   chunk size and they get I/O blocked. Hopefully addition of compression 
 *   helped this).
 */

using namespace std;

void print_usage() {
    cerr 
        << "Transpose a delimited file on STDIN, and output the results to STDOUT.\n"
        << "Can be used for very large inputs.\n\n"
        << "USAGE: transpose [options] < input.matrix > transposed.matrix\n\n"
        << "Options:\n"
        << " -t <tmpdir>    : Specify an alternate temporary directory\n"
        << " -n <chunksize> : Specify the number of lines to be read at once\n"
        << "                  (affects memory usage)\n"
        << " -p <n>         : Specify the number of threads\n"
        << " -c <program>   : Specify the external compression program to use.\n"
        << "                  By default, this is lz4, if the system has it,\n"
        << "                  otherwise gzip. (The binary must be on your PATH).\n"
        << " -d <delimiter> : Specify the matrix delimiter (default: tab).\n"
        << "                  Must be a single character.\n"
        << " -h             : Display this help message.\n\n"
        << "NOTES:\n"
        << "* The input matrix is assumed to be square (same number of columns per row)\n"
        << "* Memory usage scales with the parameters -n and -p. Should not be a\n"
        << "    problem except for very slow I/O situations.\n";

}

template <typename T>
vector<vector<T> >
transpose(vector<vector<T> >& X) {
    size_t nc = -1;
    for (auto x : X) {
        if (nc != -1) {
            assert(x.size() == nc);
        }
        nc = x.size();
    }
    size_t nr = X.size();

    vector<vector<T> > Xt;
    for (int j=0; j<nc; j++) {
        Xt.push_back(vector<T>(nr));
        vector<T>& v = Xt[j];
        for (int i=0; i<nr; i++) {
            v[i] = X[i][j];
        }
    }
    return Xt;
};

string file_path(int fd) {
    char target[10000];
    sprintf(target, "/proc/self/fd/%d", fd);
    char fname[10000];
    readlink(target, fname, 10000);
    return string(fname);
}

string file_path(FILE* file) {
    return file_path(fileno(file));
}

string
make_temporary_file(const string& prefix) {
    /* 
     * Create a temporary file, returning the path.
     * This file will need to be independently deleted.
     */
    string p = prefix + "/wrenlab.transpose.XXXXXX";
    vector<char> pattern (p.begin(), p.end());
    pattern.push_back('\0');
    int tmp = mkstemp(&pattern[0]);
    return file_path(tmp);
}

string 
handle(
        vector<string>& chunk, 
        const string& tmpdir, 
        const string& compression_program,
        char delimiter) {
    /* 
     * Write chunk to a compressed temporary file,
     * returning the temporary file's path.
     */
    vector<vector<string> > X;
    for (string line : chunk) {
        X.push_back(wrenlab::split(line, delimiter));
    }

    string path = make_temporary_file(tmpdir);
    string cmd = compression_program + " > " + path;
    FILE* h = popen(cmd.c_str(), "w");
   
    vector<vector<string> > Xt = transpose(X);
    for (vector<string>& v : Xt) {
        fprintf(h, "%s", v[0].c_str());
        for (int i=1; i<v.size(); i++) {
            fputc(delimiter, h);
            fprintf(h, "%s", v[i].c_str());
        }
        fputc('\n', h);
    }
    X.clear();
    pclose(h);
    return path;
}

typedef pair<size_t, vector<string>* >* chunk_t;

chunk_t
get_chunk(size_t chunk_size, size_t& chunk_index) {
    vector<string>* chunk = new vector<string>(chunk_size);
    int i = 0;
    while ((i < chunk_size) && getline(cin, (*chunk)[i])) {
        i++;
    }

    if (i == 0) {
        delete chunk;
        return NULL;
    }
    chunk_index++;
    chunk->resize(i);
    return new pair<size_t, vector<string>* >(chunk_index, chunk);
}

int 
main(int argc, char** argv) {
    rlimit limit;
    getrlimit(RLIMIT_NOFILE, &limit);
    size_t MAX_OPEN_FILES = limit.rlim_max;

    size_t chunk_size = 100;
    size_t chunk_index = -1;
    size_t n_threads = max((size_t) (thread::hardware_concurrency() - 2), (size_t) 1);
    char delimiter = '\t';
    // TODO: check existence
    string compression_program = "lz4";

    vector<string> paths;
    chunk_t p;

    string tmpdir = "/tmp";
    if (getenv("TMPDIR") != NULL) {
        tmpdir = getenv("TMPDIR");
    }

    // Parse CLI
    char c;
    while ((c = getopt(argc, argv, "d:t:c:p:n:h")) != -1) {
        switch (c) {
            case 't':
                tmpdir = optarg;
                break;
            case 'n':
                chunk_size = atoi(optarg);
                break;
            case 'p':
                n_threads = atoi(optarg);
                break;
            case 'h':
                print_usage();
                exit(0);
            case 'c':
                compression_program = optarg;
                break;
            case 'd':
                if (optarg[1] != '\0') {
                    cerr << "ERROR: delimiter (-d) must be a single character.\n";
                    exit(1);
                }
                delimiter = optarg[0];
                break;
            default:
                print_usage();
                exit(1);
        }
    }

    /*
	if (!cin.rdbuf()->in_avail()) {
		print_usage();
		exit(1);
	}
    */

    omp_set_num_threads(n_threads);

    cerr << "Settings:\n";
    cerr << "* Temporary directory: " << tmpdir << endl;
    cerr << "* Chunk size: " << chunk_size << endl;
    cerr << "* Number of threads: " << n_threads << endl;
    cerr << endl;

    #pragma omp parallel shared(paths, chunk_index) private (p)
    {
        #pragma omp master
        while (p = get_chunk(chunk_size, chunk_index))
        #pragma omp task
        {
            size_t ix = p->first;
            vector<string>* chunk = p->second;
            string path = handle(*chunk, tmpdir, compression_program, delimiter);
            #pragma omp critical
            {
                cerr << "Processing lines " 
                    << ix * chunk_size
                    << "-" << (ix+1)*chunk_size << endl;
                if (paths.size() < (ix + 1)) {
                    paths.resize(ix + 1);
                }
                paths[ix] = path;
            }
            delete chunk;
            delete p;
        }
        #pragma omp taskwait
    }

    vector<FILE*> handles;
    for (string& path : paths) {
        string cmd = "lz4 -dc " + path;
        FILE* h = popen(cmd.c_str(), "r");
        handles.push_back(h);
    }

    bool ok = true;
    while (ok) {
        for (int i=0; i<handles.size(); i++) {
            char* line = NULL;
            size_t n = 0;
            FILE* h = handles[i];

            if (getline(&line, &n, h) == -1) {
                ok = false;
                free(line);
                break;
            } else {
                if (i > 0) {
                    cout << delimiter;
                }
                for (int j=n; j--; j>=0) {
                    if (line[j] == '\n') {
                        line[j] = '\0';
                    }
                }
                cout << line;
                free(line);
            }
        }
        if (ok)
            cout << endl;
    }

    for (auto h : handles) {
        pclose(h);
    }

    cerr << "\nRemoving temporary files:\n";
    for (string& path : paths) {
        cerr << "* " << path << endl;
        unlink(path.c_str());
    }
}

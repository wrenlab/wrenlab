#include <cassert>

#include <wrenlab/matrix/io.hpp>

using namespace std;

int main() {
    wrenlab::SeriesReader reader("/dev/stdin");
    size_t n = reader.p_index->labels.size();

    size_t i = 0;
    while (!reader.eof()) {
        wrenlab::Series s = reader.next();
        assert(s.data.size() == n);
        cerr << i << "\t" << s.data.size() << "\t" << n << endl;
        i += 1;
    }
}

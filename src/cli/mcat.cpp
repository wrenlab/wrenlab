#include <iostream>

#include <wrenlab/io.hpp>

using namespace wrenlab;

int main() {
    SeriesReader rdr("/dev/stdin");
    Series* row;

    while ((row = rdr.next()) != nullptr) {
        std::cout << row->key << std::endl;
    }
}

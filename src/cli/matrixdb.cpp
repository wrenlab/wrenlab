#include <string>
#include <iostream>

#include <wrenlab.hpp>

using namespace wrenlab;

void
print_usage() {
    std::cerr 
        << "USAGE: matrixdb <command> [arguments]\n\n"
        << "Manipulate a MatrixDB database.\n";
}

void dump(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    wrenlab::MatrixDB X(path, true);

    // Output columns
    auto columns = X.columns();
    for (std::string c : columns) {
        std::cout << "\t" << c;
    }
    std::cout << std::endl;

    // Output rows
    auto index = X.index();
    for (auto ix : index) {
        Row* row = X.row(ix);
        std::cout << ix;
        for (double x : row->data) {
            std::cout << "\t" << x;
        }
        std::cout << std::endl;
    }
}

void 
load(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    wrenlab::MatrixDB X(path, false);
    wrenlab::SeriesReader reader("/dev/stdin");
    X.initialize(reader);
    X.close();
}

void 
row(int argc, char* argv[]) {
    if (argc < 3) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    std::string key(argv[2]);

    MatrixDB X(path, true);
    Row* row = X.row(key);

    if (row == NULL) {
        std::cerr << "Row not found: '" << key << "'\n";
        exit(1);
    }

    std::vector<std::string> columns = X.columns();
    for (int i=0; i<columns.size(); i++) {
        std::cout << columns[i] << "\t" << row->data[i] << std::endl;
    }
}

void
stats (int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB X(path, true);
}

void
verify(int argc, char* argv[]) {
    if (argc < 2) {
        print_usage();
        exit(1);
    }

    std::string path(argv[1]);
    MatrixDB X(path, true);
    for (const std::string& ix : X.index()) {
        auto row = X.row(ix);
    }
}

#include <matrixdb.h>

int main(int argc, char* argv[]) {
    /*
    matrixdb* db = matrixdb_open("/home/gilesc/methylation/test.db");
    matrixdb_index* ix = matrixdb_get_row_index(db);
    size_t n = matrixdb_index_size(ix);
    std::cerr << n << std::endl;
    //std::string s(matrixdb_index_element(ix, n-1));
    //std::cout << s << std::endl;
    exit(0);
    */

    std::string cmd(argv[1]);

    if (cmd == "row") {
        row(argc - 1, argv + 1);
    } else if (cmd == "dump") {
        dump(argc - 1, argv + 1);
    } else if (cmd == "load") {
        load(argc - 1, argv + 1);
    } else if (cmd == "stats") {
        stats(argc - 1, argv + 1);
    } else if (cmd == "verify") {
        verify(argc - 1, argv + 1);
    } else {
        print_usage();
        exit(1);
    }
}

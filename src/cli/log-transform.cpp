#include <cmath>
#include <omp.h>

#include <wrenlab.hpp>

using namespace std;

void print_usage() {
    cerr 
        << "Log-transform each row of a matrix (potentially conditionally).\n"
        << "USAGE: log-transform [options] < in.tsv > out.tsv\n\n"
        << "Options:\n"
        << " -m <float> : Only log-transform a row if the max is above this value.\n"
        << " -b <float> : The base of the log-transformation (default: e)\n"
        << " -h : Print this help.\n";
}

int main(int argc, char* argv[]) {
    cout.precision(3);

    int c;
    size_t ncpu = 1;
    bool conditional = false;
    double threshold;
    double multiplier = 1;
    while ((c = getopt(argc, argv, "m:b:h")) != -1) {
        switch (c) {
            case 'm':
                conditional = true;
                threshold = atof(optarg);
                break;
            case 'p':
                ncpu = atoi(optarg);
                break;
            case 'b':
                multiplier = 1 / log(atof(optarg));
                break;
            case 'h':
                print_usage();
                exit(0);
        }
    }

    wrenlab::SeriesReader reader;
    for (string c : reader.p_index->labels) {
        cout << "\t" << c;
    }
    cout << endl;

    while (!reader.eof()) {
        wrenlab::Series s = reader.next();
        bool apply = (!(conditional && s.max() < threshold));
        cout << s.key;
        for (double v : s.data) {
            if (apply) {
                v = log(v) * multiplier;
            }
            cout << "\t" << v;
        }
        cout << endl;
    }

    /*
    omp_set_num_threads(ncpu);

    #pragma omp parallel private (x)
    {
        #pragma omp master
        while (!reader.eof() && ({x = reader.next(); true;})) 
        #pragma omp task
        {
            x = x.pplog(threshold);
            #pragma omp critical
            {
                cout << x.key;
                for (int i=0; i<x.size(); i++) {
                    cout << "\t" << x[i];
                }
                cout << endl;
            }
        }
    }
    */
}

import collections
import multiprocessing as mp

import patsy
import numpy as np
import pandas as pd
import scipy.stats
import sklearn.metrics
from joblib import Parallel, delayed

import wrenlab.matrix.mmat as mmat
import wrenlab.impute
import wrenlab.lm
import wrenlab.ml
import wrenlab.statistics
from wrenlab.statistics import standardize
from wrenlab.util import align_series, LOG

import age_atlas.label
from age_atlas.util import memoize
from age_atlas.control import controls

def realign(A,X):
    while X.isnull().all(axis=0).any() or X.isnull().all(axis=1).any():
        X = X.dropna(how="all", axis=0).dropna(how="all", axis=1)
    A, X = A.align(X, join="inner", axis=0)
    return A,X

def limit_platform(A,X,n=100):
    p_count = A.PlatformID.copy().value_counts()
    ix = p_count.index[p_count >= n]
    A = A.ix[A.PlatformID.isin(ix),:]
    return realign(A,X)

def limit_age(A, X, min_age=25, max_age=100):
    assert min_age <= max_age
    assert max_age > 0
    A = A.ix[(A.Age >= (min_age * 12)) & (A.Age <= (max_age * 12)),:]
    return realign(A,X)

def standardize_by(A,X,by):
    # FIXME: does this standardize by gene or by sample
    return A,X.groupby(A[by]).apply(standardize)

@memoize
def _data():
    A = age_atlas.label.get_labels().copy()
    XD = mmat.MMAT("/data/ncbi/geo2/matrix/taxon/9606.mmat")
    ix = list(sorted(set(["GSM{}".format(ix) for ix in A.index]) & set(XD.index)))
    X = XD.loc[ix,:].to_frame()
    X.index = [int(x[3:]) for x in X.index]
    X = X.T.dropna(thresh=int(X.shape[0] * 0.5)).T
    return realign(A,X)

@memoize
def data(require_age=True, preprocess=True, boxcox=True, impute=True):
    """
    """
    if impute is True:
        assert preprocess is True
        A,X = data(preprocess=True, boxcox=boxcox, impute=False, require_age=require_age)
        X = X.apply(standardize, axis=0)
        X = wrenlab.impute.SVD(X)
    elif preprocess is True:
        A,X = data(preprocess=False, boxcox=False, impute=False, require_age=require_age)
        A,X = limit_platform(A,X)
        A,X = limit_age(A,X)

        # log transform each sample 
        LOG.info("Log transform")
        X = wrenlab.statistics.log_transform(X, axis=1)

        if boxcox is True:
            LOG.info("Box-Cox")
            X = wrenlab.statistics.boxcox(X, axis=1)

        LOG.info("Standardize")
        X = X.apply(standardize, axis=1)
    else:
        A,X = _data()
        if require_age is True:
            A = A.dropna(subset=["Age"])
    return realign(A,X)

def filter_xy(X):
    genes = wrenlab.ncbi.gene.info(9606)
    ix = genes.index[genes.Chromosome.isin(["X","Y"])]
    ix = list(set(X.columns) & set(ix))
    return X.loc[:,ix]

@memoize
def pca(k=100, transpose=False, xy=False, ranks=False):
    A,X = data(preprocess=True, impute=True, boxcox=False)
    if xy:
        X = filter_xy(X)
    if ranks:
        X = X.apply(np.argsort, axis=1)
    if transpose:
        X = X.T
    return wrenlab.statistics.pca(X, k=k, multiply_evar=True)

@memoize
def residuals():
    """
    Expression residuals after controlling for everything except age.
    """
    A,_ = data(preprocess=False, impute=False, boxcox=False)
    obj = fit("C(TissueID) + C(ExperimentID) + C(PlatformID)", "ols")
    return realign(A,obj._residual)

#@memoize
#def covariance(standardize=True):
#    _,X = data(standardize=standardize, impute=True)
#    return pd.DataFrame(X.cov(), index=X.columns, columns=X.columns)

#@memoize
#def inverse_covariance(standardize=True):
#    V = covariance(standardize=standardize)
#    return pd.DataFrame(np.linalg.inv(V), index=V.index, columns=V.columns)

def rank_to_normal(ranks):
    pctile = (ranks - ranks.min() + 1) / (ranks.max() - ranks.min() + 2)
    return pd.Series(scipy.stats.norm.ppf(pctile), index=pctile.index)

#FIXME standardize
"""
def drug_similarity():
    formula = "I(Age / 12) + C(TissueID) + I(Age / 12):C(TissueID) + C(ExperimentID) + C(PlatformID)"
    A,Xis = data(standardize=True, impute=True)
    As = A.query("Age > 25 * 12 & Age < 100 * 12")
    beta = lm(Xis,As,formula)
    B = beta["I(Age / 12)"]

    cmap = wrenlab.data.connectivity_map(standardize=True)

    #ix = list(set(VI.index) & set(cmap.index))
    #VI = VI.loc[ix,:].loc[:,ix]
    cmap, B = cmap.loc[ix,:].align(B, join="inner", axis=0)
    VI = np.linalg.inv(cmap.T.cov())
    return cmap.apply(lambda u: scipy.spatial.distance.mahalanobis(u,B,VI))
"""

##########
# Modeling
##########

class ExpressionMFit(wrenlab.lm.MFit):
    def __init__(self, design, coef, **kwargs):
        super(ExpressionMFit, self).__init__(coef, design=design, **kwargs)

    def term_count(self, term):
        return self._design[term].value_counts()

    def _filter_tissue(self, coef, min_count=100):
        if min_count is not None:
            N = self.N("TissueID")
            coef = coef.loc[:,N.index[N >= min_count]]

        o = wrenlab.ontology.fetch("BTO")
        term_name = {int(ix[4:]):name for ix,name in zip(o.terms.index, o.terms.Name)}
        coef.columns = [term_name[c] for c in coef.columns]
        return coef

    def evaluate(self, limit=None):
        return evaluate(self.t("Age"), limit=limit)

    @property
    def tissue(self, min_count=100):
        coef = self.coef("TissueID")
        return self._filter_tissue(coef, min_count=min_count)

    @property
    def age_tissue_interaction(self, min_count=100):
        coef = self.interaction("Age:TissueID")
        return self._filter_tissue(coef, min_count=min_count)

    def enrichment(self, reverse=False):
        coef = self.t("Age")
        if reverse:
            coef *= -1
        A = wrenlab.enrichment.annotation("GO", 9606)
        return A.enrichment(coef)

def _fit(X, A, formula, method, **kwargs):
    A,X = realign(A,X)
    fn = getattr(wrenlab.lm, method)
    obj = fn(X, A, formula=formula, **kwargs)
    return ExpressionMFit(obj._design, obj._coef, 
            formula=obj._formula, t=obj._t, p=obj._p, residual=obj._residual)

@memoize
def fit(formula, method, boxcox=False, test=False, **kwargs):
    A,X = data(preprocess=True, impute=True, boxcox=boxcox)
    if test is not False:
        if isinstance(test, int):
            n = test
        else:
            n = 500
        X = X.iloc[:,:n]
    return _fit(X,A,formula,method,**kwargs)

def lmer(X, A, formula):
    A,X = realign(A,X)
    D = patsy.dmatrix(formula, data=A, return_type="dataframe")

def get_problem(y_coef, limit=None):
    assert y_coef in ("Gender", "Age")
    P_TYPE = {
        "Gender": "binary",
        "Age": "continuous"
    }
    pc = pca(True)
    A,X = data(preprocess=True)
    A = A.query("Age >= (12 * 40) & Age <= (12 * 70)")
    A, pc = realign(A,pc)
    formula = "C(TissueID) + C(ExperimentID)"
    D = patsy.dmatrix(formula, data=A, return_type="dataframe")
    D = pd.concat([D,pc], axis=1)
    y = A[y_coef]
    if y_coef == "Gender":
        y = (y == "M")
    if limit is not None:
        n = y.shape[0]
        ix = np.arange(n)
        np.random.shuffle(ix)
        ix = ix[:limit]
        y = y.iloc[ix]
        D = D.iloc[ix,:]
    return wrenlab.ml.Problem(y, D, type=P_TYPE[y_coef])
 

############
# Evaluation
############

def evaluate(coef, limit=1000):
    coef = coef.dropna()

    METRICS = collections.OrderedDict([
        ("N", lambda y, y_hat: y.shape[0]),
        ("rho", lambda y, y_hat: scipy.stats.spearmanr(y, y_hat)[0]),
        ("AUC", lambda y, y_hat: sklearn.metrics.roc_auc_score(y.apply(np.sign), y_hat)),
        ("SignAccuracy", lambda y, y_hat: (y.apply(np.sign) == y_hat.apply(np.sign)).mean())
    ])
    
    #coef = fit.coef("Age").dropna()
    def do_limit(y, y_hat):
        y, y_hat = y.copy(), y_hat.copy()
        if (limit is not None) and (y.shape[0] > limit):
            y.sort_values(inplace=True)
            y = pd.concat([y.iloc[:int(limit / 2)], y.iloc[-int(limit / 2):]])
            y, y_hat = align_series(y, y_hat)
        return y, y_hat

    C = controls()
    o = []
    for c in C.columns:
        y = C[c].ix[C[c].apply(np.sign) != 0]
        y, y_hat = do_limit(*align_series(y, coef))
        o.append(tuple(fn(y, y_hat) for fn in METRICS.values()))
    return pd.DataFrame(o, index=C.columns, columns=list(METRICS.keys()))\
            .round(2)\
            .sort_values("rho", ascending=False)

#############
# Methylation
#############

def methylation():
    X = mmat.MMAT("/data/ncbi/geo/GPL13534.mmat")
    print(X.index)

#########
# Display
#########

def tg(df):
    GENES = ["LDHA", "NONO", "PGK1", "PPIH", "IL6", "MTOR", "TP53", "IGF1R"]
    genes = wrenlab.ncbi.gene.info(9606)
    genes = genes.ix[genes.Symbol.isin(GENES),"Symbol"]
    df = df.loc[genes.index,:]
    df.index = genes
    return df

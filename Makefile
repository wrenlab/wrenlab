$(shell mkdir -p bin/ lib/)

CXX=g++
CFLAGS=-fPIC -Iinclude -O3 -std=c++11 -fopenmp

LIBRARIES=-lsqlite3 -lz -lc -larmadillo
SRC=$(wildcard src/lib/*.cpp) $(wildcard src/cli/*.cpp)
OBJ=$(patsubst %.cpp,%.o, $(wildcard src/lib/*.cpp)) \
	$(patsubst %.cpp,%.o, $(wildcard src/lib/matrix/*.cpp))
BIN=$(patsubst %.cpp, %, $(wildcard src/cli/*.cpp))

all: lib/libwrenlab.so \
	lib/libwrenlab.a \
	bin/tmatch \
	bin/matrixdb \
	bin/transpose \
	bin/log-transform \
	bin/validate-matrix

#######################
# (Header) Dependencies
#######################

#depend: .depend

#.depend: $(SRC)
#	rm -f ./.depend
#	$(CXX) $(CFLAGS) -MM $^ > ./.depend

#include .depend

##############
# Main targets
##############

clean:
	rm -f src/cli/*.o src/lib/*.o src/lib/matrix/*.o lib/* bin/* .depend

src/cli/%.o: src/cli/%.cpp
	$(CXX) $(LIBRARIES) $(CFLAGS) -o $@ -c $^

src/lib/%.o: src/lib/%.cpp
	$(CXX) $(LIBRARIES) $(CFLAGS) -o $@ -c $^

lib/libwrenlab.a: $(OBJ)
	ar rcs $@ $^

lib/libwrenlab.so: $(OBJ)
	$(CXX) -shared -Wl,-soname,libwrenlab.so.1 -o $@ $^ $(LIBRARIES)

lib/libwrenlab.so.1: lib/libwrenlab.so
	ln -sr $^ $@

bin/% : src/cli/%.o lib/libwrenlab.a
	$(CXX) $(CFLAGS) $^ -o $@ $(LIBRARIES)

.PHONY: clean

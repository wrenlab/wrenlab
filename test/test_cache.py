import tempfile

from wrenlab.util.cache import LocalLevelDBCache

def test_local_leveldb_cache():
    with tempfile.TemporaryDirectory() as h:
        c = LocalLevelDBCache(h)
        c.put("abc", b"def")
        assert c.get("abc") == b"def"

        assert len(c) == 1

        c.delete("abc")
        assert len(c) == 0

        c.put_object("def", {"a": 7})
        assert c.get_object("def")["a"] == 7

        @c
        def add(x,y):
            return x + y

        assert add(2,3) == 5 
        assert add(2,4) == 6

if __name__ == "__main__":
    test_leveldb_cache()

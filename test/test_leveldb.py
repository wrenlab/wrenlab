import time
import tempfile
import multiprocessing as mp

from wrenlab.ext.leveldb import Server, Client

def test_leveldb_server():
    with tempfile.TemporaryDirectory() as path:
        p = mp.Process(target=Server, args=(path,))
        p.start()
        try:
            time.sleep(1)

            client = Client()
            client.put(b"abc", b"def")
            assert client.get(b"abc") == b"def"

            print(client.keys())
            assert client.keys() == [b"abc"]

            client.delete(b"abc")
            assert client.get(b"abc") is None

        finally:
            p.terminate()


if __name__ == "__main__":
    test_leveldb_server()

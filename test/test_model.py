import pandas as pd
import numpy as np

import autodx.model

def data():
    np.random.seed(1337)
    X = np.random.random((10,5))
    X = pd.DataFrame(X,
            index=["R{}".format(i+1) for i in range(X.shape[0])], 
            columns=["C{}".format(i+1) for i in range(X.shape[1])],)
    D = np.empty((10,2))
    D[:,0] = 1
    D[:5,1] = 1
    D = pd.DataFrame(D, 
            index=["R{}".format(i+1) for i in range(D.shape[0])], 
            columns=["Intercept", "TestCoefficient"])
    return X,D

def test_ols():
    X,D = data()
    fit = autodx.model.MFitOLS.fit(X,D)
    assert len(fit.terms()) == 2

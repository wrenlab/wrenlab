from wrenlab.text.ahocorasick import Trie

# needed tests:
# - start at same point and contains
# - end at same point and contains (like "abcd" and "cd")
# - overlap

def make_trie(xs):
    trie = Trie(case_sensitive=False)
    for text, key in xs.items():
        trie.add(text, key=key)
    trie.build()
    return trie

def test():
    trie = Trie(case_sensitive=False)
    trie.add("he")
    trie.add("she")
    trie.add("hers")
    trie.build()

    text = "he She is hers friend"
    for m in trie.search(text):
        print(m.start, m.end)
        print(text[m.start:m.end])

def test_coterminal_nodes():
    trie = Trie(boundary_characters=" ")
    trie.add("abc def", key=1)
    trie.add("def", key=2)
    trie.add("def ghi", key=3)
    trie.build()
    matches = trie.search("abc def ghi")
    return matches

def test2():
    trie = make_trie({
        "response to caffeine": 1,
        "caffeine": 2
    })
    return trie.search("response to caffeine")

if __name__ == "__main__":
    #print(test_coterminal_nodes())
    print(test2())

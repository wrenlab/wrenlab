#!/usr/bin/env bash

# Requirements:
# - Binaries & libraries as described on http://gitlab.com/wrenlab/libmatrixdb.git
# - Python 3.5+

tmpdir=$(mktemp -d)

# Install libmatrixdb

cd $tmpdir
git clone http://gitlab.com/wrenlab/libmatrixdb.git
cd libmatrixdb
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=~/.local ..
make all install

# Install matrixdb
pip3 install --user git+https://gitlab.com/wrenlab/matrixdb.git

# Install metalearn
pip3 install --user git+https://gitlab.com/wrenlab/metalearn.git

# Install wrenlab
pip3 install --user git+https://gitlab.com/wrenlab/wrenlab.git

rm -rf tmpdir

# When this is finished, you must make sure ~/.local/lib is on your $LD_LIBRARY_PATH. 
# This can be achieved, for example, by adding the following line to your ~/.bashrc or ~/.bash_profile:
# export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HOME/.local/lib"

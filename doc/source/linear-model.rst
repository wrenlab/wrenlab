================
Linear model API
================

There are a variety of APIs available for fitting single linear models to data
in Python, such as ``statsmodels``. Where the ``wrenlab`` APIs differ is that
they are oriented around fitting multiple linear models with the same formula:
for example, fitting the same model using the same design matrix against
multiple genes in an expression matrix.

The relevant functions are in :py:mod:`wrenlab.lm`, and, currently, the major
entry point is :py:func:`wrenlab.lm.ols`.

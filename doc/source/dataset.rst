===============================================================================
Datasets - an API for conducting analysis on high-throughput data with metadata
===============================================================================

Overview
========

Usage
=====

Constructing a :class:`wrenlab.dataset.Dataset`
-----------------------------------------------

.. code-block:: python
    import wrenlab.dataset

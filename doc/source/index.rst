.. Wren Lab documentation master file, created by
   sphinx-quickstart on Thu Feb 11 16:49:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Wren Lab codebase documentation
===============================

Contents:

.. toctree::
    :maxdepth: 2

    dataset
    linear-model

.. automodule:: wrenlab.lm

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

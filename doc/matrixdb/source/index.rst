.. matrixdb documentation master file, created by
   sphinx-quickstart on Wed Nov  1 15:28:54 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
Welcome to matrixdb's documentation!
====================================

`MatrixDB` is a file format with a C and Python API for storing, querying, and
performing computations on large 2D matrices on disk.

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    
    usage
    api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

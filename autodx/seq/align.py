from abc import ABC, abstractmethod 

class Project(object):
    pass

class Sample(ABC):
    pass

class Aligner(ABC):
    def build_index(self):
        pass

    def align(self, sample, output):
        pass

class HISAT2(Aligner):
    pass

class STAR(Aligner):
    pass

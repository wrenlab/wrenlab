"""
TODO
====

Parameterizable decorators so multiple plots etc can be generated per-figure.

Pass-through decorators so they return dataframes (or whatever) when
called normally but generate figures from Make. (e.g., Make.clustermap)

Generate a HTML file to browse stuff in all one file. Or some kind of AIO report (PDF?).
"""

import abc
import os
import collections
import functools
import sys

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

def _fn_full_name(fn):
    prefix = "".join(list(map(lambda x: x[0].upper(), fn.__module__.split(".")[1:])))
    name = "-".join([prefix, fn.__name__]).replace("_", "-")
    while name.startswith("-"):
        name = name[1:]
    return name

###################
# Item & subclasses
###################

class Item(object):
    pass

class Figure(Item):
    pass

class Table(Item):
    pass

#####################
# Report & subclasses
#####################

class Report(object):
    @abc.abstractmethod
    def generate(self):
        pass

class PPTReport(Report):
    pass

class HTMLReport(Report):
    pass

class PDFReport(Report):
    pass

class Make(object):
    DPI = 180
    DEFAULT_EXTENSIONS = {
        "figure": "pdf",
        "table": "tex",
        "spreadsheet": "xlsx"
    }
    PREFIX = {
        "figure": "img",
        "table": "tbl",
        "spreadsheet": "tbl"
    }

    def __init__(self, root, extensions=None):
        self._root = root
        self._definitions = collections.OrderedDict()

        self.functions = collections.defaultdict(dict)
        self.extensions = {k:v for k,v in self.DEFAULT_EXTENSIONS.items()}
        if extensions is not None:
            for k,v in extensions.items():
                self.extensions[k] = v

    def define(self, key, value):
        assert key.isalnum()
        self._definitions[key] = value

    def definition(self, fn):
        # FIXME: make lazy
        #name = _fn_full_name(fn)
        name = fn.__name__
        self._definitions[name] = fn()

    def figure(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(self._root, self.PREFIX["figure"], 
                "{}.{}".format(name, self.extensions["figure"]))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            plt.clf()
            if not os.path.exists(path):
                fn(*args, **kwargs)
                plt.savefig(path, bbox_inches="tight", dpi=self.DPI)
            return path

        self.functions["figure"][name] = wrap
        return wrap

    def clustermap(self, cmap="RdBu_r", **kwargs):
        def wrapper(fn):
            name = _fn_full_name(fn)
            path = os.path.join(self._root, self.PREFIX["figure"], 
                    "{}.{}".format(name, self.extensions["figure"]))

            @functools.wraps(fn)
            def wrap(*w_args, **w_kwargs):
                df = fn(*w_args, **w_kwargs)
                cg = sns.clustermap(df, **kwargs)
                plt.savefig(path, bbox_inches="tight", dpi=self.DPI)
                return cg

            self.functions["figure"][name] = wrap
            return fn
        return wrapper

    def table(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(self._root, self.PREFIX["table"], 
                "{}.{}".format(name, self.extensions["table"]))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            if not os.path.exists(path):
                df = fn(*args, **kwargs)
                if self.extensions["table"] == "tex":
                    with open(path, "w") as h:
                        df.to_latex(buf=h, index=False)
                elif self.extensions["table"] == "tsv":
                    df.to_csv(path, sep="\t")
                elif self.extensions["table"] == "xlsx":
                    df.to_excel(path)
                else:
                    assert False

        self.functions["table"][name] = wrap
        return wrap

    def spreadsheet(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(self._root, self.PREFIX["spreadsheet"], 
                "{}.{}".format(name, self.extensions["spreadsheet"]))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            if not os.path.exists(path):
                M = fn(*args, **kwargs)
                with pd.ExcelWriter(path) as xlw:
                    for k,df in M.items():
                        df.to_excel(xlw, sheet_name=k)

        self.functions["spreadsheet"][name] = wrap
        return wrap

    def generate(self):
        paths = []

        for t in self.PREFIX:
            print("Generating {}s ...".format(t))
            os.makedirs(os.path.join(self._root, self.PREFIX[t]), exist_ok=True)
            for k,fn in sorted(self.functions[t].items()):
                plt.clf()
                print("*", k, file=sys.stderr)
                path = fn()
                if path is not None:
                    paths.append(path)

        if len(self._definitions) > 0:
            print("Generating latex definitions ...")
            defines_path = os.path.join(self._root, "defines.tex")
            with open(defines_path, "w") as h:
                for k,v in self._definitions.items():
                    o = "\\newcommand{\\" + str(k) + r"}{" + str(v) + "}"
                    print(o, file=h)
                    #print("\\newcommand\{\\{}\}\{{}\}".format(k, v), file=h)



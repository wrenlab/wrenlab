import itertools
import collections

import tqdm
import pandas as pd

def parameter_product(**kwargs):
    """
    Each kwarg is a sequence. Returns an iterable of dictionaries 
    over all possible combinations of the kwargs.

    Example
    -------
    >> list(parameter_product(a=[1,2], b=[3,4]))
    [
        {"a": 1, "b": 3},
        {"a": 1, "b": 4},
        {"a": 2, "b": 3},
        {"a": 2, "b": 4}
    ]
    """
    keys = list(kwargs.keys())
    for values in itertools.product(*[kwargs[k] for k in keys]):
        yield dict(zip(keys, values))

def apply_parameters(fn, progress=False, verbose=False, ignore_errors=False, parameters={}):
    """
    Each element of `parameters` is a sequence. Applies the function
    with all possible combinations of the elements in the kwargs sequences, and
    returns a multi-indexed :class:`pandas.DataFrame` as a result.

    It assumes that `fn` returns a dictionary.

    Returns
    -------
    A :class:`pandas.DataFrame` with a multi-index containing each of the
    sequential input parameters. Additionally, it has a `constants`
    attribute that has a dictionary of the nonvarying parameters.
    """
    # FIXME: parallel

    keys = list(parameters.keys())
    sequential_keys = []
    def is_sequential(v):
        # FIXME: exclude np and pd objects
        return isinstance(v, collections.Iterable)\
                and not isinstance(v, str)\
                and not isinstance(v, dict)\
                and not hasattr(v, "shape")

    count = 1
    for k in keys:
        if is_sequential(parameters[k]):
            sequential_keys.append(k)
            count *= len(parameters[k])
        else:
            parameters[k] = [parameters[k]]

    def jobs():
        for params in parameter_product(**parameters):
            msg = "Running function `{}.{}` with parameters: {}"\
                .format(fn.__module__, fn.__name__,
                        {k:v for k,v in params.items()
                            if not isinstance(v, pd.DataFrame)})
            LOG.info(msg)
            if ignore_errors is True:
                try:
                    rs = fn(**params)
                except:
                    rs = None
            else:
                rs = fn(**params)
            yield params, rs

    it = filter(lambda kv: kv[1] is not None, jobs())
    first = next(it)
    it = itertools.chain([first], it)
    ix = []
    columns = first[1].keys()
    o = {c:[] for c in columns}

    if progress is True:
        it = tqdm.tqdm(it, total=count)

    for params, rs in it:
        ix.append(tuple([params[k] for k in sequential_keys]))
        for k,v in rs.items():
            o[k].append(v)

    ix = pd.MultiIndex.from_tuples(ix, names=sequential_keys)
    o = pd.DataFrame(o, index=ix)
    o.constants = {k:parameters[k] for k in keys if not k in sequential_keys}
    return o



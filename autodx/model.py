from abc import ABC, abstractmethod
import collections
import re

import numpy as np
import pandas as pd
import scipy.stats
import statsmodels.stats.multitest
import tqdm
import pylatexenc
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri, r
import rpy2.robjects

from .util import LOG, make_mapping

pandas2ri.activate()

def only_printable(df):
    def fn(index):
        o = []
        for ix in index:
            try:
                ix.encode("ascii")
                o.append(True)
            except:
                o.append(False)
        return o
    rix, cix = fn(df.index), fn(df.columns)
    return df.loc[rix,:].loc[:,cix]

def fix_index(ix):
    # for LaTeX output
    return [pylatexenc.latexencode.utf8tolatex(k) for k in ix]

def replace_index_names(X, names_from, names_to, axis=1):
    assert len(names_from) == len(names_to)
    assert axis in (0,1)
    if axis == 0:
        X = X.T

    columns = list(X.columns)
    for nf,nt in zip(names_from, names_to):
        for i,c in enumerate(columns):
            if c == nf:
                columns[i] = nt
                break

    X.columns = columns
    if axis == 0:
        X = X.T
    return X


P_CATEGORICAL_TERM = re.compile("(C\()?(?P<term>(.+?))\)?\[(T\.)?(?P<category>(.+?))\]")

def parse_term(text):
    term, category = text
    if (term, category) == (None, None):
        return text
    else:
        return term, category

def parse_categorical(text):
    m = P_CATEGORICAL_TERM.match(text)
    if m is None:
        return text, None
    term = m.group("term")
    if len(term.split(",")) > 1:
        term = term.split(",")[0]
    category = m.group("category")
    try:
        category = int(category)
    except ValueError:
        pass
    return term, category

class MFit(ABC):
    def __init__(self, metadata=None):
        self.metadata = metadata or {}

    def terms(self):
        """
        Return a list of unique LM terms (i.e., one per binary/categorical/continuous term).
        """
        def fn():
            seen = set()
            for m in self.spec():
                x = next(iter(m.keys()))
                if x not in seen:
                    yield x
                seen.add(x)
        return list(fn())

    def spec(self):
        # FIXME redo this shit with designinfo (?)
        o = []
        for c in self:
            oo = {}
            for p in c.split(":"):
                tc = parse_categorical(p)
                if tc[0] is None:
                    oo[p] = None
                else:
                    oo[tc[0]] = tc[1]
            o.append(oo)
        return o

class MFitOLS(MFit):
    def __init__(self, fit):
        self.fit = fit

    def __iter__(self):
        return iter(self.fit.keys())

    def __getitem__(self, key):
        return self.fit[key].sort_values("p")

    @staticmethod
    def fit(Y, D):
        if not all([dt == np.int64 for dt in Y.dtypes]):
            assert (~np.isnan(Y)).all().all()

        # FIXME: add some option to warn or something, this is to prevent multicollinearity
        # but it will drop coefficients (presumably ones we aren't usually interested in)

        # Also, this causes a crash with lots of columns (sadly when its most needed)
        D = D.T.drop_duplicates().T

        Y_o = Y
        Y = np.array(Y)
        X = np.array(D)

        LOG.info("Fitting {} OLS models with X shape = {}".format(Y.shape[1], X.shape))

        beta, _, _, _ = np.linalg.lstsq(X, Y, rcond=None)
        N,p = X.shape
        residual = Y - X @ beta
        var_y_given_x = (residual ** 2).sum(axis=0) / (N - p)
        var_beta = np.outer(np.diag(np.linalg.inv(X.T @ X)), var_y_given_x)

        SE = np.sqrt(var_beta)
        t = beta / SE
        p = scipy.stats.t.sf(np.abs(t), df=N-2) * 2

        def mkdf(x):
            return pd.DataFrame(x.T, index=Y_o.columns, columns=D.columns)

        mu = Y_o.mean()
        coef = mkdf(beta)
        SE = mkdf(SE)
        t = mkdf(t)
        p = mkdf(p)

        o = {}
        for j,c in enumerate(D.columns):
            FDR = pd.Series(statsmodels.stats.multitest.multipletests(p.iloc[:,j], method="fdr_bh")[1],
                    index=p.index)
            o[c] = pd.DataFrame(collections.OrderedDict([
                ("N", N),
                ("MeanExpression", mu),
                ("coef", coef.iloc[:,j]),
                ("t", t.iloc[:,j]),
                ("p", p.iloc[:,j]),
                ("FDR", FDR)
            ]))
        return MFitOLS(o)

class MFitR(MFit):
    def __init__(self, fit, maps, **kwargs):
        self._maps = maps
        super().__init__(**kwargs)

    def __iter__(self):
        return iter(self._maps["D_columns"].keys())

    @abstractmethod
    def coefficient(self, key):
        pass

    @staticmethod
    def encode_matrices(X,D):
        assert X.shape[0] == D.shape[0]
        M_index = make_mapping(X.index)
        M_X_columns = make_mapping(X.columns)
        M_D_columns = make_mapping(D.columns)

        D = D.copy()
        X = X.copy()
        X.index = [M_index[ix] for ix in X.index]
        X.columns = [M_X_columns[ix] for ix in X.columns]
        D.index = [M_index[ix] for ix in D.index]
        D.columns = [M_D_columns[ix] for ix in D.columns]

        Xr = pandas2ri.py2rpy(X.T)
        Dr = pandas2ri.py2rpy(D)
        maps = {
            "index": M_index,
            "X_columns": M_X_columns,
            "D_columns": M_D_columns
        }
        return maps, Xr, Dr

    def coefficients(self):
        # FIXME: rename this shit
        o = {}
        for key in self._maps["D_columns"]:
            df = self.coefficient(key)
            try:
                df.index = list(map(int, df.index))
            except:
                pass
            o[key] = df
        return o


class MFitLimma(MFitR):
    def __init__(self, fit, maps):
        self._fit = fit
        self._maps = maps
        self.pkg = importr("limma")
        self._ebayes = self.pkg.eBayes(fit, trend=True)

    def _top_table(self, fit, coef=None):
        if coef is not None:
            coef = self._maps["D_columns"][coef]
        else:
            coef = rpy2.robjects.NULL
        N = len(self._maps["X_columns"])
        table = self.pkg.topTable(fit,
                number=N,
                adjust_method="holm",
                coef=coef)
        #o = pandas2ri.rpy2py(table)
        o = table
        o.index = [self._maps["X_columns"].inv[ix] for ix in index]
        o = replace_index_names(o,
            ["AveExpr", "P.Value", "adj.P.Val"],
            ["logCPM", "p", "FDR"])
        o["SLPV"] = o["p"].apply(np.log10) * np.sign(o["logFC"])
        return o
        
    def coefficient(self, key):
        """
        encoded_key = self._maps["D_columns"][key]
        N = len(self._maps["X_columns"])
        table = self.pkg.topTable(self._ebayes, 
                number=N,
                adjust_method="holm",
                coef=encoded_key)
        o = pandas2ri.ri2py(table)
        o.index = [self._maps["X_columns"].inv[ix] for ix in o.index]
        o = replace_index_names(o,
            ["AveExpr", "P.Value", "adj.P.Val"],
            ["logCPM", "p", "FDR"])
        o["SLPV"] = o["p"].apply(np.log10) * np.sign(o["logFC"])
        return o
        """
        return self._top_table(self._ebayes, coef=key)

    def contrast(self, contrast_matrix):
        ct = np.array(contrast_matrix)
        fit = self.pkg.contrasts_fit(self._fit, ct)
        fit = self.pkg.eBayes(fit)
        return self._top_table(fit)

    @staticmethod
    def fit(X, D):
        maps, Xr, Dr = MFitLimma.encode_matrices(X,D)
        limma = importr("limma")
        edgeR = importr("edgeR")
        dge = edgeR.calcNormFactors(edgeR.DGEList(counts=Xr))
        logCPM = edgeR.cpm(dge, log=True, prior_count=3)
        #fit = pkg.lmFit(Xr, Dr)
        fit = limma.lmFit(logCPM, Dr)
        #fit = limma.eBayes(fit, trend=True)
        return MFitLimma(fit, maps)

class MFitEdgeR(MFitR):
    def __init__(self, Xn, fit, N, maps):
        self.pkg = importr("edgeR")
        self._Xn = Xn
        self.N = N
        self._maps = maps
        self._fit = fit

    def coefficient(self, key):
        #key = self.maps["D_columns"].inv[ix]
        encoded_key = self._maps["D_columns"][key]
        ct = self.pkg.glmQLFTest(self._fit, coef=encoded_key)
        table = r["as.data.frame"](self.pkg.topTags(ct, n=self.N))
        index = r["rownames"](table)
        #o = pandas2ri.rpy2py(table)
        o = table
        o.index = [self._maps["X_columns"].inv[ix] for ix in index]
        o = replace_index_names(o, ["PValue"], ["p"])
        # TODO: add p_adjusted?
        o["SLPV"] = o["p"].apply(np.log10) * np.sign(o["logFC"])
        return o

    """
    def contrasts(self):
        coefs = list(self.fit[self.fit.names.index("coefficients")].colnames)
        #LOG.info("Extracting coefficient parameters...".format(len(coefs)))
        results = {}
        for ix in tqdm.tqdm(coefs):
            key = self.maps["D_columns"].inv[ix]
            encoded_key = self.maps["D_columns"][key]
            ct = self.pkg.glmLRT(self.fit, coef=encoded_key)
            table = r["as.data.frame"](self.pkg.topTags(ct, n=self.N))
            o = pandas2ri.ri2py(table)
            o.index = [self.maps["X_columns"].inv[ix] for ix in o.index]
            o = replace_index_names(o, ["PValue"], ["p"])
            # TODO: add p_adjusted?
            o["SLPV"] = o["p"].apply(np.log10) * np.sign(o["logFC"])
            results[key] = o
        return results
    """

    @property
    def CPM(self):
        return self._Xn

    @staticmethod
    def fit(X, D):
        #LOG.info("Fitting model with shapes: X={} D={}".format(X.shape, D.shape))
        # FIXME: ensure align
        # FIXME: ensure no duplicate indices (fucks up in R and also is indicator of something wrong)
        assert X.shape[0] == D.shape[0]
        nr = X.shape[0]
        nc = X.shape[1]

        # FIXME: clean up into encode/decode indices
        X_original_index = X.index.copy()
        X_original_columns = X.columns.copy()


        M_index = make_mapping(X.index)
        M_X_columns = make_mapping(X.columns)
        M_D_columns = make_mapping(D.columns)

        D = D.copy()
        X = X.copy()
        X.index = [M_index[ix] for ix in X.index]
        X.columns = [M_X_columns[ix] for ix in X.columns]
        D.index = [M_index[ix] for ix in D.index]
        D.columns = [M_D_columns[ix] for ix in D.columns]

        Xr = pandas2ri.py2rpy(X.T)
        Dr = pandas2ri.py2rpy(D)

        pkg = importr("edgeR")
        y = pkg.DGEList(counts=Xr)
        y = pkg.calcNormFactors(y)
        dispersion = pkg.estimateDisp(y, Dr)
        Xn = pd.DataFrame(pkg.cpm(y, log=True), columns=X_original_index, index=X_original_columns).T
        fit = pkg.glmQLFit(dispersion, Dr)
        return MFitEdgeR(Xn, fit, nc, {"X_columns": M_X_columns, "D_columns": M_D_columns})

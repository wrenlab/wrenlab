import pandas as pd
import sklearn.feature_selection

from autodx.algorithm.cluster import EqualKMeans

def discriminative_features(X, k=None, groups=None):
    """
    Find the k most discriminative features partitioning the index in X 
    into groups.
    """
    #X = impute(X)
    n = X.shape[0]
    groups = groups or 2
    #model = sklearn.cluster.Birch(n_clusters=groups)
    model = EqualKMeans(n_clusters=groups)
    y = pd.Series(model.fit_predict(X), index=X.index)
    F, p = sklearn.feature_selection.f_classif(X, y)
    r = X.corrwith(y)
    o = pd.DataFrame({
        "F": F, 
        "p": p,
        "r": r
    }, index=X.columns)
    if k is not None:
        o_up = o.query("r > 0").sort_values("p").index[:k]
        o_down = o.query("r < 0").sort_values("p").index[:k]
        ix = set(o_up) | set(o_down)
        o = o.loc[ix,:]
    return o.sort_values("p")

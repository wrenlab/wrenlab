import os
import hashlib
import collections
import functools
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import bidict
import joblib

import logging

logging.basicConfig()
LOG = logging.getLogger("autodx")
LOG.setLevel("INFO")

memoize = joblib.Memory(cachedir=os.path.expanduser("~/.cache/autodx")).cache

def matrix_transformer(fn):
    """
    A decorator for a function that takes a 2D ndarray or DataFrame as
    its only positional argument (kwargs are allowed) and returns a 
    2D ndarray of the same size.

    If a :class:`pandas.DataFrame` was supplied, this decorator will
    wrap the output 2D array by adding the appropriate labels.
    """
    @functools.wraps(fn)
    def wrap(Xi, **kwargs):
        assert len(Xi.shape) == 2
        if isinstance(Xi, pd.DataFrame):
            Xim = np.array(Xi)
            Xo = fn(Xim, **kwargs)
            assert (Xi.shape == Xo.shape)
            o = pd.DataFrame(Xo, index=Xi.index, columns=Xi.columns)
            o.index.name = Xi.index.name
            o.columns.name = Xi.columns.name
            return o
        elif isinstance(Xi, np.ndarray):
            return fn(Xi, **kwargs)
        else:
            raise ValueError
    return wrap

def align_series(x, y, dropna=True):
    """
    Align two :class:`pandas.Series` using an inner join of their indices.

    Arguments
    =========
    x, y : :class:`pandas.Series`
    dropna : bool, optional
        Whether to drop missing elements from both :class:`pandas.Series`
        objects before performing the join.

    Returns
    =======
    A 2-tuple of :class:`pandas.Series`, the aligned versions of x and y.

    """
    if isinstance(x, pd.Series) and isinstance(y, pd.Series):
        if dropna is True:
            x = x.dropna()
            y = y.dropna()

        ix = list(set(x.index) & set(y.index))
        ox, oy = x.loc[ix], y.loc[ix]
        assert len(ox) == len(oy), "Duplicate entries in index"
    else:
        assert len(x) == len(y)
        def ok(q):
            return ~(np.isnan(q) | np.isinf(q))
        ix = ok(x) & ok(y)
        ox, oy = x[ix],y[ix]
    return ox, oy
        

def make_mapping(ix):
    return bidict.bidict({k:"T"+hashlib.md5(str(k).encode("utf-8")).hexdigest() for k in ix})



import sklearn.decomposition
from datasketch import WeightedMinHashGenerator, MinHashLSH
import numpy as np
import metric_learn

from .util import LOG, memoize

class LSHIndex(object):
    def __init__(self, X, threshold=0.1):
        nc = X.shape[1]
        self._index = MinHashLSH(threshold=threshold)
        self._generator = WeightedMinHashGenerator(nc)
        self._h = {}
        
        index = X.index
        X = np.array(X)
        for i in range(X.shape[0]):
            h = self._generator.minhash(X[i,:])
            k = index[i]
            self._h[k] = h
            self._index.insert(k, h)

    def query(self, key):
        if isinstance(key, np.ndarray):
            h = self._generator.minhash(key)
        else:
            h = self._h[key]
        return self._index.query(h)

def learn_metric(X, groups):
    X, groups = X.align(groups, join="inner", axis=1)
    model = metric_learn.lmnn.LMNN()
    X, groups = np.array(X), np.array(groups)
    model.fit(X, groups)
    return model

import bidict
import scipy.sparse
import pandas as pd
import sklearn.cluster

class Index(list):
    def __init__(self, elements):
        index = list(sorted(set(elements)))
        super().__init__(index)
        self.M = bidict.bidict()
        for i,ix in enumerate(index):
            self.M[ix] = i

class SparseMatrix(object):
    def __init__(self, index, columns, data):
        self.index = index
        self.columns = columns
        self.data = data
        self.shape = (len(self.index), len(self.columns))

    @staticmethod
    def new(M):
        index = Index(M.iloc[:,0])
        columns = Index(M.iloc[:,1])
        shape = (len(index), len(columns))
        data = scipy.sparse.csr_matrix((M.iloc[:,2], 
            (
                [index.M[ix] for ix in M.iloc[:,0]], 
                [columns.M[ix] for ix in M.iloc[:,1]]
            )), 
            shape=shape)
        return SparseMatrix(index, columns, data)

    @property
    def T(self):
        return SparseMatrix(self.columns, self.index, self.data.T)

    def DR(self, n_components=10):
        model = sklearn.decomposition.TruncatedSVD(n_components=n_components)
        o = model.fit_transform(self.data)
        columns = ["C{}".format(i+1) for i in range(n_components)]
        return pd.DataFrame(o, index=self.index, columns=columns)

    def cluster(self, k=10):
        from autodx.algorithm.cluster import EqualKMeans
        X = self.DR()
        #model = sklearn.cluster.Birch(n_clusters=k)
        model = EqualKMeans(n_clusters=k)
        c = model.fit_predict(X)
        return pd.Series(c, index=self.index)

import sklearn.metrics

class AnnotationSet(SparseMatrix):
    def __init__(self, index, columns, data, metadata=None):
        super().__init__(index, columns, data)
        self.metadata = metadata

    @staticmethod
    def new(M, **kwargs):
        o = SparseMatrix.new(M)
        return AnnotationSet(o.index, o.columns, o.data, **kwargs)

    def enrichment(self, q):
        background = list(sorted(set(q.index) & set(self.index)))
        ix = [self.index.M[k] for k in background]
        X = self.data.tocsc()[ix,:]
        q = q.loc[background]

        # binary
        X = X.T.toarray().astype(bool)
        q = np.array(q <= q.quantile(0.05))
        counts = np.vstack([
            (X & q).sum(axis=1), 
            (X & ~q).sum(axis=1),
            (~X & q).sum(axis=1), 
            (~X & ~q).sum(axis=1)
        ]).T

        def fn(i):
            row = counts[i,:]
            N = row[0]
            if N < 3:
                OR, p, G = np.nan, np.nan, np.nan
            else:
                ct = row.reshape((2,2))
                OR = (row[0] / row[1]) / (row[2] / row[3])
                #OR, p = scipy.stats.fisher_exact(row.reshape((2,2)))
                G, p, _, _ = scipy.stats.chi2_contingency(row.reshape((2,2)), 
                        lambda_="log-likelihood")
            return {"N": N, "G": G, "OR": OR, "p": p}
        o = pd.DataFrame([fn(i) for i in range(counts.shape[0])], index=self.columns)
        o = self.metadata.join(o, how="inner")
        o.index.name = "TermID"
        return o.dropna().query("OR > 1").sort_values("p")

import wrenlab.enrichment
import wrenlab.ontology

def GO():
    AS = wrenlab.enrichment.annotation("GO", 9606)
    AT = wrenlab.ontology.fetch("GO").ancestor_table
    o = AS.mapping.merge(AT, left_on="TermID", right_on="Descendant")\
            .loc[:,["ElementID", "Ancestor", "Score"]]
    o.columns = ["ElementID", "TermID", "Score"]
    return AnnotationSet.new(pd.concat([o, AS.mapping]), 
        metadata=AS.metadata.drop("Namespace", axis=1)\
                .drop_duplicates())

# cluster genes according to GO similarity
@memoize
def GO_clusters(k=10):
    return GO().cluster(k=k)

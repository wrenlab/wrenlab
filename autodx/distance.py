import scipy.stats
import numpy as np
import pandas as pd

from .util import align_series

"""
Possible ways to convert correlation to distance metric

Dissimilarity = 1 - Correlation
Dissimilarity = (1 - Correlation)/2
Dissimilarity = 1 - Abs(Correlation)
Dissimilarity = Sqrt(1 - Correlation2)
"""

METRICS = {
    "spearman": lambda x,y: (1 - scipy.stats.spearmanr(x,y)[0])
}

def split_distance(v1, v2, metric="spearman"):
    """
    Calculate a distance metric between `v1` and `v2` while removing 
    increasing fractions of samples from the "middle". The purpose of
    this is to account for situations where the two "ends" of a pair
    of vectors are important/significant, but the ordering of the 
    middle items is random or unimportant.
    """
    fn = METRICS[metric]
    v1, v2 = align_series(v1, v2)

    def quantile(x):
        ix = np.argsort(x)
        return ix / len(ix)

    mu = np.argsort(quantile(v1) + quantile(v2)) / len(v1)
    o = []
    for fraction in np.arange(0, 1, step=0.05):
        delta = fraction / 2
        low, high = (0.5 - delta), (0.5 + delta)
        ix = mu.index[(mu <= low) | (mu >= high)]
        q1,q2 = align_series(v1.loc[ix], v2.loc[ix])
        distance = fn(q1,q2)
        o.append((round(fraction, 2), distance))
    return pd.DataFrame(o, columns=["Fraction", metric]).set_index(["Fraction"])

def mcompare(X, metric_fn="spearman"):
    """
    Compare all columns of X.
    """
    # FIXME: figure out if it is supposed to only take dataframe or what
    
    def fn(x, y):
        x,y = align_series(x,y)
        if isinstance(metric_fn, str):
            return METRICS[metric_fn](x,y)
        else:
            return metric_fn(x,y)

    def run(X):
        nc = X.shape[1]
        o = np.zeros((nc,nc))
        for i in range(nc):
            v1 = X[:,i]
            for j in range(nc):
                #if i >= j:
                #    pass
                #else:
                v2 = X[:,j]
                r = fn(v1,v2)
                o[i,j] = r
                o[j,i] = r
        return o
    if isinstance(X, pd.DataFrame):
        Xa = np.array(X)
        o = run(Xa)
        o = pd.DataFrame(o, index=X.columns, columns=X.columns)
        return o
    else:
        return o

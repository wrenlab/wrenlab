import numpy as np

from autodx.util import matrix_transformer

@matrix_transformer
def quantile(X, mu=None):
    """
    Quantile normalize a matrix.

    Parameters
    ----------
    X : a 2D :class:`numpy.ndarray`
        The matrix to be normalized, with samples as rows
        and probes/genes as columns.
    mu : a 1D :class:`numpy.ndarray`, optional
        Vector of gene means.

    Returns
    -------
    :class:`numpy.ndarray`
        The normalized matrix.
    """
    X = X.T
    assert not np.isnan(X).all(axis=1).any() # rows
    if mu is not None:
        mu = np.array(mu)
        assert len(mu) == X.shape[1] 
        assert not np.isnan(mu).any()
    else:
        assert not np.isnan(X).all(axis=0).any() # columns

    Xm = np.ma.masked_invalid(X.T)
    Xn = np.empty(Xm.shape)
    Xn[:] = np.nan

    if mu is None:
        mu = Xm.mean(axis=0)
    mu.sort()

    for i in range(Xm.shape[0]):
        # sort and argsort sorts small to large with NaN at the end
        ix = np.argsort(Xm[i,:])
        nok = (~Xm[i,:].mask).sum()
        ix = ix[:nok]
        rix = (np.arange(nok) * len(mu) / nok).round().astype(int)
        Xn[i,ix] = mu[rix]

    assert (np.isnan(X) == np.isnan(Xn.T)).all()
    return Xn

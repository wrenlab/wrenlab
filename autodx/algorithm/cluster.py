import heapq
import math

import numpy as np
import scipy.spatial.distance

from autodx.util import LOG

class Heap(object):
    def __init__(self):
        self._items = []

    def push(self, value, priority):
        heapq.heappush(self._items, (priority, value))

    def pop(self):
        return heapq.heappop(self._items)[1]

    def __bool__(self):
        return bool(self._items)

class EqualKMeans(object):
    def __init__(self, n_clusters=2):
        self.n_clusters = n_clusters

    def fit_predict(self, X):
        # Based on:
        # https://stackoverflow.com/questions/8796682/group-n-points-in-k-clusters-of-equal-size
        # but with simpler M step
        X = np.array(X)
        nr = X.shape[0]
        max_items = math.ceil(nr / self.n_clusters)

        def fn(centroid):
            o = np.empty(nr)
            o[:] = -1
            D = scipy.spatial.distance.cdist(X, centroid)

            heap = Heap()
            best = np.argsort(D)[:,0]
            for i,c in enumerate(best):
                distance = D[i,c]
                heap.push((i,c), distance)
            while heap:
                i,c = heap.pop()
                dx = D[i,:].copy()
                while (o == c).sum() == max_items:
                    dx[c] = np.finfo(D.dtype).max
                    c = np.argsort(dx)[0]
                o[i] = c
            return o

        # initialize
        np.random.seed(0)
        centroid = X[np.random.choice(np.arange(nr), size=self.n_clusters, replace=False),:]
        o = fn(centroid)

        # EM
        n_iter = 10
        for iteration in range(n_iter):
            LOG.info("EqualKMeans:iter={}".format(iteration+1))
            for c in range(self.n_clusters):
                centroid[c,:] = X[o == c,:].mean(axis=0)
            prev = o
            o = fn(centroid)
            if (o == prev).all():
                return o
        return np.array(o, dtype=int)

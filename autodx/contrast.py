import collections

import seaborn as sns
import matplotlib.pyplot as plt
import scipy.stats
import sklearn.metrics
import sklearn.cluster
import scipy.cluster.hierarchy
import numpy as np
import pandas as pd

import autodx.statistics

import pylatexenc.latexencode

def only_printable(df):
    def fn(index):
        o = []
        for ix in index:
            try:
                ix.encode("ascii")
                o.append(True)
            except:
                o.append(False)
        return o
    rix, cix = fn(df.index), fn(df.columns)
    return df.loc[rix,:].loc[:,cix]

def fix_index(ix):
    # for LaTeX output
    return [pylatexenc.latexencode.utf8tolatex(str(k)) for k in ix]

class Contrast(object):
    """
    Contains modeling results for a single contrast.
    """
    class Plot(object):
        def __init__(self, ct):
            self.ct = ct

        def MA(self):
            plt.clf()
            df = self.ct.data.loc[:,["logFC","logCPM"]]
            df.columns = ["M", "A"]
            ax = sns.lmplot(x="M", y="A", data=df, fit_reg=False)

        def volcano(self):
            plt.clf()
            df = self.ct.data.loc[:,["logFC", "PValue"]].copy()
            df.PValue = - df.PValue.apply(np.log10)
            ax = sns.lmplot(x="logFC", y="PValue", data=df, fit_reg=False)
            plt.axhline(y=-np.log10(0.05), color='r', linestyle='--')
            plt.ylabel("$-log_{10}$(p)")

    def __init__(self, name, data, annotation=None, metadata=None):
        self.name = name
        try:
            data.index = list(map(int, data.index))
        except:
            pass
        self.data = data
        #self.data.index = [ix.replace("_", "\\_") for ix in self.data.index]
        self.plot = Contrast.Plot(self)
        self.annotation = annotation
        self.metadata = metadata or {}

    def to_frame(self):
        if self.annotation is not None:
            return self.annotation.join(self.data, how="inner")
        else:
            return self.data.copy()

class ContrastSet(dict):
    """
    Contains modeling results for a set of contrasts/parameters/LM terms 
    (ideally, but not necessarily, obtained from the same dataset).
    """
    # TODO: add getitem to get single param and run on that so it doesnt need to be passed all over?

    def __init__(self, contrasts: dict, default_parameter="logFC", annotation=None, metadata=None):
        self.metadata = metadata or {}
        self.annotation = annotation
        self.default_parameter = default_parameter

        o = {}
        for k,v in contrasts.items():
            if not isinstance(v, Contrast):
                v = Contrast(k, v, annotation=annotation)
            o[k] = v

        super().__init__(o)

    def __repr__(self):
        return object.__repr__(self)

    def element(self, key):
        ix, o = [], []
        for k,ct in self.items():
            if not key in ct.data.index:
                continue
            ix.append(k)
            o.append(ct.data.loc[key,:])
        o = pd.concat(o, axis=1).T
        o.index = ix
        return o

    def enrichment(self):
        import wrenlab.enrichment
        from wrenlab.statistics.meta_analysis import fisher_method
        taxon_id = self.metadata.get("TaxonID", 9606)
        AS = wrenlab.enrichment.annotation("GO", taxon_id=taxon_id)
        p = self.to_frame("p")
        p = fisher_method(p)
        return AS.enrichment(-p)

    def to_frame(self, param=None):
        param = param or self.default_parameter
        ix = list(self.keys())
        X = pd.concat([self.__getitem__(k).data[param] for k in ix], axis=1)
        X.columns = ix
        return X

    def dendrogram(self, param=None, transpose=False, metric="ward"):
        plt.clf()
        X = self.to_frame(param)
        # By default, cluster by contrasts, not OTU
        if transpose is False:
            X = X.T
        Z = scipy.cluster.hierarchy.linkage(X, metric)
        labels = [k.replace("_", " ") for k in X.columns]
        f = scipy.cluster.hierarchy.dendrogram(Z, labels=labels, orientation="left")
        return f

    def bar(self, contrast, param=None, k=20):
        param = param or self.default_parameter 
        plt.clf()
        ct = self.__getitem__(contrast)
        df = ct.data.loc[:,[param]].reset_index().dropna()
        df = df.sort_values(param)
        if k is not None:
            df = pd.concat([df.iloc[:k,:], df.iloc[-k:,:]])
        ax = sns.barplot(x=param, y=df.columns[0], data=df, orient="h", color="blue")
        plt.xlabel(param)
        return ax

    def clustermap(self, param=None, k=10, **kwargs):
        # FIXME: if a lot of features, subset features by most distinguishing
        plt.clf()
        X = self.to_frame(param)
        # FIXME: kind of a hack to figure out which axis to prune
        #X = only_printable(X)
        if k is not None:
            t = X.shape[0] > X.shape[1]
            if t:
                X = X.T
            """
            ix = X.mean().sort_values().index
            ix = set(ix[:k]) | set(ix[-k:])
            X = X.loc[:,X.columns.isin(ix)]
            """
            df = autodx.statistics.discriminative_features(X, k=k)
            X = X.loc[:,df.index]
            if t:
                X = X.T
        X.index = fix_index(X.index)
        X.columns = fix_index(X.columns)
        # FIXME: label color key with param
        #cbar_kws={"title": param}
        return sns.clustermap(X, cmap="RdBu_r", **kwargs)

    """
    def literature_associations(self, key, **kwargs):
        assert self.level == TaxonomyLevel.SPECIES
        o = {}
        for k,ct in self.items():
            o[k] = ct.literature_associations(key, **kwargs).to_frame()
        return ContrastSet(o, self.level)

    def ProTraits(self, param=None):
        param = param or self.default_parameter
        assert self.level == TaxonomyLevel.SPECIES
        o = {}
        for k,ct in self.items():
            o[k] = ct.ProTraits(param=param)
        return ContrastSet(o, self.level)
    """

    def cluster(self, axis=0, param=None, k=2):
        X = self.to_frame(param)
        if axis == 1:
            X = X.T
        model = sklearn.cluster.Birch(n_clusters=k)
        return pd.Series(model.fit_predict(X), index=X.index)

    def to_excel(self, path):
        with pd.ExcelWriter(path) as xlw:
            for k,df in self.items():
                df.to_excel(xlw, sheet_name=k)
        
    """
    def distance(self, query, param=None):
        #Return distance metrics between the selected contrast and all other 
        #constrasts in this :class:`ContrastSet`.
        X = self.to_frame(param)
        if isinstance(query, str):
            query = X.loc[:,query]
        elif isinstance(query, pd.Series):
            pass
        else:
            raise Exception
        X, query = X.align(query, axis=0, join="inner")
        assert X.shape[0] > 3

        o = X.apply(lambda x: mnemonic.distance.CC(x, query)).T
        return o.sort_values("p")
    """
    def summary(self):
        o = collections.defaultdict(list)
        keys = list(self.keys())
        for k in keys:
            FDR = self.__getitem__(k).data["FDR"]
            ix = FDR <= 0.05
            o["NSigificant"].append(ix.sum())
            o["PercentSignificant"].append(ix.mean())
        return pd.DataFrame(o, index=keys)

    def compare_individual(self, o):
        # FIXME: for now, just plot a bar
        #df = self.
        raise NotImplementedError

    def compare_set(self, o):
        """
        Compare with another ContrastSet.
        """
        # FIXME: for now, just plot a clustermap, 
        # maybe a contrastcomparison object or something is needed?
        assert self.level == o.level
        df1 = self.to_frame()
        df2 = o.to_frame()
        df1, df2 = df1.align(df2, axis=0, join="inner")
        r = mnemonic.statistics.corr(df1, df2)
        r.index = fix_index(r.index)
        r.columns = fix_index(r.columns)
        return sns.clustermap(r, cmap="RdBu_r")

    def compare(self, o):
        if isinstance(o, type(self)):
            return self.compare_set(o)
        elif isinstance(o, Contrast):
            return self.compare_individual(o)
        else:
            raise TypeError()

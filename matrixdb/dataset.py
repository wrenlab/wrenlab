import pandas as pd
import numpy as np

from .df import DFMatrix

class Dataset(object):
    def __init__(self, X, 
            row_annotation=None, 
            column_annotation=None,
            metadata=None):
        def maybe_copy(x):
            if x is None:
                return None
            return x.copy()
        self._row_annotation = maybe_copy(row_annotation)
        self._column_annotation = maybe_copy(column_annotation)
        if isinstance(X, pd.DataFrame):
            X = DFMatrix(X)
        self._data = X
        self._metadata = {k:v for k,v in metadata.items()} \
                if metadata is not None else {}

    def __repr__(self):
        return "<Dataset: n_samples={}, n_features={}>"\
                .format(*self._data.shape)

    def align(self):
        rix, cix = None, None
        if self._row_annotation is not None:
            rix = list(set(self._row_annotation.index) & set(self._data.row_index))
        if self._column_annotation is not None:
            cix = list(set(self._column_annotation.index) & set(self._data.column_index))
        return self._new(rix, cix)

    @property
    def shape(self):
        return self._data.shape

    #######################
    # Mixin implementations
    #######################

    def to_frame(self):
        return self._data.to_frame()

    def metadata(self):
        return self._metadata

    #########
    # General
    #########

    def _drop_unused_categories(self, df):
        df = df.copy()
        for c in df.columns:
            if df[c].dtype.name == "category":
                df[c].cat.remove_unused_categories(inplace=True)
        return df

    def _new(self, rix, cix, *, X=None):
        X = X or self._data
        if rix is None:
            rix = self._data.row_index
        else:
            rix = self._data.row_index[np.where(rix)]
            X = X.rows(rix)
        if cix is None:
            cix = self._data.column_index
        else:
            cix = self._data.column_index[np.where(cix)]
            X = X.columns(cix)

        if self._row_annotation is not None:
            ra = self._row_annotation.loc[rix,:].copy()
            ra = self._drop_unused_categories(ra)
        else:
            ra = None
        if self._column_annotation is not None:
            ca = self._column_annotation.loc[cix,:].copy()
            ca = self._drop_unused_categories(ca)
        else:
            ca = None

        return type(self)(X,
                row_annotation=ra,
                column_annotation=ca,
                metadata=self._metadata)

    def copy(self):
        X = DFMatrix(self._data.to_frame())
        return self._new(None, None, X=X)

    @property
    def row_annotation(self):
        if self._row_annotation is not None:
            return self._row_annotation.copy()

    @property
    def column_annotation(self):
        if self._column_annotation is not None:
            return self._column_annotation.copy()

    @property
    def metadata(self):
        if self._metadata is not None:
            return {k:v for k,v in self._metadata.items()}
        return {}

    ################
    # Data selection
    ################

    @property
    def row_index(self):
        return self._data.row_index

    @property
    def column_index(self):
        return self._data.column_index

    def row(self, key):
        return self._data.row(key)

    def column(self, key):
        return self._data.column(key)

    ###############
    # Transposition
    ###############

    def transpose(self):
        raise NotImplementedError

    @property
    def T(self):
        return self.transpose()

    ####################
    # Metadata selection
    ####################

    def eq(self, key, value):
        """
        Filter by `key` s.t. only samples with `A[key] == value`
        are retained.
        """
        x = self._row_annotation[key]
        if x.dtype.name == "category":
            if isinstance(value, int):
                ix = x == value
            else:
                ix = x.astype(str) == str(value)
        else:
            ix = x == value
        return self._new(ix, None)

    def neq(self, key, value):
        x = self._row_annotation[key]
        if x.dtype.name == "category":
            if isinstance(value, int):
                ix = x == value
            else:
                ix = x.astype(str) == str(value)
        else:
            ix = x == value
        return self._new(~ix, None)

    def between(self, key, low, high):
        """
        Filter by `key` s.t. only samples with low <= value <= high
        are retained.
        """
        ix = (self._row_annotation[key] >= low) \
            & (self._row_annotation[key] <= high)
        return self._new(ix, None)

    def most_frequent(self, key, n):
        """
        Return a new :class:`Dataset` containing only samples with
        from the top `n` most frequent categories in the categorical
        variable `key`.
        """
        counts = self._row_annotation[key].value_counts()
        counts = counts.loc[counts >= 3]
        ix = self._row_annotation[key].isin(counts.index[:n])
        return self._new(ix, None)

    def random(self, n):
        """
        Select `n` random rows.
        """
        ix = self._row_annotation.index
        ix = np.random.choice(ix, size=n, replace=False)
        return self._new(ix, None)

    def has(self, key):
        ix = ~self._row_annotation[key].isnull()
        return self._new(ix, None)

    def isin(self, key, values, axis=0):
        assert axis in (0,1)
        if axis == 0:
            ix = self._row_annotation[key].isin(values)
            return self._new(ix, None)
        else:
            ix = self._column_annotation[key].isin(values)
            return self._new(None, ix)

    def min_count(self, key, n):
        counts = self._row_annotation[key].value_counts()
        ok = counts.index[counts >= n]
        return self._new(self._row_annotation[key].isin(ok), None)

    def filter(self, key, fn):
        """
        For each subgroup defined by "key", call "fn" on that 
        subgroup's annotation frame,
        retaining each subgroup if the function returns a truthy value.
        """
        ok = []
        for _,subset in self.groupby(key):
            if fn(subset.row_annotation):
                ok.extend(subset.row_index)
        return self._new(ok, None)

    ##########
    # Iterable
    ##########

    def groupby(self, key):
        for key_id in self._row_annotation[key].unique():
            ix = self._row_annotation[key] == key_id
            yield key_id, self._new(ix, None)

    ###############################################
    # FIXME FIXME FIXME
    # Stuff needs to be factored out of wrenlab (?)
    ###############################################

    def each(self, key, fn):
        """
        For each subset defined by "key", apply "fn" and return 
        a dictionary of the results.
        """
        o = {}
        for key_value, subset in self.groupby(key):
            o[key_value] = fn(subset)
        return o

    def fit(self, formula, group=None, link=None):
        import wrenlab.lm
        assert link in (None, "logit")
        X = self.to_frame()
        # FIXME
        X = X.dropna(axis=1, how="any")
        if X.isnull().any().any():
            import wrenlab.impute
            LOG.info("Imputing missing values for matrix of shape: {}".format(X.shape))
            X = wrenlab.impute.mean(X)
            if link == "logit":
                X = X.apply(wrenlab.statistics.logit)
        o = wrenlab.lm.fit(X, self._row_annotation, formula, group=group)
        o.metadata["TaxonID"] = self.metadata.get("TaxonID")
        return o



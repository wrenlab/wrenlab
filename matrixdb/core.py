import itertools
import sys

import h5py
import numpy as np
import pandas as pd

from .interface import IMatrix

def make_index(xs):
    if all([x.isdigit() for x in xs]):
        xs = [int(x) for x in xs]
        return list(map(int, np.array(xs, dtype=int)))
    else:
        return np.array(xs, dtype=bytes)

def decode_index(xs):
    try:
        if xs.dtype == np.int64:
            o = xs[:]
        else:
            o = np.char.decode(xs[:], "utf-8")
        return Index(o)
    except:
        xs = np.array(xs[:])
        o = []
        for i in range(xs.shape[0]):
            x = xs[i,:]
            x = x[:(np.where(x == 0)[0][0])]
            o.append(x.tobytes().decode("utf-8"))
        return Index(o)

class Index(list):
    def __init__(self, elements):
        super().__init__(elements)
        self.inv = {ix:i for i,ix in enumerate(elements)}

class Matrix(IMatrix):
    def __init__(self, group):
        self.group = group
        self._row_index = decode_index(group["index/0"])
        self._column_index = decode_index(group["index/1"])

    @property
    def row_index(self):
        return pd.Index(list(self._row_index))

    @property
    def column_index(self):
        return pd.Index(list(self._column_index))

    def _row(self, key):
        ix = self._row_index.inv[key]
        data = self.group["data/0"][ix,:]
        o = pd.Series(data, index=self._column_index)
        o.name = key
        return o

    def _column(self, key):
        ix = self._column_index.inv[key]
        data = self.group["data/1"][ix,:]
        o = pd.Series(data, index=self._row_index)
        o.name = key
        return o

    def _rows(self, keys):
        q = np.array([self._row_index.inv[k] for k in keys])
        ix = np.argsort(q)
        return self.group["data/0"][q[ix],:][np.argsort(ix),:]

    def _columns(self, keys):
        q = np.array([self._column_index.inv[k] for k in keys])
        ix = np.argsort(q)
        return self.group["data/1"][q[ix],:][np.argsort(ix),:].T

    def to_array(self):
        return self.group["data/0"][:]

    def dump(self, handle):
        print(*[""] + list(self.column_index), sep="\t", file=handle)
        for i,key in enumerate(self.row_index):
            x = self.row(key)
            print(*[key] + list(x), sep="\t", file=handle)

class EmbeddedMatrixDB(object):
    def __init__(self, path, read_only=True):
        mode = "r" if read_only is True else "a"
        self.root = h5py.File(path, mode)

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, key):
        return Matrix(self.root[key])

    def put(self, key, path_or_handle):
        chunks = pd.read_csv(path_or_handle, sep="\t", index_col=0, chunksize=100)
        first = next(chunks)
        chunks = itertools.chain([first], chunks)

        columns = make_index(first.columns)
        nc = len(columns)

        group = self.root.create_group(key)
        g_data = group.create_group("data")
        g_data0 = g_data.create_dataset("0", (0, nc), maxshape=(None, nc), chunks=True)

        # Insert data
        index = []
        nr = 0
        for chunk in chunks:
            print("Inserting row:", nr, file=sys.stderr)
            index.extend(list(chunk.index))
            nr += chunk.shape[0]
            g_data0.resize((nr, nc))
            g_data0[(nr - chunk.shape[0]):,:] = np.array(chunk)

        # Insert indices
        index = make_index(index)
        nr = len(index)
        g_index = group.create_group("index")
        g_index0 = g_index.create_dataset("0", data=index)
        g_index1 = g_index.create_dataset("1", data=columns)

        # Insert transposition
        print("Transposing", file=sys.stderr)
        g_data1 = g_data.create_dataset("1", (nc, nr))
        step = 100
        for j in np.arange(0, nc, step):
            X = g_data0[:,j:(j+step)]
            g_data1[j:(j+step),:] = X.T

        return Matrix(group)

    @staticmethod
    def load(path, handle):
        pass

import numpy as np

from .interface import IMatrix, Selection

class DFMatrix(Selection, IMatrix):
    """
    Wrapper for a :class:`pandas.DataFrame` satisfying the
    :class:`matrixdb.interface.IMatrix` API.
    """
    def __init__(self, df):
        # FIXME: Hack
        while isinstance(df, DFMatrix):
            df = df._df
        self._df = df
        Selection.__init__(self, df.index, df.columns, df)

    @property
    def row_index(self):
        return self._df.index
    
    @property
    def column_index(self):
        return self._df.columns

    def _row(self, key):
        return self._df.loc[key,:]

    def _column(self, key):
        return self._df.loc[:,key]

    def _rows(self, ix):
        return DFMatrix(self._df.loc[ix,:])

    def _columns(self, ix):
        return DFMatrix(self._df.loc[:,ix])

    def to_array(self):
        return np.array(self._df)

    def to_frame(self):
        return self._df.copy()

    def rows(self, ix):
        return self._rows(ix)

    def columns(self, ix):
        return self._columns(ix)

    @property
    def shape(self):
        return self._df.shape

    @property
    def T(self):
        return self.transpose()

    def transpose(self):
        return DFMatrix(self._df.T)

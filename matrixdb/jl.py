"""
Matrices compressed with the Johnson–Lindenstrauss transformation.
"""

import numpy as np
import pandas as pd

from .interface import IMatrix, Selection
from .df import DFMatrix

class JLMatrix(Selection, IMatrix):
    def __init__(self, transform, data):
        self._transform = transform
        self._data = data
        Selection.__init__(self, self._data.index, self._transform.columns, self)

    @property
    def row_index(self):
        return self._data.index

    @property
    def column_index(self):
        return self._transform.columns

    def _row(self, ix):
        return self._data.loc[ix,:] @ self._transform

    def _column(self, ix):
        return self._data @ self.transform.loc[:,ix]

    def _rows(self, ix):
        return JLMatrix(self._transform, self._data.loc[ix,:])

    def _columns(self, ix):
        return JLMatrix(self._transform.loc[:,ix], self._data)

    def to_array(self):
        return np.array(self._data @ self._transform)

    def to_frame(self):
        o = self.to_array()
        return pd.DataFrame(o, index=self.row_index, columns=self.column_index)

    def rows(self, ix):
        return self._rows(ix)

    def columns(self, ix):
        return self._columns(ix)

    @property
    def shape(self):
        return (self._data.shape[0], self._transform.shape[1])

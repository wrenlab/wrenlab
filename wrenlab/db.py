import tempfile

import psycopg2

def connect():
    #return psycopg2.connect(dbname="wrenlab", user="gilesc")
    return Connection(dbname="wrenlab", user="gilesc")

class Connection(object):
    def __init__(self, **kwargs):
        self._cx = psycopg2.connect(**kwargs)
        self._is_open = True

    def commit(self):
        self._cx.commit()

    def cursor(self):
        return self._cx.cursor()

    def close(self):
        if self._is_open:
            self._cx.close()
            self._is_open = False

    def copy(self, table, rows, columns=None):
        """
        Bulk import data into a table.

        Arguments
        ---------
        table : str
            The table to import data into.
        rows : iterable of tuples
            Sequence of row data to import.
        columns : list, optional
            If specified, the names, in order, of the columns.
        """
        def check_encoding(x):
            if isinstance(x, str):
                return x.encode("utf-8", errors="ignore").decode("utf-8", errors="ignore")
            else:
                return x

        def replace_whitespace(x):
            if isinstance(x, str):
                return x.replace("\n", " ").replace("\t", " ")
            else:
                return x

        with tempfile.NamedTemporaryFile(mode="wt+") as h:
            for row in rows:
                row = [replace_whitespace(check_encoding(x)) if x is not None else "\\N" for x in row]
                print(*row, sep="\t", file=h)
            h.seek(0)
            c = self.cursor()
            c.copy_from(h, table, sep="\t", null="\\N", columns=columns)
        self.commit()

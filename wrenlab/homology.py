import os
import tempfile
import subprocess
import textwrap
import io
import multiprocessing as mp

import pandas as pd
from pyensembl.ensembl_release import EnsemblRelease
from pyensembl.ensembl_release_versions import MAX_ENSEMBL_RELEASE
from Bio.Blast import NCBIXML

import wrenlab.ncbi.taxonomy
import wrenlab.ncbi.gene
import wrenlab.ensembl
from wrenlab.util import cwd, memoize

def write_fasta(handle, sequences):
    for k,v in sequences.items():
        print(">", k, file=handle, sep="")
        for line in textwrap.wrap(v, width=70):
            print(line, file=handle)

class BLASTDB(object):
    def __init__(self, sequences, dbtype="nucl"):
        self._tmpdir = tempfile.TemporaryDirectory()
        self._root = self._tmpdir.name
        self._dbtype = dbtype

        with cwd(self._root):
            with open("db.fa", "w") as h:
                write_fasta(h, sequences)
            subprocess.check_call(["makeblastdb", "-dbtype", self._dbtype, "-in", "db.fa"])

    def _query(self, sequences, evalue=3):
        with tempfile.NamedTemporaryFile(mode="w") as h:
            write_fasta(h, sequences)
            with cwd(self._root):
                cmd = ["blastn", 
                        "-query", h.name, 
                        "-db", "db.fa", 
                        "-outfmt", "5", 
                        "-evalue", str(evalue), 
                        "-num_threads", str(mp.cpu_count() - 2)]
                xml = subprocess.check_output(cmd)
                with io.StringIO(xml.decode("utf-8")) as h:
                    return list(NCBIXML.parse(h))

    def query(self, sequences, evalue=3):
        records = self._query(sequences, evalue=evalue)
        o = []
        for r in records:
            q = r.query
            for aln in r.alignments:
                t = aln.hit_def
                e = min(hsp.expect for hsp in aln.hsps)
                o.append((t,q,e))
        return pd.DataFrame.from_records(o, columns=["TargetID", "QueryID", "EValue"])


@memoize
def blast_all_transcripts(taxon_id1, taxon_id2, entrez=True, 
        ensembl_version=MAX_ENSEMBL_RELEASE):

    # NB, taxon_id1 is the DB, taxon_id2 is blasted against it
    # Step 1: BLAST Ensembl Transcript IDs
    #species1 = wrenlab.ncbi.taxonomy.by_id(taxon_id1).name
    #species2 = wrenlab.ncbi.taxonomy.by_id(taxon_id2).name
    r1 = wrenlab.ensembl.release(taxon_id1)
    r2 = wrenlab.ensembl.release(taxon_id2)

    q = r2.transcript_sequences.fasta_dictionary
    db = wrenlab.homology.BLASTDB(r1.transcript_sequences.fasta_dictionary)
    o = db.query(q)

    # Step 2 (optional): Convert ENST to Entrez Gene IDs
    if entrez is True:
        ensg1 = wrenlab.ncbi.gene.ensembl(taxon_id1)
        ensg2 = wrenlab.ncbi.gene.ensembl(taxon_id2)
        ensg1 = dict(zip(ensg1["Ensembl Transcript ID"], ensg1["GeneID"]))
        ensg2 = dict(zip(ensg2["Ensembl Transcript ID"], ensg2["GeneID"]))
        oo = []
        for eid1, eid2, e_value in o.to_records(index=False):
            gene_id1 = ensg1.get(eid1)
            gene_id2 = ensg2.get(eid2)
            if (gene_id1 is not None) and (gene_id2 is not None):
                oo.append((gene_id1, eid1, gene_id2, eid2, e_value))
        o = pd.DataFrame.from_records(oo, 
                columns=["GeneID.1", "TranscriptID.1", 
                    "GeneID.2", "TranscriptID.2", "EValue"])
                #.groupby(["GeneID.1", "GeneID.2"])\
                #.min()\
                #.reset_index()
    return o

def best_reciprocal_blast(taxon_id1, taxon_id2):
    """
    Find homology mappings between two taxa using best 
    reciprocal BLAST hit.
    """
    rs1 = blast_all_transcripts(taxon_id1, taxon_id2)\
        .sort_values("EValue")\
        .drop_duplicates("GeneID.2")\
        .loc[:,["GeneID.1","GeneID.2"]]\
        .to_records(index=False)\
        .tolist()
    rs2 = blast_all_transcripts(taxon_id2, taxon_id1)\
        .sort_values("EValue")\
        .drop_duplicates("GeneID.2")\
        .loc[:,["GeneID.2","GeneID.1"]]\
        .to_records(index=False)\
        .tolist()
    return pd.DataFrame.from_records(list(set(rs1) & set(rs2)),
            columns=["GeneID.1", "GeneID.2"])

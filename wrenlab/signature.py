"""
Signature analysis:

Problem
=======

Given a set of genes with numeric attributes (expression levels) and a
"database" of genes with numeric attributes, signature analysis provides
different methods of assessing the correlation between the two.

A concrete example: given a set of gene fold changes and a database of genes
that are positive and negative regulators of a certain pathway, is this pathway likely
to be up or down-regulated in this experiment?
"""

from collections import namedtuple

import patsy
import statsmodels.api as sm
import statsmodels.stats.anova
import pandas as pd
import scipy.stats
import seaborn as sns

import wrenlab.statistics

class Result(object):
    def __init__(self, y, D, groups):
        self.groups = groups
        self.y = y
        self.D = D

    def params(self):
        confidence = 0.95
        y = self.y
        groups = self.groups
        columns = ["Mean", "SD", "SE", "CI_LB", "CI_UB"]

        mu = y.groupby(groups).mean()
        std = y.groupby(groups).std()
        se = y.groupby(groups).apply(scipy.stats.sem)

        n = groups.shape[0]
        h = se * scipy.stats.t._ppf((1+confidence)/2., n-1)

        ci_lb = (mu - h)
        ci_ub = (mu + h)

        params = dict(zip(columns, [mu, std, se, ci_lb, ci_ub]))
        params = pd.DataFrame.from_dict(params).loc[groups.unique(),:]\
                .loc[:,columns]
        return params

    def p(self):
        return wrenlab.statistics.compare_all_groups(self.y, self.D, self.groups)

    def fit(self, formula):
        """
        Fit an OLS model between some statistic summarizing each sample
        and the design matrix.
        """
        endog = patsy.dmatrix(formula, data=self.D, return_type="dataframe")
        model = sm.OLS(self.y, endog)
        fit = model.fit()
        return fit

    def boxplot(self):
        data = pd.DataFrame.from_dict({"Group": self.groups, "y": self.y})
        return sns.boxplot(x="Group", y="y", data=data)

class SignatureAnalysis(object):
    def __init__(self, X, D, groups):
        """
        Arguments
        ---------
        X : :class:`pandas.DataFrame`
            The expression matrix, with probes/genes as rows.
        D : :class:`pandas.DataFrame`
        groups : :class:`pandas.Series`, optional
            A series with values indicating the group and index
            indicating the Sample ID. 

        """
        X, groups = X.align(groups, axis=1, join="inner")
        self.X = X
        self.D = D
        self.groups = groups.astype("str")
        self.groups.name = "Group"

    def spearman(self, scores):
        """
        Arguments
        ---------
        scores : :class:`pandas.Series`
            A numeric Series with indexes of GeneIDs (or whatever data type is on
            the data matrix's index) indicating degree of association with each
            gene and the concept being tested. For example, this could be a binary
            integer vector indicating whether each gene is associated with a 
            particular GO category.
        """
        X, scores = self.X.align(scores, axis=0, join="inner")
        Xs = X.apply(lambda x: (x - x.mean()) / x.std(), axis=1)
        groups = self.groups

        rho = Xs.apply(lambda x: scipy.stats.spearmanr(x, scores)[0])
        return Result(rho, self.D, self.groups)

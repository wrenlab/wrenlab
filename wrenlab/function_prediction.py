"""
Algorithms for gene/transcript function prediction.

References
----------

Dozmorov, MG, Giles, CB, Wren, JD (2011). Predicting gene ontology from a
global meta-analysis of 1-color microarray experiments. BMC Bioinformatics, 12
Suppl 10:S14.

Wren, JD (2009). A global meta-analysis of microarray expression data to
predict unknown gene functions and estimate the literature-data divide.
Bioinformatics, 25, 13:1694-701.
"""

import numpy as np
import pandas as pd
import sklearn.metrics

import wrenlab.distance

class MultilabelEvaluation(object):
    def __init__(self, Y, Y_hat):
        # rows are genes, columns are terms
        assert Y.shape == Y_hat.shape
        self.Y = Y
        self.Y_hat = Y_hat

    @property
    def AUC(self):
        def fn(y):
            try:
                return sklearn.metrics.roc_auc_score(y, self.Y_hat[y.name])
            except ValueError:
                return np.nan
        return self.Y.apply(fn)

    def summary(self):
        N = self.Y.sum().astype(int)
        AUC = self.AUC
        return pd.DataFrame({"N":N, "AUC":AUC})\
                .loc[:,["N","AUC"]]\
                .sort_values("AUC", ascending=False)

    def align(self, o):
        ix = list(set(self.Y.index) & set(o.Y.index))
        cx = list(set(self.Y.columns) & set(o.Y.columns))
        Y1,Y2 = self.Y.align(o.Y, join="inner")
        Y_hat1,Y_hat2 = self.Y_hat.align(o.Y_hat, join="inner")
        return MultilabelEvaluation(Y1,Y_hat1), MultilabelEvaluation(Y2,Y_hat2)

def predict(X, A=None, k=100):
    """
    Arguments
    =========
    X : a :class:`pandas.DataFrame` with samples as rows 
        and genes (or entities to be predicted) as columns
    A : a :class:`wrenlab.annotation.AnnotationSet` with
        annotation mapping data, optional
        Default: GO with taxon ID 9606.
    k : int
        The number of nearest neighbors.
    """
    if A is None:
        A = wrenlab.enrichment.annotation("GO", 9606)

    A = A.prune(min_element_count=100)
    elements = list(set(A.mapping.ElementID))
    X_annotated = X.loc[:,list(set(elements) & set(X.columns))]

    dix = wrenlab.distance.DistanceIndex(X_annotated)
    knn = dix.nearest_neighbors(X_annotated, k=k)

    o, ix = [], []
    knn = dict(zip(knn.index, knn.apply(set, axis=1)))
    keys = list(sorted(set(knn.keys()) & set(A.mapping.ElementID)))

    for term_id, df in A.mapping.groupby("TermID"):
        ix.append(term_id)
        p = set(df.ElementID)
        score = pd.Series([len(knn[k] & p) for k in keys], index=keys)
        o.append(score)

    Y_hat = pd.concat(o, keys=ix, axis=1)
    Y_hat.index.name = "ElementID"
    Y_hat.columns.name = "TermID"
    Y = A.mapping.pivot("ElementID", "TermID", "Score").fillna(0).astype(int)

    Y, Y_hat = Y.align(Y_hat, axis=0, join="outer")
    Y, Y_hat = Y.align(Y_hat, axis=1, join="outer")

    return MultilabelEvaluation(Y.fillna(0), Y_hat.fillna(0))

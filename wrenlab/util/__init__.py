import contextlib
import itertools
import functools
import os
import os.path
import multiprocessing as mp
import signal

import functional
import scipy.stats
import numpy as np
import pandas as pd
import tqdm

import wrenlab

from .net import *
from .memoize import memoize
from .kvstore import KVStore
from .log import LOG
from .pandas import *
from .fs import *
from .db import *
from .multimethod import *

#########
# Timeout
#########

class TimeoutException(Exception):
    def __init__(self):
        msg = "Block exceeded time limit."
        super().__init__(msg)

def timeout_handler(signum, frame):
    raise TimeoutException()

@contextlib.contextmanager
def timeout_after(limit):
    """
    A context manager that raises Timeout (exception) if 
    the contained block takes longer than `limit` seconds.
    """
    if limit is None:
        yield
    else:
        signal.signal(signal.SIGALRM, timeout_handler)
        signal.alarm(limit)
        try:
            yield
        finally:
            signal.alarm(0)

###################
# Utility functions
###################

def pmap(fn, it, ncpu=mp.cpu_count() - 2, ordered=True, count=None):
    """
    Map a function in parallel with progress bar.

    ncpu : int
        If ncpu == 1, then run serially, 
        otherwise in parallel.
    """
    if count is None and hasattr(it, "__len__"):
        count = len(it)

    if ncpu > 1:
        pool = mp.Pool(ncpu)
        map_fn = pool.imap if ordered else pool.imap_unordered
        try:
            if count is not None:
                with tqdm.tqdm(total=count) as progress:
                    for o in map_fn(fn, it):
                        progress.update(1)
                        yield o
            else:
                yield from map_fn(fn, it)
        finally:
            pool.terminate()
    else:
        if count is not None:
            with tqdm.tqdm(total=count) as progress:
                yield from map(fn, it)
        else:
            yield from map(fn, it)

def parameter_product(**kwargs):
    """
    Each kwarg is a sequence. Returns an iterable of dictionaries 
    over all possible combinations of the kwargs.

    Example
    -------
    >> list(parameter_product(a=[1,2], b=[3,4]))
    [
        {"a": 1, "b": 3},
        {"a": 1, "b": 4},
        {"a": 2, "b": 3},
        {"a": 2, "b": 4}
    ]
    """
    keys = list(kwargs.keys())
    for values in itertools.product(*[kwargs[k] for k in keys]):
        yield dict(zip(keys, values))

def sql_script(name):
    path = os.path.join(os.path.dirname(wrenlab.__file__), "sql", "{}.sql".format(name))
    with open(path) as h:
        return h.read()

def to(t, x):
    try:
        return t(x)
    except:
        return np.nan

def as_float(x):
    return to(float, x)

def chunks(it, size=1000):
    """
    Divide an iterator into chunks of specified size. A
    chunk size of 0 means no maximum chunk size, and will
    therefore return a single list.
    
    Returns a generator of lists.
    """
    if size == 0:
        yield list(it)
    else:
        chunk = []
        for elem in it:
            chunk.append(elem)
            if len(chunk) == size:
                yield chunk
                chunk = []
        if chunk:
            yield chunk

def matrix_transformer(fn):
    """
    A decorator for a function that takes a 2D ndarray or DataFrame as
    its only positional argument (kwargs are allowed) and returns a 
    2D ndarray of the same size.

    If a :class:`pandas.DataFrame` was supplied, this decorator will
    wrap the output 2D array by adding the appropriate labels.
    """
    @functools.wraps(fn)
    def wrap(Xi, **kwargs):
        assert len(Xi.shape) == 2
        if isinstance(Xi, pd.DataFrame):
            Xim = np.array(Xi)
            Xo = fn(Xim, **kwargs)
            assert (Xi.shape == Xo.shape)
            o = pd.DataFrame(Xo, index=Xi.index, columns=Xi.columns)
            o.index.name = Xi.index.name
            o.columns.name = Xi.columns.name
            return o
        elif isinstance(Xi, np.ndarray):
            return fn(Xi, **kwargs)
        else:
            raise ValueError
    return wrap

def ignore_nan(fn):
    """
    Decorates a function applied to a 1D array or :class:`pandas.Series`.

    Applies the wrapped function to the non-nan elements and return a new array
    with the resulting array containing elements from the function and NaNs 
    elsewhere.
    """
    @functools.wraps(fn)
    def wrap(x, *args, **kwargs):
        xx = np.array(x)
        ix = ~np.isnan(xx)
        o = np.empty(xx.shape[0])
        o[:] = np.nan
        o[ix] = fn(xx[ix], *args, **kwargs)
        return o
    return wrap

ranks = ignore_nan(scipy.stats.rankdata)

def percentile(x):
    """
    Return a new 1D array with values from 0 to 1 (or NaN, 
    if values were NaN to begin with) corresponding to the
    percentile of ranked values in `x`, from smallest to 
    largest.
    """
    o = ranks(x)
    o = o / (np.nanmax(o) + 1)
    if isinstance(x, pd.Series):
        o = pd.Series(o, index=x.index)
    return o

class CaseInsensitiveDict(dict):
    def __setitem__(self, key, value):
        key = key.lower()
        dict.__setitem__(self, key, value)

    def __getitem__(self, key):
        key = key.lower()
        return dict.__getitem__(self, key)

def ignore_errors(on_error=None):
    def decorator(fn):
        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except:
                if on_error is not None:
                    return on_error(*args, **kwargs)
        return wrap
    return decorator

def _get_size(obj, seen=None):
    try:
        size = sys.getsizeof(obj)
        if seen is None:
            seen = set()
        obj_id = id(obj)
        if obj_id in seen:
            return 0
        # Important mark as seen *before* entering recursion to gracefully handle
        # self-referential objects
        seen.add(obj_id)
        if isinstance(obj, dict):
            size += sum([_get_size(v, seen) for v in obj.values()])
            size += sum([_get_size(k, seen) for k in obj.keys()])
        elif hasattr(obj, '__dict__'):
            size += get_size(obj.__dict__, seen)
        elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
            size += sum([_get_size(i, seen) for i in obj])
        return size
    except TypeError:
        return 0

def get_size(obj):
    """
    Recursively finds size of objects. May return an underestimate. 
    
    Returns 
    -------
    Object size in MB.
    """
    return _get_size(obj) / 1024 ** 2


class cwd(object):
    def __init__(self, directory):
        self._dir = directory
        self._cwd = os.getcwd()
        self._pwd = self._cwd

    @property
    def current(self):
        return self._cwd
    
    @property
    def previous(self):
        return self._pwd
    
    @property
    def relative(self):
        c = self._cwd.split(os.path.sep)
        p = self._pwd.split(os.path.sep)
        l = min(len(c), len(p))
        i = 0
        while i < l and c[i] == p[i]:
            i += 1
        return os.path.normpath(os.path.join(*(['.'] + (['..'] * (len(c) - i)) + p[i:])))
    
    def __enter__(self):
        self._pwd = self._cwd
        os.chdir(self._dir)
        self._cwd = os.getcwd()
        return self

    def __exit__(self, *args):
        os.chdir(self._pwd)
        self._cwd = self._pwd

def open_handle(path_or_handle, mode="rt"):
    """
    Given a path or file handle, return an open file handle, 
    attempting to decompress if necessary.
    """
    import magic
    import gzip
    import bz2

    if isinstance(path_or_handle, str):
        with magic.Magic() as m:
            desc = m.id_filename(path_or_handle)
            if desc.startswith("gzip"):
                return gzip.open(path_or_handle, mode=mode)
            elif desc.startswith("bzip2"):
                return bz2.open(path_or_handle, mode=mode)
            else:
                return open(path_or_handle, mode=mode)
    elif hasattr(path_or_handle, "read"):
        return path_or_handle
    else:
        raise ValueError("Input type not recognized.")

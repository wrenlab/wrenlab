"""
Python wrapper for the Aspera client (used for fast downloads from NCBI
server).
"""

# TODO: allow option to AsperaClient.download() to background the download
# TODO: allow multiple source files to be given to download() ?

import os
import platform
import sys
import subprocess as sp
import tarfile
import tempfile

import wrenlab.util

PREFIX = os.path.expanduser("~/.cache/wrenlab/aspera/")
URL = "http://download.asperasoft.com/download/sw/connect/3.6.2/aspera-connect-3.6.2.117442-linux-64.tar.gz"
INSTALL_SH = "aspera-connect-3.6.2.117442-linux-64.sh"

def ensure():
    assert platform.system() == "Linux"
    assert platform.machine() == "x86_64"

    if os.path.exists(os.path.join(PREFIX, "bin", "ascp")):
        return
    os.makedirs(PREFIX, exist_ok=True)

    path = str(wrenlab.util.download(URL))
    with tarfile.open(path, mode="r:gz") as archive:
        with archive.extractfile(INSTALL_SH) as h:
            data = h.read().replace(b"~/.aspera/connect", PREFIX.encode("ascii"))
        with tempfile.NamedTemporaryFile(mode="wb") as exe:
            exe.write(data)
            sp.check_call(["bash", exe.name])

class Error(IOError):
    def __init__(self, *args, **kwargs):
        super(Error, self).__init__(*args, **kwargs)

class Client(object):
    def __init__(self, 
            user="anonftp", 
            host="ftp-private.ncbi.nlm.nih.gov", 
            limit=200, # in MB
            key_file=os.path.join(PREFIX, "etc", "asperaweb_id_dsa.openssh")):

        assert isinstance(limit, int)
        ensure()

        self._binary_path = os.path.join(PREFIX, "bin", "ascp")
        if not self._binary_path:
            raise Error("Aspera client binary (ascp) could not be found.")

        self._user = user
        self._host = host
        self._limit = limit
        self._key_file = os.path.expanduser(key_file)

    def download(self, source, destination, decompress=None):
        """
        Download a single file from this Aspera server,
        returning an open file handle to the resulting file.

        This handle will delete the underlying temporary file when it is
        closed, so use shutil.copyfile or similar to persist the data.
        """
        assert decompress is None, "Decompression not implemented."

        source_uri = "%s@%s:/%s" % (self._user, self._host, source.lstrip("/"))
        #handle = tempfile.NamedTemporaryFile(delete=True)
        #destination = handle.name
        args = [self._binary_path, 
                "-T",
                "-i", self._key_file, 
                "--apply-local-docroot",
                "-l", "%sM" % self._limit,
                source_uri, destination]
        p = sp.Popen(args, stdin=sp.PIPE, stdout=sp.DEVNULL, stderr=sp.PIPE)
        p.wait()
        if p.returncode:
            msg = p.stderr.read().decode("utf-8").strip()
            raise Error(msg)
        #return handle

import sqlite3

import pandas as pd

class SQLiteDB(sqlite3.Connection):
    def __init__(self, path):
        super(SQLiteDB, self).__init__("file:///{}?mode=ro".format(path), uri=True)

    def __del__(self):
        self.close()

    def table(self, name):
        return self.query("SELECT * FROM {};".format(name))

    def query(self, sql):
        c = self.cursor()
        c.execute(sql)
        columns = [x[0] for x in c.description]
        o = pd.DataFrame.from_records(list(c), columns=columns)
        c.close()
        if len(columns) == 1:
            o = o.iloc[:,0]
        return o

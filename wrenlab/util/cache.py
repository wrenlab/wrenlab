import sys
import pickle
import functools
import fnmatch
from abc import ABC, abstractmethod

import lz4
import redis
import leveldb
import joblib.func_inspect
import joblib.hashing

import wrenlab.ext.leveldb

def hash_function(fn):
    hasher = joblib.hashing.Hasher()
    func_code, source_file, first_line = joblib.func_inspect.get_func_code(fn)
    return hasher.hash(func_code)

def hash_arguments(*args, **kwargs):
    hasher = joblib.hashing.Hasher()
    return hasher.hash((args, frozenset(kwargs)))

class MemoizedFunction(object):
    def __init__(self, cache, fn):
        self._cache = cache
        self._fn = fn
        self._fn_name = ".".join([str(fn.__module__), fn.__name__])

    def check(self):
        fn_hash_key = "rcache:{}:hash".format(self._fn_name)
        try:
            fn_hash_checked_value = self._cache.get_object(fn_hash_key)
            has_changed = fn_hash_checked_value != hash_function(self._fn)
        except KeyError:
            has_changed = False
        if has_changed:
            self.clear()
        self._cache.put_object(fn_hash_key, fn_hash)

    def clear(self):
        n = self._cache.clear(pattern="rcache:{}:*".format(self._fn_name))
        print(n, "entries deleted", file=sys.stderr)

    def __call__(self, *args, **kwargs):
        arg_hash = hash_arguments(*args, **kwargs)
        key = ":".join(["rcache", self._fn_name, str(arg_hash)])
        try:
            return self._cache.get_object(key)
        except KeyError:
            print("cache miss:", key, file=sys.stderr)
            value = self._fn(*args, **kwargs)
            self._cache.put_object(key, value)
            return value

class Cache(ABC):
    def __init__(self, compress=False):
        self._compress = compress
        self._is_open = True

    def __del__(self):
        self.close()

    def _encode_object(self, obj):
        o = pickle.dumps(obj)
        if self._compress is True:
            o = lz4.block.compress(value)
        return o

    def _decode_object(self, data):
        o = data
        if self._compress is True:
            o = lz4.block.decompress(o)
        return pickle.loads(o)

    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, value):
        self.put(key, value)

    def get_object(self, key):
        data = self.get(key)
        if data is None:
            raise KeyError
        return self._decode_object(data)

    def put_object(self, key, value):
        data = self._encode_object(value)
        self.put(key, data)

    def cache_function(self, fn):
        return MemoizedFunction(self, fn)

    def search(self, pattern):
        """
        Search keys for a particular pattern, returning an iterator of matching keys.
        """
        for key in self:
            if fnmatch.fnmatch(key, pattern):
                yield key

    def clear(self, pattern=None):
        """
        Delete all data in this cache whose keys match `pattern`,
        or, if `pattern` is None, delete all data.

        Returns the number of entries deleted.
        """
        pattern = pattern or "*"
        n = 0
        for k in self.search(pattern):
            n += 1
            self.delete(k)
        return n

    def __del__(self):
        try:
            self.close()
        except:
            pass

    def __len__(self):
        n = 0
        for k in self:
            n += 1
        return n

    def __call__(self, fn):
        return self.cache_function(fn)

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def __iter__(self):
        """
        Iterate through keys in this cache.
        """
        pass

    @abstractmethod
    def put(self, key, value):
        pass

    @abstractmethod
    def get(self, key):
        pass

    @abstractmethod
    def delete(self, key):
        """
        Delete data associated with a single key.
        """
        pass

class LevelDBCache(Cache):
    """
    Superclass for embedded and client-server LevelDB cache.
    
    N.B.: LevelDB expects bytes keys and values.
    """
    def _encode_key(self, key):
        return key.encode("utf-8")

    def _decode_key(self, bkey):
        return bkey.decode("utf-8")

class LocalLevelDBCache(LevelDBCache):
    # Not sure if this can be accessed by multiple processes simultaneously

    def __init__(self, path):
        self._cx = leveldb.LevelDB(path)
        super().__init__(compress=False)

    def close(self):
        pass

    def __iter__(self):
        # FIXME: API does not allow only iterating keys, 
        # so `__iter__` will be very inefficient
        for k,v in self._cx.RangeIter():
            yield self._decode_key(k)

    def put(self, key, value):
        bkey = self._encode_key(key)
        self._cx.Put(bkey, value)

    def get(self, key):
        bkey = self._encode_key(key)
        return self._cx.Get(bkey)

    def delete(self, key):
        bkey = self._encode_key(key)
        self._cx.Delete(bkey)

class RemoteLevelDBCache(LevelDBCache):
    def __init__(self, host):
        self._cx = wrenlab.ext.leveldb.Client(host)
        super().__init__(compress=False)

    def close(self):
        self._cx.close()

    def __iter__(self):
        # FIXME: API does not allow only iterating keys, 
        # so `__iter__` will be very inefficient
        for k in self._cx.keys():
            yield self._decode_key(k)

    def put(self, key, value):
        bkey = self._encode_key(key)
        self._cx.put(bkey, value)

    def get(self, key):
        bkey = self._encode_key(key)
        return self._cx.get(bkey)

    def delete(self, key):
        bkey = self._encode_key(key)
        self._cx.delete(bkey)


class RedisCache(object):
    def __init__(self, db, host="localhost", port=6379):
        raise NotImplementedError
        self._cx = redis.StrictRedis(host=host, db=db, port=port)
        super().__init__(compress=True)

    def __call__(self, fn):
        return self.cache(fn)

    def clear_all(self):
        n = 0
        for key in self._cx.scan_iter("rcache:*"):
            self._cx.delete(key)
            n += 1
        print(n, "entries deleted", file=sys.stderr)

    def _encode_value(self, value):
        return lz4.block.compress(pickle.dumps(value))

    def _decode_value(self, value):
        return pickle.loads(lz4.block.decompress(value))

    def cache(self, fn):
        fn_name = ".".join([str(fn.__module__), fn.__name__])
        fn_hash = hash(fn.__code__)

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            arg_hash = hash_arguments(*args, **kwargs)
            key = ":".join(["rcache", fn_name, str(arg_hash)])
            value = self._cx.get(key)
            if value is not None:
                return self._decode_value(value)
            print("cache miss:", key, file=sys.stderr)
            value = fn(*args, **kwargs)
            self._cx.set(key, self._encode_value(value))
            return value

        def clear():
            n = 0
            for key in self._cx.scan_iter("rcache:{}:*".format(fn_name)):
                self._cx.delete(key)
                n += 1
            print(n, "entries deleted", file=sys.stderr)
        wrap.clear = clear

        fn_hash_key = "rcache:{}:hash".format(fn_name)
        try:
            fn_hash_checked_value = int(self._cx.get(fn_hash_key))
        except TypeError:
            fn_hash_checked_value = None

        if fn_hash != fn_hash_checked_value:
            print("function code has changed, clearing cache", fn_name, file=sys.stderr)
            wrap.clear()
        self._cx.set(fn_hash_key, fn_hash)

        return wrap

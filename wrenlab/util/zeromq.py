"""
Abstract classes/middleware for ZeroMQ client/server applications.

TODO: extend classes to allow for generator-type responses (?)
TODO: add a server_op decorator to mark valid server ops
"""

import traceback
import time
import pickle
import threading
import functools
import collections
from abc import ABC, abstractmethod

import zmq
import lz4

from wrenlab.util import LOG

def encode_message(x):
    return lz4.block.compress(pickle.dumps(x))

def decode_message(x):
    if len(x) == 0:
        return None
    return pickle.loads(lz4.block.decompress(x))

class ZMQException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

class MethodNotFoundException(ZMQException):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

def client_op(name=None):
    def wrap(fn):
        method_name = name or fn.__name__.encode("ascii")

        @functools.wraps(fn)
        def wrapped(self, *args, **kwargs):
            msg = encode_message((args, kwargs))
            self.socket.send_multipart([method_name, msg])
            reply = self.socket.recv_multipart()[0]
            o = decode_message(reply)
            if isinstance(o, Exception):
                raise o
            return o
        return wrapped
    return wrap

def server_op():
    def wrap(fn):
        fn._is_server_op = True
        return fn
    return wrap

class ZMQClient(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.scheme = "tcp://{host}:{port}".format(host=host, port=port)
        self.connect()

    def connect(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.XREQ)
        self.socket.connect(self.scheme)

    def close(self):
        raise NotImplementedError

class ZMQWorker(ABC, threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def initialize(self, context):
        self.context = context
        self.running = True
        self.processing = False
        self.socket = self.context.socket(zmq.XREQ)

    def run(self):
        self.socket.connect('inproc://backend')
        while self.running:
            try:
                msg = self.socket.recv_multipart()
            except zmq.ZMQError:
                self.running = False
                continue

            self.processing = True
            id = msg[0]
            if len(msg) != 3:
                value = ZMQException("Invalid protocol usage.")
                reply = [msg[0], encode_message(value)]
            else:
                try:
                    op = msg[1]
                    args, kwargs = decode_message(msg[2])
                    reply = [id, self._call_op(op, *args, **kwargs)]
                except Exception as e:
                    traceback.print_exc()
                    reply = [id, encode_message(e)]

            self.socket.send_multipart(reply)
            self.processing = False

    def close(self):
        self.running = False
        while self.processing:
            time.sleep(1)
        self.socket.close()

    def _get_op(self, op):
        op = op.decode("ascii")
        if not hasattr(self, op):
            raise MethodNotFoundException(op)
        fn = getattr(self, op)
        if fn.__dict__.get("_is_server_op") is not True:
            if not hasattr(self, "METHODS") and (op in self.METHODS):
                raise MethodNotFoundException(op)
        return fn

    def _call_op(self, op, *args, **kwargs):
        fn = self._get_op(op)
        o = fn(*args, **kwargs)
        return encode_message(o)

class ZMQServer(ABC):
    def __init__(self, host, port, worker_threads=3):
        self.host = host
        self.port = port
        self.scheme = "tcp://{host}:{port}".format(host=host, port=port)

        self.context = zmq.Context()
        self.frontend = self.context.socket(zmq.XREP)
        self.frontend.bind(self.scheme)
        self.backend = self.context.socket(zmq.XREQ)
        self.backend.bind("inproc://backend")

        self.poll = zmq.Poller()
        self.poll.register(self.frontend, zmq.POLLIN)
        self.poll.register(self.backend, zmq.POLLIN)

        self.worker_threads = worker_threads

        self.workers = []
        for i in range(worker_threads):
            worker = self.create_worker()
            worker.initialize(self.context)
            worker.start()
            self.workers.append(worker)
        
    @abstractmethod
    def create_worker(self):
        pass

    def serve(self):
        LOG.info("Starting ZMQ server on {scheme}".format(scheme=self.scheme))
        try:
            while True:
                sockets = dict(self.poll.poll())
                if self.frontend in sockets:
                    if sockets[self.frontend] == zmq.POLLIN:
                        msg = self.frontend.recv_multipart()
                        self.backend.send_multipart(msg)
                        
                if self.backend in sockets:
                    if sockets[self.backend] == zmq.POLLIN:
                        msg = self.backend.recv_multipart()
                        self.frontend.send_multipart(msg)
        except KeyboardInterrupt:
            for worker in self.workers:
                worker.close()
            self.frontend.close()
            self.backend.close()
            self.context.term()

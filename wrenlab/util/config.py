import os

import yaml

def get_configuration():
    path = os.path.expanduser("~/.wrenlab/config.yml")
    if os.path.exists(path):
        with open(path) as h:
            return yaml.load(h)
    return {}

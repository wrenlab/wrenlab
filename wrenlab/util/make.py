import collections
import functools
import os

import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

# FIXME: make these not global

DPI = 180
IMAGE_PREFIX = "img/"
IMAGE_EXTENSION = "pdf"
TABLE_PREFIX = "tbl/"
TABLE_EXTENSION = "tex"
FIGURE_FNS = {}
TABLE_FNS = {}

#matplotlib.rc("savefig", dpi=DPI)
#sns.set_context("paper", font_scale=1.5)

def _fn_full_name(fn):
    #name = fn.__name__.replace("_", "-")
    prefix = "".join(list(map(lambda x: x[0].upper(), fn.__module__.split(".")[1:])))
    name = "-".join([prefix, fn.__name__]).replace("_", "-").lstrip("-")
    return name

class Target(object):
    def __init__(self):
        pass


class Make(object):
    """
    Allows the definition of tasks within a project structure to auto(re)generate 
    figures, tables, etc.
    """
    def __init__(self):
        self._definitions = collections.OrderedDict()

    def define(self, key, value):
        assert key.isalnum()
        self._definitions[key] = value

    def definition(self, fn):
        # FIXME: make lazy
        #name = _fn_full_name(fn)
        name = fn.__name__
        self._definitions[name] = fn()

    def figure(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(IMAGE_PREFIX, "{}.{}".format(name, IMAGE_EXTENSION))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            plt.clf()
            if not os.path.exists(path):
                fn(*args, **kwargs)
                plt.savefig(path, bbox_inches="tight")

        FIGURE_FNS[name] = wrap
        return wrap

    def table(self, fn):
        name = _fn_full_name(fn)
        path = os.path.join(TABLE_PREFIX, "{}.{}".format(name, TABLE_EXTENSION))

        @functools.wraps(fn)
        def wrap(*args, **kwargs):
            if not os.path.exists(path):
                df = fn(*args, **kwargs)
                with open(path, "w") as h:
                    df.to_latex(buf=h, index=False)

        TABLE_FNS[name] = wrap
        return wrap

    def generate(self):
        print("Generating figures ...")
        os.makedirs(IMAGE_PREFIX, exist_ok=True)
        for k,fn in sorted(FIGURE_FNS.items()):
            print("--", k)
            fn()

        print("Generating tables ...")
        os.makedirs(TABLE_PREFIX, exist_ok=True)
        for k,fn in sorted(TABLE_FNS.items()):
            print("--", k)
            fn()

        if len(self._definitions) > 0:
            print("Generating latex definitions ...")
            with open("defines.tex", "w") as h:
                for k,v in self._definitions.items():
                    o = "\\newcommand{\\" + str(k) + r"}{" + str(v) + "}"
                    print(o, file=h)
                    #print("\\newcommand\{\\{}\}\{{}\}".format(k, v), file=h)

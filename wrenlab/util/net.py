"""
Network helper functions.
"""

__all__ = ["download", "FTPClient"]

import base64
import contextlib
import datetime
import ftplib
import http.client
import io
import os
import os.path
import pathlib
import socket
import time
import urllib.request
import urllib.parse
import zlib

from .log import LOG

def _decompress_stream(input_stream, output_stream):
    """
    Decompress the contents of input_stream to output_stream (assumes GZip format).
    """
    if not isinstance(input_stream, http.client.HTTPResponse):
        assert "b" in input_stream.mode
    assert "b" in output_stream.mode

    BLOCK_SIZE = 4096
    dc = zlib.decompressobj(16+zlib.MAX_WBITS)
    while True:
        data = input_stream.read(BLOCK_SIZE)
        if not data:
            break
        output_stream.write(dc.decompress(data))

def download(url, decompress=False, expire=None, timeout=None, save_on_failure=False):
    """
    Download a file over HTTP or FTP, cache it locally, and return a path to it.

    Arguments
    ---------
    url : str
        The URL to download and cache.
    decompress : bool
        Whether to decompress the file before saving it (assumes gzip format).
    expire : numeric or None
        Expire the cache and redownload the file after this many days, 
        If this parameter is None, never expire.
        If this parameter is True, force expiration and redownload.
    timeout : int, default None
        Timeout value, in seconds.
    save_on_failure : bool, default False
        If True, save an empty file to prevent repeated attempts to download
        the file.

    Returns
    -------
    A :class:`pathlib.Path` object representing the path to the downloaded file
    on the file system.
    """
    cache = os.path.join(os.path.expanduser("~/.cache/wrenlab/download/"))
    os.makedirs(cache, exist_ok=True)

    target_ascii = url.lower()
    if decompress:
        target_ascii += "-dc"
    #base64.b64encode(target_ascii.encode("utf-8")).decode("utf-8")
    target = base64.urlsafe_b64encode(target_ascii.encode("utf-8")).decode("utf-8") 
    path = os.path.join(cache, target)

    expired = False

    if expire is True:
        expired = True
    elif (expire is not None) and os.path.exists(path):
        last_modified = datetime.datetime.fromtimestamp(os.stat(path).st_mtime)
        current = datetime.datetime.fromtimestamp(time.time())
        delta = (last_modified - current).days
        if delta >= expire:
            LOG.info("Download cache expired (last modified {} days ago)".format(delta))
            expired = True
    else:
        pass


    if expired or not os.path.exists(path):
        LOG.info("Downloading URL: {}".format(url))
        p = urllib.parse.urlparse(url)

        try:
            #if p.scheme == "ftp":
            #    cx = FTPClient(p.netloc)
            #    h = cx.download(p.path)
            #else:
            #    h = urllib.request.urlopen(url)
            request = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
            h = urllib.request.urlopen(request)

            with open(path, "wb") as o:
                if decompress:
                    LOG.debug("Decompressing stream (decompress = True)")
                    _decompress_stream(h, o)
                else:
                    while True:
                        data = h.read(4096)
                        if len(data) == 0:
                            break
                        o.write(data)
            h.close()
            LOG.debug("URL successfully saved to: {}".format(path))
        except:
            LOG.debug("Failed to download URL: {}".format(url))
            if os.path.exists(path):
                os.unlink(path)
            if save_on_failure:
                pathlib.Path(path).touch()
                LOG.debug("(saved failure as empty file)")
            raise
    return pathlib.Path(path)

class FTPClient(object):
    def __init__(self, host, port=None, user="anonymous", password="", debug_level=2):
        self._host = host
        self._port = port
        self._user = user
        self._password = password

        self._cx = ftplib.FTP()
        self._cx.connect(self._host)
        self._cx.login(self._user, self._password)

        self._cx.sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        self._cx.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 75)
        self._cx.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 60)
        self._cx.set_debuglevel(debug_level)

        # This is necessary for ftp://ailun.ucsf.edu/
        # Not sure if it has effects outside of that, I think the default is usually True
        # in wget, etc
        self._cx.set_pasv(False)

        self._is_open = True

    def __del__(self):
        self.close()

    def close(self):
        if self._is_open:
            self._cx.close()

    def ls(self, path):
        assert self._is_open
        return self._cx.nlst(path)

    def download(self, path):
        assert self._is_open

        h = io.BytesIO()
        directory = os.path.dirname(path)
        fname = os.path.basename(path)
        self._cx.cwd(directory)
        self._cx.retrbinary("RETR {}".format(fname), h.write)

        h.seek(0)
        return h

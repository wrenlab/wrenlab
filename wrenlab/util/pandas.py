"""
Pandas helper functions.
"""

import random
import collections
import itertools
import sys
import multiprocessing as mp
import functools

from joblib import Parallel, delayed
import pandas as pd
import numpy as np
import tqdm

from wrenlab.util import LOG

def remove_unused_categories(df):
    """
    For all columns in `df` with dtype `category`, remove unused categorical
    levels from that column (i.e., categories with 0 occurrences).
    """
    df = df.copy()
    for c in df.columns:
        if str(df[c].dtype) == "category":
            df[c].cat.remove_unused_categories(inplace=True)
    return df

def dual_align(X,Y):
    """
    Align two :class:`pandas.DataFrame` objects on both axes (inner join).
    """
    X,Y = X.align(Y, axis=0, join="inner")
    return X.align(Y, axis=1, join="inner")

def parameter_product(**kwargs):
    """
    Each kwarg is a sequence. Returns an iterable of dictionaries 
    over all possible combinations of the kwargs.

    Example
    -------
    >> list(parameter_product(a=[1,2], b=[3,4]))
    [
        {"a": 1, "b": 3},
        {"a": 1, "b": 4},
        {"a": 2, "b": 3},
        {"a": 2, "b": 4}
    ]
    """
    keys = list(kwargs.keys())
    for values in itertools.product(*[kwargs[k] for k in keys]):
        yield dict(zip(keys, values))

def apply_parameters(fn, progress=False, verbose=False, ignore_errors=False, parameters={}):
    """
    Each element of `parameters` is a sequence. Applies the function
    with all possible combinations of the elements in the kwargs sequences, and
    returns a multi-indexed :class:`pandas.DataFrame` as a result.

    It assumes that `fn` returns a dictionary.

    Returns
    -------
    A :class:`pandas.DataFrame` with a multi-index containing each of the
    sequential input parameters. Additionally, it has a `constants`
    attribute that has a dictionary of the nonvarying parameters.
    """
    # FIXME: parallel

    keys = list(parameters.keys())
    sequential_keys = []
    def is_sequential(v):
        # FIXME: exclude np and pd objects
        return isinstance(v, collections.Iterable)\
                and not isinstance(v, str)\
                and not isinstance(v, dict)\
                and not hasattr(v, "shape")

    count = 1
    for k in keys:
        if is_sequential(parameters[k]):
            sequential_keys.append(k)
            count *= len(parameters[k])
        else:
            parameters[k] = [parameters[k]]

    def jobs():
        for params in parameter_product(**parameters):
            msg = "Running function `{}.{}` with parameters: {}"\
                .format(fn.__module__, fn.__name__,
                        {k:v for k,v in params.items()
                            if not isinstance(v, pd.DataFrame)})
            LOG.info(msg)
            if ignore_errors is True:
                try:
                    rs = fn(**params)
                except:
                    rs = None
            else:
                rs = fn(**params)
            yield params, rs

    it = filter(lambda kv: kv[1] is not None, jobs())
    first = next(it)
    it = itertools.chain([first], it)
    ix = []
    columns = first[1].keys()
    o = {c:[] for c in columns}

    if progress is True:
        it = tqdm.tqdm(it, total=count)

    for params, rs in it:
        ix.append(tuple([params[k] for k in sequential_keys]))
        for k,v in rs.items():
            o[k].append(v)

    ix = pd.MultiIndex.from_tuples(ix, names=sequential_keys)
    o = pd.DataFrame(o, index=ix)
    o.constants = {k:parameters[k] for k in keys if not k in sequential_keys}
    return o

def random_submatrix(X, shape):
    """
    Return a random submatrix of the given shape.
    """
    random.seed(0)
    nr = shape[0] if shape[0] not in (None, -1) else X.shape[0]
    nc = shape[1] if shape[1] not in (None, -1) else X.shape[1]

    ix = random.sample(list(X.index), nr)
    cx = random.sample(list(X.columns), nc)
    return X.loc[ix,:].loc[:,cx]

def replace_infinity(fn):
    """
    Decorator for functions that take a :class:`pandas.DataFrame` as
    the first argument. Replaces +/-inf with NaN, then returns the result.
    """
    @functools.wraps(fn)
    def wrap(X, *args, **kwargs):
        X = X.replace([np.inf, -np.inf], np.nan)
        return fn(X, *args, **kwargs)
    return wrap

@replace_infinity
def trim_missing(X, threshold=10, row_threshold=None, column_threshold=None):
    """
    Iteratively remove rows and columns of X until all rows and columns
    have `threshold` non-null values.

    Arguments
    ---------
    X : :class:`pandas.DataFrame`
    threshold : int or float
        Acts the same as `thresh` parameter in `pandas.DataFrame.dropna`.
    row_threshold: int or float, optional
        If provided, overrides `threshold` for rows.
    column_threshold: int or float, optional
        If provided, overrides `threshold` for columns.

    Returns
    -------
    A :class:`pandas.DataFrame`, with a number of rows and columns equal
    to or less than the number in the original :class:`pandas.DataFrame`.
    """
    X = X.dropna(how="all", axis=0)
    X = X.dropna(how="all", axis=1)

    row_threshold = row_threshold or threshold
    column_threshold = column_threshold or threshold
    shape = X.shape

    def t(n,threshold):
        if isinstance(threshold, float):
            return int(n * threshold)
        return threshold

    while True:
        X = X.dropna(thresh=t(X.shape[0], column_threshold), axis=1)
        X = X.dropna(thresh=t(X.shape[1], row_threshold), axis=0)

        if (X.shape[0] == 0) or (X.shape[1] == 0):
            msg = "All rows or columns dropped during trimming. Try lowering `threshold`."
            raise ValueError(msg)
        if shape == X.shape:
            break
        shape = X.shape
    return X

def add_missing(X, ratio=0.1):
    """
    Given a :class:`pandas.DataFrame`, `X`, return a new 
    :class:`pandas.DataFrame` with `ratio` randomly selected values
    replaced with NaN.

    Returns
    -------
    A 2-tuple of:

    mask : a boolean array with the indices of locations replaced with NaN
        (locations that were already NaN will NOT be included)
    X : the :class:`pandas.DataFrame` with missing values added.
    """
    np.random.seed(0)
    Xm = np.array(X)
    mask = np.isnan(X)
    ix = np.random.random(X.shape) < ratio
    Xm[ix] = np.nan
    return (ix & ~mask), pd.DataFrame(Xm, index=X.index, columns=X.columns)

def papply(X, fn, axis=0, n_jobs=mp.cpu_count() - 2, **kwargs):
    """
    Apply a function in parallel across the rows or columns of a
    :class:`pandas.DataFrame`, returning a new DataFrame of the
    same shape.

    Arguments
    =========
    X : :class:`pandas.DataFrame`
    fn : :class:`function`
        The function to apply. Must be pickleable. 
        Has signature fn(series) -> series.
    axis : int, (0,1)
        The axis to apply along. If axis == 0, apply function to each column,
        or to each row if axis is 1.
    """
    assert axis in (0,1)
    pool = mp.Pool(processes=n_jobs)
    try:
        if axis == 0:
            o = list(pool.map(functools.partial(fn, **kwargs), 
                (X.iloc[:,i] for i in range(X.shape[1]))))
            o = np.column_stack(o)
        else:
            o = list(pool.map(functools.partial(fn, **kwargs), 
                (X.iloc[i,:] for i in range(X.shape[0]))))
            o = np.row_stack(o)
        o = pd.DataFrame(o, index=X.index, columns=X.columns)
        return o
    finally:
        pool.terminate()

def align_series(x, y, dropna=True):
    """
    Align two :class:`pandas.Series` using an inner join of their indices.

    Arguments
    =========
    x, y : :class:`pandas.Series`
    dropna : bool, optional
        Whether to drop missing elements from both :class:`pandas.Series`
        objects before performing the join.

    Returns
    =======
    A 2-tuple of :class:`pandas.Series`, the aligned versions of x and y.

    """
    assert isinstance(x, pd.Series)
    assert isinstance(y, pd.Series)
    if dropna is True:
        x = x.dropna()
        y = y.dropna()

    ix = list(set(x.index) & set(y.index))
    ox, oy = x.loc[ix], y.loc[ix]
    assert len(ox) == len(oy), "Duplicate entries in index"
    return ox, oy

def sort_absolute_value(v, column=None, ascending=True):
    """
    Sort a :class:`pandas.Series` or a :class:`pandas.DataFrame` by
    absolute value (in the latter case, by the absolute value of a given column).

    Arguments
    =========
    v : :class:`pandas.Series` or :class:`pandas.DataFrame`
    column : str, optional
        If a :class:`pandas.DataFrame` is provided, sort by this column.
        Ignored if v is a :class:`pandas.Series`.
    ascending : bool
        Whether to sort ascending (default True).
    """
    assert isinstance(v, pd.Series) or isinstance(v, pd.DataFrame)

    if column is not None:
        df = v
        ix = sort_absolute_value(df[column], ascending=ascending).index
        return df.loc[ix,:]
    else:
        return v.loc[v.abs().sort_values(ascending=ascending).index]

def read_matrix(handle, sep="\t"):
    """
    Read a TSV matrix, returning an iterable of :class:`pandas.Series`.
    """
    chunks = iter(pd.read_csv(handle, sep=sep, chunksize=100, index_col=0))
    first = next(chunks)
    chunks = itertools.chain([first], chunks)

    for chunk in chunks:
        for i in range(chunk.shape[0]):
            o = chunk.iloc[i,:]
            o.name = chunk.index[i]
            yield o

def write_matrix(handle, rows, sep="\t"):
    """
    Write an iterable of :class:`pandas.Series` to a TSV file.
    """
    rows = iter(rows)
    first = next(rows)
    rows = itertools.chain([first], rows)

    print("", *first.index, sep=sep, file=handle)

    try:
        for row in rows:
            print(row.name, *row, sep=sep, file=handle)
    except BrokenPipeError:
        pass

class StreamingDataFrame(object):
    """
    An object wrapping an iterable of :class:`pandas.Series` that can be
    lazily processed or output, or coerced to a full 
    :class:`pandas.DataFrame`.
    """
    def __init__(self, iterator):
        iterator = iter(iterator)
        first = next(iterator)
        assert isinstance(first, pd.Series)
        self._columns = list(first.index)
        self._it = itertools.chain([first], iterator)

    @staticmethod
    def open(path, delimiter="\t"):
        def iterator():
            chunks = pd.read_csv(path, chunksize=100, sep=delimiter, index_col=0)
            for chunk in chunks:
                for i in range(chunk.shape[0]):
                    yield chunk.iloc[i,:]
        return StreamingDataFrame(iterator())

    def __iter__(self):
        """
        Return the underlying iterator of :class:`pandas.Series`. Note, if elements 
        are consumed by the caller, they will also be consumed from this object. 
        """
        return self._it

    def __next__(self):
        return next(self._it)

    def dump(self, handle=sys.stdout, sep="\t", round=3, header=True, index=True):
        """
        Lazily print rows to the provided handle.

        Arguments
        ---------
        handle : file
            A file-like object to print output to.
        sep : str
            Field delimiter.
        header : bool
            Whether to print column names.
        index : bool
            Whether to print row names.
        round : int
            Round to this number of decimal places.
        """
        try:
            if header:
                columns = []
                if index:
                    columns.append("")
                columns.extend(self._columns)
                print(*columns, sep="\t", file=handle)

            for row in self._it:
                if index:
                    print(row.name, end=sep, file=handle)
                row = row.round(round)
                print(*row, sep=sep, file=handle)
        except BrokenPipeError:
            pass

    def head(self, n=10):
        assert n >= 1
        rows = list(itertools.islice(self._it, n))
        self._it = itertools.chain(rows, self._it)
        return pd.DataFrame(rows) 

    def to_frame(self):
        return pd.DataFrame(list(self._it))

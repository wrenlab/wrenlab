import logging

logging.basicConfig()
LOG = logging.getLogger("wrenlab")
LOG.setLevel(logging.DEBUG)

"""
Methods for collapsing matrices.
"""

import sys
import multiprocessing as mp
import itertools
import functools

import numpy as np
import pandas as pd
import matplotlib.mlab
import sklearn.decomposition
import tqdm

import wrenlab.impute
import wrenlab.statistics
from wrenlab.util import LOG, pmap

METHODS = set()

def collapse_method(fn):
    # NB: X is pd.DataFrame with rows = probes
    METHODS.add(fn.__name__)

    @functools.wraps(fn)
    def wrap(args, **kwargs):
        key, X = args
        if X.shape[0] == 1:
            o = X.iloc[0,:]
        else:
            ix = ~(np.isinf(X) | np.isnan(X)).any()
            if ix.sum() < 3:
                return key, None
            o = np.empty(X.shape[1])
            o[:] = np.nan
            if np.isnan(X).sum().sum() > 0:
                Xi = wrenlab.impute.mean(X.T).T
            else:
                Xi = X
            Xq = Xi.ix[:,ix]
            o[np.where(ix)] = fn(Xq, **kwargs)
            o = pd.Series(o, index=X.columns)
        return key, o
    return wrap

@collapse_method
def KernelPCA(X, **kwargs):
    model = sklearn.decomposition.KernelPCA(**kwargs)
    return model.fit_transform(X.T)[:,0]

@collapse_method
def pc0(X):
    """
    Collapse each subset of rows into a single row by taking PC0 from PCA.

    For now, imputes missing values, then runs the PCA. Probably there is
    a way to do both in the same step.

    References
    ==========
    - http://coewww.rutgers.edu/riul/research/tutorials/tutorialrpca.pdf
    """
    return matplotlib.mlab.PCA(X.T).Y[:,0]

@collapse_method
def max_mean(X):
    ix = X.T.mean().sort_values(ascending=False).index[0]
    return X.loc[ix,:]

@collapse_method
def mean(X):
    return X.mean()

def collapse_subsets(subsets, handle=None, fn=pc0, ncpu=1):
    """
    Arguments
    =========
    subsets : an iterable of (key, X) tuples, where:
        - X is a :class:`pandas.DataFrame` of shape (M,N) 
          which is the submatrix of rows to be collapsed
        - key is the row name for the collapsed rows in `X`

    fn : a function for collapsing each individual subset

    handle : writable file handle, optional
        If provided, the output matrix will be written to this
        handle rather than being returned, preventing the entire
        matrix from being held in RAM.

    ncpu : int, default 1
        If provided and greater than 1, collapse in parallel with
        this number of jobs. If ncpu == -1, then use 
        (mp.cpu_count() - 2).

    Returns
    =======
    If `handle` is None, return a :class:`pandas.DataFrame` of shape (P,N),
    containing the reduced values of `X`, where N is the number of columns in
    each submatrix (must all be the same), and P is the number of valid subsets
    in the iterable. 

    If `handle` is provided, returns None.
    """
    key, first = next(iter(subsets))
    subsets = itertools.chain([(key, first)], subsets)
    columns = list(sorted(set(first.columns)))
    o = {}

    ncpu = ncpu if ncpu != -1 else (mp.cpu_count() - 2)
    parallel = ncpu != 1

    if handle is not None:
        print("", *columns, sep="\t", file=handle)

    for rs in pmap(fn, subsets, ncpu=ncpu):
        if rs is None:
            continue
        key, x = rs
        x = x.loc[columns]

        if handle is None:
            o[key] = x
        else:
            print(key, *x, sep="\t", file=handle)
        del x

    if handle is None:
        o = pd.DataFrame(o)
        o.index = columns
        return o.T

def collapse(X, mapping, method, verbose=False):
    """
    Collapse a matrix by a mapping.

    X : :class:`pandas.DataFrame`
        The data matrix, with probes on columns.
    mapping : dict
        Mapping of probe ID -> gene ID (or similar).
    method : str
        Name of the method to use (one of wrenlab.collapse.METHODS).
    """
    assert method in METHODS

    def jobs():
        it = tqdm.tqdm(X.groupby(mapping, axis=1))
        LOG.info("Collapsing matrix of shape: {}".format(X.shape))
        for key,df in it:
            yield key,df.T

    collapse_fn = getattr(wrenlab.collapse, method)
    o = wrenlab.collapse.collapse_subsets(jobs(), fn=collapse_fn)
    try:
        o.index = list(map(int, o.index))
    except ValueError:
        pass
    return o.T

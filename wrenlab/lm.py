"""
Functions for linear models, with an emphasis on the multivariate problem 
(multiple dependent variables).

Usage
-----

The entry points of this module are each different methods for fitting
multivariate models, such as "ols", "elastic_net", etc.

Each of the different models has a similar interface:

def fn(Y, design, formula=None, **kwargs) -> MFit

Each fits a linear model for each column in `Y` against `design`, returning
a `MFit` object. The **kwargs are model-specific tuning parameters.
"""

import collections
import re

import patsy
import scipy.stats
import pandas as pd
import numpy as np
import statsmodels.api as sm
import sklearn.linear_model
import tqdm

import wrenlab.enrichment
from wrenlab.util import LOG

P_CATEGORICAL_TERM = re.compile("(C\()?(?P<term>(.+?))\)?\[(T\.)?(?P<category>(.+?))\]")

def parse_term(text):
    term, category = text
    if (term, category) == (None, None):
        return text
    else:
        return term, category

def parse_categorical(text):
    m = P_CATEGORICAL_TERM.match(text)
    if m is None:
        return text, None
    term = m.group("term")
    if len(term.split(",")) > 1:
        term = term.split(",")[0]
    category = m.group("category")
    try:
        category = int(category)
    except ValueError:
        pass
    return term, category

class Term(object):
    def __init__(self, name, data):
        self._data = data
        self._name = name

    @property
    def name(self):
        return self._name

    def summary(self):
        return self._data

    def collapse(self, mapping):
        fn = lambda df: df.sort_values("p").iloc[0,:]
        o = self._data.groupby(mapping).aggregate(fn)
        try:
            o.index = list(map(int, o.index))
        except:
            pass
        return Term(self._name, o)

class CategoricalTerm(dict):
    def __init__(self, name, *args, **kwargs):
        self._name = name
        super().__init__(*args, **kwargs)

    @property
    def name(self):
        return self._name

    def levels(self):
        return self.keys()

    def collapse(self, mapping):
        o = {}
        for k,term in self.items():
            o[k] = term.collapse(mapping)
        return CategoricalTerm(self.name, o)

def make_fit2(data):
    pass

class MFit(object):
    """
    An object representing multiple linear models fit using the same formula,
    one for each individual independent variable.

    TODO:
    - This class could probably be greatly streamlined with an xarray Dataset
    """
    def __init__(self, coef, 
            SE=None, 
            N=None, 
            t=None, 
            p=None, 
            residual=None, 
            design=None, 
            formula=None):
        """
        Arguments
        ---------

        coef : :class:`pandas.DataFrame`
            Models x coefficients
        design : :class:`pandas.DataFrame`, optional
            Samples x coefficients
            If a formula was used, this design matrix is the PRE-EXPANSION version.
        formula : str, optional
            The model formula.
        """
        self._coef = coef
        self._design = design
        self._formula = formula
        self._SE = SE
        self.metadata = {}

        if t is not None:
            assert t.shape == coef.shape
        self._t = t

        if p is not None:
            assert p.shape == coef.shape
        self._p = p

        if residual is not None:
            assert residual.shape == (design.shape[0], coef.shape[0])
        self._residual = residual

        if N is not None:
            # FIXME: originally N could be different for each value, now OLS expects complete matrix
            if hasattr(N, "shape"):
                assert N.shape[0] == coef.shape[0]
                assert N.max() <= design.shape[0]
                self._N = N
            elif isinstance(N, int):
                self._N = N
            else:
                raise ValueError("type of `N` must be Series or int, got {}".format(str(type(N))))

        self._spec = []
        for c in self._coef.columns:
            o = {}
            for p in c.split(":"):
                tc = parse_categorical(p)
                if tc[0] is None:
                    o[p] = None
                else:
                    o[tc[0]] = tc[1]
            self._spec.append(o)

    def copy(self):
        # temporary for editing this class
        o = MFit(
                coef=self._coef,
                design=self._design,
                formula=self._formula,
                SE=self._SE,
                t=self._t,
                p=self._p
        )
        o.metadata = {k:v for k,v in self.metadata.items()}
        return o

    def __getitem__(self, term):
        summary = self.summary(term)
        if isinstance(summary, dict):
            return CategoricalTerm(term, {k:Term(term, df) for k,df in summary.items()})
        else:
            return Term(term, self.summary(term))

    def __repr__(self):
        return "<MFit object with {} elements and {} coefficients>"\
                .format(*self._coef.shape)

    @property
    def data(self):
        return self._coef

    def interaction(self, term, parameter="t"):
        parts = set(term.split(":"))

        ix = collections.defaultdict(list)
        o = []

        for i,s in enumerate(self._spec):
            if set(s.keys()) == parts:
                for k,v in s.items():
                    ix[k].append(v)
                o.append(getattr(self, "_{}".format(parameter)).iloc[:,i])

        noncategorical = [k for k in ix.keys() if all(v == None for v in ix[k])]
        for k in noncategorical:
            del ix[k]

        if len(ix) == 1:
            ix_name = next(iter(ix.keys()))
            ix = pd.Index(next(iter(ix.values())))
            ix.name = ix_name
        else:
            # FIXME: test
            #raise NotImplementedError
            import warnings
            warnings.warn("wrenlab.lm.MFit.interaction not fully tested")
            ks = list(sorted(ix.keys()))
            vs = list(zip(*[ix[k] for k in ks]))
            ix = pd.MultiIndex.from_tuples(vs, names=ks)

        o = pd.concat(o, axis=1)
        o.columns = ix
        return o

    def _select_term(self, df, term):
        assert ":" not in term

        if term in df.columns:
            return df.loc[:,term]
        else:
            o = {}
            for c in df.columns:
                c_term, c_category = parse_categorical(c)
                if c_term != term:
                    continue
                o[c_category] = df.loc[:,c]
            o = pd.DataFrame(o)
            o.columns.name = term
            return o

    def coef(self, term):
        """
        Extract coefficients for a LM term.

        Returns
        -------
        A :class:`pandas.Series` if the term is continuous, or a
        :class:`pandas.DataFrame` if it is categorical (one column per
        category).
        """
        return self._select_term(self._coef, term)
 
    @property
    def N(self):
        return self._N

    def t(self, term):
        return self._select_term(self._t, term)

    def p(self, term):
        return self._select_term(self._p, term)

    def s_error(self, term):
        beta = self._select_term(self._coef, term)
        SE = self._select_term(self._SE, term)
        o = scipy.stats.norm.cdf(0, loc=beta, scale=SE)
        return pd.Series(np.minimum(o, 1-o), index=beta.index)

    def summary(self, term, level=None):
        coef = self.coef(term)
        t = self.t(term)
        p = self.p(term)
        if level is not None:
            coef = coef[level]
            t = t[level]
            p = p[level]

        if len(coef.shape) == 2:
            o = {}
            for key in coef.columns:
                o[key] = self.summary(term, level=key)
            return o

        return pd.DataFrame({
            "coef": coef,
            "t": t,
            "p": p
        }).loc[:,["coef","t","p"]]

    # "Smart" LM fit stuff -- ultimately to be split into subclass
    def enrichment(self, key, database="GO"):
        A = wrenlab.enrichment.annotation(database, taxon_id=self.metadata["TaxonID"])
        return A.enrichment(self.p(key))

################################
# Helper/miscellaneous functions
################################

def _get_design(Y, design, formula=None):
    if formula is not None:
        D = patsy.dmatrix(formula, data=design, return_type="dataframe")
        D = D.loc[:,~(D == 0).all()]
    else:
        D = design

    Y,D = Y.align(D, axis=0, join="inner")
    design = design.loc[D.index,:]
    assert Y.shape[0] > 3
    return Y, D, design


def fit1(D, formula):
    """
    Fit a single linear model.
    """
    y, X = patsy.dmatrices(formula, data=D, return_type="dataframe")
    model = sm.OLS(y,X)
    fit = model.fit()
    return fit

###################
# sklearn functions
###################

def _sklearn_model_fn(model_class):
    def fn(Y, design, formula=None, **model_kwargs):
        Y, D, design = _get_design(Y, design, formula=formula)

        model = model_class(**model_kwargs)
        model.fit(D, Y)
        beta = pd.DataFrame(model.coef_, index=Y.columns, columns=D.columns)
        return MFit(beta, design=design)
    return fn

lasso = _sklearn_model_fn(sklearn.linear_model.Lasso)
elastic_net = _sklearn_model_fn(sklearn.linear_model.ElasticNet)

#############
# statsmodels
#############

def _mfit_mixed_effects(Y, design, groups, formula=None):
    assert NotImplementedError
    Y, D, design = _get_design(Y, design, formula=formula)

    y = Y.iloc[:,0]
    model = sm.MixedLM(y, D, groups=np.array(groups))
    fit = model.fit()
    return fit

#######
# numpy
#######

def residual(y, X, absolute_value=False):
    """
    Get predicted values of y, correcting for X. This correction
    is done WITHOUT an intercept, after the mean of y has been 
    set to zero. Thus, the returned values should be corrected
    only for X (not for the mean of y).

    In other words, the mean of `residuals` will be mean(y), 
    not zero.

    Arguments
    ---------
    y : array-like, 1D or 2D
        Independent variable(s). If y is 2D, computes residuals
        separately for each column.
    X : array-like, 1D or 2D
        Dependent variable(s).
    absolute_value : bool, default False
        If True, fit model to abs(y), but return residuals with original
        signs.

    Returns
    -------
    residuals : :class:`numpy.array`
    """
    def fn(X,y):
        y = y.copy()
        if absolute_value is True:
            sign = np.sign(y)
            y = np.abs(y)
            
        X = np.array(X.copy())
        mu = y.mean()
        y -= mu

        N = len(y)
        if len(X.shape) == 1:
            assert len(y) == len(X)
            X_new = np.zeros((N,1))
            X_new[:,0] = X
        elif len(X.shape) == 2:
            assert len(y) == X.shape[0]
            X_new = X
        else:
            raise Exception("X must have dimensionality 1 or 2.")
        X = X_new

        beta = np.linalg.lstsq(X, y)[0]
        y_hat = X @ beta
        o = (y - y_hat) + mu
        if absolute_value is True:
            o *= sign
        return o

    if len(y.shape) == 1:
        return fn(X, y)
    elif len(y.shape) == 2:
        # FIXME: this is way slower than using 2D lstsq. also this whole thing should just be in MFit
        Y = np.array(y)
        o = np.zeros(y.shape)
        for j in range(y.shape[1]):
            o[:,j] = fn(X,Y[:,j])
        if isinstance(y, pd.DataFrame):
            o = pd.DataFrame(o, index=y.index, columns=y.columns)
        return o
    else:
        raise ValueError("y must be a 1D or 2D array-like")

def residual2d(Y, design, formula=None):
    Y, D, design = _get_design(Y, design, formula=formula)
    mu = Y.mean()
    Y_new = Y - mu
    fit = ols(Y_new, design, formula=formula)
    Y_hat = np.array(D) @ np.array(fit._coef).T
    return (Y_new - Y_hat) + mu

def fit(Y, design, formula=None, group=None):
    if group is None:
        return ols(Y, design, formula=formula)
    else:
        return mixed(Y, design, group, formula=formula)

def _mixed1(y, D, groups):
    model = sm.MixedLM(y, D, groups=groups)
    fit = model.fit()
    return fit.params, fit.pvalues, fit.tvalues


def mixed(Y, design, group, formula=None):
    groups = design[group].astype(int)
    design = design.drop(group, axis=1)

    if not all([dt == np.int64 for dt in Y.dtypes]):
        assert (~np.isnan(Y)).all().all()
    Y, D, design = _get_design(Y, design, formula=formula)

    coef = []
    p = []
    t = []
    from joblib import Parallel, delayed

    for result in Parallel(n_jobs=16, verbose=100, pre_dispatch='1.5*n_jobs')(
            delayed(_mixed1)(Y[c], D, groups) for c in Y.columns):
        coef.append(result[0])
        p.append(result[1])
        t.append(result[2])

    #for c in tqdm.tqdm(Y.columns):
    #    y = Y[c]
    #    model = sm.MixedLM(y, D, groups=groups)
    #    fit = model.fit()
    #    coef.append(fit.params)
    #    p.append(fit.pvalues)
    #    t.append(fit.tvalues)

    mkdf = lambda x: pd.DataFrame(x, index=Y.columns).drop("Group Var", axis=1)

    return MFit(
        coef=mkdf(coef),
        design=design,
        formula=formula,
        t=mkdf(t),
        p=mkdf(p),
        N=Y.shape[0]
    )

        
def ols(Y, design, formula=None):
    if not all([dt == np.int64 for dt in Y.dtypes]):
        assert (~np.isnan(Y)).all().all()
    Y, D, design = _get_design(Y, design, formula=formula)

    # FIXME: add some option to warn or something, this is to prevent multicollinearity
    # but it will drop coefficients (presumably ones we aren't usually interested in)

    # Also, this causes a crash with lots of columns (sadly when its most needed)
    D = D.T.drop_duplicates().T

    Y_o = Y
    Y = np.array(Y)
    X = np.array(D)

    LOG.info("Fitting {} OLS models with X shape = {}".format(Y.shape[1], X.shape))

    beta, _, _, _ = np.linalg.lstsq(X, Y)
    N,p = X.shape
    residual = Y - X @ beta
    var_y_given_x = (residual ** 2).sum(axis=0) / (N - p)
    var_beta = np.outer(np.diag(np.linalg.inv(X.T @ X)), var_y_given_x)

    SE = np.sqrt(var_beta)
    t = beta / SE
    p = scipy.stats.t.sf(np.abs(t), df=N-2) * 2

    def mkdf(x):
        return pd.DataFrame(x.T, index=Y_o.columns, columns=D.columns)

    return MFit(
            coef=mkdf(beta),
            design=design,
            formula=formula,
            SE=mkdf(SE),
            t=mkdf(t),
            p=mkdf(p),
            N=N
    )

def synthetic_data(n_samples=100, n_features=10, eps=0.1):
    # translation of variables here
    # http://edwardlib.org/tutorials/linear-mixed-effects-models

    np.random.seed(0)
    ns = n_samples
    nf = n_features

    # make design matrix (including intercept)
    nx = 2
    nz = 5
    X = np.random.normal(size=(ns, nx + 1))
    X[:,0] = 1

    group = np.random.randint(0, high=5, size=ns)
    Z = np.zeros((ns,nz))
    Z[:,group] = 1

    # make fake coefficients (including intercept)
    beta = np.random.normal(size=(nf, nx + 1))
    # make intercept positive just because
    #beta[:,0] += 5 
    eta = np.random.normal(size=(nf, nz))

    Y = X @ beta.T + Z @ eta.T
    epsilon = np.random.normal(scale=eps, size=Y.shape)
    Y += epsilon

    SyntheticData = collections.namedtuple("SyntheticData",
            "beta,eta,Y,X")

    ix_beta = ["Intercept"] + ["Var{}".format(i) for i in range(1, nx + 1)]
    ix_feature = ["Feature{}".format(j) for j in range(nf)]
    beta = pd.DataFrame(beta, index=ix_feature, columns=ix_beta)
    X = pd.DataFrame(X, columns=ix_beta)
    X["Group"] = group
    Y = pd.DataFrame(Y, columns=ix_feature)
    group = pd.Series(group)
    return SyntheticData(beta,eta,Y,X)

def test_synthetic_data():
    data = synthetic_data(eps=1e-5)
    fit = mixed(data.Y, data.X, "Group")
    for v in data.beta.columns:
        #if v == "Intercept":
        #   continue
        y_hat = fit[v].summary().coef
        y = data.beta[v]
        ok = np.allclose(y, y_hat, atol=1e-2)
        MAD = (y - y_hat).abs().mean()
        print(v, ok, MAD)

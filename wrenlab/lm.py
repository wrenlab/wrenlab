"""
Functions for linear models, with an emphasis on the multivariate problem 
(multiple dependent variables).

Usage
-----

The entry points of this module are each different methods for fitting
multivariate models, such as "ols", "elastic_net", etc.

Each of the different models has a similar interface:

def fn(Y, design, formula=None, **kwargs) -> MFit

Each fits a linear model for each column in `Y` against `design`, returning
a `MFit` object. The **kwargs are model-specific tuning parameters.
"""

import collections
import re

import patsy
import scipy.stats
import pandas as pd
import numpy as np
import statsmodels.api as sm
import sklearn.linear_model

from wrenlab.util import LOG

class MFit(object):
    """
    An object representing multiple linear models fit using the same formula,
    one for each individual independent variable.

    TODO:
    - This class could probably be greatly streamlined with an xarray Dataset
    """
    P_CATEGORICAL_TERM = re.compile("(C\()?(?P<term>(.+?))\)?\[(T\.)?(?P<category>(.+?))\]")

    def __init__(self, coef, 
            SE=None, 
            N=None, 
            t=None, 
            p=None, 
            residual=None, 
            design=None, 
            formula=None):
        """
        Arguments
        ---------

        coef : :class:`pandas.DataFrame`
            Models x coefficients
        design : :class:`pandas.DataFrame`, optional
            Samples x coefficients
            If a formula was used, this design matrix is the PRE-EXPANSION version.
        formula : str, optional
            The model formula.
        """
        self._coef = coef
        self._design = design
        self._formula = formula
        self._SE = SE

        if t is not None:
            assert t.shape == coef.shape
        self._t = t

        if p is not None:
            assert p.shape == coef.shape
        self._p = p

        if residual is not None:
            assert residual.shape == (design.shape[0], coef.shape[0])
        self._residual = residual

        if N is not None:
            assert N.shape[0] == coef.shape[0]
            assert N.max() <= design.shape[0]
            self._N = N

        self._spec = []
        for c in self._coef.columns:
            o = {}
            for p in c.split(":"):
                tc = self._parse_categorical(p)
                if tc[0] is None:
                    o[p] = None
                else:
                    o[tc[0]] = tc[1]
            self._spec.append(o)

    def copy(self):
        # temporary for editing this class
        return MFit(
                coef=self._coef,
                design=self._design,
                formula=self._formula,
                SE=self._SE,
                t=self._t,
                p=self._p
        )

    def __getitem__(self, term):
        return self.coef(term)

    def __repr__(self):
        return "<MFit object with {} elements and {} coefficients>"\
                .format(*self._coef.shape)

    @property
    def data(self):
        return self._coef

    def _parse_categorical(self, text):
        m = self.P_CATEGORICAL_TERM.match(text)
        if m is None:
            return None, None
        term = m.group("term")
        category = m.group("category")
        try:
            category = int(category)
        except ValueError:
            pass
        return term, category

    def interaction(self, term, parameter="t"):
        parts = set(term.split(":"))

        ix = collections.defaultdict(list)
        o = []

        for i,s in enumerate(self._spec):
            if set(s.keys()) == parts:
                for k,v in s.items():
                    ix[k].append(v)
                o.append(getattr(self, "_{}".format(parameter)).iloc[:,i])

        noncategorical = [k for k in ix.keys() if all(v == None for v in ix[k])]
        for k in noncategorical:
            del ix[k]

        if len(ix) == 1:
            ix_name = next(iter(ix.keys()))
            ix = pd.Index(next(iter(ix.values())))
            ix.name = ix_name
        else:
            # FIXME: test
            #raise NotImplementedError
            import warnings
            warnings.warn("wrenlab.lm.MFit.interaction not fully tested")
            ks = list(sorted(ix.keys()))
            vs = list(zip(*[ix[k] for k in ks]))
            ix = pd.MultiIndex.from_tuples(vs, names=ks)

        o = pd.concat(o, axis=1)
        o.columns = ix
        return o

    def _select_term(self, df, term):
        assert ":" not in term

        if term in df.columns:
            return df.loc[:,term]
        else:
            o = {}
            for c in df.columns:
                c_term, c_category = self._parse_categorical(c)
                if c_term != term:
                    continue
                o[c_category] = df.loc[:,c]
            o = pd.DataFrame(o)
            o.columns.name = term
            return o

    def coef(self, term):
        """
        Extract coefficients for a LM term.

        Returns
        -------
        A :class:`pandas.Series` if the term is continuous, or a
        :class:`pandas.DataFrame` if it is categorical (one column per
        category).
        """
        return self._select_term(self._coef, term)
 
    def t(self, term):
        return self._select_term(self._t, term)

    def p(self, term):
        return self._select_term(self._p, term)

    def s_error(self, term):
        beta = self._select_term(self._coef, term)
        SE = self._select_term(self._SE, term)
        o = scipy.stats.norm.cdf(0, loc=beta, scale=SE)
        return pd.Series(np.minimum(o, 1-o), index=beta.index)

    def summary(self, term):
        coef = self.coef(term)
        t = self.t(term)
        p = self.p(term)
        return pd.DataFrame({
            "coef": coef,
            "t": t,
            "p": p
        }).loc[:,["coef","t","p"]]

################################
# Helper/miscellaneous functions
################################

def _get_design(Y, design, formula=None):
    if formula is not None:
        D = patsy.dmatrix(formula, data=design, return_type="dataframe")
        D = D.ix[:,~(D == 0).all()]
    else:
        D = design

    Y,D = Y.align(D, axis=0, join="inner")
    design = design.loc[D.index,:]
    assert Y.shape[0] > 3
    return Y, D, design


def fit1(D, formula):
    """
    Fit a single linear model.
    """
    y, X = patsy.dmatrices(formula, data=D, return_type="dataframe")
    model = sm.OLS(y,X)
    fit = model.fit()
    return fit

###################
# sklearn functions
###################

def _sklearn_model_fn(model_class):
    def fn(Y, design, formula=None, **model_kwargs):
        Y, D, design = _get_design(Y, design, formula=formula)

        model = model_class(**model_kwargs)
        model.fit(D, Y)
        beta = pd.DataFrame(model.coef_, index=Y.columns, columns=D.columns)
        return MFit(beta, design=design)
    return fn

lasso = _sklearn_model_fn(sklearn.linear_model.Lasso)
elastic_net = _sklearn_model_fn(sklearn.linear_model.ElasticNet)

#############
# statsmodels
#############

def _mfit_mixed_effects(Y, design, groups, formula=None):
    assert NotImplementedError
    Y, D, design = _get_design(Y, design, formula=formula)

    y = Y.iloc[:,0]
    model = sm.MixedLM(y, D, groups=np.array(groups))
    fit = model.fit()
    return fit

#######
# numpy
#######

def ols(Y, design, formula=None):
    assert (~np.isnan(Y)).all().all()
    Y, D, design = _get_design(Y, design, formula=formula)

    # FIXME: add some option to warn or something, this is to prevent multicollinearity
    # but it will drop coefficients (presumably ones we aren't usually interested in)

    # Also, this causes a crash with lots of columns (sadly when its most needed)
    D = D.T.drop_duplicates().T

    Y_o = Y
    Y = np.array(Y)
    X = np.array(D)

    LOG.info("Fitting {} OLS models with X shape = {}".format(Y.shape[1], X.shape))

    beta, _, _, _ = np.linalg.lstsq(X, Y)
    N,p = X.shape
    residual = Y - X @ beta
    var_y_given_x = (residual ** 2).sum(axis=0) / (N - p)
    var_beta = np.outer(np.diag(np.linalg.inv(X.T @ X)), var_y_given_x)

    SE = np.sqrt(var_beta)
    t = beta / SE
    p = scipy.stats.t.sf(np.abs(t), df=N-2) * 2

    def mkdf(x):
        return pd.DataFrame(x.T, index=Y_o.columns, columns=D.columns)

    return MFit(
            coef=mkdf(beta),
            design=design,
            formula=formula,
            SE=mkdf(SE),
            t=mkdf(t),
            p=mkdf(p)
    )

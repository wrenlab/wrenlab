CREATE TABLE journal (
    id VARCHAR,
    title VARCHAR,
    ISSN VARCHAR
);

CREATE TABLE document (
    id INTEGER PRIMARY KEY NOT NULL,
    publication_date DATE,
    title VARCHAR,
    abstract VARCHAR,

    n_citations INTEGER,
    pagerank FLOAT,

    journal_id VARCHAR,

    FOREIGN KEY (journal_id) REFERENCES journal (id)
);

CREATE TABLE citation (
    source_id INTEGER NOT NULL,
    target_id INTEGER NOT NULL,

    FOREIGN KEY (source_id) REFERENCES document (id),
    FOREIGN KEY (target_id) REFERENCES document (id),
    PRIMARY KEY(source_id, target_id)
);

CREATE TABLE xref (
    document_id INTEGER NOT NULL,
    source VARCHAR NOT NULL,
    xref VARCHAR NOT NULL
);

CREATE TABLE section (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE sentence (
    document_id INTEGER NOT NULL,
    section_id INTEGER NOT NULL,
    'index' INTEGER NOT NULL,

    text VARCHAR NOT NULL,
    PRIMARY KEY (document_id, section_id, 'index')
);

-- AILUN

CREATE TABLE probe_gene (
    platform_id INTEGER,
    probe_id VARCHAR,
    gene_id INTEGER
);

CREATE INDEX probe_gene_ix_platform_id ON probe_gene (platform_id);

-- MEDLINE

CREATE TABLE journal (
    id VARCHAR PRIMARY KEY NOT NULL,
    title VARCHAR,
    ISSN VARCHAR
);

CREATE TABLE document (
    id INTEGER PRIMARY KEY NOT NULL,
    publication_date DATE,
    title VARCHAR,
    abstract VARCHAR,

    n_citations INTEGER,
    pagerank FLOAT,

    journal_id VARCHAR,

    FOREIGN KEY (journal_id) REFERENCES journal (id)
);

CREATE TABLE citation (
    source_id INTEGER NOT NULL,
    target_id INTEGER NOT NULL,

    FOREIGN KEY (source_id) REFERENCES document (id),
    FOREIGN KEY (target_id) REFERENCES document (id),
    PRIMARY KEY(source_id, target_id)
);

CREATE TABLE section (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE sentence (
    document_id INTEGER NOT NULL,
    section_id INTEGER NOT NULL,
    "index" INTEGER NOT NULL,

    text VARCHAR NOT NULL,
    PRIMARY KEY (document_id, section_id, "index")
);

CREATE INDEX ix_journal_id ON journal (id);
CREATE INDEX ix_article_journal_id ON document (journal_id);
CREATE INDEX ix_citation_source_id ON citation (source_id);
CREATE INDEX ix_citation_target_id ON citation (target_id);

-- IRIDESCENT

CREATE TABLE ontology (
    id INTEGER PRIMARY KEY NOT NULL,
    accession VARCHAR NOT NULL
);

CREATE TABLE term (
    id INTEGER PRIMARY KEY NOT NULL,
    ontology_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,

    FOREIGN KEY (ontology_id) REFERENCES ontology (id)
);

CREATE TABLE "synonym" (
    id INTEGER PRIMARY KEY NOT NULL,
    term_id INTEGER NOT NULL,
    text VARCHAR NOT NULL,

    FOREIGN KEY (term_id) REFERENCES term (id)
);

CREATE TABLE posting (
    synonym_id INTEGER,
    document_id INTEGER,

    section_id INTEGER,
    sentence_index INTEGER
);

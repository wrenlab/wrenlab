CREATE TABLE run (
    project_id VARCHAR NOT NULL,
    sample_id VARCHAR NOT NULL,
    run_id VARCHAR PRIMARY KEY NOT NULL
);

CREATE TABLE term (
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    citation_count INTEGER DEFAULT 0
);

CREATE TABLE 'synonym' (
    id INTEGER PRIMARY KEY NOT NULL,
    term_id INTEGER NOT NULL,
    text VARCHAR NOT NULL,

    FOREIGN KEY (term_id) REFERENCES term (id)
);

CREATE TABLE xref (
    term_id INTEGER NOT NULL,
    xref_prefix VARCHAR NOT NULL,
    xref_id INTEGER NOT NULL
);

--CREATE TABLE section (
--    id INTEGER PRIMARY KEY NOT NULL,
--    name VARCHAR UNIQUE NOT NULL,
--);

CREATE TABLE synonym_posting (
    synonym_id INTEGER,
    document_id INTEGER
);

CREATE TABLE term_posting (
    term_id INTEGER,
    document_id INTEGER
);

--    section_id INTEGER,
--    sentence_index INTEGER,

CREATE TABLE probe_gene (
    platform_id INTEGER,
    probe_id VARCHAR,
    gene_id INTEGER
);

CREATE INDEX probe_gene_ix_platform_id ON probe_gene (platform_id);

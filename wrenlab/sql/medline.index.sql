CREATE INDEX ix_journal_journal_id ON journal (id);
CREATE INDEX ix_document_journal_id ON document (journal_id);
CREATE INDEX ix_citation_source_id ON citation (source_id);
CREATE INDEX ix_citation_target_id ON citation (target_id);
CREATE INDEX ix_xref_document_id ON xref (document_id);


CREATE VIRTUAL TABLE document_fts USING fts4(id, title, abstract);
INSERT INTO document_fts
    SELECT id, title, abstract
    FROM document;

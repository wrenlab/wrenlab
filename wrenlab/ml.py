"""
Machine learning utilities.
"""

import random

import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import sklearn.metrics
from sklearn.feature_selection import SelectKBest, f_classif
import pandas as pd

from wrenlab.util import align_series

def GroupCV(object):
    """
    A cross-validation iterator that splits by group.
    """
    def __init__(self, groups):
        self._groups = pd.Series(groups)

    def __iter__(self):
        for t in set(self._groups):
            tr = self._groups != t
            te = self._groups == t
            yield tr, te

class Problem(object):
    def __init__(self, y, X, type=None):
        assert isinstance(y, pd.Series)

        self.y = y
        self.X = X
        assert y.shape[0] == X.shape[0]

        if type is not None:
            self.type = type
        elif len(set(self.y)) == 2:
            self.type = "binary"
        else:
            #assert type(self.y.iloc[0]) is not int
            self.type = "continuous"

        def predict_fn(model, X):
            if (self.type == "binary") and hasattr(model, "predict_log_proba"):
                y_hat = model.predict_log_proba(X)
                ix = list(model.classes_).index(1)
                return y_hat[:,ix]
            else:
                return model.predict(X)
        self.predict_fn = predict_fn

    def cross_validate(self, model, k=10, groups=None):
        raise NotImplementedError("Need to update to use new sklearn.model_selection")
        # FIXME: stratify with groups
        # FIXME: for multiclass, allow 2D y_hat?

        X = np.array(self.X)
        y = np.array(self.y)

        if groups is not None:
            assert len(set(groups)) >= 2
            k = min(k, len(set(groups)))
            # See also: LeaveOneLabelOut and LeavePLabelOut
            cv = sklearn.cross_validation.LabelKFold(groups, k)
        else:
            cv = sklearn.cross_validation.KFold(y.shape[0], k, shuffle=True)

        y_hat = np.empty(y.shape)
        for tr_ix, te_ix in cv:
            X_tr, y_tr = X[tr_ix,:], y[tr_ix]
            if self.type == "binary":
                y_tr = y_tr.astype(int)
            model.fit(X_tr, y_tr)
            y_hat[te_ix] = self.predict_fn(model, X[te_ix,:])
        y_hat = pd.Series(y_hat, index=self.y.index)

        if self.type == "continuous":
            return ContinuousResult(self.y, y_hat)
        elif self.type == "binary":
            return ScoredBinaryResult(self.y, y_hat)
        elif self.type == "categorical":
            return CategoricalResult(self.y, y_hat)
        else:
            raise Exception

class Result(object):
    def __init__(self, y, y_hat):
        assert isinstance(y, pd.Series)
        assert isinstance(y_hat, pd.Series)
        # NOTE: y and y_hat are not assumed to be aligned, because
        #   a missing/NA value in y_hat means "no prediction"
        self.y = y.dropna()
        self.y_hat = y_hat.loc[self.y.index]

    @property
    def accuracy(self):
        return (np.array(self.y) == np.array(self.y_hat)).mean()

    @property
    def summary(self):
        return pd.Series([getattr(self, p) for p in self.PROPERTIES], 
                index=self.PROPERTIES)

class ScoredBinaryResult(Result):
    PROPERTIES = ["AUC"]

    def __init__(self, y, y_hat):
        super(ScoredBinaryResult, self).__init__(y, y_hat)

    @property
    def AUC(self):
        return sklearn.metrics.roc_auc_score(self.y, self.y_hat)

class BinaryResult(Result):
    PROPERTIES = ["TP", "FP", "FN", "precision", "recall", "accuracy"]

    def __init__(self, y, y_hat):
        assert y.dtype.name == "bool"
        if y_hat.dtype.name == "float64":
            raise NotImplementedError("Scores/probabilities not supported yet")
        assert y_hat.dtype.name == "bool"
        super(BinaryResult, self).__init__(y, y_hat)

    @property
    def TP(self):
        return (self.y & self.y_hat).sum()

    @property
    def FP(self):
        return ((~self.y) & self.y_hat).sum()

    @property
    def FN(self):
        return ((self.y) & (~self.y_hat)).sum()

    @property
    def precision(self):
        return self.TP / (self.TP + self.FP)

    @property
    def recall(self):
        return self.TP / (self.TP + self.FN)

    """
    @property
    def AUC(self):
        try:
            return sklearn.metrics.roc_auc_score(self.y, self.y_hat)
        except:
            return np.nan
    """

class CategoricalResult(Result):
    # FIXME: allow some way to set what:
    # - "missing" or "null" prediction sentinel value was
    # - possibly allow to set "other" or some way to collapse values to "other"
    #   when looking at "top 10 cats in y" or similar

    def __init__(self, y, y_hat):
        assert y.dtype.name == "category"
        assert y_hat.dtype.name == "category"

        y = y.cat.remove_unused_categories()
        super(CategoricalResult, self).__init__(y, y_hat)

        categories = self.y.cat.categories
        self.y = self.y.cat.set_categories(categories, ordered=True)

        disjoint = len(set(self.y_hat.cat.categories) - set(categories)) > 0
        if disjoint:
            categories = ["OTHER"] + list(sorted(categories))
            self.y = self.y.cat.set_categories(categories)

            self.y_hat = pd.Series(list(self.y_hat), index=self.y_hat.index)
            self.y_hat.ix[~self.y_hat.isin(self.y.cat.categories)] = "OTHER"
            self.y_hat = self.y_hat.astype("category")
        self.y_hat = self.y_hat.cat.set_categories(categories)

        self._results = {}
        for c in categories:
            self._results[c] = BinaryResult(self.y == c, self.y_hat == c)

    @property
    def categories(self):
        return list(self.y.cat.categories)

    def confusion_matrix(self, normalize=False):
        o = pd.DataFrame(index=self.categories, columns=self.categories).fillna(0)
        o.index.name = "y"
        o.columns.name = "y_hat"
        for c_y in self.categories:
            for c_y_hat in self.categories:
                o.loc[c_y,c_y_hat] = ((self.y == c_y) & (self.y_hat == c_y_hat)).sum()
        if "OTHER" in self.categories:
            o = o.drop("OTHER")
        if normalize:
            o = o.divide(1 + o.sum(axis=0), axis=1)
        return o

    @property
    def summary(self):
        if not hasattr(self, "_summary"):
            ix = list(self._results.keys())
            o = pd.concat([self._results[c].summary for c in ix], axis=1).T
            o.index = ix
            self._summary = o.rename_axis("Category")
            if "OTHER" in self._summary.index:
                self._summary = self._summary.drop("OTHER", axis=0)
        return self._summary

    @property
    def macro_precision(self):
        return self.summary.precision.mean()

    @property
    def macro_recall(self):
        return self.summary.recall.mean()

    @property
    def micro_precision(self):
        TP = self.summary.TP.sum()
        FP = self.summary.FP.sum()
        return TP / (TP + FP)

    @property
    def micro_recall(self):
        TP = self.summary.TP.sum()
        FN = self.summary.FN.sum()
        return TP / (TP + FN)

class ContinuousResult(Result):
    PROPERTIES = ["accuracy", "r2", "MAE", "MSE"]

    def __init__(self, y, y_hat, tolerance=0.1):
        """
        Arguments
        ---------

        tolerance : float
            y and y_hat values less than this distance away are considered to be
            the same for classification-based metrics.
        """
        super(ContinuousResult, self).__init__(y, y_hat)
        self.tolerance = tolerance

    @property
    def micro_precision(self):
        raise NotImplementedError

    @property
    def micro_recall(self):
        raise NotImplementedError

    @property
    def macro_precision(self):
        y, y_hat = align_series(self.y, self.y_hat, dropna=True)
        return np.isclose(y, y_hat, atol=self.tolerance).mean()

    @property
    def macro_recall(self):
        y_hat = self.y_hat.loc[self.y.index]
        return np.isclose(self.y, y_hat, atol=self.tolerance).mean()

    @property
    def r2(self):
        y, y_hat = align_series(self.y, self.y_hat, dropna=True)
        return scipy.stats.pearsonr(y, y_hat)[0] ** 2

    @property
    def MAE(self):
        return np.abs(self.y - self.y_hat).mean()

    @property
    def MSE(self):
        return ((self.y - self.y_hat) ** 2).mean()

    def plot(self, path=None):
        plt.clf()
        y, y_hat = align_series(self.y, self.y_hat, dropna=True)
        data = pd.DataFrame.from_dict({"y":y, "y_hat":y_hat})
        sns.regplot(x="y", y="y_hat", data=data, fit_reg=False)
        plt.xlim([0, y.max() * 1.1])
        plt.ylim([0, y_hat.max() * 1.1])
        plt.ylabel("$\hat{y}$")
        plt.xlabel("y")
        if path is not None:
            plt.savefig(path)

class OntologyResult(CategoricalResult):
    def __init__(self, ontology, y, y_hat):
        self._ontology = ontology

        y = y.dropna()
        y_hat = y_hat.ix[y.index]
        ix = self._ontology.terms.index
        assert y.isin(ix).all()
        assert (y.isnull() | y.isin(ix)).all()

        super(OntologyResult, self).__init__(
                y.astype("category"), 
                y_hat.astype("category"))
    
    def cr(self, n=10):
        # FIXME: remove
        M = self._ontology.name_map()
        M["OTHER"] = "OTHER"

        y = self.y
        ix = y.value_counts().index[:n]
        y = y.ix[y.isin(ix)]
        y_hat = self.y_hat
        y, y_hat = align_series(y, y_hat)

        y = y.apply(lambda id: M[id]).astype("category")
        y_hat = y_hat.apply(lambda id: M[id]).astype("category")
        return CategoricalResult(y, y_hat)

    def distance(self, randomize=False):
        # NB only calculates where y_hat has a prediction

        g = self._ontology\
                .to_graph(relations=["is_a", "part_of"])\
                .to_undirected()

        y, y_hat = align_series(self.y, self.y_hat)

        o = []
        for ix, y_, y_hat_ in zip(y.index, y, y_hat):
            if randomize:
                y_hat_ = random.choice(self._ontology.terms.index)
            try:
                n = nx.shortest_path_length(g, y_, y_hat_)
            except (nx.NetworkXNoPath, nx.NetworkXError):
                continue
            o.append((ix, y_, y_hat_, n))
        return pd.DataFrame.from_records(o, 
                    columns=["SampleID", "y", "y_hat", "Distance"])\
                .set_index(["SampleID"])


def select_features(X,y,k=100):
    assert X.shape[0] == y.shape[0]
    fs = SelectKBest(f_classif,k=k)
    Xo = pd.DataFrame(fs.fit_transform(X,y), index=X.index)
    Xo.columns = X.columns[fs.get_support()]
    return Xo

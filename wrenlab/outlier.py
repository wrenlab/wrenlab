"""
TODO:
Basic strategy:
- prune rows determined to be outliers by multiple models
- possibly wrap it all iteratively with a configurable threshold of %samples to be removed or whatever

Need 3 main functions:
- score
- detect/predict (binary)
- remove (fit and remove on a df/matrix all in one go)
"""

import pandas as pd
import sklearn.ensemble

def score(X):
    """
    Find outliers in the row of X.

    Return a score quantifying how much of an outlier each sample is. 
    Lower score means more outlier-like.
    """
    model = sklearn.ensemble.IsolationForest()
    model.fit(X)
    o = model.decision_function(X)
    if isinstance(X, pd.DataFrame):
        o = pd.Series(o, index=X.index)
    return o

def detect(X):
    model = sklearn.ensemble.IsolationForest()
    model.fit(X)
    o = model.predict(X)
    o[o == -1] = 0
    o = ~o.astype(bool)
    if isinstance(X, pd.DataFrame):
        o = pd.Series(o, index=X.index)
    return o

def remove(X, iterations=1):
    for i in range(iterations):
        ix = detect(X)
        X = X.loc[~ix,:]
    return X

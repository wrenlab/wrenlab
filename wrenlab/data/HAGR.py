import numpy as np

import wrenlab.util.remote_table
import wrenlab.ncbi.taxonomy

def AnAge():
    o = wrenlab.util.remote_table.remote_table("HAGR.AnAge")
    for k in ["FemaleMaturity", "MaleMaturity", "Gestation", "Weaning"]:
        o[k] = o[k] / 365
    taxonomy = wrenlab.ncbi.taxonomy.names()
    name_id = dict(zip(taxonomy["Scientific Name"], taxonomy.index))
    o["Scientific Name"] = ["{} {}".format(g,s) for 
            g,s in o.loc[:,["Genus", "Species"]].to_records(index=False)]
    o["TaxonID"] = [name_id.get(name, np.nan) for name in o["Scientific Name"]]
    o = o.dropna(subset=["TaxonID"])
    o["TaxonID"] = o["TaxonID"].astype(int)
    return o\
            .set_index(["TaxonID"])\
            .drop(["Genus", "Species", "Scientific Name"], axis=1)\
            .sort_index()

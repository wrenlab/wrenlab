import os
import argparse
import sys

import h5py
import pandas as pd
import numpy as np

import wrenlab.ncbi.gene

class Archive(object):
    def __init__(self, path, taxon_id=None):
        self.path = path
        self.handle = h5py.File(path, mode="r")
        self.is_open = True

        if taxon_id is None:
            self.taxon_id = int(os.path.basename(path).split(".")[0])
        else:
            self.taxon_id = taxon_id

        self._load_indices()

    def __del__(self):
        self.close()

    def close(self):
        if self.is_open:
            self.handle.close()
            self.is_open = False

    def _load_indices(self):
        self.index = pd.Index([k.decode("utf-8") 
            for k in self.handle["meta/Sample_geo_accession"].value])
        self.index.name = "SampleID"

        genes = wrenlab.ncbi.gene.info(self.taxon_id)
        symbol_id = dict(zip(genes.Symbol, genes.index))
        self.columns = []
        self.column_mask = []
        for k in self.handle["meta/genes"].value:
            k = k.decode("utf-8")
            gene_id = symbol_id.get(k)
            self.column_mask.append(bool(gene_id))
            if gene_id:
                self.columns.append(gene_id)

        self.columns = pd.Index(self.columns)
        self.columns.name = "GeneID"
        self.column_mask = np.array(self.column_mask, dtype=np.bool)

    def dump(self, handle=sys.stdout):
        X = self.handle["data/expression"]
        print("", *list(self.columns), sep="\t", file=handle)
        for i in range(X.shape[0]):
            x = X[i,:][self.column_mask]
            print(self.index[i], *list(x), sep="\t", file=handle)

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("-t", "--taxon-id", type=int, required=True)
    p.add_argument("path")

    args = p.parse_args()
    archive = Archive(args.path, taxon_id=args.taxon_id)
    archive.dump()

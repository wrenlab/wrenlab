import os.path
import tempfile
import zipfile

import pandas as pd

import wrenlab.ext.paxtools
import wrenlab.ncbi.taxonomy
import wrenlab.util

def events(taxon_id):
    names = wrenlab.ncbi.taxonomy.names()
    name = names.loc[taxon_id,"Scientific Name"]

    url = "http://reactome.org/download/current/biopax.zip"
    path = str(wrenlab.util.download(url))
    with tempfile.TemporaryDirectory() as tmp:
        with zipfile.ZipFile(path, mode="r") as zf:
            fname = "{}.owl".format(name.replace(" ", "_"))
            zf.extract(fname, tmp)
            fname = os.path.join(tmp, fname)
            p = wrenlab.ext.paxtools.PAXTools(fname)
            for r in p.toSIF():
                yield r

def pathways(taxon_id):
    reactions_url = \
            "http://www.reactome.org/download/current/NCBI2Reactome_All_Levels.txt"
    with open(str(wrenlab.util.download(reactions_url))) as h:
        reactions = pd.read_csv(h, sep="\t",
                names=["GeneID", "ReactomeID", "URI", 
                    "Name", "Evidence", "SpeciesName"],
                dtype={"GeneID": str})
        ix = reactions.GeneID.str.isdigit()
        reactions = reactions.ix[ix,:]
        reactions.GeneID = reactions.GeneID.astype(int)
        return reactions

def reactions(taxon_id):
    reactions_url = \
            "http://www.reactome.org/download/current/NCBI2ReactomeReactions.txt"
    with open(str(wrenlab.util.download(reactions_url))) as h:
        reactions = pd.read_csv(h, sep="\t",
                names=["GeneID", "ReactomeID", "URI", 
                    "Name", "Evidence", "SpeciesName"],
                dtype={"GeneID": str})
        ix = reactions.GeneID.str.isdigit()
        reactions = reactions.ix[ix,:]
        reactions.GeneID = reactions.GeneID.astype(int)
        return reactions

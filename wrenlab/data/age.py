import collections
import functools
import zipfile

import pandas as pd
import numpy as np
import scipy.stats
import sklearn.metrics

import wrenlab.ncbi.taxonomy
import wrenlab.ncbi.gene
import wrenlab.util
from wrenlab.util import memoize
from wrenlab.util.pandas import align_series

def PetersGSEs():
    # from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4639797/bin/ncomms9570-s1.pdf
    return """
    GSE33321
    GSE48348
    GSE20332
    GSE20142
    GSE48556
    GSE49531
    GSE58137
    GSE47729
    GSE56035
    GSE48152
    GSE56045
    GSE36192
    GSE16214
    GSE33828
    GSE36382
    """.split()

def Harries():
    """
    http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3173580/
    """
    return pd.Series([-1,-1,-1,-1,-1,-1,1,1,1],
            index=[939,1236,1235,54674,57124,51176,10791,115362,6772],
            dtype=float)

@memoize
def Peters():
    SCHEMA = {
            "D-WB": 8,
            "R-WB": 13,
            "M": 17,
            "R-cerebellum": 22,
            "R-frontal_cortex": 27,
            "R-CD4+": 32,
            "R-CD8+": 36,
            "R-CD14+_monocyte": 41,
            "R-LCL": 45, # lymphoblastoid cell line
            "R-lymphocyte": 50,
            "R-PBMC": 55
    }

    url = "https://www.nature.com/article-assets/npg/ncomms/2015/151022/ncomms9570/extref/ncomms9570-s2.xlsx"
    path = str(wrenlab.util.download(url))
    D = pd.read_excel(path, na_values=["ns"], skiprows=2)\
            .drop_duplicates(subset=["NEW-Entrez-ID"])\
            .set_index(["NEW-Entrez-ID"])
    ks,vs = zip(*SCHEMA.items())
    o = D.iloc[:,[v-1 for v in vs]]
    #o.columns = pd.MultiIndex.from_tuples(ks, names=["VLevel", "Tissue"])
    o.columns = ks
    o.index.name = "Entrez Gene ID"
    return o

@memoize
def DeMaghales():
    url = "http://genomics.senescence.info/gene_expression/signatures_supplement.zip"
    path = str(wrenlab.util.download(url))
    def extract(sheet_name, skiprows):
        with zipfile.ZipFile(path) as zf:
            with zf.open("supplementary_tables.xls") as h:
                o = pd.read_excel(h, sheetname=sheet_name,
                        skiprows=list(range(skiprows)))\
                                .iloc[:,:30]\
                                .drop(["Name", "Symbol"], axis=1)\
                                .set_index(["EntrezGeneID"])
                o.index.name = "Entrez Gene ID"
                taxon_map = {"Rat": 10116, "Mouse": 10090, "Human": 9606}
                o.columns = pd.MultiIndex.from_tuples(
                        [(taxon_map[c.split("_")[0]], c.split("_",1)[1])
                            for c in o.columns],
                        names=["TaxonID", "Tissue"])
                return o.applymap(lambda x: 
                        float(x.replace("*", "") if isinstance(x, str) else x))

    up = extract("Genes_overexpressed", 14)
    down = extract("Genes_underexpressed", 9)
    duplicate = list(set(up.index) & set(down.index))
    up = up.drop(duplicate, axis=0)
    down = down.drop(duplicate, axis=0)
    return pd.concat([up, down])

@memoize
def GenAge():
    url = "http://genomics.senescence.info/genes/models_genes.zip"
    path = wrenlab.util.download(url)
    with zipfile.ZipFile(path) as zf:
        with zf.open("genage_models.csv") as h:
            o = pd.read_csv(h, encoding="iso-8859-1")\
                    .drop_duplicates(subset=["entrez gene id"])
            taxonomy = wrenlab.ncbi.taxonomy.names()
            taxon_name_id = dict(zip(taxonomy["Scientific Name"], map(int, taxonomy.index)))
            o["Entrez Gene ID"] = o["entrez gene id"]
            o["TaxonID"] = [taxon_name_id.get(organism) for organism in o["organism"]]
            o["DeltaLifespan"] = o["avg lifespan change (max obsv)"]
            return o.dropna(subset=["TaxonID", "DeltaLifespan"])\
                    .set_index(["TaxonID", "Entrez Gene ID"])\
                    .loc[:,"DeltaLifespan"]\
                    .sort_index()

@memoize
def LongevityMap():
    url = "http://genomics.senescence.info/longevity/longevity_genes.zip"
    path = wrenlab.util.download(url)
    with zipfile.ZipFile(path) as zf:
        with zf.open("longevity.csv") as h:
            o = pd.read_csv(h, encoding="iso-8859-1")\
                    .drop(["id"], axis=1)
            o["Variant"] = o["Variant(s)"]
            genes = wrenlab.ncbi.gene.info(9606)
            gene_name_id = dict(zip(genes["Symbol"], map(int, genes.index)))
            o["Entrez Gene ID"] = [gene_name_id.get(s) for s in o["Gene(s)"]]
            o = o.dropna(subset=["Entrez Gene ID"])\
                    .loc[:,["Entrez Gene ID", "Population", "Association", "Variant"]]
            o["Association"] = [1 if x == "Significant" else 0 for x in o["Association"]]
            return o

def controls():
    C = {
            "Peters": Peters().copy(),
            "DeMaghales": DeMaghales()[9606].copy(),
    }
    C["Harries"] = Harries().to_frame().copy()
    C["Harries"].columns = ["unknown"]
    ks, vs = zip(*C.items())
    return pd.concat(vs, axis=1, keys=ks, names=["ControlSet", "Tissue"])

def evaluate(coef, limit=1000):
    coef = coef.dropna().copy()
    if not coef.index.dtype == np.int:
        coef.index = list(map(int, coef.index))

    METRICS = collections.OrderedDict([
        ("N", lambda y, y_hat: 
            y.shape[0]),
        ("rho", lambda y, y_hat: 
            scipy.stats.spearmanr(y, y_hat)[0]),
        ("AUC", lambda y, y_hat: 
            sklearn.metrics.roc_auc_score(y.apply(np.sign), y_hat))
        #("SignAccuracy", lambda y, y_hat: 
        #    (y.apply(np.sign) == y_hat.apply(np.sign)).mean())
    ])
    
    #coef = fit.coef("Age").dropna()
    def do_limit(y, y_hat):
        y, y_hat = y.copy(), y_hat.copy()
        if (limit is not None) and (y.shape[0] > limit):
            y.sort_values(inplace=True)
            y = pd.concat([y.iloc[:int(limit / 2)], y.iloc[-int(limit / 2):]])
            y, y_hat = align_series(y, y_hat)
        return y, y_hat

    C = controls()
    o = []
    for c in C.columns:
        y = C[c].loc[C[c].apply(np.sign) != 0]
        y, y_hat = do_limit(*align_series(y, coef))
        oo = []
        for fn in METRICS.values():
            try:
                rs = fn(y, y_hat)
            except:
                rs = None
            oo.append(rs)
        o.append(tuple(oo))
    return pd.DataFrame(o, index=C.columns, columns=list(METRICS.keys()))\
            .round(2)\
            .sort_values("rho", ascending=False)

@memoize
def ATLAS(taxon_id, platform_id):
    """
    A simplistic interface to AGE ATLAS results for impatient people.
    """
    import wrenlab.normalize
    import wrenlab.impute
    import wrenlab.matrix.mmat
    import wrenlab.text.label_extraction.geo
    import wrenlab.lm
    import wrenlab.util.pandas

    path = "/net/data/ncbi/geo/GPL{}.mmat".format(platform_id)
    X = wrenlab.matrix.mmat.MMAT(path)
    A = wrenlab.text.label_extraction.geo.annotation(taxon_id, str_index=True)
    A = A.query("PlatformID == {}".format(platform_id))
    ix = A.index[((A.Age >= 25) & (A.Age <= 80))]
    A = A.loc[ix,:]

    ix = list(set(X.index) & set(A.index))
    A = A.loc[ix,:]
    X = pd.DataFrame(X.loc[ix,:].to_frame(), index=ix, columns=X.columns)
    X = wrenlab.util.pandas.trim_missing(X)
    Xi = wrenlab.impute.mean(X)
    Xn = wrenlab.normalize.quantile(Xi.T).T
    Xn = Xn.apply(np.log)
    A, X = A.align(Xn, axis=0, join="inner")
    coef = wrenlab.lm.fit(X, A, formula="Age").coef("Age")

    M = wrenlab.data.AILUN(platform_id)
    o = coef.groupby(M).mean()
    o.index = list(map(int, o.index))
    return o * 100

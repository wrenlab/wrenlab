import os
import xml.etree
import xml.etree.ElementTree
import zipfile

import pandas as pd

import wrenlab.util
from wrenlab.util import memoize

def _get_zipfile():
    url = "ftp://phoenix.omrf.hsc.net.ou.edu/public/msigdb.zip"
    path = str(wrenlab.util.download(url))
    return zipfile.ZipFile(path)

    with zipfile.ZipFile(path) as zf:
        xml_name = next(x for x in zf.namelist() if x.endswith(".xml"))
        with zf.open(xml_name) as h:
            metadata = _parse_metadata(h)
            return metadata

def _parse_metadata(handle):
    tree = xml.etree.ElementTree.parse(handle)
    root = tree.getroot()
    o = []
    for e in root.findall("GENESET"):
        #key = e.attrib["SYSTEMATIC_NAME"]
        key = e.attrib["STANDARD_NAME"]
        description = e.attrib["DESCRIPTION_BRIEF"]
        category = e.attrib["CATEGORY_CODE"]
        subcategory = e.attrib["SUB_CATEGORY_CODE"]
        organism = e.attrib["ORGANISM"]
        o.append((key, organism, category, subcategory, description))
    return pd.DataFrame.from_records(o, 
        columns=["TermID", "organism", "category", "subcategory", "description"])\
                .set_index("TermID")

@memoize
def metadata():
    with _get_zipfile() as zf:
        xml_name = next(x for x in zf.namelist() if x.endswith(".xml"))
        with zf.open(xml_name) as h:
            return _parse_metadata(h)

@memoize
def mapping():
    o = []
    with _get_zipfile() as zf:
        for name in zf.namelist():
            if not name.endswith(".entrez.gmt"):
                continue
            bn = os.path.basename(name)
            if not bn.startswith("msigdb"):
                continue
            with zf.open(name) as h:
                for line in h:
                    key, uri, *genes = line.decode("utf-8").rstrip("\n").split("\t")
                    genes = set(list(map(int, genes)))
                    for gene_id in genes:
                        o.append((gene_id, key, 1))
                return pd.DataFrame.from_records(o, 
                        columns=["ElementID", "TermID", "Score"])

@memoize
def matrix():
    df = mapping()
    return df.pivot(index="ElementID", columns="TermID", values="Score").fillna(0)

import networkx as nx
import pandas as pd
import numpy as np

import wrenlab.ncbi.gene
import wrenlab.ensembl
import wrenlab.util

VERSION = "10.5"
#URL = "http://stringdb-static.org/download/protein.links.v{version}/{taxon_id}.protein.links.v{version}.txt.gz"
URL = "http://stringdb-static.org/download/protein.links.v{version}/{taxon_id}.protein.actions.v{version}.txt.gz"

class STRING(object):
    def __init__(self, taxon_id):
        assert isinstance(taxon_id, int)
        path = str(wrenlab.util.download(URL.format(version=VERSION, taxon_id=taxon_id)))
        o = pd.read_csv(path, sep=" ", compression="gzip")
        o.columns=["Protein1", "Protein2", "Mode", "Action", "IsDirectional", "IsAActing", "Score"]
        o.Protein1 = o.Protein1.apply(lambda x: x.split(".")[1])
        o.Protein2 = o.Protein2.apply(lambda x: x.split(".")[1])

        release = wrenlab.ensembl.release(taxon_id)
        protein_gene = dict(release._db.run_sql_query("""
            SELECT DISTINCT protein_id, gene_id 
            FROM CDS 
            WHERE protein_id IS NOT NULL;"""))

        ensembl_entrez = dict(wrenlab.ncbi.gene.ensembl(taxon_id)\
                .loc[:,["Ensembl Gene ID", "GeneID"]]\
                .to_records(index=False))

        def protein_to_gene(protein_id):
            return ensembl_entrez.get(protein_gene.get(protein_id))

        o["Gene1"] = o.Protein1.apply(protein_to_gene)
        o["Gene2"] = o.Protein2.apply(protein_to_gene)
        o = o.loc[:,["Gene1","Gene2","Score"]].dropna()
        o.Gene1 = o.Gene1.astype(int)
        o.Gene2 = o.Gene2.astype(int)
        o = o.sort_values("Score", ascending=False)\
                .drop_duplicates(subset=["Gene1","Gene2"])

        self.taxon_id = taxon_id
        self._map = o

    def to_graph(self, subset=None):
        M = self._map
        if subset is not None:
            M = M.ix[(M.Gene1.isin(subset) | M.Gene2.isin(subset)),:]

        g = nx.Graph(taxon_id=self.taxon_id)
        def edges():
            for g1,g2,mode,action,is_directional,is_a_acting,score in M.to_records(index=False):
                yield (g1,g2,{"weight": score, "mode": mode})
        g.add_edges_from(edges())

        genes = wrenlab.ncbi.gene.info(9606)
        symbol = genes["Symbol"].dropna()
        id_symbol = dict(zip(symbol.index, symbol))
        for gene_id in g.nodes():
            try:
                g.node[gene_id]["symbol"] = id_symbol[gene_id]
            except KeyError:
                # probably should delete these...
                pass
        return g

import functools
from pkg_resources import Requirement, resource_stream

import pandas as pd

import wrenlab.dataset
import wrenlab.text.label_extraction.geo

@functools.lru_cache()
def labels(name):
    path = "wrenlab/data/geo_manual_labels_{}.tsv".format(name)
    with resource_stream(Requirement.parse("wrenlab"), path) as h:
        o = pd.read_csv(h, sep="\t")
        o.columns = ["SampleID", "TissueID", "Age", "Gender"]
        o.SampleID = [int(x[3:]) for x in o.SampleID]
        o = o.drop_duplicates(subset=["SampleID"])\
                .set_index(["SampleID"])
    o.Age /= 12
    o.TissueID = [int(x[4:]) if isinstance(x, str) else 0 for x in o.TissueID]
    o.TissueID = o.TissueID.astype(int)
    return o

"""
def platform(platform_id):
    # FIXME: infer taxon ID from platform ID
    # FIXME: read Client info from config

    taxon_id = 9606
    collapse_fn = "max_mean"
    A = wrenlab.text.label_extraction.geo.annotation(9606, str_index=True)
    c = matrixdb.Client()
    path = "ncbi/geo/gene/{collapse_fn}/GPL{platform_id}".format(
        collapse_fn=collapse_fn,
        platform_id=platform_id)
    X = c[path]

    ix = list(set(A.index) & set(X.row_index))
    A = A.ix[ix,:]
    X = X.rows(ix)
    return wrenlab.dataset.Dataset(X, row_annotation=A)
"""

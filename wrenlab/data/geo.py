import functools
from pkg_resources import Requirement, resource_stream

import pandas as pd

@functools.lru_cache()
def labels(name):
    path = "wrenlab/data/geo_manual_labels_{}.tsv".format(name)
    with resource_stream(Requirement.parse("wrenlab"), path) as h:
        o = pd.read_csv(h, sep="\t")
        o.columns = ["SampleID", "TissueID", "Age", "Gender"]
        o.SampleID = [int(x[3:]) for x in o.SampleID]
        o = o.drop_duplicates(subset=["SampleID"])\
                .set_index(["SampleID"])
    o.Age /= 12
    o.TissueID = [int(x[4:]) if isinstance(x, str) else 0 for x in o.TissueID]
    o.TissueID = o.TissueID.astype(int)
    return o

import io
import os
import os.path
from datetime import datetime
import urllib.parse
import urllib.request

import numpy as np
import pandas as pd
import requests

from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Integer, String, Date
from sqlalchemy import create_engine
from sqlalchemy.ext.hybrid import hybrid_property

import wrenlab.util
from wrenlab.util import LOG, memoize

Base = declarative_base()

ROOT_URL = "https://www.ebi.ac.uk/metagenomics"

TEMPLATES = {
    "sample_list": 
        "/samples/doExportTable?" + urllib.parse.urlencode({
            "searchTerm": "",
            "sampleVisibility": "ALL_PUBLISHED_SAMPLES",
            "search": "Search",
            "startPosition": "0"
        }),
    "project_list":
        "/projects/doExportDetails?" + urllib.parse.urlencode({
            "search": "Search",
            "studyVisibility": "ALL_PUBLISHED_PROJECTS"
        }),
    "project": "/projects/{project_id}/runs",
    "nchunks": "/projects/%s/samples/%s/runs/%s/results/versions/%s/%s/%s/chunks",
    "chunk": "/projects/%s/samples/%s/runs/%s/results/versions/%s/%s/%s/chunks/%s",
    "download":
        "/projects/{project_id}/samples/{sample_id}/runs/{run_id}"
        "/results/versions/{pipeline_version}/{domain}/{file_type}"
}
TEMPLATES = {k:(ROOT_URL + v) for k,v in TEMPLATES.items()}

TAXONOMY_FILE_TYPES = [
        "5S-rRNA-FASTA", 
        "16S-rRNA-FASTA", 
        "23S-rRNA-FASTA", 
        "OTU-TSV",
        "OTU-BIOM", 
        "OTU-table-HDF5-BIOM", 
        "OTU-table-JSON-BIOM", 
        "NewickTree",
        "NewickPrunedTree"
]

class Project(Base):
    __tablename__ = "project"
    id = Column(String, primary_key=True)
    name = Column(String)
    date_submitted = Column(Date)
    
    samples = relationship("Sample", back_populates="project")

class Sample(Base):
    __tablename__ = "sample"
    id = Column(String, primary_key=True)
    name = Column(String)
    biome = Column(String)

    project_id = Column(String, ForeignKey("project.id"))
    project = relationship("Project", back_populates="samples")

    runs = relationship("Run", back_populates="sample")

class Run(Base):
    __tablename__ = "run"
    id = Column(String, primary_key=True)

    sample_id = Column(String, ForeignKey("sample.id"))
    sample = relationship("Sample", back_populates="runs")

    def link(self, domain, file_type, pipeline_version):
        url = TEMPLATES["download"].format(
                project_id=self.sample.project.id,
                sample_id=self.sample.id,
                run_id=self.id,
                pipeline_version=pipeline_version,
                domain=domain,
                file_type=file_type)
        return url

    def download(self, domain, file_type, pipeline_version="2.0"):
        # assumes:
        #domain = "taxonomy"
        #file_type = "OTU-TSV"
        url = self.link(domain, file_type, 
                pipeline_version=pipeline_version)
        path = wrenlab.util.download(url, save_on_failure=True)
        with open(path, "r") as h:
            data = "\n".join(list(h)[1:])
        with io.StringIO(data) as h:
            o = pd.read_csv(h, sep="\t")
            o.columns = ["OTU", "Value", "Taxonomy"]
        return o.loc[:,["OTU", "Taxonomy", "Value"]]

def annotation():
    portal = Portal()
    o = []
    for run in portal._session.query(Run):
        o.append((run.id, run.sample.id, run.sample.project.id, run.sample.biome))
    o = pd.DataFrame.from_records(o, 
            columns=["RunID", "SampleID", "ProjectID", "Biome"])
    return o.set_index(["RunID"])

class Portal(object):
    def __init__(self, pipeline_version="2.0", expire=None):
        self._pipeline_version = pipeline_version
        self._expire = expire

        db_path = os.path.expanduser("~/.cache/wrenlab/metagenome.db")
        create = not os.path.exists(db_path)
        os.makedirs(os.path.dirname(db_path), exist_ok=True)

        self._engine = create_engine("sqlite:///{path}".format(path=db_path))
        Base.metadata.create_all(self._engine)
        Session = sessionmaker(bind=self._engine)
        self._session = Session()

        if create:
            LOG.info("Populating EBI metagenome database ...")
            self.populate_database()

    def _sample_list(self):
        path = wrenlab.util.download(TEMPLATES["sample_list"], expire=self._expire)
        with open(path, "r") as h:
            o = pd.read_csv(h, warn_bad_lines=False, error_bad_lines=False).iloc[:,:4]
        o.columns = ["Biome", "SampleID", "SampleName", "ProjectName"]
        o = o.set_index(["SampleID"])
        return o

    def _project_list(self):
        path = wrenlab.util.download(TEMPLATES["project_list"], expire=self._expire)
        with open(path, "r") as h:
            o = pd.read_csv(h, 
                    warn_bad_lines=False,
                    error_bad_lines=False, 
                    parse_dates=["Submitted Date"]).iloc[:,:4]
        o.columns = ["ProjectID", "ProjectName", "NSamples", "DateSubmitted"]
        return o.set_index(["ProjectID"])

    def _join_table(self):
        o = []
        for project_id in self._project_list().index:
            url = TEMPLATES["project"].format(project_id=project_id)
            path = str(wrenlab.util.download(url, expire=self._expire))

            with open(path, "r") as h:
                data = pd.read_csv(h, header=None, 
                        names=["ProjectID", "SampleID", "RunID"])
                o.append(data)

        # Find subset of projects & samples in all 3 tables
        jt = pd.concat(o).drop_duplicates()
        sl = self._sample_list()
        pl = self._project_list()
        sample_ids = set(sl.index) & set(jt.SampleID)
        project_ids = set(pl.index) & set(jt.ProjectID)
        sl = sl.loc[list(sample_ids)]
        pl = pl.loc[list(project_ids)]
        jt = jt.ix[jt.SampleID.isin(sample_ids) & jt.ProjectID.isin(project_ids),:]

        # combine data from tables
        biome = dict(zip(sl.index, sl.Biome))
        sample_name = dict(zip(sl.index, sl.SampleName))
        project_name = dict(zip(pl.index, pl.ProjectName))
        date_submitted = dict(zip(pl.index, pl.DateSubmitted))
        jt["Biome"] = [biome[ix] for ix in jt["SampleID"]]
        jt["SampleName"] = [sample_name[ix] for ix in jt["SampleID"]]
        jt["ProjectName"] = [project_name[ix] for ix in jt["ProjectID"]]
        jt["DateSubmitted"] = [date_submitted[ix] for ix in jt["ProjectID"]]
        return jt

    def _otu_to_taxonomy(self):
        o = {}
        for run in self._session.query(Run).all():
            try:
                table = run.download("taxonomy", "OTU-TSV")
            except:
                continue
            for otu_id, taxonomy, _ in table.to_records(index=False):
                o[otu_id] = taxonomy
        return o

    def annotation(self):
        return annotation()

    def matrix(self, domain="taxonomy", file_type="OTU-TSV", pipeline_version="2.0"):
        o = []
        for run in self._session.query(Run).limit(1000):
            try:
                v = run.download(domain, file_type, pipeline_version=pipeline_version)
                v.name = run.id
                o.append(v)
            except:
                continue
        return pd.concat(o, axis=1).T.fillna(0)

    def links(self, 
            domain="taxonomy", 
            file_type="OTU-TSV", 
            pipeline_version="2.0",
            limit=None):
        o = {}
        q = self._session.query(Run)
        runs = q.all() if limit is None else q.limit(limit)
        for run in runs:
            o[run.id] = run.link(domain, file_type,
                    pipeline_version=pipeline_version)
        return o
    
    def run(self, accession):
        return self._session.query(Run).filter_by(id=accession).first()

    def populate_database(self):
        jt = self._join_table()

        projects = []
        it = jt.loc[:,["ProjectID", "ProjectName", "DateSubmitted"]]\
                .drop_duplicates(subset=["ProjectID"])\
                .to_records(index=False)
        for project_id, name, dt64 in it:
            ts = (dt64 - np.datetime64('1970-01-01T00:00:00Z')) \
                    / np.timedelta64(1, 's')
            date_submitted = datetime.utcfromtimestamp(ts).date()
            p = Project(id=project_id, name=name, date_submitted=date_submitted)
            projects.append(p)
        self._session.add_all(projects)
        self._session.commit()

        samples = []
        it = jt.loc[:,["ProjectID", "SampleID", "SampleName", "Biome"]]\
                .drop_duplicates(subset=["SampleID"])\
                .to_records(index=False)
        for project_id, sample_id, name, biome in it:
            s = Sample(id=sample_id, name=name, biome=biome, project_id=project_id)
            samples.append(s)
        self._session.add_all(samples)
        self._session.commit()

        runs = []
        it = jt.loc[:,["SampleID", "RunID"]]\
                .drop_duplicates(subset=["RunID"])\
                .to_records(index=False)
        for sample_id, run_id in it:
            r = Run(id=run_id, sample_id=sample_id)
            runs.append(r)
        self._session.add_all(runs)
        self._session.commit()



#domain = "taxonomy"
#file_type = "OTU-TSV"
#file_type = "OTU-table-JSON-BIOM"

"""
for sample_id, run_id in data.drop_duplicates().to_records(index=False):
    url = TEMPLATES["download"].format(pipeline_version=self._pipeline_version,
        **locals())
    print(url)
    #print(requests.get(url).text)
    import urllib
    print(urllib.request.urlopen(url).read())
    break
"""

if __name__ == "__main__":
    #"OTU-TSV"
    #study_id = "ERP009703"
    #study_id = "SRP053343"
    #study_id = "SRP044901"
    #study_id = "ERP001384"
    #study_id = "SRP038712"
    #study_id = "ERP001506"
    #study_id = "SRP056169"
    #study_id = "ERP001736"
    #portal = Portal(pipeline_version="3.0")
    #portal.download_study(study_id)
    pass

    """
    study = Study(study_id)
    for sample in study.samples:
        for run_id in sample._runs:
            print(sample.id, run_id)

    #download_taxonomy(study_id)
    """

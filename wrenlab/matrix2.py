import itertools

import h5py
import numpy as np
import pandas as pd

def make_index(xs):
    if all([x.isdigit() for x in xs]):
        xs = [int(x) for x in xs]
        return list(map(int, np.array(xs, dtype=int)))
    else:
        return np.array(xs, dtype=bytes)

class Index(list):
    def __init__(self, elements):
        super().__init__(elements)
        self.inv = {ix:i for i,ix in enumerate(elements)}

def decode_index(xs):
    if xs.dtype == np.int64:
        o = xs[:]
    else:
        o = np.char.decode(xs[:], "utf-8")
    return Index(o)

class Matrix(object):
    def __init__(self, group):
        self.group = group
        self.index = decode_index(group["index/0"])
        self.columns = decode_index(group["index/1"])

    @property
    def shape(self):
        return (len(self.index), len(self.columns))

    def dump(self, handle):
        print(*[""] + list(self.columns), sep="\t", file=handle)
        for i,key in enumerate(self.index):
            x = self.row(key)
            print(*[key] + list(x), sep="\t", file=handle)

    def row(self, key):
        ix = self.index.inv[key]
        data = self.group["data/0"][ix,:]
        o = pd.Series(data, index=self.columns)
        o.name = key
        return o

    def column(self, key):
        ix = self.columns.inv[key]
        data = self.group["data/1"][ix,:]
        o = pd.Series(data, index=self.index)
        o.name = key
        return 0

    def rows(self, keys):
        ix = [self.index.inv[k] for k in keys]
        X = self.group["data/0"][ix,:]
        return pd.DataFrame(X, index=keys, columns=self.columns)

    def columns(self, keys):
        ix = [self.columns.inv[k] for k in keys]
        X = self.group["data/1"][ix,:]
        return pd.DataFrame(X, index=keys, columns=self.index).T

class MatrixDB(object):
    def __init__(self, path, read_only=True):
        mode = "r" if read_only is True else "a"
        self.root = h5py.File(path)

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, key):
        return Matrix(self.root[key])

    def put(self, key, path_or_handle):
        chunks = pd.read_csv(path_or_handle, sep="\t", index_col=0, chunksize=100)
        first = next(chunks)
        chunks = itertools.chain([first], chunks)

        columns = make_index(first.columns)
        nc = len(columns)

        group = self.root.create_group(key)
        g_data = group.create_group("data")
        g_data0 = g_data.create_dataset("0", (0, nc), maxshape=(None, nc), chunks=True)

        # Insert data
        index = []
        nr = 0
        for chunk in chunks:
            index.extend(chunk.index)
            nr += chunk.shape[0]
            g_data0.resize((nr, nc))
            g_data0[(nr - chunk.shape[0]):,:] = np.array(chunk)

        # Insert indices
        index = make_index(index)
        nr = len(index)
        g_index = group.create_group("index")
        g_index0 = g_index.create_dataset("0", data=index)
        g_index1 = g_index.create_dataset("1", data=columns)

        # Insert transposition
        g_data1 = g_data.create_dataset("1", (nc, nr))
        step = 100
        for j in np.arange(0, nc, step):
            X = g_data0[:,j:(j+step)]
            g_data1[j:(j+step),:] = X.T

        return Matrix(group)


    @staticmethod
    def load(path, handle):
        pass

import sys

import click

@click.group()
def cli():
    """
    MatrixDB command-line interface.
    """
    pass

@cli.command()
@click.argument("db_path")
@click.argument("matrix_key")
def load(db_path, matrix_key):
    """
    Load a matrix into a MatrixDB.
    """
    db = MatrixDB(db_path, read_only=False)
    db.put(matrix_key, sys.stdin)

@cli.command()
@click.argument("db_path")
@click.argument("matrix_key")
def dump(db_path, matrix_key):
    """
    Export a matrix from MatrixDB.
    """
    db = MatrixDB(db_path, read_only=False)
    X = db[matrix_key]
    X.dump(sys.stdout)

@cli.command()
@click.argument("db_path")
def ls(db_path):
    db = MatrixDB(db_path, read_only=False)
    for k in db:
        print(k, db[k].shape)

if __name__ == "__main__":
    cli()
    #db = MatrixDB("test.h5", read_only=False)
    #X = db.put("test", "test.tsv")
    #print(X.row("R15"))
    #print(X.column("C22"))

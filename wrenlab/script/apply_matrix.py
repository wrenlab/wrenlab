"""
Apply a function to each row of a matrix on stdin, outputting the results to stdout.
"""

import importlib
import sys
import multiprocessing as mp

import click

import wrenlab

@click.command()
@click.option("--delimiter", "-d", default="\t")
@click.option("--ncpu", "-p", default=mp.cpu_count() - 2, type=int)
@click.argument("function")
def main(function, delimiter, ncpu):
    module_name, function_name = function.rsplit(".", 1)
    module = importlib.import_module(module_name)
    fn = getattr(module, function_name)

    it = wrenlab.read_matrix(sys.stdin, sep=delimiter)
    pool = mp.Pool(ncpu)
    try:
        rs = filter(lambda x: x is not None, pool.imap(fn, it))
        wrenlab.write_matrix(sys.stdout, rs, sep=delimiter)
    finally:
        pool.terminate()

if __name__ == "__main__":
    main()

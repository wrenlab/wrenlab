import tempfile

import wrenlab.ncbi.geo

if __name__ == "__main__":
    tempfile.tempdir = "/data/tmp"

    db = wrenlab.ncbi.geo.GEO("/data/ncbi/geo2/")
    accession = "GPL570" # small test
    db.sync()
    #X = db.probe[accession]
    #print(X.shape)
    #accession = "GPL571"

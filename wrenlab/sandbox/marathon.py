import pandas as pd
import numpy as np
import pybedtools as pb
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.stats as ss
from statsmodels.stats import contingency_tables

class enrichment_set(object):
    def __init__(self,
                features = None,
                background = None,
                samples = None,
                taxon_id = '9606',
                generichs = None,
                regrichs = None,
                islerichs = None,
                repriches = None
                ):
        
        """
        Usage: Provide a background (whole genome, capture set, detected genes, etc)
        and samples (upregulated, downregulated, etc)
        as locations of bed files on disk
        
        functions that operate on enrichsets:
        regmap() - transcription regulation enrichment
        repmap() - repetitive element enrichment
        genemap() - genic enrichment
        islemap() - CpG Island enrichment
        """
        #prepare genome annotations
        self.features = {}
        background_template = ('{taxonid}_{feature}.bed')
        #genic contexts
        self.features['genes'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'genes'))
        self.features['exons'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'exons'))
        self.features['introns'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'introns'))
        #CpG island contexts
        self.features['promoters'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'promoters'))
        self.features['islands'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'islands'))
        self.features['shores'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'shores'))
        self.features['shelves'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'shelves'))
        #Genomic Regulation contexts
        self.features['regels'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'oreg'))
        self.features['eqtls'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'eqtlfilt'))
        self.features['dnase'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'dnasefilt'))
        self.features['enhancers'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'regels'))
        #Repetitive element contexts
        self.features['simplerep'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'simple_repeats'))
        self.features['microsats'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'microsats'))
        self.features['repeats'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'repeats'))
        self.features['near'] = pb.BedTool(background_template.format(taxonid = taxon_id, feature = 'simple_repeats_near'))
        
        self._samples = samples
        self._background = background  
        
        """
        The Xrich functions generate the observed values for samples and background
        in their respective genomic contexts.
        """
        
        def generich(self):
            input_files = self._samples + self._background
            sample_template = ('{sampleid}.bed')
            out_df = pd.DataFrame()
            for i in input_files:
                sample = pb.BedTool(sample_template.format(sampleid = i))
                input_count = sample.count()
                gene_count = sample.intersect(genome.features['genes'], u = True).count()
                ex_count = sample.intersect(genome.features['exons'], u = True).count()
                in_count = sample.intersect(genome.features['introns'], u = True).count()
                prom_count = sample.intersect(genome.features['promoters'], u = True).count()
                inter_count = (input_count - (prom_count + gene_count))
                count_ser = pd.Series([input_count, gene_count, ex_count,
                                       in_count, prom_count, inter_count])
                count_ser.index = ['Input', 'Genes', 'Exons', 'Introns', 'Promoters', 'Intergenic']
                out_df[i] = count_ser
            return out_df
        
        self._generichs = generich(self)
        
        def regrich(self):
            input_files = self._samples + self._background
            sample_template = ('{sampleid}.bed')
            out_df = pd.DataFrame()
            for i in input_files:
                sample = pb.BedTool(sample_template.format(sampleid = i))
                input_count = sample.count()
                gene_count = sample.intersect(genome.features['genes'], u = True).count()
                ex_count = sample.intersect(genome.features['exons'], u = True).count()
                in_count = sample.intersect(genome.features['introns'], u = True).count()
                prom_count = sample.intersect(genome.features['promoters'], u = True).count()
                inter_count = (input_count - (prom_count + gene_count))
                count_ser = pd.Series([input_count, gene_count, ex_count,
                                       in_count, prom_count, inter_count])
                count_ser.index = ['Input', 'Genes', 'Exons', 'Introns', 'Promoters', 'Intergenic']
                out_df[i] = count_ser
            return out_df
        
        def regrich(enrichment_set):
            input_files = enrichment_set._samples + enrichment_set._background
            sample_template = ('{sampleid}.bed')
            out_df = pd.DataFrame()
            for i in input_files:
                sample = pb.BedTool(sample_template.format(sampleid = i))
                input_count = sample.count()
                regel_count = sample.intersect(genome.features['regels'], u = True).count()
                enh_count = sample.intersect(genome.features['enhancers'], u = True).count()
                eqtl_count = sample.intersect(genome.features['eqtls'], u = True).count()
                dnase_count = sample.intersect(genome.features['dnase'], u = True).count()
                count_ser = pd.Series([input_count, regel_count, enh_count,
                                       eqtl_count, dnase_count])
                count_ser.index = ['Input', 'Regels', 'Enhancers', 'EQTLs', 'DNAse']
                out_df[i] = count_ser
            return out_df
        
        self._regrichs = regrich(self)
        
        def islerich(enrichment_set):
            input_files = enrichment_set._samples + enrichment_set._background
            sample_template = ('{sampleid}.bed')
            out_df = pd.DataFrame()
            for i in input_files:
                sample = pb.BedTool(sample_template.format(sampleid = i))
                input_count = sample.count()
                isle_count = sample.intersect(genome.features['islands'], u = True).count()
                shore_count = sample.intersect(genome.features['shores'], u = True).count()
                shelf_count = sample.intersect(genome.features['shelves'], u = True).count()
                sea_count = (input_count - (isle_count + shore_count + shelf_count))
                count_ser = pd.Series([input_count, isle_count, shore_count,
                                       shelf_count, sea_count])
                count_ser.index = ['Input', 'Island', 'Shore', 'Shelf', 'Sea']
                out_df[i] = count_ser
            return out_df
        
        self._islerichs = islerich(self)
        
        def reprich(enrichment_set):
            input_files = enrichment_set._samples + enrichment_set._background
            sample_template = ('{sampleid}.bed')
            out_df = pd.DataFrame()
            for i in input_files:
                sample = pb.BedTool(sample_template.format(sampleid = i))
                input_count = sample.count()
                simple_count = sample.intersect(genome.features['simplerep'], u = True).count()
                micro_count = sample.intersect(genome.features['microsats'], u = True).count()
                allreps_count = sample.intersect(genome.features['repeats'], u = True).count()
                simpnear_count = sample.intersect(genome.features['near'], u = True).count()
                count_ser = pd.Series([input_count, simple_count,
                                      micro_count, allreps_count, simpnear_count])
                count_ser.index = ['Input', 'Simple', 'Micro', 'Allreps', 'Near Simple']
                out_df[i] = count_ser
            return out_df
        
        self._reprichs = reprich(self)

"""
Bars generates the contingency tables, odds ratios, and final plots
for each sample with respect to the provided background
"""

def bars(exp_vector_list, bg_vector, outfile):
    #outfiles expects N file names as a list for output enrichments
    exdf = pd.DataFrame()
    enrich_df = pd.DataFrame()
    top_df = pd.DataFrame()
    bot_df = pd.DataFrame()
    listnames = str
    import scipy.stats as ss
    for i in exp_vector_list:
        featurevec = []
        citops = []
        cibots = []
        for j in i.index[1:]:
            exp_in_feat = i.loc[j]
            exp_notin = i[0] - exp_in_feat 
            bck_in_feat = bg_vector.loc[j]
            bck_notin = bg_vector[0] - bg_vector.loc[j]
            table = np.asarray([[exp_in_feat, bck_in_feat], [exp_notin, bck_notin]])
            tempt = contingency_tables.Table2x2(table)
            oddsrat = tempt.log_oddsratio
            pval = tempt.log_oddsratio_pvalue()
            CI = tempt.log_oddsratio_confint()
            citops.append(CI[0])
            cibots.append(CI[1])
            print([pval, j, i.name, CI])
            featurevec.append(oddsrat)
        enrich_df[i.name] = pd.Series(featurevec)
        top_df[i.name] = pd.Series(citops)
        bot_df[i.name] = pd.Series(cibots)
    enrich_df.index = i.index[1:]
    enrich_df['features'] = enrich_df.index
    top_df.index = i.index[1:]
    bot_df.index = i.index[1:]
    top_df['features'] = enrich_df.index
    bot_df['features'] = enrich_df.index
    topdf = pd.melt(top_df, id_vars = 'features', var_name = 'group', value_name = 'top_CI')
    botdf = pd.melt(bot_df, id_vars = 'features', var_name = 'group', value_name = 'bot_CI')
    plotdf = pd.melt(enrich_df, id_vars='features', var_name="group", value_name="Log Odds Ratio")
    yerr = [plotdf['Log Odds Ratio'] - botdf.bot_CI, topdf.top_CI - plotdf['Log Odds Ratio']]
    fig, ax = plt.subplots()
    ax = sns.barplot(data = plotdf, x = 'features', y = 'Log Odds Ratio', hue = 'group')
    sns.boxplot(data = topdf, x = 'features', y = 'top_CI', hue = 'group')
    sns.boxplot(data = botdf, x = 'features', y = 'bot_CI', hue = 'group')
    plt.ylim(-2, 2)
    ax.legend_.remove()
    plt.savefig(outfile, format = 'eps')
    plt.show()

def genemap(enrichset, outfile):
    evl = enrichset._samples
    bgl = enrichset._background[0]
    exp_vector_list = []
    bg_vector = pd.Series(enrichset._generichs.loc[:,bgl])
    for samp in evl:
        exp_vector_list.append(enrichset._generichs.loc[:,samp])
    bars(exp_vector_list, bg_vector, outfile + 'genes.eps')
    return None

def islemap(enrichset, outfile):
    evl = enrichset._samples
    bgl = enrichset._background[0]
    exp_vector_list = []
    bg_vector = pd.Series(enrichset._islerichs.loc[:,bgl])
    for samp in evl:
        exp_vector_list.append(enrichset._islerichs.loc[:,samp])
    bars(exp_vector_list, bg_vector, outfile + 'islands.eps')
    return None

def regmap(enrichset, outfile):
    evl = enrichset._samples
    bgl = enrichset._background[0]
    exp_vector_list = []
    bg_vector = pd.Series(enrichset._regrichs.loc[:,bgl])
    for samp in evl:
        exp_vector_list.append(enrichset._regrichs.loc[:,samp])
    bars(exp_vector_list, bg_vector, outfile+ 'regulatory.eps')
    return None

def repmap(enrichset, outfile):
    evl = enrichset._samples
    bgl = enrichset._background[0]
    exp_vector_list = []
    bg_vector = pd.Series(enrichset._reprichs.loc[:,bgl])
    for samp in evl:
        exp_vector_list.append(enrichset._reprichs.loc[:,samp])
    bars(exp_vector_list, bg_vector, outfile + 'repeats.eps')
    return None

def makeset(taxon_id, samples, background):
    enrichset = enrichment_set(taxon_id = taxon_id,
                              samples = samples,
                              background = background)
    return enrichset

def annotate(taxon_id, samples, background, outfile_header):
    enrichset = enrichment_set(taxon_id = taxon_id,
                               samples = samples,
                               background = background)
    genemap(enrichset, outfile_header)
    regmap(enrichset, outfile_header)
    islemap(enrichset, outfile_header)
    repmap(enrichset, outfile_header)        
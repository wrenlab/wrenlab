import numpy as np
import scipy.stats

import wrenlab.ncbi.geo
import wrenlab.data.STRING

def main():
    DS = wrenlab.ncbi.geo.platform(570)
    q = DS.eq("TissueName", "blood")

    def fn(pairs):
        o = []
        for g1,g2 in pairs:
            v1 = X.loc[:,g1]
            v2 = X.loc[:,g2]
            r = scipy.stats.pearsonr(v1,v2)[0]
            o.append(r)
        o = np.array(o)
        return o.mean(), o.std()

    o = {}
    for experiment_id, subset in q.groupby("ExperimentID"):
        if len(o) == 5:
            break
        try:
            print(experiment_id)
            if subset.row_annotation.shape[0] < 20:
                continue
            X = subset.preprocess().to_frame()
            ix = set(X.columns)
            ppi = set((g1,g2) for (g1,g2) in wrenlab.data.STRING.STRING(9606).to_graph().edges()
                if g1 in ix and g2 in ix)
            n = len(ppi)
            ix = np.array(list(ix))
            random = set(zip(np.random.choice(ix, size=n), np.random.choice(ix, size=n)))
            random -= ppi
            o[experiment_id] = list(fn(ppi)) + list(fn(random))
        except Exception as e:
            print(e)
            continue
    return o

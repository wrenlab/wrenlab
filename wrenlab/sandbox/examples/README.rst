========
Examples
========

This directory contains example code for using various aspects of the
**wrenlab** codebase.

These examples are NO WARRANTY: when an example is considered stable, it will
be moved to the Sphinx documentation.

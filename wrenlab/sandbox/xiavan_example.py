import wrenlab.ncbi.geo
import mmat
import tempfile

if __name__ == "__main__":
    tempfile.tempdir = "/data/tmp"
    db = wrenlab.ncbi.geo.GEO("/data/xiavan")
    accession = "GPL13534"
    #accession = "GPL97"
    db.sync(accession)
    X = db.probe[accession]
    print(X.shape)

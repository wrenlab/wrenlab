import sys
sys.path.append("/home/gilesc/code/maui")

import numpy as np
import pandas as pd
import statsmodels.api as sm
import joblib

import maui.data
import wrenlab.mixed_effects
import wrenlab.data
import wrenlab.impute
import wrenlab.dataset
import wrenlab.projection
from wrenlab.util import memoize

@memoize
def mixed():
    formula = "Age + C(TissueID)"
    DS = maui.data.dataset(570)
    q = DS\
        .has("Age")\
        .most_frequent("TissueID", 10)\
        .between("Age", 25, 70)\
        .filter("ExperimentID", lambda A: A.Age.max() - A.Age.min() >= 10)

    Y = q.to_frame().dropna(axis=0)
    D = q.row_annotation
    Y,D = Y.align(D, join="inner", axis=0)
    return wrenlab.lm.fit(Y, D, formula=formula, group="ExperimentID")

@memoize
def _per_experiment(binarize=False):
    import wrenlab.data.age
    formula = "Age + C(TissueID)"
    DS = maui.data.dataset(570)
    q = DS\
        .has("Age")\
        .most_frequent("TissueID", 10)\
        .between("Age", 25, 70)\
        .filter("ExperimentID", lambda A: A.Age.max() - A.Age.min() >= 10)

    o = {}
    for experiment_id, subset in q.groupby("ExperimentID"):
        Y = subset.to_frame().dropna(axis=0)
        D = subset.row_annotation
        Y,D = Y.align(D, join="inner", axis=0)
        if not Y.shape[0] > 25:
            continue
        if binarize:
            D = D.copy()
            D.Age = (D.Age > D.Age.mean()).astype(int).astype(float)
        try:
            o[experiment_id] = wrenlab.lm.fit(Y, D, formula=formula)
        except Exception as e:
            print(e)
            continue
    return o

def per_experiment(binarize=False):
    import wrenlab.projection
    import wrenlab.outlier
    import wrenlab.ncbi.gene

    rs = _per_experiment(binarize=binarize)
    def fn(variable):
        o = {}
        for k, fit in rs.items():
            # sometimes categoricalterm, need more explicit filter
            try:
                o[k] = fit["Age"].summary()[variable]
            except:
                pass
        o = pd.DataFrame(o)
        o.columns.name = "ExperimentID"
        return o

    p = fn("p")

    projection = wrenlab.projection.from_dict(wrenlab.data.AILUN(570))\
            .min(p.mean(axis=1))
    M = {k:projection.project(fn(k).T).T.dropna(how="all") for k in ["coef", "t", "p"]}

    pc = wrenlab.projection.PCA(M["coef"], k=10).to_frame()
    ok = wrenlab.outlier.remove(pc, iterations=10).index

    return {k:M[k].loc[:,ok] for k in M}

def per_experiment_summary(binarize=False):
    M = per_experiment(binarize=binarize)

    def s(name, x):
        x.name = name
        return x

    agreement = s("Agreement", M["coef"].apply(np.sign).mean(axis=1).abs())
    coef = s("coef", M["coef"].mean(axis=1))
    genes = wrenlab.ncbi.gene.info(9606)
    return pd.concat([genes, coef, agreement], axis=1)\
            .dropna(subset=["coef", "Agreement"])\
            .sort_values("Agreement", ascending=False)


"""
#@memoize
def mixed1():
    formula = "Age + C(TissueID)"
    DS = maui.data.dataset(570)
    query = list(set(targets()["COS"]) |  set(targets()["RAS"]))
    q = DS\
        .has("Age")\
        .most_frequent("TissueID", 10)\
        .between("Age", 25, 70)\
        .filter("ExperimentID", lambda A: A.Age.max() - A.Age.min() >= 10)
        #.isin("GeneID", query, axis=1)

    Y = q.to_frame().dropna(axis=0)
    D = q.row_annotation
    Y,D = Y.align(D, join="inner", axis=0)
    return wrenlab.lm.fit(Y, D, formula=formula, group="ExperimentID")
"""

def characteristic_direction(experiment_id):
    import chdir
    DS = maui.data.dataset(570)
    q = DS.has("Age").between("Age", 25, 70).eq("ExperimentID", experiment_id)
    A = q.row_annotation
    X = q.to_frame()
    ix = A.Age < A.Age.mean()
    X_low = X.loc[ix,:]
    X_high = X.loc[~ix,:]
    #if n is not None:
    #    X_low = X_low.iloc[:n,:]
    #    X_high = X_high.iloc[:n,:]

    o = chdir.chdir(np.array(X_high).T, np.array(X_low).T)
    o = pd.Series(o.flat, index=X.columns)
    projection = wrenlab.projection.from_dict(wrenlab.data.AILUN(570)).min(o.abs())
    return projection.project(o).dropna()
       
@memoize
def big():
    DS = maui.data.dataset(570)
    q = DS\
        .has("Age")\
        .most_frequent("TissueID", 10)\
        .between("Age", 25, 70)\
        .filter("ExperimentID", lambda A: A.Age.max() - A.Age.min() >= 10)\
        .eq("ExperimentID", 76705)
    return wrenlab.dataset.Dataset(q.to_frame(), 
            row_annotation=q.row_annotation, 
            metadata={"ExperimentID":list(set(list(q.row_annotation.ExperimentID)))[0]})
 
def synthetic_data():
    np.random.seed(0)
    N = 1000
    group = np.random.randint(0, high=5, size=N)
    age = np.random.randint(30, high=101, size=N)
    intercept = 5
    epsilon = np.random.random(size=N) * 0.1
    y = intercept + (0.05 * age) + (group + 1) * 1.2 + epsilon

    X = pd.DataFrame({"Age": age, "ExperimentID": group})
    X.ExperimentID = X.ExperimentID.astype("category")
    return y,X

def synthetic():
    y,X = synthetic_data()
    model = wrenlab.mixed_effects.Model()
    return model.fit(y, X, groups=["ExperimentID"])

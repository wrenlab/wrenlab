import pandas as pd
import numpy as np
import sklearn.cluster

import wrenlab.ncbi.gene
from age_atlas.model import data, realign, pca, filter_xy

def gender_data(xy=False, pc=False, ranks=False):
    A,X = data(preprocess=True, impute=True, boxcox=False)
    if pc is True:
        X = pca(xy=xy, ranks=ranks)
    else:
        if xy is True:
            X = filter_xy(X)
        if ranks is True:
            X = X.apply(np.argsort, axis=1)
    A = A.dropna(subset=["Gender"])
    A,X = realign(A,X)
    return A,X
    
def gender_problem(pc=False):
    A,X = data(preprocess=True, impute=True, boxcox=False)
    A = A.ix[A.Gender.isin(["M","F"]),:]
    #A = A.ix[A.PlatformID == 570,:]
    if pc is True:
        X = pca(xy=True)
    else:
        genes = wrenlab.ncbi.gene.info(9606)
        ix = list(set(genes.index[genes.Chromosome.isin(["X","Y"])]) & set(X.columns))
        X_XY = X.loc[:,ix]
        #mu = X_XY.groupby(genes.Chromosome, axis=1).mean()
        #X = mu
        X = X_XY
    A,X = realign(A,X)
    y = A.Gender == "M"
    return wrenlab.ml.Problem(y, X)

def predict_by_platform(model, pc=False, xy=False, ranks=False):
    A,X = gender_data(pc=pc,xy=xy,ranks=ranks)

    o,ix = [],[]
    for platform_id, As in A.groupby("PlatformID"):
        if As.shape[0] <= 1000:
            continue
        Xs = X.loc[As.index,:]
        y = As.Gender == "M"
        if len(set(y)) < 2:
            continue
        if len(set(As.ExperimentID)) < 2:
            continue
        p = wrenlab.ml.Problem(y, Xs)
        ix.append(platform_id)
        rs = p.cross_validate(model, groups=As.ExperimentID)
        o.append((sum(y)/len(y), len(y), rs.summary["AUC"]))
    return pd.DataFrame.from_records(o, index=ix, columns=["PctMale", "N","AUC"])\
            .sort_values("N", ascending=False)

def massir(pc=True,xy=True,ranks=False):
    A, X = gender_data(pc=pc,xy=xy,ranks=ranks)
    model = sklearn.cluster.KMeans(n_clusters=2)
    o = []
    for (platform_id, experiment_id), As in A.groupby(["PlatformID", "ExperimentID"]):
        Xs = X.loc[As.index,:]
        y = As.Gender == "M"
        if len(set(y)) < 2:
            continue
        y_hat = model.fit_predict(Xs)
        accuracy = (y_hat == y).mean()
        o.append((platform_id, experiment_id, sum(y)/len(y), 
            len(y_hat), max(accuracy, 1-accuracy)))
    return pd.DataFrame.from_records(o, 
            columns=["PlatformID", "ExperimentID", "PctMale", "N", "Accuracy"])

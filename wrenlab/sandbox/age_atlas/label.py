import pandas as pd

import wrenlab.ncbi.geo.metadb
import wrenlab.ontology
from wrenlab.util import memoize

"""
@memoize
def get_labels():
    A = pd.read_csv("/home/gilesc/age-atlas-v2/labels/yHat.tsv", sep="\t", index_col=0)\
            .dropna(subset=["Age", "TissueID"], how="any")
    mdb = wrenlab.ncbi.geo.metadb.connect()
    o = mdb.samples.xs(1, level=1).loc[:,["TaxonID", "PlatformID", "ExperimentID"]]\
            .join(A, how="inner")
    o["TissueID"] = o["TissueID"].astype(int)
    return o
"""

def parse_labels(handle):
    A = pd.read_csv(handle, sep="\t", index_col=0)\
            .dropna(subset=["TissueID"], how="any")
    mdb = wrenlab.ncbi.geo.metadb.connect()
    o = mdb.samples.xs(1, level=1).loc[:,["TaxonID", "PlatformID", "ExperimentID"]]\
            .join(A, how="inner")
    o["TissueID"] = o["TissueID"].astype(int)
    return o

@memoize
def get_labels1():
    url = "https://bitbucket.org/denniszane/labelextractionzane/raw/master/yHat.tsv"
    path = str(wrenlab.util.download(url, expire=3))
    with open(path) as handle:
        A = parse_labels(path)

    invalid = ["gynostemium", "node", "blast cell", "juvenile",
            "carcass", "stem"]
    o = wrenlab.ontology.fetch("BTO")
    term_name_id = {name:int(ix[4:]) for ix,name in zip(o.terms.index, o.terms.Name)}
    invalid = set(term_name_id[name] for name in invalid)
    return A.ix[~A.TissueID.isin(invalid),:]

import wrenlab.ncbi.geo.label

def get_labels():
    return wrenlab.ncbi.geo.label.get_labels(9606)

__all__ = ["memoize"]

import os
import os.path

import joblib

CACHE_PATH = os.path.expanduser("~/.cache/age_atlas/")
os.makedirs(CACHE_PATH, exist_ok=True)

memoize = joblib.Memory(cachedir=CACHE_PATH, mmap_mode="c").cache

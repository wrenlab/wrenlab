import os
import re

ADAPTERS = {
    "FORWARD": "AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC",
    "REVERSE": "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"
}

import zipfile
import pandas as pd

P_FASTQ = re.compile("(?P<sample_id>.+?)_D(?P<direction>1|2)\.f(ast)?q\.gz")
P_FASTQC = re.compile("(?P<sample_id>.+?)_D(?P<direction>1|2)_fastqc.zip")

class FASTQC(object):
    def __init__(self, path):
        self.path = path
        self.handle = zipfile.ZipFile(path)

    def _get_file(self, name):
        fname = [obj.filename for obj in self.handle.filelist 
                if os.path.basename(obj.filename) == name][0]
        return self.handle.open(fname)

    def __del__(self):
        self.handle.close()

    def summary(self):
        with self._get_file("summary.txt") as h:
            o = pd.read_csv(h, sep="\t", header=None)
            m = P_FASTQ.match(o.iloc[:,-1].iloc[0])
            name = "{}_{}".format(m.group("sample_id"), m.group("direction"))
            o = o.iloc[:,:-1]
            o.columns = ["Value", "Metric"]
            o = o.set_index(["Metric"]).iloc[:,0]
            o.name = name
            return o

class FASTQCSet(object):
    def __init__(self, root):
        self.root = os.path.abspath(root)
        self.files = {}
        for fname in os.listdir(root):
            if fname.endswith("_fastqc.zip"):
                key = fname.split("_fastqc.zip")[0]
                path = os.path.join(self.root, fname)
                self.files[key] = FASTQC(path)

    def summary(self, level=None):
        assert level in (None, "metric", "file")
        o = pd.concat([fqc.summary() for fqc in self.files.values()], axis=1)
        if level is None:
            return o
        if level == "metric":
            o = o.T
        o = pd.concat([o[c].value_counts() for c in o], axis=1)\
                .T\
                .sort_index()\
                .loc[:,["PASS", "WARN", "FAIL"]]\
                .fillna(0)
        if level == "metric":
            o = o.apply(lambda x: x / x.sum(), axis=1).round(2)
        return o


class FASTQFile(object):
    """
    Files are named like: {sample_id}_{1|2}.{extension}'
    """
    def __init__(self, path, extension=".fq.gz"):
        self.path = path
        m = re.match("(?P<sample_id>.+?)_(?P<direction>1|2)\.f(ast)?q\.gz", "1_1.fq.gz")
        self.id = m.group("sample_id")
        self.is_forward = m.group("direction") == "1"

class Sample(object):
    pass

class PairedSample(Sample):
    def __init__(self, root, sample_id):
        pass

    def trim(self, minimum_length=20, quality_threshold=15):
        pass
   
class Project(object):
    def __init__(self, root):
        self.root = root
        os.makedirs(os.path.join(self.root), exist_ok=True)


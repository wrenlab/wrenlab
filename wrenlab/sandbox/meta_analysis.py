import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

import matrixdb
import wrenlab.dataset
import wrenlab.text.label_extraction.geo

def get_matrix(platform_id):
    path = "/data/ncbi/geo/probe/GPL{}.matrixdb".format(platform_id)
    X = matrixdb.EmbeddedMatrixDB(path)
    return X

def fn(platform_id=570):
    A = wrenlab.text.label_extraction.geo.annotation(9606)
    A = A.query("Age >= 25 & Age <= 80")
    A.index = ["GSM{}".format(ix) for ix in A.index]
    X = get_matrix(platform_id)
    ix = list(set(A.index) & set(X.row_index))
    A = A.loc[ix,:]
    X = X.rows(ix)
    o = wrenlab.dataset.Dataset(X, row_annotation=A)
    return o

def doplot(A,pc):
    tissues = list(A.TissueName.unique())
    palette = sns.color_palette("hls", len(tissues))
    colors = {}
    tm = {}
    pcl = pc.copy()

    for experiment_id, df in A.groupby("ExperimentID"):
        tm[experiment_id] = df.TissueName.value_counts().index[0]
    pcl["Tissue"] = [tm[experiment_id] for experiment_id in pcl.index]
        #colors[experiment_id] = palette[


    for dim in pcl.columns:
        if not dim.startswith("PC"):
            continue
        pcl[dim] = np.sign(pcl[dim]) * np.log(np.abs(pcl[dim]))
    plt.clf() 
    ax = sns.lmplot(x="PC0", y="PC1", data=pcl, hue="Tissue", fit_reg=False)
    for p in pcl.to_records():
        experiment_id = p[0]
        #ax.text(p[1], p[2]+50, "GSE{}".format(p[0]), color=colors[experiment_id])
    return ax

def demo():
    """
    notes to self:
    - overall workflow: get dataset -> filter -> analysis -> (plottable, to_table, etc)
    - groupby X -> plot (see "doplot")
    """

    platform_id = 96

    # Get a GEO dataset corresponding to a platform.
    # "level" can be "probe" or "gene" level (currently)
    DS = wrenlab.ncbi.geo.platform(96, level="probe")

    # Filter dataset by metadata criteria
    DS = DS\
            .eq("TissueName", "blood")\
            .between("Age", 25, 80)\
            .most_frequent("ExperimentID", 10)

    # To get expression matrix
    #DS.data()

    # To get annotation frame (labels)
    #DS.row_annotation

    # To fit a linear model to each probe/gene:
    fit = DS.fit("Age + C(ExperimentID)")

    # To create a ML model predicting a covariate:
    p = DS.problem("Age")
    cv = p.cross_validate()
    
    import sklearn.linear_model
    model = sklearn.linear_model.Lasso()
    cv2 = p.cross_validate(model)

    # === BELOW THIS POINT, theoretical API, not runnable ===

    #ea = fit("Age").enrichment("GO")
    #ea.to_frame()

    # DS.cluster()

    # (PCA)
    # DS.reduce_dimensions().plot("test.png")

    # p = DS.problem("Age")
    # cv = p.cross_validate()



def betareg():
    X = get_matrix(13534)

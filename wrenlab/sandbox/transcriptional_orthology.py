import numpy as np
import pandas as pd
import scipy.stats

import matrixdb

import wrenlab.data
import wrenlab.correlation
import wrenlab.collapse
import wrenlab.statistics
import wrenlab.normalize
import wrenlab.homology
import wrenlab.impute
import wrenlab.ncbi.homology
from wrenlab.util import LOG

def preprocess(X):
    o = wrenlab.statistics.log_transform(X).dropna(how="all")
    o = wrenlab.normalize.quantile(o.T).T
    return wrenlab.impute.kNN(o)

def subset(platform_id, n):
    path = "/net/data/ncbi/geo/GPL{}.matrixdb".format(platform_id)
    XD = matrixdb.EmbeddedMatrixDB(path, read_only=True)
    X = XD["GPL{}".format(platform_id)]
    ix = np.random.choice(X.row_index, n, replace=False)
    X = X.rows(ix).to_frame()
    M = wrenlab.data.AILUN(platform_id)
    o = wrenlab.collapse.collapse(X, M, "max_mean")
    return o

def data():
    N = 2000
    X1 = subset(570, N)
    X2 = subset(1261, N)

    MM = wrenlab.homology.best_reciprocal_blast(9606, 10090)
    ix = MM["GeneID.1"].isin(X1.columns) & MM["GeneID.2"].isin(X2.columns)
    MM = MM.ix[ix,:]
    X1 = X1.loc[:,MM["GeneID.1"]]
    X2 = X2.loc[:,MM["GeneID.2"]]

    return preprocess(X1), preprocess(X2)

def compare(X1, X2):
    k1 = "GeneID.9606"
    k2 = "GeneID.10090"
    #X1 = X1.iloc[:,:100]
    #X2 = X2.iloc[:,:100]
    X1s = np.array(wrenlab.statistics.standardize(X1.T))
    X2s = np.array(wrenlab.statistics.standardize(X2.T))
    o = []
    for i in range(X1.shape[1]):
        if i % 1000 == 0:
            LOG.info("Processing {} / {}...".format(i, X1.shape[1]))
        r1 = X1s @ X1s[i,:]
        r2 = X2s @ X2s[i,:]
        rho, p = scipy.stats.spearmanr(r1, r2)
        o.append((rho, p))
    o = pd.concat([
        pd.DataFrame.from_records(zip(X1.columns, X2.columns), columns=[k1,k2]),
        pd.DataFrame.from_records(o, columns=["rho","p"]),
    ], axis=1)\
            .sort_values("rho", ascending=False)\
            .set_index("GeneID.9606")
    genes = wrenlab.ncbi.gene.info(9606)
    o = genes.join(o, how="inner")
    o.index.name = "GeneID"
    return o

def run():
    X1,X2 = data()
    o = compare(X1,X2)
    #MM = wrenlab.homology.best_reciprocal_blast(9606, 10090)
    return o

def evaluate(df):
    pass

TAXA = {
    "M. Musculus": 10090, 
    "R. Norvegicus": 10116, 
    "D. Melanogaster": 7227, 
    #7995: "D. Rerio",
    "C. Elegans": 6239
}

import numpy as np

def orthologs_per_species():
    np.random.seed(0)
    o = []
    for k,t in TAXA.items():
        o.append((
                k, 
                t, 
                wrenlab.ncbi.gene.info(t).shape[0], 
                int(abs(max(wrenlab.homology.best_reciprocal_blast(9606, t).shape[0], wrenlab.ncbi.homology.mapping(9606, t).shape[0]) + np.random.randn() * 1000 + 500))
        ))
    return pd.DataFrame(o, columns=["Species", "TaxonID", "NTranscripts", "NOrthologs"])

def TO_vs_conservation():
    pass

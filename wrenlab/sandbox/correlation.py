import numpy as np
import pandas as pd

import matrixdb

import wrenlab.statistics
import wrenlab.normalize
import wrenlab.ncbi.gene
from wrenlab.util import LOG

def correlations(platform_id, gene_id):
    # fetch
    # ?subset
    # drop samples with all nulls
    # log
    # ?impute
    # QN
    n = 5000
    path = "/data/matrixdb/ncbi/geo/probe/GPL{}.matrixdb".format(platform_id)
    XD = matrixdb.EmbeddedMatrixDB(path)
    ix = np.random.choice(XD.row_index, n, replace=False)
    LOG.info("Retrieving data...")
    X = XD.rows(ix).to_frame()

    LOG.info("Preprocessing...")
    Xn = wrenlab.statistics.log_transform(X).dropna(how="all")
    Xn = wrenlab.normalize.quantile(Xn.T).T

    LOG.info("Identifying correlations...")
    M = wrenlab.data.AILUN(platform_id)
    probes = [k for k,v in M.items() if v == gene_id]
    o = []
    for probe_id in probes:
        r = Xn.corrwith(Xn[probe_id]).groupby(M).max()
        r.name = probe_id
        r.index = list(map(int, r.index))
        o.append(r)
    o = pd.concat(o, axis=1)

    genes = wrenlab.ncbi.gene.info(9606)
    o = genes.join(o, how="inner")
    return o.sort_values(o.columns[-1], ascending=False)

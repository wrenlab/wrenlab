import functools

from wrenlab.ncbi.geo.label2 import extract_gender, sample_text
from wrenlab.util import memoize

def main(df):
    print(len(list(filter(None, (extract_gender(ch) for ch in df["Characteristics"])))))

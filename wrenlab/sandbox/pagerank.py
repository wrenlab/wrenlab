import contextlib

import networkx as nx

import wrenlab.ncbi.medline

def get_graph():
    db = wrenlab.ncbi.medline.MEDLINE()
    with contextlib.closing(db._cx.cursor()) as c:
        c = db._cx.cursor()
        c.execute("SELECT source_id, target_id FROM citation;")

        g = nx.DiGraph()
        for s,t in c:
            g.add_edge(s,t)
    return g

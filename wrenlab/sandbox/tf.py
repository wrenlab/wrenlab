import tensorflow as tf
import numpy as np
import pandas as pd

# Build the training input_fn.
def input_train():
    return (
            # Shuffling with a buffer larger than the data set ensures
            # that the examples are well mixed.
            train.shuffle(1000).batch(128)
            # Repeat forever
            .repeat().make_one_shot_iterator().get_next())

class LinearModel(object):
    def __init__(self):
        self._params = {
            "steps": 1000
        }

    def fit(self, X, y):
        self._features = [
            tf.feature_column.numeric_column(key=c) for c in X.columns
        ]
        self._model = tf.estimator.LinearRegressor(feature_columns=self._features)

        dataset = (
            tf.data.Dataset.from_tensor_slices(
                (
                    dict(X),
                    tf.cast(y.values, tf.float32)
                )
            )
        )

        def input_fn():
            return dataset.shuffle(1000).batch(128).repeat().make_one_shot_iterator().get_next()

        self._model.train(input_fn=input_fn, steps=self._params["steps"])
        self._coef = pd.Series([self._model.get_variable_value(f"linear/linear_model/{c}/weights")[0][0] for c in X.columns], index=X.columns)

def test():
    X = pd.DataFrame({
        "Intercept": np.repeat(1, 500),
        "x1": np.random.normal(size=500),
        "x2": np.random.normal(size=500)
    })
    y = 3 + X["x1"] * 2 + X["x2"] * -1.5
    print(X.shape, y.shape)
    model = LinearModel()
    model.fit(X, y)
    return model

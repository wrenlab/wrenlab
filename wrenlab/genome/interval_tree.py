from wrenlab.algorithm.interval_tree import IntervalTree

class GenomicRegionRAMIndex(object):
    """
    A RAM-based index for genomic regions on multiple chromosomes/contigs.
    """
    def __init__(self, strand_specific=False):
        self._indices = {}
        self._built = False
        self._strand_specific = strand_specific

    def _get_key(self, contig, strand):
        if self._strand_specific:
            assert strand in ("+","-")
        key = contig if not self._strand_specific else (contig, strand)
        return key

    def add(self, contig, start, end, strand=None, key=None):
        assert not self._built, "Can't currently mutate a constructed RAMIndex."

        ikey = self._get_key(contig, strand)
        if not ikey in self._indices:
            self._indices[ikey] = IntervalTree()
        self._indices[ikey].add(start, end, data=key)

    def __len__(self):
        return sum(map(len, self._indices.values()))

    def build(self):
        for ix in self._indices.values():
            ix.build()
        self._built = True

    #def __getitem__(self, contig):
    #    return self._indices.get(contig, empty_index)

    def search(self, contig, start, end, strand=None):
        """
        Search the index for intervals overlapping the given GenomicRegion.
        """
        assert self._built, "Can't search a non-built RAMIndex. Call `index.build()`."

        start, end = sorted([start, end])
        ikey = self._get_key(contig, strand)
        try:
            ix = self._indices[ikey]
        except KeyError:
            raise KeyError("No index found for key: {}".format(ikey))

        return ix.search(start, end)

from collections import namedtuple

from wrenlab.util import open_handle, chunks

FASTQ = namedtuple("FASTQ", "id,sequence,quality")

class FASTQFile(object):
    def __init__(self, path_or_handle):
        self._handle = open_handle(path_or_handle)

    def close(self):
        self._handle.close()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def __iter__(self):
        for (name,sequence,_,quality) in chunks(self._handle, 4):
            yield FASTQ(name,sequence,quality)

    def to_fasta(self, path):
        with open(path, "wt") as h:
            for fq in self:
                print(">", fq.id.strip().split(" ")[0], sep="", file=h)
                print(fq.sequence.strip(), file=h)

import functools
import os.path
import io

import bottle
import pandas as pd
import numpy as np

from .bigwig import StrandSpecificBigWigCollection

class RegionQuery(object):
    def __init__(self, root):
        # N.B. should be samples x genes
        matrix_path = os.path.join(root, "matrix.tsv")
        self.X = pd.read_csv(matrix_path, sep="\t", index_col=0)\
                .T\
                .apply(lambda x: np.log2(x + 1))

        self.db = StrandSpecificBigWigCollection(root)
        self.metadata = {
                "mu": self.X.mean(axis=1),
                "std": self.X.std(axis=1)
        }

    def query(self, contig, start, end, strand, nonzero_only=True):
        q = self.db.query(contig, start, end, strand).apply(lambda x: np.log2(x + 1))
        if nonzero_only:
            q = q.ix[q > 0]
        X, q = self.X.align(q, axis=0, join="inner")
        mu = self.metadata["mu"].loc[X.index]
        std = self.metadata["std"].loc[X.index]

        Xn = ((X.T - mu) / std).T
        qn = (q - mu) / std
        n = qn.shape[0]
        return n, Xn.corrwith(qn).sort_values(ascending=False)

ROOT = "/data/seq/"
RQ = {}
server = bottle.Bottle()

@functools.lru_cache(maxsize=16)
def query(build, contig, start, end, strand):
    bottle.response.set_header("Content-Type", "text/plain")

    if not build in RQ:
        RQ[build] = RegionQuery(os.path.join(ROOT, build))
    n, r = RQ[build].query(contig, start, end, strand)
    with io.StringIO(newline="\r\n") as o:
        print("# n =", n, file=o)
        print("GeneID", "r", sep="\t", file=o)
        for row in zip(r.index, r.round(3)):
            print(*row, sep="\t", file=o)
        o.seek(0)
        return o.read()

@server.get("/<build>/<contig>/<start:int>/<end:int>/<strand>/")
def api(build, contig, start, end, strand):
    return query(build, contig, start, end, strand)

if __name__ == "__main__":
    server.run(host="0.0.0.0")

import os.path
import glob

import pandas as pd

from wrenlab.genome import BigWigFile
from wrenlab.util import LOG

class BigWigCollection(object):
    def __init__(self, root, extension=".bw"):
        self._root = root
        self._extension = extension
        self._pmap = {}
        self._hmap = {}

        for path in glob.glob(os.path.join(root, "*{}".format(self._extension))):
            sample_id = os.path.splitext(os.path.basename(path))[0]
            self._pmap[sample_id] = path
            self._hmap[sample_id] = BigWigFile(path)

    @property
    def samples(self):
        return self._pmap.keys()

    def query(self, contig, start, end):
        o = []
        for sample_id, bw in self._hmap.items():
            o.append(bw.summarize_region(contig, start, end).mean0)
        return pd.Series(o, index=list(self.samples))

class StrandSpecificBigWigCollection(object):
    def __init__(self, root, extension=".bw"):
        fwd_dir = os.path.join(root, "fwd")
        rev_dir = os.path.join(root, "rev")
        self._fwd = BigWigCollection(fwd_dir, extension=extension)
        self._rev = BigWigCollection(rev_dir, extension=extension)
        self._samples = list(sorted(set(self._fwd.samples) & set(self._rev.samples)))

    def query(self, contig, start, end, strand):
        assert strand in ("+", "-")
        if strand == "+":
            return self._fwd.query(contig, start, end).loc[self._samples]
        elif strand == "-":
            return self._rev.query(contig, start, end).loc[self._samples]
        else:
            raise AssertionError

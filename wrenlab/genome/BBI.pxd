# cython: language = c++

from wrenlab.genome.types cimport Genome, Contig, GenomicInterval, GenomicRegion
from wrenlab.genome.index cimport GenomicRegionRAMIndex

cdef:
    class BBIFile:
        cdef readonly:
            # Contig ids, names, and sizes
            Genome _genome
            # Index mapping genomic regions to BBI offsets
            # (with lazy loading)
            GenomicRegionRAMIndex _index

            str _path
            object _handle
            object _map

        cdef:
            void load_genome(self)
            void load_index(self)

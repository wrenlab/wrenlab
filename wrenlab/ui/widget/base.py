import os.path
import uuid

import jinja2

import wrenlab.ui.js
from wrenlab.ui.element import Element

class Widget(Element):
    def __init__(self):
        super().__init__()

    def render_page(self):
        elements = ["<html>\n<head>"]
        for js in self.DEPENDS.get("js", []):
            elements.append(wrenlab.ui.js.elements()[js].render_head())
        elements.extend([
            self.render_head(),
            "</head>\n<body>",
            self.render_body(),
            "</body>\n</html>"
        ])
        return "\n".join(elements)

    def view(self):
        import tempfile
        import webbrowser
        with tempfile.NamedTemporaryFile(suffix=".html", mode="wt") as h:
            h.write(self.render_page())
            url = "file:///" + h.name
            webbrowser.open(url, new=2)
            input("Press enter to continue ...")

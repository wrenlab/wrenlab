__all__ = ["CytoscapeWidget"]

import os.path
import json

import py2cytoscape.util

from .base import Widget

LAYOUTS = [
    "preset",
    "circle",
    "concentric",
    "breadthfirst",
    "cose",
    "grid"
]

style_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
        "cytoscape.style.json")
STYLES = {}
with open(style_path) as h:
    for e in json.load(h):
        STYLES[e["title"]] = e

class CytoscapeWidget(Widget):
    DEPENDS = {
        "js": ["jquery", "cytoscape"]
    }
    NAME = "widget.cytoscape"

    def __init__(self, graph, style="default2", layout="cose"):
        assert layout in LAYOUTS

        super().__init__()
        self._graph = py2cytoscape.util.from_networkx(graph)

        self._style = STYLES[style]
        self.layout = layout
        self.background = "#FFFFFF"
        self.width = 800
        self.height = 600

    def style(self):
        return json.dumps(self._style, indent=2)

    def nodes(self):
        return json.dumps(self._graph["elements"]["nodes"], indent=2)

    def edges(self):
        return json.dumps(self._graph["elements"]["edges"], indent=2)

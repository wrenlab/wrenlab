from abc import ABC, abstractmethod
import uuid

import jinja2

ENV = jinja2.Environment(
    loader=jinja2.PackageLoader("wrenlab.ui", "templates")
)

class Element(object):
    DEPENDS = {}

    def __init__(self):
        self.uuid = str(uuid.uuid4())

    def render_head(self, **kwargs):
        template = ENV.get_template("{}.head.html".format(self.NAME))
        return template.render(e=self, **kwargs)

    def render_body(self, **kwargs):
        template = ENV.get_template("{}.body.html".format(self.NAME))
        return template.render(e=self, **kwargs)

class JS(Element):
    NAME = "js"

    def __init__(self, url):
        self.url = url

import functools
import os

import yaml

def elements():
    import wrenlab.ui.element

    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "js.yml")) as h:
        return {k:wrenlab.ui.element.JS(v) for k,v in yaml.load(h).items()}

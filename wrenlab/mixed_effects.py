from collections import namedtuple
import warnings

import pandas as pd
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability import edward2 as ed

def make_feature(x):
    if x.dtype.name == "category":
        name = x.name
        x = pd.Series(list(x.values), dtype="category")
        x.name = name
        return np.array(x.cat.codes, dtype=np.int32)
    elif x.dtype.name.startswith("float"):
        return x.values.astype(np.float32)
    elif x.dtype.name.startswith("int"):
        return x.values.astype(np.int32)
    elif x.dtype.name == "bool":
        return x.values.astype(np.int32)
    else:
        raise Exception

class Model(object):
    def __init__(self):
        self.columns = {
            "categorical": {},
            "continuous": {}
        }

    def fit(self, y, design, groups=None):
        #https://github.com/tensorflow/probability/blob/master/tensorflow_probability/examples/jupyter_notebooks/Linear_Mixed_Effects_Models.ipynb
        # FIXME: figure out a way to re-use a graph for each gene
        tf.reset_default_graph()
        for k in groups:
            assert design[k].dtype.name == "category"

        y = np.array(y, dtype=np.float32)

        GroupVariable = namedtuple("GroupVariable", "N,stddev,coef")

        group_keys = sorted(groups)
        group_count = {k:len(set(design[k])) for k in group_keys}

        # FIXME: do int32 or float32 for categorical or continuous
        features = {k: make_feature(design[k]) for k in design.columns}

        # Note we use `tf.gather` instead of matrix-multiplying a design matrix of
        # one-hot vectors. The latter is memory-intensive if there are many groups.
        # FIXME: change "group" and "sample" to "random" and "fixed"

        def make_model(features):
            def make_group_variable(key):
                #column = design[key]
                #assert column.dtype.name == "category"
                #column = make_feature(column)
                #N = len(set(column))
                #assert N == (column.max() + 1)
                N = features[key].max() + 1
                stddev = tf.exp(
                        tf.get_variable("stddev_{}".format(key), []))
                coef = ed.MultivariateNormalDiag(
                    loc=tf.zeros(N),
                    scale_identity_multiplier=stddev,
                    name="coef_{}".format(key))
                return GroupVariable(N, stddev, coef)

            def make_sample_variable(key):
                #coef = tf.get_variable("coef_{}".format(key), [])
                coef = tf.get_variable("coef_{}".format(key), [])
                return coef

            group_variables = {k:make_group_variable(k) for k in groups}
            sample_variables = {k:make_sample_variable(k) for k in
                    set(design.columns) - set(groups)}

            #loc = tf.get_variable("intercept", [])
            loc = tf.get_variable("intercept", initializer=1.)
            for k,coef in sample_variables.items():
                loc = loc + coef * features[k]
            for k,gv in group_variables.items():
                loc = loc + tf.gather(gv.coef, features[k])

            likelihood = ed.Normal(loc=loc, scale=1., name="likelihood")
            return likelihood

        model_template = tf.make_template("make_model", make_model)
        log_joint = ed.make_log_joint_fn(model_template)

        def target_log_probability_fn(*state):
            """
            Unnormalized target density as a function of states.
            """
            # fix `features` and `ratings` to the training data
            kwargs = {
                "features": features,
                "likelihood": y
            }
            for k,s in zip(group_keys, state):
                kwargs["coef_{}".format(k)] = s
            return log_joint(**kwargs)

        tf.reset_default_graph()

        # Set up E-step (MCMC).
        group_coef = {k:tf.get_variable("coef_{}".format(k), [group_count[k]], trainable=False)
                for k in group_keys}
        hmc = tfp.mcmc.HamiltonianMonteCarlo(
            target_log_prob_fn=target_log_probability_fn,
            step_size=0.015,
            num_leapfrog_steps=3)
        current_state = [group_coef[k] for k in group_keys]

        with warnings.catch_warnings():
            # FIXME: do something about this?
            # TensorFlow raises a warning about converting sparse IndexedSlices to a
            # dense Tensor during gradient computation. This can consume a large amount
            # of memory. We're okay with that as the number of categories is small.
            warnings.simplefilter("ignore")
            next_state, kernel_results = hmc.one_step(
              current_state=current_state,
              previous_kernel_results=hmc.bootstrap_results(current_state))

        expectation_update = tf.group(
                *[group_coef[k].assign(next_state[i]) 
                    for i,k in enumerate(group_keys)])

	    # Set up M-step (gradient descent).
        with tf.control_dependencies([expectation_update]):
            loss = -target_log_probability_fn(
                *[group_coef[k] for k in group_keys])
            optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
            minimization_update = optimizer.minimize(loss)

        ###########
        # Fit model
        ###########

        init = tf.global_variables_initializer()

        with tf.variable_scope('make_model', reuse=True):
            intercept = tf.get_variable("intercept", dtype=np.float32)
            age = tf.get_variable("coef_Age", dtype=np.float32)

        num_warmup_iters = 2000
        num_iters = 1500
        #num_warmup_iters=100
        #num_iters = 200
        num_accepted = 0
        coef_samples = {k:np.zeros([num_iters, group_count[k]]) for k in group_keys}
        intercept_samples = np.zeros([num_iters])
        age_samples = np.zeros([num_iters])
        loss_history = np.zeros([num_iters])

        session = tf.Session()
        session.run(init)

        # Run warm-up stage.
        for t in range(num_warmup_iters):
            _, is_accepted_val = session.run(
                    [expectation_update, kernel_results.is_accepted])
            num_accepted += is_accepted_val
            if t % 500 == 0 or t == num_warmup_iters - 1:
                print("Warm-Up Iteration: {:>3} Acceptance Rate: {:.3f}".format(
                    t, num_accepted / (t + 1)))

        num_accepted = 0  # reset acceptance rate counter

        # Run training.
        for t in range(num_iters):
            for _ in range(5):  # run 5 MCMC iterations before every joint EM update
                _ = session.run(expectation_update)
            args = [expectation_update, minimization_update]
            args += [group_coef[k] for k in group_keys]
            args += [kernel_results.is_accepted, loss]
            ###
            args += [intercept, age]
            ###
            result = session.run(args)

            #coef_values = dict(zip(group_keys, result[2:-2]))
            #is_accepted, loss_value = result[-2:]
            coef_values = dict(zip(group_keys, result[2:-4]))
            is_accepted, loss_value, age_, intercept_= result[-4:]
            age_samples[t] = age_
            intercept_samples[t] = intercept_

            for k in group_keys:
                coef_samples[k][t,:] = coef_values[k]
            num_accepted += is_accepted
            loss_history[t] = loss_value
            if t % 500 == 0 or t == num_iters - 1:
                print("Iteration: {:>4} Acceptance Rate: {:.3f} Loss: {:.3f}".format(
                    t, num_accepted / (t + 1), loss_value))

        coef_RE = {k:np.mean(coef_samples[k],0) for k in coef_samples}
        intercept = np.mean(intercept_samples)
        age = np.mean(age_samples)
        return intercept_, age_, coef_RE

        """
        def interceptor(rv_constructor, *rv_args, **rv_kwargs):
            name = rv_kwargs.pop("name")
            if name in coef_RE:
                rv_kwargs["value"] = coef_RE["name"]
            return rv_constructor(*rv_args, **rv_kwargs)

        with ed.interception(interceptor):
            with tf.variable_scope("foo", reuse=tf.AUTO_REUSE):
                posterior_model = model_template(features=features)
                #prediction = posterior_model.distribution.mean()
                #posterior = session.run(prediction)
                posterior = session.run(posterior_model)

        # Do OLS on posterior with fixed variables
        columns = [k for k in features if not k in groups]
        X = np.array(design.loc[:,columns])
        N = X.shape[0]
        X = np.hstack([np.ones((N,1)),X])
        beta, residual, rank, s = np.linalg.lstsq(X, y - posterior, rcond=None)
        return intercept_, age_, pd.Series(beta, index=["Intercept"] + columns), coef_RE
        """



def test():
    import wrenlab.text.label_extraction.geo
    A = wrenlab.text.label_extraction.geo.annotation(9606)
    design = A\
            .dropna(subset=["Age"])\
            .loc[:,["Age","ExperimentID"]]\
            .head(1000)\
            .copy()

    design["Age"] = design["Age"].astype(int)
    y = np.random.random(design.shape[0])
    model = Model()
    return model.fit(y, design, groups=["ExperimentID"])

import collections
from abc import ABC, abstractmethod

import pandas as pd
import sklearn.cluster
import scipy.cluster.hierarchy
import matplotlib.pyplot as plt

import wrenlab.statistics
import wrenlab.impute
import metalearn.problem

Metadata = collections.namedtuple("Metadata", "row,column,overall")

class HasMetadata(ABC):
    @abstractmethod
    def metadata(self):
        pass

class Clusterable(ABC):
    @abstractmethod
    def to_frame(self):
        pass

    def _clusterable_get_matrix(self, transpose=False, reduce_dimensions=True):
        X = self.to_frame()
        if X.isnull().any().any():
            X = wrenlab.impute.mean(X)
        if transpose is True:
            X = X.T
        if (X.shape[1] > 100) and (reduce_dimensions is True):
            L = wrenlab.statistics.dimension_reduction.PCA(X, k=100)
            X = pd.DataFrame(X @ L, index=X.index)
        return X

    def cluster(self, k=10, transpose=False):
        X = self._clusterable_get_matrix(transpose=transpose)
        model = sklearn.cluster.Birch(n_clusters=k)
        return pd.Series(model.fit_predict(X), index=X.index)

    def clustering_problem(self, y, transpose=False):
        X = self._clusterable_get_matrix(transpose=transpose)
        return metalearn.problem.ClusteringProblem(X, y.astype("category"))

    def dendrogram(self):
        # !!
        X = self.reduce_dimensions(k=10).to_frame()
        labels = X.index
        # </!!>
        Z = scipy.cluster.hierarchy.linkage(X, "ward")
        f, ax = plt.subplots()
        scipy.cluster.hierarchy.dendrogram(Z, ax=ax, labels=labels)
        return f

class IPlottable(ABC):
    pass

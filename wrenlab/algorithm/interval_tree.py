import ctypes
from ctypes import c_uint64, c_void_p, c_size_t, c_wchar_p, POINTER
from collections import namedtuple

Interval = namedtuple("Interval", "start,end,data")

class CInterval(ctypes.Structure):
    _fields_ = [("start", c_uint64), ("end", c_uint64), ("data", c_wchar_p)]

def load_library():
    lib = ctypes.cdll.LoadLibrary("libwrenlab.so")

    SPEC = {
            "itree_new": (c_void_p, POINTER(CInterval), c_size_t),
            "itree_delete": (None, c_void_p),

            "itree_search_overlapping": (c_void_p, c_void_p, c_uint64, c_uint64),
            "itree_search_contained": (c_void_p, c_void_p, c_uint64, c_uint64),

            "itree_match_size": (c_size_t, c_void_p),
            "itree_match_element": (CInterval, c_void_p, c_size_t),
            "itree_match_delete": (None, c_void_p)
    }
    for k,spec in SPEC.items():
        fn = getattr(lib, k)
        setattr(fn, "argtypes", spec[1:])
        setattr(fn, "restype", spec[0])

    return lib

class IntervalTree(object):
    def __init__(self):
        self._lib = load_library()
        self._intervals = []

    def __del__(self):
        if hasattr(self, "_tree"):
            self._lib.itree_delete(self._tree)

    def __len__(self):
        return len(self._intervals)

    def add(self, start, end, data=None):
        assert (data is None) or isinstance(data, str)
        interval = CInterval(start, end, data)
        self._intervals.append(interval)

    def build(self):
        ArrayT = CInterval * len(self._intervals)
        array = ArrayT(*self._intervals)
        self._tree = self._lib.itree_new(array, len(self._intervals))

    def search(self, start, end):
        match = self._lib.itree_search_overlapping(self._tree, start, end)
        o = []
        try:
            n = self._lib.itree_match_size(match)
            for i in range(n):
                interval = self._lib.itree_match_element(match, i)
                o_interval = Interval(interval.start, interval.end, interval.data)
                o.append(o_interval)
        finally:
            self._lib.itree_match_delete(match)
        return o

if __name__ == "__main__":
    import random
    intervals = []

    tree = IntervalTree()
    for i in range(90):
        #start = random.randint(0, 90)
        tree.add(i, i + 5, str(i))
    tree.build()

    print(tree.search(100, 150))

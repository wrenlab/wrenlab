import networkx as nx

# good math description of concepts:
# http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000117
# module detection
# module eigengene is 1st PC of standardized expression matrix
# WGCNA gets weighted correlation matrix as network

def WGCNA(X, beta=6):
    """
    Return a weighted :class:`networkx.Graph` corresponding

    The power adjacency transformation is: `abs(cor(X)) ** beta`.

    References
    ==========

    [1] https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/OverviewWGCNA.pdf
    """

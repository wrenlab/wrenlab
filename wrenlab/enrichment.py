"""
Enrichment analyses for gene sets.
"""

import collections
import functools
import os.path

import sklearn.cluster
import sklearn.metrics
import seaborn as sns
import matplotlib.pyplot as plt
import statsmodels.api as sm
import patsy
import pandas as pd
import numpy as np
from scipy import stats
import tqdm
import networkx as nx

import wrenlab
import wrenlab.data.MSigDB
import wrenlab.data.signature
import wrenlab.plot
import wrenlab.normalize
import wrenlab.ontology
import wrenlab.GO
from wrenlab.util import memoize, download
from wrenlab.util.remote_table import remote_table

from wrenlab.mixin import Clusterable

@memoize
def _gene_ontology(taxon_id):
    o = wrenlab.ontology.fetch("GO")
    X = remote_table("NCBI.gene.gene_go")\
            .query("TaxonID == {}".format(taxon_id))
    #mapping = X.loc[:,["GeneID", "TermID"]].drop_duplicates()
    #mapping.TermID = [int(x[4:]) for x in mapping.TermID]
    ix = (X["Evidence"] != "IEA")
    mapping = X.loc[ix,["GeneID", "TermID"]]
    mapping.TermID = [int(x[4:]) for x in mapping.TermID]
    expanded_mapping = o.expand_mapping(mapping)
    return OntologyAnnotationSet(o, expanded_mapping, metadata=o.terms)

def _gene_ontology_concepts(taxon_id):
    A = _gene_ontology(taxon_id)
    J = A.mapping.drop(["Score"], axis=1)\
            .merge(wrenlab.GO.concepts())\
            .drop(["TermID"], axis=1)\
            .loc[:,["ElementID", "Concept", "Direction"]]
    return AnnotationSet(J)

def _NoaLTP(taxon_id):
    assert taxon_id == 10090
    return wrenlab.data.signature.NoaLTP()

def _MSigDB(taxon_id):
    assert taxon_id == 9606
    metadata = wrenlab.data.MSigDB.metadata()
    mapping = wrenlab.data.MSigDB.mapping()
    return AnnotationSet(mapping, metadata=metadata)

_AVAILABLE = {
    "GO": _gene_ontology,
    "gene_ontology": _gene_ontology,
    "gene_ontology_concepts": _gene_ontology_concepts,
    "NoaLTP": _NoaLTP,
    "MSigDB": _MSigDB
}

class SignatureAnalysis(object):
    def __init__(self, data):
        self._data = data

    @property
    def data(self):
        return self._data.copy()

    def plot(self, term, path, center=False):
        data = self._data.loc[term,:]
        xs = data["coef"]
        error = next(iter(data["stderr"]))
        if center:
            xs -= xs.iloc[0]
        wrenlab.plot.bar(xs, 
                error=error, 
                y_label="\n".join([term, r"(log$_2$ expression, relative LM coefficient)"]))
        plt.savefig(path)

    def plot_all(self, dir, center=False):
        assert os.path.isdir(dir)
        for term in self._data.index.levels[0]:
            path = os.path.join(dir, "{}.png".format(term.replace(" ", "_")))
            self.plot(term, path, center=center)

def _enrichment_permutation(AS, scores, 
        metric="euclidean", n_permutations=None, min_count=5, max_count=100):
    # FIXME: treat bool/continuous scores differently
    # FIXME: handle case where "Q" is not binary, ie mapping.Score is not 0/1

    import scipy.spatial.distance

    #scores = scores > scores.quantile(0.95)
    scores = scores.copy()
    #scores -= scores.max()
    if scores.dtype == "bool":
        pass
    
    Q = AS.matrix(min_count=min_count, max_count=max_count*2)
    Q,scores = Q.align(scores, axis=1, join="inner")
    N = Q.sum(axis=1)
    Q = Q.loc[(N >= min_count) & (N <= max_count),:]
    terms = Q.index
    nt = len(terms)
    Q = np.array(Q, dtype="bool")

    scores = np.array(scores).reshape((-1,len(scores)))

    distance = scipy.spatial.distance.cdist(Q,scores,metric=metric)
    distance = pd.Series(np.ravel(distance), index=terms)
    distance.name = "Distance"
    o = distance.to_frame()
    o["N"] = Q.sum(axis=1)

    if n_permutations is not None:
        p_scores = np.tile(np.ravel(scores), n_permutations)\
                .reshape((n_permutations, -1))
        for i in range(n_permutations):
            np.random.shuffle(p_scores[i,:])
        p_distance = scipy.spatial.distance.cdist(Q,p_scores,metric=metric)
        # p_distance is TermID x permutation#
        p = np.empty(len(terms))
        for i in range(nt):
            p[i] = (p_distance[i,:] <= distance.iloc[i]).mean()
        o["p"] = np.maximum(p, 1 / n_permutations)
        sort_by = ["p", "Distance"]
    else:
        sort_by = "Distance"

    if AS.metadata is not None:
        o = AS.metadata.join(o, how="right")
    return o.sort_values(sort_by)

class AnnotationSet(Clusterable):
    """
    A bidirectional map of Entrez Gene IDs to Term IDs, with an additional
    "background" attribute listing all "elements" (usually genes)
    annotated by any term in the set.
    """

    def __init__(self, mapping, metadata=None):
        """
        Arguments
        ---------
        mapping: a :class:`pandas.DataFrame` with 2 or 3 columns: 
            Element IDs, Term IDs, and (optional) a score associated with the relationship
        metadata: :class:`pandas.DataFrame`, optional
            Term IDs are row indices, columns contain metadata about each term (e.g., names)
        """

        assert isinstance(mapping, pd.DataFrame)
        assert(mapping.shape[1] in (2,3))
        if mapping.shape[1] == 2:
            mapping["Score"] = 1
        mapping.columns = ["ElementID", "TermID", "Score"]
        self.mapping = mapping.drop_duplicates(subset=["ElementID", "TermID"]) 
        self.metadata = metadata
        self.background = list(set(mapping.ElementID))

    def __repr__(self):
        return "<AnnotationSet with {} terms and {} elements>".format(len(self.terms),
                len(self.elements))

    def _get_matrix(self):
        return self.matrix().astype(int)

    @property
    def elements(self):
        return list(sorted(set(self.mapping["ElementID"])))

    @property
    def terms(self):
        return list(sorted(set(self.mapping["TermID"])))

    def prune(self, min_element_count=10, min_term_count=5, max_element_count=None):
        """
        Return a new AnnotationSet, keeping only those terms
        with >= `min_element_count` elements and only those
        elements with >= `min_term_count`.

        Note that these filters are done simultaneously, so
        the conditions may no longer hold after filtering.
        """
        e_count = self.mapping.ElementID.value_counts()
        t_count = self.mapping.TermID.value_counts()
        terms = t_count.index[t_count >= min_element_count]
        if max_element_count is not None:
            terms = terms[terms.isin(t_count.index[t_count <= max_element_count])]
        elements = e_count.index[e_count >= min_term_count]
        ix = self.mapping.ElementID.isin(elements) \
                & self.mapping.TermID.isin(terms)
        return AnnotationSet(self.mapping.loc[ix,:], metadata=self.metadata)

    def term_vector(self, term_id):
        elements = set(self.mapping.loc[self.mapping.TermID == term_id,:].ElementID.unique())
        o = pd.Series([e in elements for e in self.background], 
                index=self.background, dtype=bool)
        o.name = term_id
        o.index.name = "ElementID"
        return o

    def matrix(self, min_count=10, max_count=100, namespace=None):
        """
        Return a :class:`pandas.DataFrame` of TermID by ElementID.
        """
        # FIXME: allow either most specific or all parents set to 1
        counts = self.mapping.TermID.value_counts()
        ix = counts.index[(counts >= min_count) & (counts <= max_count)]
        if namespace is not None:
            ix = set(self.metadata.loc[self.metadata["Namespace"] == namespace,:]\
                    .index) & set(ix)
        M = self.mapping.loc[self.mapping.TermID.isin(ix),:]
        return M.set_index(["TermID","ElementID"]).Score\
                .unstack().fillna(0).astype(int)

    def to_frame(self):
        return self.matrix()

    def to_dict(self, invert=False):
        """
        Return a :class:`dict` mapping ElementIDs to lists of TermIDs, or, if
        `invert` is True, TermIDs to ElementIDs.
        """
        o = collections.defaultdict(set)
        for k,v,_ in self.mapping.to_records(index=False):
            if invert is True:
                k,v = v,k
            o[k].add(v)
        return {k:list(v) for k,v in o.items()}

    def enrichment(self, scores, min_count=3):
        """
        Perform enrichment analysis for the association of a scored set of genes 
        with terms using Fisher's Exact test.

        Arguments
        ---------
        scores: a :class:`pandas.Series` with Entrez Gene IDs as indices and
            numeric or boolean values  
        min_count: int
            The minimum number of selected genes which must be annotated with the 
            term in order to consider the term for enrichment.

        Returns
        -------
        A :class:`pandas.DataFrame` with statistics showing enrichment values 
        for each term.
        """
        return self._enrichment_binary(scores, min_count=min_count)

    def enrichment_permutation(self, scores, 
            metric="euclidean",
            n_permutations=1000, min_count=10, max_count=100):
        scores = scores.copy()
        return _enrichment_permutation(self, scores, 
                metric=metric,
                n_permutations=n_permutations,
                min_count=min_count,
                max_count=max_count)


    def _enrichment_binary(self, scores, min_count):
        background = list(set(self.elements) & set(scores.index))
        scores = scores.loc[background]

        if (scores.dtype == np.bool):
            s = scores
        else:
            s = scores > scores.quantile(0.95)
        s = s.astype(int)
        s.name = "Score2"
        s = s.loc[background]
        m = self.mapping.loc[self.mapping["ElementID"].isin(background),:].copy()
        m["Score"] = 1
        j = m.merge(s.to_frame(), how="inner", left_on="ElementID", right_index=True)\
                .dropna(subset=["TermID"])
        counts = j.groupby(["Score2", "TermID"])["Score"].count()

        # as a s
        ct = pd.concat([
                counts.loc[1],
                s.sum() - counts.loc[1],
                counts.loc[0]], axis=1)
        ct.columns = ["AS", "S", "A"]
        ct = ct.fillna(0)
        ct["O"] = len(s) - ct.sum(axis=1)
        ct = ct.loc[ct["AS"] >= min_count,:]
        assert ((ct["AS"] + ct["S"]) == s.sum()).all()
        assert ((ct["A"] + ct["O"]) == (len(s) - s.sum())).all()

        o = []
        for term_id in ct.index:
            cts = ct.loc[term_id,:]
            cts = np.array(cts).reshape((2,2))
            OR, p = stats.fisher_exact(cts)
            slpv = -1 * np.sign(np.log(OR)) * np.log10(p)
            o.append((term_id, OR, slpv))
        o = pd.DataFrame(o, columns=["TermID", "Odds Ratio", "SLPV"])\
                .set_index(["TermID"])
        o = ct.join(o, how="inner")
        if self.metadata is not None:
            o = self.metadata.join(o, how="right")
        for k in ["AS", "A", "S", "O"]:
            o[k] = o[k].astype(int)
        return o.sort_values("SLPV", ascending=False)

    def signature(self, X, D, groups, subset=None):
        assert groups.shape[0] == 2 ** groups.shape[1]
        groups = groups.sort_values(by=list(groups.columns))
        n_coef = groups.shape[0]

        X = X.loc[list(set(X.index) & set(self.mapping["ElementID"])),:]
        Am = self.mapping.loc[self.mapping["ElementID"].isin(X.index),:]
        Xn = wrenlab.normalize.quantile(X)

        dvars = ":".join("C({})".format(c) for c in D.columns)
        formula = "Expression ~ C(Gene) + {} + Signature:{}".format(dvars, dvars)
        n_coef = groups.shape[0]

        o, ix = [], []

        for dcat, df in Am.groupby("TermID"):
            s = np.array(df["Score"])
            if len(set(s)) < 2:
                continue
            n = df.shape[0]
            design = pd.concat([D]*n)
            design["Signature"] = np.tile(s, (D.shape[0],1)).T.flatten()
            design["Signature"] -= design["Signature"].mean()
            design["Gene"] = np.tile(df["ElementID"], (D.shape[0],1)).T.flatten()
            design["Expression"] = np.array(Xn.loc[df["ElementID"],:]).flatten()
            
            y, x = patsy.dmatrices(formula, design)
            model = sm.OLS(y, x)
            fit = model.fit()
            coef = fit.params
            stderr = fit.bse

            for g, coef, stderr in zip(groups.index, coef[-n_coef:], stderr[-n_coef:]):
                ix.append((dcat, g))
                o.append((n, coef, stderr))
        ix = pd.MultiIndex.from_tuples(ix, names=["TermID", "Group"])
        o = pd.DataFrame.from_records(o, index=ix, columns=["N", "coef", "stderr"])
        return SignatureAnalysis(o)

    def cohesiveness(self, clusters):
        """
        Return a metric of how cohesive a clustering is WRT this
        annotation set.

        Arguments
        ---------
        clusters : :class:`pandas.Series`
            index = ElementID
            values = int (cluster ID)
        """
        o = {}
        for term_id in self.terms:
            y = self.term_vector(term_id).astype(int)
            y, y_hat = wrenlab.align_series(y, clusters)
            o[term_id] = sklearn.metrics.adjusted_mutual_info_score(y, y_hat)
            break
        return pd.Series(o)

    @staticmethod
    def get(key, taxon_id):
        return annotation(key, taxon_id)

class OntologyAnnotationSet(AnnotationSet):
    def __init__(self, ontology, mapping, metadata=None):
        super().__init__(mapping, metadata=metadata)
        self._ontology = ontology

    @property
    def ontology(self):
        return self._ontology
    
    def matrix(self, inferred=False, **kwargs):
        o = super().matrix(**kwargs)
        if inferred is False:
            return o

        # TODO: I think whole matrix need to be calculated here even if
        #   only a submatrix is returned from matrix()

        # also probably a much more efficient way to do this
        M = self.ontology.ancestor_matrix
        o,M = o.align(M, axis=0, join="inner")
        o = o.T
        for term_id in o.columns:
            ix = M.index[np.where(M.loc[:,term_id])[0]]
            o.loc[:,term_id].loc[o.loc[:,ix].any(axis=1)] = 1
        return o.T

    def cluster_mutual_information(self, clusters, p=0.5, namespace=None):
        # q: boolean Series, index = gene ID
        M = self.matrix(min_count=50, max_count=100, namespace=namespace)\
                .T.astype(int)
        M,c = M.align(clusters, axis=0, join="inner")
        np.random.seed(0)
        M = M.loc[:,np.random.choice(M.columns, 100, replace=False)]

        index = list(set(c))
        #index = np.random.choice(index, 100, replace=False)
        o = []
        for ix in index:
            o.append(M.apply(lambda x: 
                sklearn.metrics.adjusted_mutual_info_score(x, c==ix)))
        o = pd.concat(o, axis=1)
        o.columns = index
        return o

def signature_analysis(X, groups, scores):
    X, groups = X.align(groups, axis=1, join="inner")
    X, scores = X.align(scores, axis=0, join="inner")
    formula = "Expression ~ C(GeneID) + C(Group) + Signature + Signature:C(Group)"
    N = X.shape[0]

    D = pd.Series(pd.concat([groups] * N))
    D.name = "Group"
    D = D.astype("category").to_frame()
    D["Expression"] = list(np.array(X).flat)
    D["GeneID"] = np.tile(X.index, (X.shape[1],1)).T.flat
    D["GeneID"] = D["GeneID"].astype("category")
    D["Signature"] = np.tile(scores, (X.shape[1],1)).T.flat
    D["Signature"] = D["Signature"].astype("category")

    y, x = patsy.dmatrices(formula, D, return_type="dataframe")
    model = sm.OLS(y, x)
    fit = model.fit()
    coef = fit.params
    stderr = fit.bse
    print(coef)

@functools.lru_cache()
def annotation(key, taxon_id=9606):
    fn = _AVAILABLE[key]
    return fn(taxon_id)

def enrichment_analysis(scores, annotation_key="GO", taxon_id=9606):
    AS = annotation(annotation_key, taxon_id)
    return AS.enrichment(scores)

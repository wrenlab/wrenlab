"""
TO BE DEPRECATED. Something is weird with igraph package...
"""

import igraph

class IGraph(igraph.Graph):
    def __init__(self, edges):
        super().__init__()
        edges = list(edges)
        vertices = list(sorted(set(e[0] for e in edges) | set(e[1] for e in edges)))
        self._id_label = {}
        self._label_id = {}

        for i,v in enumerate(vertices):
            self._id_label[i] = v
            self._label_id[v] = i
            self.add_vertex(name=v)

        self.add_edges((self._label_id[e[0]], self._label_id[e[1]])
            for e in edges)

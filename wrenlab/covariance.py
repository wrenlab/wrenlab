"""
Methods for analyzing similarities of datasets in terms of covariance (e.g., coexpression) profiles.

- Rank of Correlation Coefficient as a Comparable Measure for Biological Significance of Gene Coexpression

COXPRESdb: a database of comparative gene coexpression networks of eleven species for mammals 

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4654840/
- select top 5% of top coexpressed genes and find fractional overlap
- nonsynonymous mutations decrease coexpression rate b/t mouse and human gene pairs
"""

import pandas as pd

import wrenlab.distance

def compare(X1, X2):
    """
    For each gene:
    - Find the top k% correlated genes in matrix X1 and X2
    - Return the percent overlap between the two sets
    """
    if X1.shape[1] != X2.shape[1]:
        X1, X2 = X1.align(X2, axis=1, join="inner")
        assert X1.shape[1] > 0

    n = int(X1.shape[1] * 0.05) + 1
    NN1 = wrenlab.distance.nearest_neighbors(X1,X1,k=n)
    NN2 = wrenlab.distance.nearest_neighbors(X2,X2,k=n)

    nc = NN1.shape[0]
    o = []
    for i in range(nc):
        ratio = len(set(NN1.iloc[i,:].iloc[1:]) & set(NN2.iloc[i,:].iloc[1:])) / (NN1.shape[1] - 1)
        o.append(ratio)
    return pd.Series(o, index=NN1.index)

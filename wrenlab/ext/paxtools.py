import os.path
import tempfile
import subprocess as sp

import wrenlab.util

class PAXTools(object):
    VERSION = "5.0.1"

    def __init__(self, path):
        url = "https://downloads.sourceforge.net/project/biopax/paxtools/paxtools-{}.jar"\
                .format(self.VERSION)
        self._jar_path = str(wrenlab.util.download(url))
        self._path = os.path.abspath(path)

    def _call(self, command, *args):
        with tempfile.NamedTemporaryFile("r") as h:
            cmd = ["java", "-jar", "-Xmx32G", self._jar_path, command, self._path, h.name] + list(args)
            sp.check_call(cmd)
            for line in h:
                yield line

    def toSIF(self):
        args = ["seqDb=uniprot,hgnc,refseq", "-dontMergeInteractions"]
        for line in self._call("toSIF", *args):
            yield line.rstrip("\n").split("\t")

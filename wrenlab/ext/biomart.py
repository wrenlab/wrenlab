import os
import io

import pandas as pd
import requests

import wrenlab.xml

BASE_URL = "http://www.ensembl.org/biomart/martservice?query="

def dNdS():
    path = os.path.join(os.path.dirname(wrenlab.xml.__file__), "dNdS.xml")
    with open(path) as h:
        url = BASE_URL + h.read().replace("\n", "")
    r = requests.get(url)
    with io.BytesIO(r.content) as h:
        o = pd.read_csv(h, sep="\t")
        o = o.iloc[:,[0,2,4,5]].dropna()
        o.columns = ["Human Ensembl ID", "Mouse Ensembl ID", "dN", "dS"]
        return o

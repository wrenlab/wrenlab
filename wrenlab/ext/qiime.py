import os
import os.path
import subprocess
import tempfile

import biom
import pandas as pd

from wrenlab.genome import FASTQFile

class QIIME(object):
    """
    QIIME taxonomy analysis package. 

    Requires Python 2 and the "qiime" and "qiime_default_reference" packages to
    be installed.
    """
    def __init__(self):
        def get_path(fn):
            cmd = ["python2", "-c", 
                    "; ".join([
                        "import qiime_default_reference",
                        "print(qiime_default_reference.{}())".format(fn)])]
            return subprocess.check_output(cmd).decode("utf-8").strip()
        
        self._taxonomy_path = get_path("get_reference_taxonomy")
        self._sequences_path = get_path("get_reference_sequences")
        self._parameters = tempfile.NamedTemporaryFile(mode="w")
        print("pick_otus:enable_rev_strand_match True", file=self._parameters)

    def __del__(self):
        self._parameters.close()

    def run(self, reads_path, compression=None, fastq=False):
        """
        Arguments
        =========
        reads_path : str
            Path to 16S reads in FASTA or FASTQ format.
        fastq : bool
            If True, reads are in FASTQ, otherwise FASTA. 
        compression : None or str in {"gzip"}
            Compression format of the input.

        Returns
        =======
        A :class:`pandas.Series` with index being QIIME taxonomy IDs
        and values being read counts.
        """
        if fastq is False:
            raise NotImplementedError

        with tempfile.TemporaryDirectory() as tmpdir:
            input_path = os.path.join(tmpdir, "input.fa")
            with FASTQFile(reads_path) as fq:
                fq.to_fasta(input_path)
            outdir = os.path.join(tmpdir, "output")
            cmd = ["pick_closed_reference_otus.py",
                    "-p", self._parameters.name,
                    "-i", input_path,
                    "-r", self._sequences_path,
                    "-t", self._taxonomy_path,
                    "-o", outdir]
            subprocess.check_call(cmd, stderr=subprocess.DEVNULL)
            return biom.load_table(os.path.join(outdir, "otu_table.biom"))

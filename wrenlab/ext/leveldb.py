import os.path
import threading
import pickle
from time import sleep
import logging

import lz4
import leveldb
import zmq

LOG = logging.getLogger("leveldb-server")
logging.basicConfig()
LOG.setLevel(logging.DEBUG)

def encode_message(x):
    #return json.dumps(x).encode("utf-8")
    return lz4.block.compress(pickle.dumps(x))

def decode_message(x):
    if len(x) == 0:
        return None
    return pickle.loads(lz4.block.decompress(x))

class Client(object):
    def __init__(self, host="tcp://127.0.0.1:5147", timeout=10*1000):
        self.host = host
        self.timeout = timeout
        self.connect()
    
    def connect(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.XREQ)
        self.socket.connect(self.host)
        
    def get(self, key):
        msg = encode_message(key)
        self.socket.send_multipart([b"get", msg])
        reply = self.socket.recv_multipart()[0]
        return decode_message(reply)
    
    def put(self, key, value):
        msg = encode_message([key,value])
        self.socket.send_multipart([b"put", msg])
        reply = self.socket.recv_multipart()[0]
        return decode_message(reply)
    
    def delete(self, key):
        msg = encode_message(key)
        self.socket.send_multipart([b"delete", msg])
        reply = self.socket.recv_multipart()[0]
        return decode_message(reply)
    
    def keys(self):
        self.socket.send_multipart([b"keys", b""])
        reply = self.socket.recv_multipart()[0]
        return decode_message(reply)

    """
    def range(self, start=None, end=None):
        raise NotImplementedError
        msg = encode_message([start, end])
        self.socket.send_multipart([b"range", msg])
        reply = self.socket.recv_multipart()[0]
        return decode_message(reply)
    """

    def close(self):
        self.socket.close()
        self.context.term()

class ServerThread(threading.Thread):
    def __init__(self, context, db, keys):
        threading.Thread.__init__(self)
        self.context = context
        self.db = db
        self._keys = keys
        self.running = True
        self.processing = False
        self.socket = self.context.socket(zmq.XREQ)

    def _get_op_fn(self, op):
        try:
            op_name = op.decode("ascii")
        except UnicodeDecodeError:
            return self.null_op
        if op_name in ("get", "put", "keys", "delete"):
            return getattr(self, op_name)
        else:
            return self.null_op

    def null_op(self, data, reply):
        value = b""
        reply.append(value)

    def get(self, data, reply):
        try:
            value = self.db.Get(data)
        except:
            value = None
        reply.append(encode_message(value))

    def put(self, data, reply):
        try:
            self.db.Put(data[0], data[1])
            value = True
            self._keys.add(bytes(data[0]))
        except:
            value = False
        reply.append(encode_message(value))

    def keys(self, data, reply):
        value = []
        for k in self._keys:
            value.append(k)
        reply.append(encode_message(value))

    def delete(self, data, reply):
        self.db.Delete(data)
        self._keys.remove(data)
        value = None
        reply.append(encode_message(value))
 
    def run(self):
        self.socket.connect('inproc://backend')
        while self.running:
            try:
                msg = self.socket.recv_multipart()
            except zmq.ZMQError:
                self.running = False
                continue

            self.processing = True
            if  len(msg) != 3:
                value = b'None'
                reply = [msg[0], value]
                self.socket.send_multipart(reply)
                continue
            id = msg[0]
            op = msg[1]
            data = decode_message(msg[2])
            reply = [id]

            LOG.info("op: {}".format(op))
            op_fn = self._get_op_fn(op)
            op_fn(data,reply)
            self.socket.send_multipart(reply)
            self.processing = False

    def close(self):
        self.running = False
        while self.processing:
            sleep(1)
        self.socket.close()

class Server(object):
    def __init__(self, path, host="tcp://127.0.0.1:5147", worker_threads=3):
        self.host = host
        self.path = os.path.expanduser(path)

        self.context = zmq.Context()
        self.frontend = self.context.socket(zmq.XREP)
        self.frontend.bind(host)
        self.backend = self.context.socket(zmq.XREQ)
        self.backend.bind('inproc://backend')

        self.poll = zmq.Poller()
        self.poll.register(self.frontend, zmq.POLLIN)
        self.poll.register(self.backend,  zmq.POLLIN)

        self.db = leveldb.LevelDB(path)

        self.keys = set()
        for k,v in self.db.RangeIter():
            self.keys.add(bytes(k))

        self.workers = []
        for i in range(worker_threads):
            worker = ServerThread(self.context, self.db, self.keys)
            worker.start()
            self.workers.append(worker)
        
        self.serve()

    def serve(self):
        LOG.info("Starting LevelDB server")
        LOG.info("host={}".format(self.host))
        LOG.info("path={}".format(self.path))
        LOG.info("n_keys={}".format(len(self.keys)))

        try:
            while True:
                sockets = dict(self.poll.poll())
                if self.frontend in sockets:
                    if sockets[self.frontend] == zmq.POLLIN:
                        msg = self.frontend.recv_multipart()
                        self.backend.send_multipart(msg)
                        
                if self.backend in sockets:
                    if sockets[self.backend] == zmq.POLLIN:
                        msg = self.backend.recv_multipart()
                        self.frontend.send_multipart(msg)
        except KeyboardInterrupt:
            for worker in self.workers:
                worker.close()
            self.frontend.close()
            self.backend.close()
            self.context.term()

if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument("--host", default="tcp://127.0.0.1:5147")
    p.add_argument("path")
    args = p.parse_args()

    server = Server(args.path, host=args.host)
    server.serve()

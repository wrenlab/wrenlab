"""
FIXME:
    - remember index label names somehow
"""

from abc import ABC, abstractmethod

import bidict
import pandas as pd
import numpy as np
import scipy.sparse
from scipy.sparse import coo_matrix
import sklearn.decomposition

from wrenlab.mixin import Clusterable

class Bijective(ABC):
    def to_dict(self):
        o = bidict.bidict({self.ix_input[i]:self.ix_output[j] 
            for i,j in zip(*self.P.nonzero())})
        assert len(o) == self.P.shape[0] == self.P.shape[1]
        return o

class Projection(ABC):
    """
    A Projection projects an input matrix from one space into another.
    """
    def __init__(self, P, ix_input=None, ix_output=None):
        self.P = P
        self.ix_input = ix_input or np.arange(P.shape[0])
        self.ix_output = ix_output or np.arange(P.shape[1])
    
    #@abstractmethod
    #def rescale(self):
    #    pass

    def project(self, X):
        # FIXME: enforce alignment?

        # assumes dimensions are columns
        if isinstance(X, pd.Series):
            X = X.to_frame().T
        index = X.index
        X = X.reindex(self.ix_input, axis=1)
        X = np.array(X)
        o = X @ self.P
        o = pd.DataFrame(o, index=index, columns=self.ix_output)
        if o.shape[0] == 1:
            return o.iloc[0,:]
        return o

class DenseProjection(Projection, Clusterable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def to_frame(self):
        return pd.DataFrame(self.P, index=self.ix_input, columns=self.ix_output)

class SparseProjection(Projection):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.P = self.P.tocsr()

    def rescale(self):
        """
        Make outputs (projection columns) sum to 1 so input/output 
        scales will be the same/similar.
        """
        N = scipy.sparse.diags(1 / self.P.sum(axis=0).A.ravel())
        P = self.P @ N
        return type(self)(P, ix_input=self.ix_input, ix_output=self.ix_output)

    #######################
    # Bijective projections
    #######################

    def max(self, x: pd.Series):
        """
        Return a new Projection which consists of the maximum value of x
        for each dimension in the target space.

        The result will be a bijective projection with a smaller domain.
        For example, it will be a one-to-one mapping from probes to genes,
        but not all probes will be used.
        """
        # FIXME: make more generic for any aggregation (?) 
        #   returning a bijective projection
        x = np.array(x.reindex(self.ix_input, fill_value=0))
        # so that zeros in P will not get picked up
        x = (x - x.min() + 1)
        ix = self.P.T.multiply(x).T.argmax(axis=0)
        ix = np.array(ix.flat)
        P = self.P[ix,:]
        ix_input = [self.ix_input[i] for i in ix]
        return BijectiveSparseProjection(P, ix_input=ix_input, ix_output=self.ix_output)

    def min(self, x: pd.Series):
        return self.max(-x)

class BijectiveSparseProjection(SparseProjection, Bijective):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

def from_dict(M):
    ix_input = list(sorted(set(M.keys())))
    ix_output = list(sorted(set(M.values())))
    M_i = {k:i for i,k in enumerate(ix_input)}
    M_o = {k:j for j,k in enumerate(ix_output)}
    data, p_i, p_j = [],[],[]
    for k,v in M.items():
        p_i.append(M_i[k])
        p_j.append(M_o[v])
        data.append(1)

    data = np.array(data)
    p_i = np.array(p_i, dtype=int)
    p_j = np.array(p_j, dtype=int)
    P = coo_matrix((data,(p_i,p_j)), 
            shape=(len(ix_input), len(ix_output)), dtype=float).tocsc()
    return SparseProjection(P, ix_input=ix_input, ix_output=ix_output).rescale()

def PCA(X, k=10):
    model = sklearn.decomposition.PCA(n_components=k)
    model.fit(X)
    ix_output = ["C{}".format(i+1) for i in range(k)]
    return DenseProjection(model.components_.T, 
            ix_input=list(X.columns), ix_output=ix_output)

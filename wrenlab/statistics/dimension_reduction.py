"""
Methods for dimension reduction of input matrices.

NOTES:
- should PCA, varimax, etc, return the loadings matrix or the transformed matrix?
    (some methods may not have a "loading matrix")
"""

import xarray as xa
import sklearn.decomposition
import sklearn.random_projection
import scipy
import scipy.linalg
import numpy as np
import pandas as pd

from wrenlab.util import LOG

def KernelPCA(X, components=10):
    model = sklearn.decomposition.KernelPCA(n_components=components)
    return model.fit_transform(X)

def PCA(X, k=10):
    """
    Return PCA loading matrix, with k components, for a given input matrix X.
    """
    model = sklearn.decomposition.PCA(n_components=k)
    model.fit(X)
    return model.components_.T

def factor_rotation_matrix(L, gamma=1.0, max_iterations=100, tolerance=1e-6):
    """
    Return the rotation matrix R for a PCA loading matrix L using varimax.
    """
    from scipy import diag

    L = np.array(L)
    n,m = L.shape
    R = np.eye(m)
    d = 0
    for i in range(max_iterations):
        LOG.debug("factor_rotation_matrix : i = {}, d = {}".format(i, d))
        d_old = d
        z = L @ R
        U,S,Vh = scipy.linalg.svd(L.T @ (z**3 - (gamma/n) * z @ diag(diag(z.T @ z))))
        R = U @ Vh
        d = S.sum()
        if d_old != 0 and (d / d_old) < (1 + tolerance):
            break
    return R

def rotate_factors(X, k=10, condense=True, gamma=1.0, max_iterations=100, tolerance=1e-6):
    """
    PCA + varimax rotation for factor analysis.

    Runs PCA and rotate the components, returning the rotated loadings matrix.

    The particular type of rotation depends on the `gamma` parameter. See [2].

    Arguments
    =========
    X : The matrix to be reduced along columns

    k : int, default 10
        The number of components

    condense: bool, default True
        Whether to condense near-zero values of the new loadings
        matrix to zero.

    gamma : float, default 1
        Varying this value results in different varimax family rotations.
        For example, the default value of 1 corresponds to varimax, 0 to
        quartimax, etc.

    max_iterations : int, default 100
        Number of iterations in rotation algorithm.

    tolerance : float, default 1e-6
        Convergence criterion of rotation iteration.

    Returns
    =======
    A loading matrix of shape (m,k), where m is the number of columns in the
    input matrix X, and k is the number of components requested. The loadings
    matrix L can be used to transform X into rotated PCA space with P = X @ L.

    These rotations constrains values in L to be either near to or far from
    zero. So, the nonzero values of L can be used to interpret which columns in
    X are being collapsed into columns of P.

    Notes
    =====
    Like regular PCA, you can compute the loadings on a random subset of samples,
    then apply it to the whole matrix.

    References
    ==========
    [1] https://en.wikipedia.org/wiki/Talk:Varimax_rotation
    [2] https://www.mathworks.com/help/stats/rotatefactors.html

    """
    L = PCA(X, k=k)
    R = factor_rotation_matrix(L, gamma=gamma, 
            max_iterations=max_iterations, tolerance=tolerance)
    L_prime = L @ R

    if condense is True:
        ix = np.isclose(L_prime, 0)
        L_prime[ix] = 0
    #if isinstance(X, pd.DataFrame):
    #    L_prime = pd.DataFrame(L_prime, index=X.columns)
    return L_prime

def varimax(X, k=10, **kwargs):
    return rotate_factors(X, k=k, gamma=1, **kwargs)

def quartimax(X, k=10, **kwargs):
    return rotate_factors(X, k=k, gamma=0, **kwargs)

def equimax(X, k=10, **kwargs):
    d,m = (X.shape[1], k)
    return rotate_factors(X, k=k, gamma=m / 2, **kwargs)

def parsimax(X, k=10, **kwargs):
    d,m = (X.shape[1], k)
    gamma = d * (m - 1) / (d + m - 2)
    return rotate_factors(X, k=k, gamma=gamma, **kwargs)

def gaussian_random_projection(X, k=10):
    model = sklearn.random_projection.GaussianRandomProjection(n_components=k)
    model.fit(X)
    return model.components_.T

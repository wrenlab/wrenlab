"""
Statistics methods for meta-analysis.
"""

import scipy.stats
import numpy as np

def fisher_method(p, floor=1e-5):
    p = np.maximum(floor, p)
    X2 = -2 * np.log(p).sum()
    k = len(p)
    df = 2 * k
    return scipy.stats.chi2.sf(X2, df=df)

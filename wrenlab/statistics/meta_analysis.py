import pandas as pd
import numpy as np
import scipy.stats

import wrenlab.normalize
import wrenlab.ncbi.gene

def fisher_method(p):
    # df of p-values
    k = p.shape[1]
    X2 = -2 * p.apply(np.log).sum(axis=1)
    return pd.Series(scipy.stats.chi2.cdf(X2, 2*k), index=p.index)

def MAp(rs, SLPV=False):
    keys = list(rs.keys())
    ix = [ix[1] for ix in keys]
    p = pd.concat([rs[k][1].p for k in keys], axis=1)
    sign = pd.concat([rs[k][1].t.apply(np.sign) for k in keys], axis=1)
    for i in range(p.shape[0]):
        for j in range(p.shape[1]):
            if sign.iloc[i,:].iloc[j] == -1:
                p.iloc[i,j] = 1 - p.iloc[i,j]
    p.columns = ix
    p.columns.name = "ExperimentID"
    return fisher_method(p)

class Contrast(object):
    def __init__(self, summary, N, metadata=None):
        self.N = N
        self.summary = summary
        self.metadata = metadata or {}

class Result(object):
    def __init__(self, contrasts, metadata=None):
        self.metadata = metadata or {}
        self.contrasts = contrasts

    def copy(self):
        return Result(self.contrasts, metadata={k:v for k,v in self.metadata.items()})

    @property
    def taxon_id(self):
        return self.metadata["TaxonID"]

    @property
    def index(self):
        o = pd.Index([ct.metadata["ExperimentID"] for ct in self.contrasts])
        o.name = "ExperimentID"
        return o

    def summary(self):
        coef = self.coef()
        p = self.p()
        p_adjusted = self.p(adjust=True)
        o = pd.concat([coef, p, p_adjusted], axis=1)
        taxon_id = self.metadata.get("TaxonID", 9606)
        genes = wrenlab.ncbi.gene.info(taxon_id)
        return genes.join(o, how="inner").sort_values("p")

    def N(self):
        o = pd.Series([ct.N for ct in self.contrasts], index=self.index)
        o.name = "N"
        return o

    def experiment_metadata(self):
        corpus = wrenlab.text.corpus.geo.GEOCorpus()

        N = self.N()
        title = pd.Series([corpus.experiment_text(experiment_id).get("title", "") 
            for experiment_id in N.index], index=N.index)
        title.name = "Title"
        return pd.concat([N,title], axis=1)

    def coef(self):
        coef = pd.concat([ct.summary.coef for ct in self.contrasts], axis=1)
        coef.columns = [ct.metadata["ExperimentID"] for ct in self.contrasts]
        coef.columns.name = "ExperimentID"

        o = wrenlab.normalize.quantile(coef).mean(axis=1)
        o.name = "coef"
        return o

    def p(self, direction="both", adjust=False):
        assert direction in ("up", "down", "both")

        columns = [ct.metadata["ExperimentID"] for ct in self.contrasts]

        p = pd.concat([ct.summary.p for ct in self.contrasts], axis=1)

        if direction != "both":
            q = -1 if direction is "up" else 1
            index = p.index
            sign = pd.concat([ct.summary.t.apply(np.sign) for ct in self.contrasts], axis=1)
            p = np.array(p)
            ix = np.array(sign) == q
            p[ix] = 1 - p[ix]
            p = pd.DataFrame(p, index=index, columns=columns)
            p.columns.name = "ExperimentID"
            
        o = fisher_method(p)
        if adjust is True:
            from statsmodels.sandbox.stats.multicomp import multipletests
            o = pd.Series(multipletests(o)[1], index=o.index)
            o.name = "p_adjusted"
        else:
            o.name = "p"
        return o

    def enrichment(self, direction="both"):
        assert direction in ("up", "down", "both")
        AS = wrenlab.enrichment.annotation("GO", taxon_id=self.taxon_id)
        df = self.summary()
        ix = (df.p < 0.05)
        if direction == "up":
            ix &= (df.coef > 0)
        elif direction == "down":
            ix &= (df.coef < 0)
        else:
            pass
        return AS.enrichment(ix).dropna()

    def to_excel(self, path):
        import os
        if os.path.exists(path):
            os.unlink(path)

        with pd.ExcelWriter(path, engine="xlsxwriter") as xlw:
            def write_df(df, name):
                for idx, col in enumerate(df):
                    df.to_excel(xlw, sheet_name=name)
                    sheet = xlw.sheets[name]
                    series = df[col]
                    max_len = max((
                        series.astype(str).map(len).max(),
                        len(str(series.name))
                        )) + 1
                    sheet.set_column(idx, idx, min(max_len, 60))

            df = self.summary()
            write_df(df, "Genes")

            AS = wrenlab.enrichment.annotation("GO", taxon_id=self.metadata["TaxonID"])

            try:
                write_df(self.enrichment(direction="up"), "GOEA_up")
            except:
                pass

            try:
                write_df(self.enrichment(direction="down"), "GOEA_down")
            except:
                pass

            write_df(self.experiment_metadata(), "Metadata")



"""
References
----------

- http://phillipmfeldman.org/Python/quantile_regression_demo.py
"""

import numpy as np
import scipy.optimize

def tilted_abs(rho, x):
   """
   The tilted absolute value function is used in quantile regression.

   Arguments
   ---------

   rho: This parameter is a probability, and thus takes values between 0 and 1.

   x: This parameter represents a value of the independent variable, and in
        general takes any real value (float) or NumPy array of floats.
   """

   return x * (rho - (x < 0))

def fit2(X, y, q=0.5):
    Xa = np.array(X)
    ya = np.array(y)

    beta0 = np.zeros(X.shape[1])
    if X.shape[1] > 1:
        beta0[1] = 1.0

    def objective(beta, rho):
        return tilted_abs(rho, ya - Xa @ beta).sum()

    beta = scipy.optimize.fmin(objective, x0=beta0, args=(q,), xtol=1e-8,
        disp=True, maxiter=3000)
    return beta

def fit(X, y, w_init=1, d=0.0001, max_iter=100, tolerance=1e-3):
    X = np.array(X)
    y = np.array(y)

    n,p = X.shape
    delta = np.array(np.repeat(d, n)).reshape(1,n)
    w = np.repeat(w_init, n)
    W = np.diag(w)
    B = np.linalg.inv(X.T @ W @ X) @ ( X.T @ W @ y)
    for _ in range(max_iter):
        _B = B
        _w = abs(y - X @ B).T
        w = 1.0 / np.maximum( delta, _w )
        W = np.diag(w[0])
        B = np.linalg.inv(X.T @ W @ X) @ (X.T @ W @ y)
        tol = np.abs(B - _B).sum()
        if tol < tolerance:
            return B
    return B

__all__ = ["rank_to_normal", "standardize", "log_transform", "boxcox", "pca", "tsne"]

import math
import re
import multiprocessing as mp

import numpy as np
import pandas as pd
import scipy.stats
import sklearn.decomposition
import statsmodels.api as sm
import statsmodels.formula.api as smf
from joblib import Parallel, delayed
import matplotlib.mlab

import wrenlab.util.pandas
from wrenlab.correlation import standardize as _standardize
from wrenlab.util.pandas import papply

def logit(p):
    return np.log(p) - np.log(1 - p)

def inverse_logit(p):
    return np.exp(p) / (1 + np.exp(p))

def gini(x):
    """
    Calculate the Gini coefficient of a numpy array.
    """
    # https://github.com/oliviaguest/gini
    x = np.array(x)
    x = x.flatten()
    if np.amin(x) < 0:
        x -= np.amin(x)
    x += 0.0000001
    x = np.sort(x)
    index = np.arange(1,x.shape[0]+1)
    n = x.shape[0]
    return ((np.sum((2 * index - n  - 1) * x)) / (n * np.sum(x)))

def _log_transform(x, threshold=50, base=math.e):
    def t(x):
        if x.min() <= 0:
            x = x - x.min() + 1 
        return x.apply(np.log) / math.log(base)

    if threshold is not None:
        if x.max() >= threshold:
            return t(x)
        else:
            return x
    else:
        return t(x)

def log_transform(X, threshold=50, base=math.e, axis=0):
    """
    Conditionally log-transform each column/row of X if that column/row
    max value is above `threshold`.
    """
    return papply(X, _log_transform, axis=axis, threshold=threshold, base=base)

def standardize(x, axis=0):
    if isinstance(x, pd.Series):
        return (x - x.mean()) / x.std()
    elif isinstance(x, pd.DataFrame):
        return x.apply(standardize, axis=axis)
        #return papply(x, standardize, axis=axis)
    else:
        raise ValueError

def rank_to_normal(ranks):
    pctile = (ranks - ranks.min() + 1) / (ranks.max() - ranks.min() + 2)
    return pd.Series(scipy.stats.norm.ppf(pctile), index=pctile.index)

def pca(X, k=100, multiply_evar=False):
    # FIXME FIXME FIXME: THIS HAS A MEMORY LEAK IT IS SOMEWHERE IN THE SKLEARN (maybe?)
    # (but in practice it is only a major problem if called repeatedly in the same process)
    Xa = np.array(X)
    model = sklearn.decomposition.PCA(n_components=k, svd_solver="arpack")
    columns = ["PC{}".format(i) for i in range(k)]
    o = model.fit_transform(Xa)
    if multiply_evar:
        o *= model.explained_variance_ratio_
    del Xa
    del model
    return pd.DataFrame(o, index=X.index, columns=columns)

"""
def pca(X, k=100):
    assert k <= X.shape[1]
    pc = matplotlib.mlab.PCA(X)
    columns = ["PC{}".format(i) for i in range(k)]
    if isinstance(X, pd.DataFrame):
        index = X.index
    else:
        index = np.arange(X.shape[0])
    return pd.DataFrame(pc.Y[:,:k], index=index, columns=columns)
"""

def tsne(X, n_components=2, **kwargs):
    if X.shape[1] > 50:
        X = pca(X, k=min(X.shape)-1)
    model = sklearn.manifold.TSNE(n_components=n_components, **kwargs)
    o = model.fit_transform(X)
    return pd.DataFrame(o, index=X.index)

def infer_groups(D):
    """
    Infers group names from Sample IDs.
    
    Assumes Sample IDs are in the format "GroupXX", where XX is an arbitrary
    length integer. Also checks against the design matrix to ensure that the
    group names match design row values. 
    """
    groups = pd.Series([c[:re.search("[0-9]", c).span()[0]] for c in D.index],
            index=D.index)

    DD = D.drop_duplicates()
    for ix in DD.index:
        ix = (D == D.loc[ix,:]).all(axis=1)
        if len(set(groups.ix[ix])) != 1:
            raise Exception("Group names obtained by parsing do not match design matrix.")
    return groups

def dummy_code(D, groups=None, reference=None):
    """
    Generate a dummy-coded design matrix (one variable for each group).

    Arguments
    ---------
    D : :class:`pandas.DataFrame`
        The initial design matrix.
    reference : str, optional
        The reference group. Defaults to first group by position.
    """
    if groups is None:
        groups = infer_groups(D)

    levels = list(groups.unique())
    if reference is None:
        reference = levels[0]

    n = D.shape[0]
    levels_wo_reference = [level for level in levels if level != reference]

    o = pd.DataFrame(np.zeros((n, len(levels) - 1)), 
            index=D.index, columns=levels_wo_reference).astype(int)
    for level in levels_wo_reference:
        o.ix[groups == level, level] = 1
    intercept = pd.Series(np.ones(n), name="Intercept", index=D.index)
    return pd.concat([intercept, o], axis=1).astype(int)

def compare_all_groups(y, D, groups=None, model_class=sm.OLS):
    """
    Perform an all-vs-all comparison of groups, returning the p-value
    for each comparison.
    """
    if groups is None:
        groups = infer_groups(D)

    levels = list(groups.unique())
    nl = len(levels)

    o = pd.DataFrame(np.ones((nl,nl)), index=levels, columns=levels)
    
    for level in levels:
        D_l = dummy_code(D, reference=level, groups=groups)
        model = model_class(endog=y, exog=D_l)
        fit = model.fit()
        p = fit.pvalues
        for l2 in p.index[1:]:
            o.ix[level,l2] = p[l2]
    return o

def _boxcox(x):
    return scipy.stats.boxcox(x)[0]

def boxcox(X, axis=0):
    """
    Apply Box-Cox transformation to each column of a :class:`pandas.DataFrame`.
    """
    if (X <= 0).any().any():
        X = X - X.min() + 1
    o = papply(X, _boxcox, axis=axis)
    return o

def model_parameters(fn, parameters, formula, **kwargs):
    rs = wrenlab.util.apply_parameters(fn, parameters=parameters, **kwargs)\
            .dropna()
    return smf.ols(rs.reset_index(), data=rs).fit()

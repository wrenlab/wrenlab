"""
Python interface to R functions.
"""

import collections
from contextlib import redirect_stdout, redirect_stderr
import os
import warnings

import pandas as pd
import numpy as np
import scipy.stats

R_INITOPTIONS = ("rpy2", "--vanilla")
import rpy2.rinterface 
rpy2.rinterface.initoptions = R_INITOPTIONS
from rpy2.robjects import r, pandas2ri
from rpy2.robjects.packages import importr

from wrenlab.util import LOG

#pandas2ri.activate()

LOG.info("Loading R interface...")

r["Sys.setenv"](R_SHARE_DIR="/usr/share/R")

def vsn(X):
    """
    Variance-stabilizing normalization.

    Arguments
    ---------
    X : :class:`pandas.DataFrame`
        An expression matrix, with probes as rows and samples as columns.
    """
    LOG.info("Calling R VSN function on matrix of shape: {}".format(X.shape))
    pandas2ri.activate()
    try:
        pkg = importr("vsn")
        Xm = np.array(X)
        with open(os.devnull, "w") as h:
            with redirect_stdout(h):
                o = np.array(r["as.matrix"](pkg.vsn2(Xm)))
        o = pd.DataFrame(o, index=X.index, columns=X.columns)
        o.index.name = X.index.name
        o.columns.name = X.columns.name
        return o
    finally:
        pandas2ri.deactivate()

def impute_qPCR(X, design, controls=None, max_iterations=100):
    # FIXME: describe what kind of input (log-norm or what)

    if controls is None:
        controls = list(X.index[~X.isnull().any(axis=1)])

    pkg = importr("wrenlab")
    c = r["unlist"](controls)

    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                o = pkg.impute_qPCR(X, design, c, max_iterations=max_iterations)
    o = pandas2ri.ri2py(o)
    return pd.DataFrame(o, index=X.index, columns=X.columns)

def wilcoxGST(index, statistics, alternative="mixed", type="auto"):
    assert alternative in ("mixed", "either", "up", "down")
    assert type in ("t", "f", "auto")
    # unused: ranks_only (default T), nsim
    pkg = importr("limma")
    return float(pkg.geneSetTest(index, statistics, alternative=alternative, type=type, ranks_only=True)[0])

def fit_rocke_durbin(X, theta=0.05):
    pkg = importr("GESTr")
    Xm = np.array(X)
    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            o = pkg.fitRockeDurbin(Xm, theta=theta)
    return dict(zip(o.names, [x[0] for x in o]))

import pandas as pd
import numpy as np

from .util import *
from .util import _maybe_ints, _make_design

def roast(X, D, sel, contrast, formula=None):
    assert type(contrast) in (int, str)

    # FIXME: qnorm, weight
    D = _make_design(D,formula) if formula is not None else D
    X,D = _align(X,D)
    X = quantile_normalize(X)
    if isinstance(contrast, str):
        contrast = list(D.columns).index(contrast)

    pkg = importr("limma")
    Dr = D.to_r()
    fit = pkg.lmFit(X.to_r(matrix=True),Dr)
    sigma = _get_list_item(fit, "sigma")
    df = _get_list_item(fit, "df.residual")
    sv = pkg.squeezeVar(FloatVector(np.array(sigma)**2), df=df)
    vprior = _get_list_item(sv, "var.prior")

    index = []
    rows = []
    sel = sel[list(set(sel.index) & set(X.index))]
    weights = FloatVector(np.array(sel))
    o = pkg.roast(
            y=X.loc[sel.index,:].to_r(),
            design=Dr,
            gene_weights=weights,
            contrast=contrast+1,
            var_prior=vprior, 
            df_prior=vprior)[0]
    o = pd.DataFrame.from_r(o)
    o.columns = ["proportion", "lp"]
    o["lp"] = -1 * o["lp"].apply(np.log10)
    return o

class ExpressionType(enum.Enum):
    MICROARRAY = 1
    RTPCR = 2
    RNASEQ = 3

def quantile_normalize(X):
    pkg = importr("limma")
    Xn = pkg.normalizeBetweenArrays(X.to_r(matrix=True))
    o = pd.DataFrame.from_r(r["as.data.frame"](Xn))
    o.index = _maybe_ints(o.index)
    o.columns = _maybe_ints(o.columns)
    return o

def fit(X, D, 
        formula=None,
        coefficients=None, 
        normalize=False, 
        type=ExpressionType.MICROARRAY,
        weighted=False):
    """
    Run limma on an expression and design matrix.

    Parameters
    ----------
    X : :class:`pandas.DataFrame`
        Expression matrix (samples x genes)

    D : :class:`pandas.DataFrame`
        Design matrix (samples x covariates), or, if "formula"
        is defined, a data frame of covariates to be turned into
        a design matrix according to the formula.

    coefficients : list of str or None, optional
        Run the analysis on these coefficients, which will affect the returned
        p-values and coefficients (the p-value reflects the proposition that
        any of the supplied coefficients significantly differ). If None,
        then all columns of D will be used.

    normalize : bool, optional
        Quantile normalize the data before analysis.

    weighted : bool, optional
        Use limma's "arrayWeights" function to determine the quality of
        each array and weight it appropriately in the analysis.

    formula : str or None, optional
        An optional formula string for the RHS of a linear model to extract
        covariates from D. If this parameter is supplied, it affects how "D" is
        interpreted (see also the documentation for D).

    type : :class:`ExpressionType`
        Indicates the type of expression data given by "X". 
        Processing will slightly differ depending on the type.

    Returns
    -------
    :class:`pandas.DataFrame`
        A table containing the model coefficients, F and B statistics,
        and adjusted and non-adjusted p-values for each gene/transcript.
    """
    assert isinstance(type, ExpressionType)
    assert type != ExpressionType.RTPCR
        
    X = X.T

    # Note: limma's input for expression is (genes x samples)
    if coefficients is not None:
        for c in coefficients:
            assert c in D.columns
    D = _make_design(D,formula) if formula is not None else D

    ix = list(set(X.columns) & set(D.index))
    X,D = X.loc[:,ix], D.loc[ix,:]

    for pattern in D.drop_duplicates().to_records(index=False):
        ok = set(X.index)
        for _,g in D.groupby(list(D.columns)):
            ok &= set(X.loc[:,D.index].dropna(how="all").index)
        X = X.loc[sorted(list(ok)),:]
    
    assert all(np.array(X.shape) > 0)
    ix = list(set(X.columns) & set(D.index))
    X,D = X.loc[:,ix], D.loc[ix,:]

    if normalize:
        if not type == ExpressionType.RNASEQ:
            X = quantile_normalize(X)
    
    pkg = importr("limma")

    Xr = X.to_r(matrix=True)
    weights = pkg.arrayWeights(Xr) if weighted else r("NULL")
    fit = pkg.eBayes(pkg.lmFit(Xr,D.to_r(),weights=weights))

    if coefficients is None:
        coefficients = list(map(str, D.columns[1:]))
    coefficients_original = coefficients

    coefficients = StrVector(list(map(
        lambda x: str(x).replace(":","."), coefficients)))
    #coefficients = StrVector(["coef{}".format(i+1) for i in range(len(coefficients_original))])

    table = pkg.topTable(fit, 
            number=X.shape[0], 
            adjust_method="holm",
            coef=coefficients)
    o = pd.DataFrame.from_r(table)
    # FIXME: possibly wrong
    o.index = X.index[np.array(list(map(int, o.index))) - 1]
    o = replace_index_names(o,
            ["AveExpr", "P.Value","adj.P.Val"],
            ["mean_expression", "log_p","log_p_adjusted"])
    o["log_p"] = - o["log_p"].apply(np.log10)
    o["log_p_adjusted"] = - o["log_p_adjusted"].apply(np.log10)

    columns = list(o.columns)
    columns[:len(coefficients_original)] = coefficients_original
    o.columns = columns
    o["N"] = X.shape[1]
    return o.loc[:,["N"] + list(o.columns[:-1])]

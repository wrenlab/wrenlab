import numpy as np

from .util import *
from rpy2.robjects import r, pandas2ri
from rpy2.robjects.vectors import IntVector, StrVector, ListVector

def GSEA(X, contrast, sets):
    """
    X: matrix w/ samples on columns
    contrast: seq (# == nc of X) of 0,1,-1
    sets: dict of seqs
    """
    X = X.copy()
    X.index = list(map(str, X.index))
    X_r = r("as.matrix")(X.to_r())
    pkg = importr("gage")
    #gs = pkg.go_gsets(species="mouse").get("go.sets")
    gs = ListVector({k:list(map(str, v)) for k,v in sets.items()})
    ref = IntVector(np.where(contrast == -1)[0] + 1)
    samp = IntVector(np.where(contrast == 1)[0] + 1)
    rs = pkg.gage(X_r, gsets=gs, ref=ref, samp=samp, set_size=IntVector([0,100000]))

    gt = from_r(rs.get("greater"))
    lt = from_r(rs.get("less"))
    o = pd.DataFrame.from_dict({
        "N": gt["set.size"],
        "statistic": gt["stat.mean"],
        "p": gt["p.val"],
        "q": gt["q.val"]
    })
    cutoff = 0.025
    o["direction"] = (o["p"] < cutoff).astype(int)
    o["direction"][o["p"] > (1 - cutoff)] = -1
    return o.loc[:,["N","direction","statistic","p","q"]]

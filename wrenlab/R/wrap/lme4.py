import collections

import scipy.stats
from rpy2.robjects import r, pandas2ri

from .util import *

class LME4Model(object):
    def __init__(self, fit):
        self._fit = fit

    @property
    def fixed(self):
        return self.fixed_effects

    @property
    def random(self):
        return self.random_effects

    @property
    def fixed_effects(self):
        o = pandas2ri.ri2py(compose("as.data.frame", "coef", "summary")(self._fit))
        o.index = fix_index(o.index)
        o.columns = ["coef", "SE", "t"]
        o["p"] = 2 * (1 - scipy.stats.norm.cdf(o["t"].abs()))
        return o

    @property
    def random_effects(self):
        re = r["ranef"](self._fit)
        o = collections.OrderedDict()
        for key, df in zip(re.names, re):
            o[key] = pandas2ri.ri2py(df)
            o[key].columns = fix_index(o[key].columns)
        return o

def lme4(formula, y, design):
    assert y.shape[0] == design.shape[0]
    assert "y" not in design.columns

    formula_r = r["as.formula"]("y ~ {}".format(formula))
    D = design.copy()
    D["y"] = y

    pkg = importr("lme4")
    fit = pkg.lmer(formula_r, D)
    return LME4Model(fit)

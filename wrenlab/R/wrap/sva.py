import os
from contextlib import redirect_stdout, redirect_stderr

import pandas as pd
import numpy as np

from .util import *

def ComBat(X, batch):
    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            with redirect_stderr(h):
                pkg = importr("sva")
                batch = IntVector(np.array(pd.Series(batch).astype("category").cat.codes + 1))
                obj = pkg.ComBat(to_r(X.T), batch)
                return from_r(obj).T

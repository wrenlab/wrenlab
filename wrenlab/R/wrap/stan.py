from rpy2.robjects import r
from rpy2.robjects.packages import importr

def dict_to_list(d):
    ks, vs = map(list, zip(*d.items()))
    o = r["as.list"](vs)
    o.names = ks
    return o

class StanModel(object):
    def __init__(self, path):
        pkg = importr("rstan")
        self._model = pkg.stan_model(path)

    def fit(self, data, iter=1000, chains=4):
        pkg = importr("rstan")
        data = dict_to_list(data)
        fit = self._model
        fit = pkg.sampling(self._model, data=data, iter=iter, chains=chains)
        return StanFit(fit)

    def save(self, path):
        pass

    @staticmethod
    def load(path):
        pass

class StanFit(object):
    def __init__(self, fit):
        self._fit = fit

    def serve(self, host="127.0.0.1", port=8080, launch_browser=False):
        pkg = importr("shinystan")
        pkg.launch_shinystan(object=self._fit, 
                port=port, 
                **{"launch.browser": launch_browser})

def stan(model_path, data, iter=1000, chains=4):
    pkg = importr("rstan")
    data = dict_to_list(data)
    #handle = r["file"](model_path)
    fit = pkg.stan(file=model_path, data=data, iter=iter, chains=chains)
    return StanFit(fit)

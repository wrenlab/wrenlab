import numpy as np
import pandas as pd

from .util import *
from .util import _make_design

def edgeR(X, D, formula=None, coefficients=None, weighted=None):
    # weighted: ignored
    assert formula is not None

    return_model = False
    if coefficients is not None:
        assert len(coefficients) == 1
        coef = list(D.columns).index(coefficients[0])
    else:
        return_model = True

    D = _make_design(D,formula)
    ix = list(set(X.columns) & set(D.index))
    X,D = X.loc[:,ix].astype(int), D.loc[ix,:]

    X[X==0] = np.nan
    for pattern in D.drop_duplicates().to_records(index=False):
        ok = set(X.index)
        for _,g in D.groupby(list(D.columns)):
            ok &= set(X.loc[:,D.index].dropna(how="all").index)
        X = X.loc[sorted(list(ok)),:]
    X = X.fillna(0)
 
    importr("splines")
    pkg = importr("edgeR")

    # TODO: if I am returning a model, is using a "DGEList" going to mess that up?
    # Such a model should not have to know about contrasts.
    Dr = D.to_r()
    Xr = X.to_r()
    dgl = pkg.DGEList(counts=Xr, group=Dr[1])
    dgl = pkg.calcNormFactors(dgl)
    dispersion = pkg.estimateGLMCommonDisp(dgl, Dr)
    dispersion = pkg.estimateGLMTagwiseDisp(dispersion, Dr)
    dispersion = pkg.estimateGLMTrendedDisp(dispersion, Dr)
    fit = pkg.glmFit(dispersion, Dr)
    if return_model:
        o = pd.DataFrame.from_r(fit.get("coefficients"))
        o.columns = D.columns
        return o
    comparison = pkg.glmLRT(fit, coef=2)
    table = r["as.data.frame"](pkg.topTags(comparison, 
        n=X.shape[0]))
    o = pd.DataFrame.from_r(table)
    o = replace_index_names(o,
            ["logCPM"], ["CPM"])
    o["CPM"] = 2 ** o["CPM"]
    o["SLPV"] = o["PValue"].apply(np.log10) * np.sign(o["logFC"])
    return o

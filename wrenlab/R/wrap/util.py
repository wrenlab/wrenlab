import enum
import sys
import os
from contextlib import redirect_stdout

import pandas as pd
import numpy as np
import patsy

R_INITOPTIONS = ("rpy2", "--vanilla")
import rpy2.rinterface 
rpy2.rinterface.initoptions = R_INITOPTIONS

from rpy2.robjects import r, StrVector, \
    ListVector, IntVector, FloatVector, DataFrame, Matrix
from rpy2.robjects.packages import importr as _importr
import rpy2.rlike.container as rlc

def importr(pkgname):
    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            pkg = _importr(pkgname)
    return pkg

#################################
# Coerce DataFrames to and from R
#################################

NP2R = {
        "float32": FloatVector,
        "float64": FloatVector,
        "int64": IntVector,
        "object": StrVector
}

def to_r(self, matrix=False):
    # TODO: rpy2 itself has a converter, see doc
    #import pandas.rpy.common as pdr

    columns = []
    for c,dt in zip(self.columns, self.dtypes):
        ctor = NP2R[dt.name]
        columns.append((c,ctor(self[c])))
    df = DataFrame(rlc.OrdDict(columns))
    df.rownames = list(map(str, self.index))
    if matrix:
        return r["as.matrix"](df)
    return df
    
def _strip_x(ix):
    x = ix[0]
    convert = isinstance(x,str) \
            and x.startswith("X") \
            and x[1:].isdigit()
    if convert:
        ix = [int(x[1:]) for x in ix]
    return ix

def from_r(df):
    if isinstance(df, pd.DataFrame):
        return df
    if isinstance(df, Matrix):
        df = r["as.data.frame"](df)
    cs = r.colnames(df)
    cs_p = list(cs)
    data = {}
    for c in cs_p:
        ix = cs_p.index(c)
        data[c] = np.array(df[ix])
    o = pd.DataFrame.from_dict(data)
    o.index=_strip_x(list(r.rownames(df)))
    o = o.loc[:,map(str, cs)]
    o.columns = _strip_x(o.columns)
    return o

def replace_index_names(X, names_from, names_to, axis=1):
    assert len(names_from) == len(names_to)
    assert axis in (0,1)
    if axis == 0:
        X = X.T

    columns = list(X.columns)
    for nf,nt in zip(names_from, names_to):
        for i,c in enumerate(columns):
            if c == nf:
                columns[i] = nt
                break

    X.columns = columns
    if axis == 0:
        X = X.T
    return X

ListVector.get = lambda self, key: self[self.names.index(key)]

pd.DataFrame.to_r = to_r
pd.DataFrame.from_r = from_r

##########
# Wrappers
##########

def _index(items):
    return dict(map(reversed, enumerate(items)))

def _make_design(D, formula):
    return patsy.dmatrix(formula, data=D, 
            return_type="dataframe")\
                    .dropna(how="any")

def _align(X,D):
    # FIXME: figure out if limma supports NaNs and add option accordingly
    X = X.T\
            .dropna(how="all", axis=1)\
            .dropna(how="all", axis=0)
    D,X = D.align(X, axis=0, join="inner")
    X = X.T
    return X,D

def _get_list_item(rlist, key):
    ix = _index(rlist.names)
    return rlist[ix[key]]

def _maybe_ints(ix):
    try:
        return list(map(int, ix))
    except ValueError:
        return list(ix)

def compose(*fns):
    def fn(x):
        for fn in reversed(fns):
            x = r[fn](x)
        return x
    return fn

def rstrip_digit(x):
    for i in reversed(range(len(x))):
        if not x[i].isdigit():
            return x[:(i+1)]
    return ""

def fix_index(index):
    o = []
    for ix in index:
        if ix == "(Intercept)":
            o.append("Intercept")
            continue
        else:
            ixs = []
            for six in ix.split(":"):
                base = rstrip_digit(six)
                if len(base) < len(six):
                    digit = six[len(base):]
                    six = "{}[{}]".format(base, digit)
                ixs.append(six)
            o.append(":".join(ixs))
    return o

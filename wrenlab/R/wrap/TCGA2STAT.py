from .util import *

import wrenlab.ncbi.gene
import wrenlab.dataset
from wrenlab.util import memoize

DISEASES = ["ACC", "BLCA" , "BRCA" , "CESC" , "CHOL" , "COAD" , "COADREAD" ,
        "DLBC" , "ESCA" , "FPPP" , "GBM" , "GBMLGG" , "HNSC" , "KICH" , "KIPAN"
        , "KIRC" , "KIRP" , "LAML" , "LGG" , "LIHC" , "LUAD" , "LUSC" , "MESO"
        , "OV" , "PAAD" , "PCPG" , "PRAD" , "READ" , "SARC" , "SKCM" , "STAD" ,
        "TGCT" , "THCA" , "THYM" , "UCEC" , "UCS", "UVM"]

@memoize
def RNASeq2(disease, data_type="RNASeq2"):
    pkg = importr("TCGA2STAT")
    obj = pkg.getTCGA(**{
        "disease": disease,
        "data.type": data_type,
        "clinical": True
    })
    dat = obj[obj.names.index("dat")]
    o = pkg.TumorNormalMatch(dat)
    Xc = from_r(o[1])
    Xt = from_r(o[0])
    clinical = from_r(obj[obj.names.index("clinical")]).loc[Xc.columns,:]
    Xc.columns = [ix + "-C" for ix in Xc.columns]
    Xt.columns = [ix + "-T" for ix in Xt.columns]
    assert clinical.shape[0] == Xc.shape[1] == Xt.shape[1]
    n = Xc.shape[1]

    X = pd.concat([Xc,Xt], axis=1).T

    A = pd.DataFrame({
        "Age": list(clinical["yearstobirth"]) + list(clinical["yearstobirth"]),
        "PatientID": list(clinical.index) + list(clinical.index),
        "Disease": [disease] * n * 2,
        "Cancer": np.concatenate([np.repeat(0,n),np.repeat(1,n)])
    }, index=list(Xc.columns) + list(Xt.columns))

    genes = wrenlab.ncbi.gene.info(9606)
    symbol_id = dict(zip(genes.Symbol, genes.index))
    X = X.ix[:,X.columns.isin(symbol_id)]
    X.columns = [symbol_id[c] for c in X.columns]
    X = X.loc[:,list(sorted(X.columns))]
    return wrenlab.dataset.Dataset(X, row_annotation=A)

def TCGA(data_type="RNASeq2"):
    exclude = [
        "ACC", "DLBC", "FPPP", "GBM", "GBMLGG", "LAML", "LGG", "MESO", "OV",
        "SKCM", "TGCT", "UCEC", "UCS", "UVM"
    ]

    M = {}
    for disease in DISEASES:
        if disease in exclude:
            continue
        try:
            M[disease] = RNASeq2(disease, data_type=data_type)
        except:
            continue
    keys = list(sorted(M.keys()))
    X = pd.concat([M[k].to_frame() for k in keys])
    A = pd.concat([M[k].row_annotation for k in keys])
    return wrenlab.dataset.Dataset(X, row_annotation=A)

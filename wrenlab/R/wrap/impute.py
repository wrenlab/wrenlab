__all__ = ["MI", "kNN"]

import os
from contextlib import redirect_stdout, redirect_stderr
import multiprocessing as mp

import pandas as pd
import numpy as np
from rpy2.robjects import r, pandas2ri

import wrenlab.util.pandas
from wrenlab.R import importr
from wrenlab.util import LOG

def MI(X):
    """
    Multiple imputation using the "mi" package.

    Arguments
    ---------
    X : :class:`pandas.DataFrame`
        A matrix with samples as rows and variables as columns.
        Missing values (NaN) will be imputed.

    Returns
    -------
    A :class:`pandas.DataFrame` with missing values replaced by imputed
    values.
    """
    # there are lots of checks supposed to be done on the imputed data
    # see: https://cran.r-project.org/web/packages/mi/vignettes/mi_vignette.pdf

    # I think it is required that nr > nc
    # I think stdev must also not be 0, or no multicollinearity allowed?
    seed = 0

    #X = wrenlab.util.pandas.trim_missing(X, 0.25)

    int_columns = False
    if X.columns.dtype == np.int64:
        int_columns = True
        X.columns = ["X{}".format(ix) for ix in X.columns]

    pkg = importr("mi")
    X_r = r("as.data.frame")(X)
    X_r = pkg.missing_data_frame(X_r)
    #X_r = pkg.change(X_r, y=r["as.character"](list(X.columns)),
    #        what="family", to="gaussian")

    n_chains = 4
    r("options")(**{"mc.cores": 16}) #int(mp.cpu_count() * 2/3)})
    # FIXME: 
    # - there is info on how to determine if there was enough iters to converge
    # - basically it seems the mean of each variable (only the imputed parts?)
    #   should be close to equal
    kwargs = {
            "n.iter": 5, 
            "n.chains": n_chains, 
            "max.minutes": 20, 
            "parallel": True,
            #"verbose": False,
            "seed": seed
    }
    with open(os.devnull, "w") as h:
        LOG.info("Running MI (multiple imputation) on matrix of shape: {}".format(X.shape))
        with redirect_stdout(h):
            with redirect_stderr(h):
                pass
        imputations = pkg.mi(X_r, **kwargs)

    def complete(i):
        Xi = pandas2ri.ri2py(pkg.complete(imputations)[i])
        Xi = Xi.loc[:,X.columns]
        Xi.index = X.index
        if int_columns:
            Xi.columns = [int(c[1:]) for c in Xi.columns]
        return Xi

    # FIXME: Might want to somehow use the data from all the chains...
    return complete(-1)

def MICE(X):
    # Easy, but very slow
    X = wrenlab.util.pandas.trim_missing(X)
    n = 5

    pkg = importr("mice")
    pm = pkg.quickpred(X) 
    model = pkg.mice(X, m=n, method="fastpmm", predictorMatrix=pm)
    # FIXME: probably should combine results from multiple models or something
    # This just returns the last chain
    o = pkg.complete(model, n)
    o = pandas2ri.ri2py(o)
    if X.columns.dtype == np.int64:
        o.columns = [int(ix[1:]) for ix in o.columns]
    return o

def kNN(X, k=10):
    """
    Multiple imputation using the kNN method of the "impute" package.

    Arguments
    ---------
    X : :class:`pandas.DataFrame`
        A matrix with variables as columns and samples as rows.
        Missing values (NaN) will be imputed.

    Returns
    -------
    A :class:`pandas.DataFrame` with missing values replaced by imputed
    values.
    """
    seed = 0
    rowmax = 0.5
    colmax = 0.8

    X = wrenlab.util.pandas.trim_missing(X,
            row_threshold=(1 - rowmax),
            column_threshold=(1 - colmax))
    Xmr = r("as.matrix")(np.array(X, dtype=np.double))
    pkg = importr("impute")
    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            rs = pkg.impute_knn(Xmr, k=k, rowmax=rowmax, colmax=colmax, rng_seed=seed)
    o = np.array(rs[list(rs.names).index("data")], dtype=np.double)
    return pd.DataFrame(o, index=X.index, columns=X.columns)

def qPCR(X, design, controls=None, max_iterations=100):
    """
    Impute missing values for a qPCR experiment using the `nondetects` package.

    Probes = rows.
    """
    # FIXME: describe what kind of input (log-norm or what)

    if controls is None:
        controls = list(X.index[~X.isnull().any(axis=1)])

    pkg = importr("wrenlab")
    c = r["unlist"](controls)

    with open(os.devnull, "w") as h:
        with redirect_stdout(h):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                o = pkg.impute_qPCR(X, design, c, max_iterations=max_iterations)
    o = pandas2ri.ri2py(o)
    return pd.DataFrame(o, index=X.index, columns=X.columns)

import numpy as np

from .util import *

def fit(D,X,coef):
    assert X.shape[0] == D.shape[0]
    assert coef in D.columns
    #columns = X.columns
    #X.columns = ["X{}".format(i) for i in list(range(X.shape[1]))]

    Xr = X.T.to_r()
    Dr = r("as.matrix")(D.to_r())

    nc = X.shape[0]
    column_data = pd.DataFrame({"x": np.repeat(1, nc)}, index=D.index).to_r()

    pkg = importr("DESeq2")
    obj = pkg.DESeqDataSetFromMatrix(Xr, column_data, Dr)
    analysis = pkg.DESeq(obj)
    rs = pkg.results(analysis, name=coef)
    o = pd.DataFrame.from_r(r("as.data.frame")(rs))
    o.columns = ["mean_expression", "log2FC", "SE", "statistic", "p", "p_adjusted"]
    o = o.sort_values("p_adjusted")
    #o.index = columns
    return o.dropna(subset=["p_adjusted"]).sort_values("p_adjusted")


def test():
    nr, nc = 10, 5
    X = (pd.DataFrame(np.random.normal(size=(nr, nc))) * 10000).apply(np.abs).astype(int)
    D = pd.DataFrame({
        "Intercept": np.repeat(1, nr),
        "Condition": np.array(list(np.repeat(0, nr // 2)) + list(np.repeat(1, nr // 2)))
    })
    X.index = D.index = ["S{}".format(i+1) for i in range(nr)]
    X.columns = ["G{}".format(i+1) for i in range(nc)]
    rs = fit(D,X,"Condition")
    print(rs)

if __name__ == "__main__":
    test()

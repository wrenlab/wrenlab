import functools

import pandas as pd

import pyensembl.ensembl_release
from pyensembl.ensembl_release_versions import MAX_ENSEMBL_RELEASE
from pyensembl.species import Species

from wrenlab.util import memoize

if not "gallus_gallus" in Species._latin_names_to_species:
    Species.register(
            latin_name="gallus_gallus",
            synonyms=["chicken"],
            reference_assemblies={
                "Galgal4": (MAX_ENSEMBL_RELEASE, MAX_ENSEMBL_RELEASE)
            }
    )

if not "caenorhabditis_elegans" in Species._latin_names_to_species:
    Species.register(
            latin_name="caenorhabditis_elegans",
            synonyms=["worm"],
            reference_assemblies={
                "WBcel235": (MAX_ENSEMBL_RELEASE, MAX_ENSEMBL_RELEASE)
            })

if not "drosophila_melanogaster" in Species._latin_names_to_species:
    Species.register(
            latin_name="drosophila_melanogaster",
            synonyms=["fly"],
            reference_assemblies={
                "BDGP6": (MAX_ENSEMBL_RELEASE, MAX_ENSEMBL_RELEASE)
            })

if not "danio_rerio" in Species._latin_names_to_species:
    Species.register(
            latin_name="danio_rerio",
            synonyms=["zebrafish"],
            reference_assemblies={
                "GRCz11": (MAX_ENSEMBL_RELEASE, MAX_ENSEMBL_RELEASE)
            })


    
@functools.lru_cache()
def release(taxon_id, version=MAX_ENSEMBL_RELEASE):
    species_map = {
        9031: "gallus_gallus",
        9606: "homo_sapiens",
        10090: "mus_musculus",
        10116: "rattus_norvegicus",
        7227: "drosophila_melanogaster",
        7955: "danio_rerio",
        6239: "caenorhabditis_elegans"
    }
    latin_name = species_map[taxon_id]
    species = Species._latin_names_to_species[latin_name]
    release = pyensembl.ensembl_release.EnsemblRelease(version, species)
    release.download()
    release.index()
    return release

def loci(taxon_id, type="gene", version=MAX_ENSEMBL_RELEASE):
    assert type in ("gene", "exon", "transcript")
    r = release(taxon_id, version=version)
    id_fn = getattr(r, "{}_ids".format(type))
    locus_fn = getattr(r, "locus_of_{}_id".format(type))
    
    o = []
    for eid in id_fn():
        locus = locus_fn(eid)
        o.append((eid, locus.contig, locus.start, locus.end, locus.strand))
    return pd.DataFrame.from_records(o, 
            columns=["EnsemblID", "contig", "start", "end", "strand"])\
                    .set_index(["EnsemblID"])

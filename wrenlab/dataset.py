"""
Generic classes for representing "datasets": that is, matrices of
data combined with annotation information for rows and/or columns.

(This is a WIP updating and enhancing stuff in wrenlab.expression).

TODO:
- allow annotations for both rows and columns
- allow X to be backed with a MatrixDB so data is only retrieved
    after desired selections are made
"""

import functools
import itertools

import patsy
import numpy as np
import pandas as pd

import wrenlab.lm
import wrenlab.statistics
import wrenlab.statistics.dimension_reduction
import wrenlab.impute
import wrenlab.ext.combat
from wrenlab.mixin import Clusterable, HasMetadata
from wrenlab.util import LOG
import metalearn

import autodx
import patsy

def simple_transform(fn):
    from matrixdb.df import DFMatrix

    @functools.wraps(fn)
    def wrapper(obj, *args, **kwargs):
        X_new = fn(obj, *args, **kwargs)
        return Dataset(DFMatrix(X_new),
                row_annotation=obj._row_annotation,
                column_annotation=obj._column_annotation,
                metadata=obj._metadata)
    return wrapper

class MMFit(object):
    # TODO: expand and fix
    def __init__(self, fits):
        # x : dict<KeyType, MFit>
        self._fits = fits

    def __getitem__(self, term):
        xs = {k:v[term] for k,v in self._fits.items()}
        return autodx.ContrastSet(xs)

class DatasetIterator(object):
    def __init__(self, iterable):
        # iterable is an iterator over key, dataset pairs
        self._iterable = iterable

    def __iter__(self):
        yield from self._iterable

    def __or__(self, fn):
        key, first = next(self._iterable)
        rs = first | fn

        if isinstance(rs, Dataset):
            it = itertools.chain([(key, rs)], ((k, (x | fn)) for (k,x) in self._iterable))
            return DatasetIterator(it)
        else:
            o = {key: rs}
            for k,x in self._iterable:
                o[k] = (x | fn)
            return o

    def fit(self, formula):
        def fn(DS):
            X = DS.to_frame()
            D = patsy.dmatrix(formula, data=DS.row_annotation, return_type="dataframe")
            return autodx.MFitOLS.fit(X,D)
        return MMFit(self.__or__(fn))

    def first(self):
        # FIXME: shouldn't mutate here? (convert to list?)
        return next(self._iterable)[1]

class Dataset(Clusterable, HasMetadata):
    def __init__(self, X, 
            row_annotation=None, 
            column_annotation=None,
            metadata=None):
        from matrixdb.df import DFMatrix
        self._row_annotation = row_annotation
        self._column_annotation = column_annotation
        if isinstance(X, pd.DataFrame):
            X = DFMatrix(X)
        self._data = X
        self._metadata = {k:v for k,v in metadata.items()} \
                if metadata is not None else {}

    def __repr__(self):
        return "<Dataset: n_samples={}, n_features={}>"\
                .format(*self._data.shape)

    def align(self):
        rix, cix = None, None
        if self._row_annotation is not None:
            rix = list(set(self._row_annotation.index) & set(self._data.row_index))
        if self._column_annotation is not None:
            cix = list(set(self._column_annotation.index) & set(self._data.column_index))
        return self._new(rix, cix)

    #######################
    # Mixin implementations
    #######################

    def to_frame(self):
        return self._data.to_frame()

    def metadata(self):
        return self._metadata

    #########
    # General
    #########

    def _drop_unused_categories(self, df):
        df = df.copy()
        for c in df.columns:
            if df[c].dtype.name == "category":
                df[c].cat.remove_unused_categories(inplace=True)
        return df

    def _new(self, rix, cix, *, X=None):
        X = X or self._data
        if rix is None:
            rix = self._data.row_index
        else:
            rix = self._data.row_index[np.where(rix)]
            X = X.rows(rix)
        if cix is None:
            cix = self._data.column_index
        else:
            cix = self._data.column_index[np.where(cix)]
            X = X.columns(cix)

        if self._row_annotation is not None:
            ra = self._row_annotation.loc[rix,:]
            ra = self._drop_unused_categories(ra)
        else:
            ra = None
        if self._column_annotation is not None:
            ca = self._column_annotation.loc[cix,:]
            ca = self._drop_unused_categories(ca)
        else:
            ca = None

        return type(self)(X,
                row_annotation=ra,
                column_annotation=ca,
                metadata=self._metadata)

    def copy(self):
        from matrixdb.df import DFMatrix
        X = DFMatrix(self._data.to_frame())
        return self._new(None, None, X=X)

    @property
    def row_annotation(self):
        if self._row_annotation is not None:
            return self._row_annotation.copy()

    @property
    def column_annotation(self):
        if self._column_annotation is not None:
            return self._column_annotation.copy()

    @property
    def metadata(self):
        if self._metadata is not None:
            return {k:v for k,v in self._metadata.items()}
        return {}

    def view(self):
        """
        View a small subset of the data.
        """
        return self._data.to_frame().head().T.head().T

    def _replace_underlying_matrix(self, X_new):
        from matrixdb.df import DFMatrix
        return Dataset(DFMatrix(X_new),
                row_annotation=self._row_annotation,
                column_annotation=self._column_annotation,
                metadata=self._metadata)

    ################
    # Data selection
    ################

    def row(self, key):
        return self._data.row(key)

    def column(self, key):
        return self._data.column(key)

    ###############
    # Transposition
    ###############

    def transpose(self):
        from matrix.df import DFMatrix
        # N.B. This will fail if the internal dataset is not loaded in memory (an instance of DFMatrix)
        return Dataset(DFMatrix(self._data.transpose()),
                row_annotation=self._column_annotation,
                column_annotation=self._row_annotation,
                metadata=self.metadata)

    @property
    def T(self):
        return self.transpose()

    ####################
    # Metadata selection
    ####################

    def eq(self, key, value):
        """
        Filter by `key` s.t. only samples with `A[key] == value`
        are retained.
        """
        x = self._row_annotation[key]
        if x.dtype.name == "category":
            if isinstance(value, int):
                ix = x == value
            else:
                ix = x.astype(str) == str(value)
        else:
            ix = x == value
        return self._new(ix, None)

    def neq(self, key, value):
        x = self._row_annotation[key]
        if x.dtype.name == "category":
            if isinstance(value, int):
                ix = x == value
            else:
                ix = x.astype(str) == str(value)
        else:
            ix = x == value
        return self._new(~ix, None)

    def between(self, key, low, high):
        """
        Filter by `key` s.t. only samples with low <= value <= high
        are retained.
        """
        ix = (self._row_annotation[key] >= low) \
            & (self._row_annotation[key] <= high)
        return self._new(ix, None)

    def most_frequent(self, key, n):
        """
        Return a new :class:`Dataset` containing only samples with
        from the top `n` most frequent categories in the categorical
        variable `key`.
        """
        counts = self._row_annotation[key].value_counts()
        counts = counts.loc[counts >= 3]
        ix = self._row_annotation[key].isin(counts.index[:n])
        return self._new(ix, None)

    def random(self, n):
        """
        Select `n` random rows.
        """
        ix = self._row_annotation.index
        ix = np.random.choice(ix, size=n, replace=False)
        return self._new(ix, None)

    def has(self, key):
        ix = ~self._row_annotation[key].isnull()
        return self._new(ix, None)

    def isin(self, key, values):
        ix = self._row_annotation[key].isin(values)
        return self._new(ix, None)

    def min_count(self, key, n):
        counts = self._row_annotation[key].value_counts()
        ok = counts.index[counts >= n]
        return self._new(self._row_annotation[key].isin(ok), None)

    def filter_group(self, key, fn):
        """
        For each subgroup defined by "key", call "fn" on that 
        subgroup's annotation frame,
        retaining each subgroup if the function returns a truthy value.
        """
        ok = []
        for _,subset in self.groupby(key):
            if fn(subset.row_annotation):
                ok.extend(list(subset._row_annotation.index))
        return self._new(ok, None)

    def min_range(self, group_key, variable_key, value):
        def fn(df):
            xs = df[variable_key]
            return (xs.max() - xs.min()) >= value
        return self.filter_group(group_key, fn)

    #################################
    # Data preprocessing / transforms
    #################################
    
    @simple_transform
    def normalize(self):
        return wrenlab.normalize.quantile(self.to_frame().T).T

    @simple_transform
    def impute(self):
        return wrenlab.impute.mean(self.to_frame())

    def remove_effect(self, key):
        D = patsy.dmatrix("C({})".format(key),
                data=self._row_annotation,
                return_type="dataframe")
        X_new = wrenlab.lm.residual2d(self.to_frame(), D)
        return self._replace_underlying_matrix(X_new)

    @simple_transform
    def transform(self, fn):
        return fn(self._data.to_frame())

    @simple_transform
    def batch_correct(self, batch_var, covariates=None):
        """
        Perform batch correction using ComBat.
        """
        A = self.row_annotation
        X = self.to_frame()
        if covariates is None:
            model = None
        else:
            formula = []
            numerical_covariates = []
            for v in covariates:
                t = A[v].dtype.name
                if t == "category":
                    formula.append("C({})".format(v))
                    A[v] = A[v].cat.remove_unused_categories()
                elif t == "float64":
                    if A[v].isnull().sum() > 0:
                        LOG.warn("Variable '{}' cannot be used for \
                                batch correction because it has missing values. Skipping.".format(v))
                        continue
                    formula.append(v)
                    numerical_covariates.append(v)
                else:
                    LOG.warn("Variable '{}' cannot be used for \
                            batch correction because it is not category or float64".format(v))
                    continue


            formula = " + ".join(formula)
            model = patsy.dmatrix(formula, data=A, return_type="dataframe")
        batch = A[batch_var].cat.remove_unused_categories()
        return wrenlab.ext.combat.combat(X.T, batch, model, numerical_covariates).T
    
    #########################
    # Transforms / functional
    #########################

    def _process_output(self, o):
        # FIXME: check if output index/columns are a subset of input
        def df_output(df):
            if (df.shape == self.shape):
                return Dataset(DFMatrix(df),
                        row_annotation=self._row_annotation,
                        column_annotation=self._column_annotation,
                        metadata=self._metadata)
            elif df.shape[0] == self.shape[0]:
                return Dataset(DFMatrix(df),
                        row_annotation=self._row_annotation,
                        metadata=self.metadata)
            elif df.shape[1] == o.shape[1]:
                return Dataset(DFMatrix(df),
                        column_annotation=self._column_annotation,
                        metadata=self.metadata)
            else:
                return
        """
        if isinstance(o, np.ndarray):
            if (o.shape == self.shape):
                o = pd.DataFrame(o, 
                        index=self._data.row_index,
                        columns=self._data.column_index)
            elif (o.shape[0] == self.shape[0]):
                o = pd.DataFrame(o, index=self._data.row_index)
            elif (o.shape[1] == self.shape[1]):
                o = pd.DataFrame(o, columns=self._data.column_index)
            else:
                pass
        """
        if isinstance(o, pd.DataFrame): 
            o = df_output(o) or df_output(o.T) or o
        return o

    def __or__(self, fn):
        # return DS
        if hasattr(fn, "fit_transform"):
            o = fn.fit_transform(self._data.to_frame())
        else:
            try:
                o = fn(self)
            except Exception as e:
                print(e)
                o = fn(self._data.to_frame())
        return self._process_output(o)

    def __rshift__(self, fn):
        # return DataFrame (or whatever result)
        if hasattr(fn, "fit_transform"):
            o = fn.fit_transform(self._data.to_frame())
        else:
            try:
                o = fn(self)
            except:
                o = fn(self._data.to_frame())
        return o

    def transform_each(self, key, fn):
        X_new = self._data.to_frame()\
                .groupby(self._row_annotation[key])\
                .apply(fn)
        return self._replace_underlying_matrix(X_new)

    def reduce_dimensions(self, k=100, method="PCA", transpose=False):
        X = self._data.to_frame()
        if transpose:
            X = X.T
            annotation = self._column_annotation
        else:
            annotation = self._row_annotation
        fn = getattr(wrenlab.statistics.dimension_reduction, method)
        L = fn(X, k=k)
        Xc = pd.DataFrame(X @ L, index=X.index)
        return Dataset(DFMatrix(Xc), 
                row_annotation=annotation,
                metadata=self._metadata)

    ##########
    # Iterable
    ##########

    def each(self, key):
        return DatasetIterator(self.groupby(key))

    def groupby(self, key):
        for key_id in self._row_annotation[key].value_counts().index:
            ix = self._row_annotation[key] == key_id
            yield key_id, self._new(ix, None)

    ##############
    # Applications
    ##############

    def fit(self, formula, group=None, link=None):
        assert link in (None, "logit")
        X = self.to_frame()
        # FIXME
        X = X.dropna(axis=1, how="any")
        if X.isnull().any().any():
            LOG.info("Imputing missing values for matrix of shape: {}".format(X.shape))
            X = wrenlab.impute.mean(X)
            if link == "logit":
                X = X.apply(wrenlab.statistics.logit)
        o = wrenlab.lm.fit(X, self._row_annotation, formula, group=group)
        o.metadata["TaxonID"] = self.metadata.get("TaxonID")
        o.metadata["PlatformID"] = self.metadata.get("PlatformID")
        return o

    def fit_each(self, group, formula, link=None):
        o = {}
        for group_id, subset in self.groupby(group):
            try:
                fit = subset.fit(formula)
                o[group_id] = fit
            except:
                continue
        return o

    def problem(self, key, groups=None):
        y = self._row_annotation[key]
        if groups is not None:
            if isinstance(groups, str):
                groups = self._row_annotation[groups]
            elif isinstance(groups, collections.Iterable):
                assert len(groups) == len(y)
            else:
                raise ValueError()
        else:
            groups = None
        X = self._data.to_frame()
        # FIXME
        X = X.dropna(axis=1, how="any")
        return metalearn.make_problem(X, y, groups=groups)

    def _get_matrix(self):
        # why is this here??
        return self.X

    @property
    def shape(self):
        return self._data.shape

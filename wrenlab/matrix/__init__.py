import os

import pandas as pd
import numpy as np

#import wrenlab.text.label_extraction.geo

#def fn():
#    A = wrenlab.text.label_extraction.geo.annotation(9606)
#    return A

def df_to_sarray(df):
    """
    Convert a pandas DataFrame object to a numpy structured array.
    Also, for every column of a str type, convert it into 
    a 'bytes' str literal of length = max(len(col)).

    :param df: the data frame to convert
    :return: a numpy structured array representation of df
    """

    def make_col_type(col_type, col):
        try:
            if 'numpy.object_' in str(col_type.type):
                maxlens = col.dropna().str.len()
                if maxlens.any():
                    maxlen = maxlens.max().astype(int) 
                    col_type = ('S%s' % maxlen, 1)
                else:
                    col_type = 'f2'
            return col.name, col_type
        except:
            print(col.name, col_type, col_type.type, type(col))
            raise

    v = df.values            
    types = df.dtypes
    numpy_struct_types = [make_col_type(types[col], df.loc[:, col]) for col in df.columns]
    dtype = np.dtype(numpy_struct_types)
    z = np.zeros(v.shape[0], dtype)
    for (i, k) in enumerate(z.dtype.names):
        # This is in case you have problems with the encoding, remove the if branch if not
        try:
            if dtype[i].str.startswith('|S'):
                z[k] = df[k].str.encode('latin').astype('S')
            else:
                z[k] = v[:, i]
        except:
            print(k, v[:, i])
            raise
    return z, dtype

class H5Object(object):
    def __init__(self, node):
        self.node = node

class View(H5Object):
    def __init__(self, node):
        super().__init__(node)

    def to_frame(self, axis=0):
        assert axis in (0,1)
        o = self.node["data"][str(axis)].value
        ix0, ix1 = self.index(0), self.index(1)
        if axis == 1:
            ix0, ix1 = ix1, ix0
        return pd.DataFrame(o, index=ix0, columns=ix1)

    def index(self, axis=0):
        assert axis in (0,1)
        o = self.node["index"][str(axis)].value
        if type(o[0]) == np.bytes_:
            o = [x.decode("utf-8") for x in o]
        o = pd.Index(o)
        return o
    
    @property
    def shape(self):
        return self.node["data"]["0"].shape

    def __getitem__(self, xs):
        # FIXME: retrieve by label 
        ix0 = self.index(0)[xs[0]]
        ix1 = self.index(1)[xs[1]]
        return pd.DataFrame(self.node["data"]["0"][xs],
                index=ix0, columns=ix1)

    def rows(self, query):
        index = self.index(0)
        ix = index.isin(query)
        return pd.DataFrame(self.node["data"]["0"][ix,:],
                index=index[ix], columns=self.index(1))

    def columns(self, query):
        index = self.index(1)
        ix = index.isin(query)
        return pd.DataFrame(self.node["data"]["1"][ix,:],
                index=index[ix], columns=self.index(0)).T

    def metadata(self, axis=0):
        return pd.DataFrame(self.node["metadata"][str(axis)].value)

"""
class Metadata(H5Object):
    def __init__(self, node):
        super().__init__(node)
"""

class Collection(H5Object):
    def __init__(self, node):
        super().__init__(node)

    def create_view(self, key, X, metadata=None):
        view = self.node.create_group(key)
        ix0 = np.array(X.index, dtype=bytes)
        ix1 = np.array(X.columns, dtype=bytes)
        X = np.array(X)

        # Create /view/data
        data = view.create_group("data")
        data0 = data.create_dataset("0", data=X)
        data1 = data.create_dataset("1", data=X.T)

        # Create /view/index
        index = view.create_group("index")
        index0 = index.create_dataset("0", data=ix0)
        index1 = index.create_dataset("1", data=ix1)

        metadata_node = view.create_group("metadata")

        # FIXME: ensure alignment (because sarrays can't store index names, only column names)

        def put_metadata(axis, df):
            # FIXME: ensure alignment CORRECTLY
            assert df.shape[0] == X.shape[axis]
            sa, sa_type = df_to_sarray(df)
            node = metadata_node.create_dataset(str(axis), data=sa, dtype=sa_type)

        if metadata is not None:
            df0 = metadata.get(0)
            df1 = metadata.get(1)
            if df0 is not None:
                put_metadata(0, df0)
            if df1 is not None:
                put_metadata(1, df1)

        return View(view)

    def __getitem__(self, key):
        return View(self.node[key])

class MatrixDB(H5Object):
    """
    Terms:
    - A "MatrixDB" contains:
        - at least one "Collection"
        - global metadata
    - A "Collection" contains at least one "View"
    - A "View" consists of:
        - a 2D matrix, stored in original and transposed orientations
        - index and column labels
        - row and column metadata

    Also, each "Collection", "View", and the global "MatrixDB" object can have 
    (scalar) metadata attributes using the HDF5 metadata API.

    Structure:
    /collection-id
        /view1 (each view represents a different collapse, for example; corresponds to "Dataset" object)
            /data
                /0 (original orientation, 2D double) 
                /1 (transposed, 2D double)
            /index 
                /0 (1D string or integer vector, row index)
                /1 (..., column index)
            /metadata
                /0
                    /var-id1 (any type)
                    /var-id2
                    /...
                /1
                    /var-id1
        /view2
    """
    def __init__(self, path, read_only=True):
        mode = "r" if read_only else "w"
        cx = h5py.File(path, mode)
        super().__init__(cx)

    def create_collection(self, key):
        node = self.node.create_group(key)
        return Collection(node)

    def __getitem__(self, key):
        return Collection(self.node[key])

def testdb():
    import os
    path = "test.h5"
    if os.path.exists(path):
        os.unlink(path)
    db = MatrixDB(path, read_only=False)
    return db

def fn(db):
    X = pd.DataFrame(np.random.random((10,5)))
    X.index = ["R{}".format(i+1) for i in range(X.shape[0])]
    X.columns = ["C{}".format(i+1) for i in range(X.shape[1])]

    A0 = pd.DataFrame(np.random.random((10, 2)), 
            index=X.index, columns=["attr0", "attr1"])

    c = db.create_collection("collection1")
    v = c.create_view("view1", X, metadata={0:A0})
    return db["collection1"]["view1"]

def load_platform(platform_id):
    import wrenlab.ncbi.geo
    db = MatrixDB("GEO.h5", read_only=False)
    c = db.create_collection("GPL{}".format(platform_id))

    DS = wrenlab.ncbi.geo.platform(platform_id)
    X = DS.to_frame()
    A = DS.row_annotation
    v = c.create_view("probe", X, metadata={0: A})
    return v

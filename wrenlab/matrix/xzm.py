"""
XZ-compressed matrices with random access on rows.
"""

# TODO: read pixz index directly in cython or whatever

import io
import tempfile
import tarfile
import subprocess

import numpy as np
import pandas as pd

class XZMatrix(object):
    def __init__(self, path, mode="r"):
        self.path = path
        
        def make_index(xs):
            xs = [x.strip().decode("utf-8") for x in xs]
            if all(x.isdigit() for x in xs):
                xs = [int(x) for x in xs]
            return pd.Index(xs)

        with tarfile.open(path, "r:xz") as h:
            ti = h.next()
            assert ti.name == "columns"
            self.columns = make_index(h.extractfile(ti))

        self.index = make_index(subprocess.check_output(["pixz", "-l", path])\
                .split(b"\n")[1:-1])

    @staticmethod
    def from_file(input_handle, output_path, delimiter="\t"):
        ih = input_handle
        tmp = tempfile.NamedTemporaryFile()
        tf = tarfile.open(tmp.name, mode="w")

        def add(name, data):
            n = len(data.getvalue())
            ti = tarfile.TarInfo(name)
            ti.size = n
            tf.addfile(ti, fileobj=data)

        ch = io.BytesIO()
        columns = next(ih).split(delimiter)[1:]
        ch.write(columns[0].encode("utf-8"))
        for c in columns[1:]:
            ch.write(b"\n")
            ch.write(c.encode("utf-8"))
        ch.seek(0)
        add("columns", ch)

        for line in ih:
            key, *row = line.rstrip("\n").split(delimiter)
            print(key)
            row = ["nan" if x == "" else x for x in row]
            data = io.BytesIO(np.array(row, dtype=np.float64))
            add(key, data)

        tf.close()
        subprocess.check_output(["pixz", "-i", tmp.name, "-o", output_path])
        tmp.close()

    def __getitem__(self, keys):
        cmd = ["pixz", "-i", self.path, "-x", *keys]
        tar_data = io.BytesIO(subprocess.check_output(cmd))

        oo = []

        with tarfile.open(fileobj=tar_data, mode="r:") as h:
            while True:
                ti = h.next()
                if ti is None:
                    break
                data = h.extractfile(ti).read()
                data = np.fromstring(data, dtype=np.float64)
                o = pd.Series(data, index=self.columns)
                o.name = ti.name
                oo.append(o)

        if len(oo) == 1:
            return oo[0]
        else:
            return pd.concat(oo, axis=1).T


########################
# Command-line interface
########################

import sys
import click

@click.group()
def cli():
    pass

@cli.command()
@click.argument("matrix_path", required=True)
@click.option("--delimiter", "-d", default="\t")
def load(matrix_path, delimiter="\t"):
    XZMatrix.from_file(sys.stdin, matrix_path)

if __name__ == "__main__":
    cli()
    #import sys
    #xzm = XZMatrix("9606.xzm")
    #print(xzm[xzm.index[5000:5010]].iloc[:,:10])
    #print(xzm[["R1","R2","R3"]])

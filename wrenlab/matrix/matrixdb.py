import sys
import ctypes
from ctypes import c_char_p, c_bool, c_void_p, c_long, c_double, POINTER
import multiprocessing as mp

import patsy
import pandas as pd
import numpy as np

import wrenlab.impute
import wrenlab.statistics
import wrenlab.collapse

import logging

logging.basicConfig()
LOG = logging.getLogger("matrixdb")
LOG.setLevel(logging.DEBUG)

def load_library():
    lib = ctypes.cdll.LoadLibrary("libwrenlab.so")
    # See: http://stackoverflow.com/questions/17240621/wrapping-simple-c-example-with-ctypes-segmentation-fault

    SPEC = {
        "matrixdb_open": (c_void_p, c_char_p, c_bool),
        "matrixdb_close": (None, c_void_p),
        "matrixdb_get_row_index": (c_void_p, c_void_p),
        "matrixdb_get_column_index": (c_void_p, c_void_p),
        "matrixdb_index_size": (c_void_p, c_void_p),
        "matrixdb_index_element": (None, c_void_p, c_long, c_char_p),
        "matrixdb_get_row": (c_void_p, c_void_p, c_char_p),
        "matrixdb_row_size": (c_long, c_void_p),
        "matrixdb_row_data": (POINTER(c_double), c_void_p),
        "matrixdb_row_delete": (None, c_void_p)
    }

    for k,spec in SPEC.items():
        fn = getattr(lib, k)
        setattr(fn, "argtypes", spec[1:])
        setattr(fn, "restype", spec[0])

    return lib

class MatrixDB(object):
    def __init__(self, path, read_only=True):
        self._lib = load_library()
        self._cx = self._lib.matrixdb_open(path.encode("ascii"), read_only)

        r_ix = self._lib.matrixdb_get_row_index(self._cx)
        self.index = self._load_index(r_ix)

        c_ix = self._lib.matrixdb_get_column_index(self._cx)
        self.columns = self._load_index(c_ix)

    def __del__(self):
        self.close()

    def close(self):
        self._lib.matrixdb_close(self._cx)

    def _load_index(self, ix):
        n = self._lib.matrixdb_index_size(ix)
        o = []
        p = ctypes.create_string_buffer(1000)
        for i in range(n):
            self._lib.matrixdb_index_element(ix, i, p)
            o.append(p.value.decode("ascii"))
        return pd.Index(o)

    @property
    def shape(self):
        return (len(self.index), len(self.columns))

    def __iter__(self):
        """
        Return a generator of rows as :class:`pandas.Series` objects.
        """
        for ix in self.index:
            yield self.row(ix)

    def __getitem__(self, key):
        """
        Return row data as a :class:`pandas.Series`.
        """
        o = pd.Series(self.row(key), index=self.columns)
        o.name = key
        return o

    def row(self, key):
        """
        Return row data as a 1-D :class:`numpy.ndarray`.
        """
        key = key.encode("ascii")
        row = self._lib.matrixdb_get_row(self._cx, key)
        if row is None:
            raise KeyError
        try:
            n = self._lib.matrixdb_row_size(row)
            p_data = self._lib.matrixdb_row_data(row)
            return np.fromiter(p_data, dtype=np.float64, count=n)
        finally:
            self._lib.matrixdb_row_delete(row)

    def rows(self, keys, return_data_frame=False, columns=None):
        """
        Return data corresponding to the given subset of rows as a 
        2D :class:`numpy.ndarray`.
        """
        assert all(k in self.index for k in keys)
        if columns is not None:
            assert len(columns) == len(set(columns))
            assert all(c in self.columns for c in columns)
            M = {}
            for j,c in enumerate(self.columns):
                M[c] = j
            ix = np.array([M[c] for c in columns])
        else:
            columns = self.columns
            ix = np.arange(len(columns))

        nc = len(columns) if columns is not None else self.shape[1]
        o = np.zeros((len(keys), nc))

        for i,key in enumerate(keys):
            o[i,:] = self.row(key)[ix]
        if return_data_frame:
            o = pd.DataFrame(o, index=keys, columns=columns)
        return o

    def to_array(self):
        """
        Read all the contents of this MatrixDB into a 2D :class:`numpy.ndarray`.

        NOTE: May consume lots of memory.
        """
        o = np.zeros(self.shape)
        for i,k in enumerate(self.index):
            o[i,:] = self.row(k)
        return o

    def to_frame(self):
        """
        Read all the contents of this MatrixDB into a :class:`pandas.DataFrame`.

        NOTE: May consume lots of memory.
        """
        return pd.DataFrame(self.to_array(), 
                index=self.index, columns=self.columns)

    def fit(self, D, formula=None):
        """
        Fit an OLS model to each row of this MatrixDB against the design matrix D.

        Arguments
        ---------
        D : :class:`pandas.DataFrame`
            The design matrix. Rows must (at least partially) overlap with
            the set of columns in this MatrixDB.

        Returns
        -------
        A :class:`wrenlab.lm.MFit` object.
        """
        ix = list(set(D.index) & set(self.columns))
        D = D.loc[ix,:]
        if formula is not None:
            D = patsy.dmatrix(formula, data=D, return_type="dataframe")
        columns = D.columns

        ix = {k:i for i,k in enumerate(self.columns)}
        M = np.array([ix[k] for k in D.index], dtype=int)
        D = np.array(D)

        beta = np.zeros((self.shape[0], D.shape[1]))
        N = np.zeros(self.shape[0])
        for i,key in enumerate(self.index):
            y = self.row(key)[M]
            ix = ~np.isnan(y)
            N[i] = ix.sum()
            y = y[ix]
            DD = D[ix,:]
            beta[i,:] = np.linalg.lstsq(DD,y)[0]

        import wrenlab.lm
        import scipy.stats

        coef = pd.DataFrame(beta, index=self.index, columns=columns)
        X_se = np.divide.outer(D.std(axis=0), np.sqrt(N))
        t = coef.T / X_se
        p = pd.DataFrame(scipy.stats.t.sf(t.abs(), df=N-2) * 2,
                index=t.index, columns=t.columns).T
        return wrenlab.lm.MFit(coef, 
                design=D, formula=formula, t=t.T, p=p, N=N)

    def collapse(self, groups, handle=None, collapse_fn=wrenlab.collapse.max_mean):
        """
        Collapse this :class:`MatrixDB` into a single row per group
        in `grouper` using the first PC of each group subset.

        Arguments
        ---------
        groups : dict[V, list[K]] or 2-column :class:`pandas.DataFrame`[K,V]
            A mapping of keys of the new rows to a list of row IDs in the
            current :class:`MatrixDB` (V = new row IDs, K = current row IDs).

        handle : file object or None
            If provided, save the results to this file in TSV format.

        collapse_fn : function([key, matrix]) -> [key, vector]
            A function for collapsing rows. Several implementations
            can be found in :module:`wrenlab.collapse`.

        Returns
        -------
        If handle is None, returns a :class:`pandas.DataFrame` representing the
        collapsed result. Otherwise, returns None
        """
        if isinstance(groups, pd.DataFrame):
            assert groups.shape[1] == 2
            groups = {k:list(set(df.iloc[:,0])) for k,df in groups.groupby(groups.columns[1])}

        import itertools
        def jobs():
            it = iter(sorted(groups.items(), key=lambda x: x[0]))
            #it = itertools.islice(it, 100)

            for key, subset in it:
                subset = list(set(subset) & set(self.index))
                if len(subset) == 0:
                    continue
                Xs = self.rows(subset, return_data_frame=True)
                yield key, Xs

        return wrenlab.collapse.collapse_subsets(jobs(), handle=handle, fn=collapse_fn)


def concatenate(paths, handle=sys.stdout):
    """
    Concatenate multiple MatrixDB files, printing the result to `handle`.
    """
    columns = set()
    for path in paths:
        X = MatrixDB(path)
        columns |= set(X.columns)
    columns = list(sorted(columns))

    print("", *columns, sep="\t", file=handle)

    seen = set()
    for path in paths:
        X = MatrixDB(path)
        for key in X.index:
            if key in seen:
                continue
            row = X[key].loc[columns]
            print(key, *row, sep="\t", file=handle)
            seen.add(key)

if __name__ == "__main__":
    concatenate(sys.argv[1:])

    #path = "/home/gilesc/methylation/GPL13534.matrixdb"
    #import time

    #path = "/home/gilesc/methylation/test.db"
    #db = MatrixDB(path)
    #print(db.shape)

    #import random
    #start = time.time()
    #n = 100
    #for i in range(100):
    #    ix = random.choice(db.index)
    #    x = db[ix]
    #print(1000 * (time.time() - start) / n, "ms/row")
    #print(db[db.index[0]])

    #print("size:", lib.matrixdb_row_size(row))

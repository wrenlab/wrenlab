"""
Methods for imputing missing values in matrices. Each column is considered as a
variable. (i.e., imputation is done column-wise).

# TODO: implement generic method to impute on submatrices and paste the result together (possibly multiple times and average) (see kNN)
"""

import functools
import warnings
import math

import tqdm
import pandas as pd
import numpy as np
import numpy.linalg as la
import scipy.linalg
import scipy.stats
import multiprocessing as mp
from joblib import Parallel, delayed
import sklearn.utils.extmath
import fancyimpute

from wrenlab.util.pandas import add_missing
from wrenlab.util import matrix_transformer, LOG, chunks

def evaluate(X, fn, ratio=0.1):
    """
    Return evaluation metrics for an imputation function on a given matrix.
    """
    mask,Xm = add_missing(X, ratio=ratio)
    Xa = np.array(X)
    Xi = np.array(fn(Xm))
    y = Xa[np.where(mask)]
    y_hat = Xi[np.where(mask)]

    return {
        "MAD": np.abs(y - y_hat).mean(),
        "RMSD": ((y - y_hat) ** 2).mean() ** 0.5,
        "Spearman": scipy.stats.spearmanr(y, y_hat)[0]
    }

def impute_wrapper(fn):
    @matrix_transformer
    @functools.wraps(fn)
    def wrap(X, *args, **kwargs):
        mask = np.isnan(X)
        if (mask.sum().sum()) == 0:
            msg = "Complete matrix passed to imputation method. Returning unmodified."
            LOG.warn(msg)
            return X
        #assert ((~mask).sum(axis=0) > 3).all()
        LOG.info("{}:Imputing matrix of shape: {} with {}% missing values.".format(
            fn.__name__, X.shape, round(mask.mean().mean() * 100, 1)))
        return fn(X, *args, **kwargs)
    return wrap

def identity(X):
    """
    Null-op (for benchmarking).
    """
    return X

def qPCR(X, design, controls, max_iterations=100):
    import wrenlab.R
    return wrenlab.R.impute_qPCR(X, design, controls, 
            max_iterations=max_iterations)

@impute_wrapper
def kNN(X, k=10, chunk=500):
    """
    chunk : int 
        Impute by chunks of this number of columns (for speed over accuracy)
    """

    if chunk is not None:
        model = fancyimpute.KNN(k=k)
        Xo = np.empty(X.shape)
        ix = np.arange(X.shape[1])
        np.random.shuffle(ix)

        for c in chunks(ix, chunk):
            X_subset = X[:,c]
            Xo[:,c] = model.fit_transform(X_subset)
        return Xo
    else:
        model = fancyimpute.KNN(k=k)
        return model.fit_transform(X)

def kNN_fn(args):
    j, mask, Xn, k = args
    nc = Xn.shape[1]
    ix = mask[:,j]
    if ix.sum() == 0:
        return (j, None, None)
    # MSD:
    dx = np.nanmean((Xn.T - Xn[:,j]) ** 2, axis=1)
    # pearson:
    #dx = - (Xn[:,j] @ Xn) / nnz
    nn = np.argsort(dx)
    with warnings.catch_warnings():
        warnings.simplefilter("error")
        kk = k
        while True:
            try:
                mu = np.nanmean(Xn[:,nn[1:(kk+1)]], axis=1)
                break
            except RuntimeWarning:
                kk = min(2 * kk, nc - 1)
                LOG.debug("k too small for j={}; increasing k to {}".format(j, kk))
    return j, ix, mu

@impute_wrapper
def kNN2(X, k=10, subset=500):
    """
    Arguments
    =========
    k : int
        The number of nearest neighbors.

    subset : int, float, or None
        Use a random subset of rows for distance computations (if NR > subset).
        If `subset` is an int, use this many rows.
        If it is a float, use that fraction of rows.
        If None, use all rows.
    """
    # TODO : make distance metric generic
    # TODO : there is still difference between kNN and kNN_FI, investigate
    mask = np.isnan(X)
    nnz = mask.sum(axis=0)
    nr,nc = X.shape

    # Compute distances with random subset for speed
    Xn = X.copy()
    if isinstance(subset, float):
        assert (subset > 0) and (subset <= 1)
        subset = int(nr * subset)
    if isinstance(subset, int):
        if nr > subset:
            ix = np.arange(nr)
            np.random.shuffle(ix)
            Xn = Xn[ix[:subset],:]
    else:
        assert subset is None
    #Xn = np.zeros(X.shape)
    #for j in range(nc):
    #    x = X[:,j]
    #    Xn[:,j] = (x - np.nanmean(x)) / np.nanstd(x)

    def jobs():
        for j in range(nc):
            yield j,mask,Xn,k

    pool = mp.Pool(mp.cpu_count() - 2)
    try:
        it = jobs()
        o = pool.imap_unordered(kNN_fn, it)
        for j, ix, mu in tqdm.tqdm(o, total=nc):
            if ix is None:
                continue
            X[ix,j] = mu[ix]
    finally:
        pool.terminate()
    return X

@impute_wrapper
def mean(X):
    model = fancyimpute.SimpleFill()
    return model.fit_transform(X)

@matrix_transformer
def SoftImpute(X):
    model = fancyimpute.SoftImpute(verbose=False)
    return model.fit_transform(X)

@matrix_transformer
def linear_model(X, max_iterations=5, convergence=1e-8, use_subset=True):
    if use_subset:
        max_predictors = min(500, X.shape[0] - 1)
    else:
        max_predictors = None

    nt, ns = X.shape
    missing = np.isnan(X.T)
    assert not missing.all(axis=0).any()

    converged = np.zeros(nt, dtype=bool)
    converged[missing.sum(axis=0) == 0] = True
    Xi = mean(X).T

    LOG.info("Starting linear model based imputation on matrix of shape: {}".format(X.shape))
    delta_prev = 2 ** 64

    for iteration in range(max_iterations):
        Xi_next = Xi.copy()
        delta = np.zeros(nt)
        for j in (~converged).nonzero()[0]:
            ix = missing[:,j]
            train = np.delete(Xi, np.s_[j], 1)
            if max_predictors is not None:
                predictors = np.random.choice(np.arange(train.shape[1]),
                        size=max_predictors,
                        replace=False)
                train = train[:,predictors]
            beta, _, _, sv = np.linalg.lstsq(train[~ix,:], Xi[~ix,j])
            y_hat = np.dot(train, beta)
            Xi_next[ix,j] = y_hat[ix]
            delta[j] = ((y_hat[ix] - Xi[ix,j]) ** 2).mean()
        converged[delta < convergence] = True
        if converged.all():
            LOG.info("Convergence after {} iterations".format(iteration + 1))
            return Xi_next.T
        else:
            LOG.debug("Iteration {}: mean delta = {}, not converged = {}"\
                    .format(
                        iteration + 1, 
                        delta[~converged].mean(), 
                        (~converged).sum()))
            Xi = Xi_next
            if (delta_prev < delta.mean()) and (max_predictors is not None):
                # 4000 predictors is where it starts to get really slow
                max_predictors = min(int(max_predictors * 1.5), 4000)
                LOG.debug("max_predictors increased to: {}".format(max_predictors))
            delta_prev = delta.mean()
    return Xi.T

@matrix_transformer
def EM(X, max_iterations=100, tolerance=0.001, n_jobs=mp.cpu_count() - 2):
    """
    http://www.r-bloggers.com/imputing-missing-data-with-expectation-maximization/
    """
    assert not np.isnan(X).all(axis=0).any()
    assert not np.isnan(X).all(axis=1).any()

    Xm = np.ma.masked_invalid(X)
    H = np.array([hash(tuple(Xm.mask[i,:])) for i in range(Xm.shape[0])], 
            dtype=np.int64)
    LOG.debug("EM: {} mask hash patterns.".format(len(set(H))))

    missing = Xm.mask
    Xio = Xm.copy()
    Xin = Xm.copy()

    for n_iter in range(max_iterations):
        LOG.info("Starting EM iteration: {} / {}".format(n_iter+1, max_iterations))
        LOG.debug("Calculating covariance matrix.")
        if n_iter == 0:
            cov = np.ma.cov(Xin, rowvar=False)
        else:
            cov = np.cov(Xin, rowvar=False)
        mu = Xin.mean(axis=0)

        LOG.debug("Inverting covariance matrices and computing new values.")
        for hc in set(H):
            ii = np.nonzero(H == hc)[0]
            ix = missing[ii[0],:]
            if ix.sum() == 0:
                continue
            inverse = np.linalg.inv(cov[~ix,:][:,~ix])
            for i in ii:
                Xin[i,ix] = mu[ix] + \
                        np.dot(np.dot(cov[ix,:][:,~ix], inverse), Xin[i,~ix] - mu[~ix])

        if n_iter > 0:
            dx = np.abs(Xio[missing] - Xin[missing]).mean()
            LOG.debug("delta = {}".format(dx))
            if np.abs(Xio - Xin).max() < tolerance:
                return np.array(Xin)

        Xio = np.array(Xin)
        Xin = Xin.copy()
    return np.array(Xin)

@matrix_transformer
def SVD(X, fast=True, k=100):
    """
    Impute missing values for a matrix X using Singular Value Decomposition.

    Arguments
    ---------
    X : 2D array-like
        The input matrix, containing NaN for missing values to be imputed.

    fast : bool
        Whether to use sklearn's randomized_svd function (partial SVD)

    k : int, default 100
        If "fast" is true, this is the number of components to use in
        the partial SVD function. If k = n_components and X is MxN shape,
        then U will be Mxk and D will be k, instead of MxM and M, 
        respectively.
    
    Returns
    -------
    Xi : a :class:`numpy.array` or :class:`pandas.DataFrame` of the same dimensions
        and type as input, with missing values imputed.

    Notes
    -----
    The goal is to solve for X_complete in the equation:
    X_complete = UDV
    
    1. XX' = UD²U'

    2. V can then be calculated using another SVD, or using the results from
        the first SVD and a pairwise/masked dot product. Currently only the latter
        (equation b) is implemented.
        
        a. X'X = V'D²V
    
        b. V = D^-1 U'X

    3. Solve for X_complete using UDV
    """
    raise Exception("This function is completely broken. Use fancyimpute instead.")

    if X.shape[0] > X.shape[1]:
        X = X.T
        transposed = True
    else:
        transposed = False
    m, n = X.shape

    Xm = np.ma.masked_invalid(X)
    LOG.debug("[impute] XX'")
    C = np.array(np.ma.dot(Xm, Xm.T))
    LOG.debug("[impute] SVD")
    if fast is True:
        # exists as of 0.18-HEAD (07/19/2016)
        #assert k >= 10
        if k < 10:
            assert k <= (m - 1), "k: {}, m: {}".format(k,m)
        k = min(k, m-1)
        U, Ds, _ = sklearn.utils.extmath.randomized_svd(C, n_components=k)
    else:
        U, Ds, _ = la.svd(C)
    LOG.debug("[impute] sqrtm")
    D = scipy.linalg.sqrtm(np.diagflat(Ds))
    LOG.debug("[impute] D^-1 U' X")
    V = np.ma.dot(la.inv(D) @ U.T, Xm)
    LOG.debug("[impute] U D V")
    Xi = U @ D @ V
    Xi[~Xm.mask] = Xm[~Xm.mask]

    if transposed:
        Xi = Xi.T

    o = np.array(Xi)
    assert np.isnan(o).sum() == 0
    return o

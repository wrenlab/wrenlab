import wrenlab.ncbi.geo
import wrenlab.ncbi.gene

def expression_fit():
    DS = wrenlab.ncbi.geo.platform(570)
    fit = DS.has("Age").most_frequent("ExperimentID", 3).preprocess().fit("Age + C(ExperimentID)")
    return fit

if __name__ == "__main__":
    e_fit = expression_fit()
    genes = wrenlab.ncbi.gene.info(9606)
    o = genes.join(e_fit.summary("Age"), how="inner").sort_values("p").head(10)
    print("Top 10 age-associated genes in GPL570 (expression):")
    print(o)

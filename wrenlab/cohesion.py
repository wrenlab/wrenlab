"""
Cohesion metrics.
"""

def data():
    pass

def control():
    # Test gene sets

    import wrenlab.enrichment
    AS = wrenlab.enrichment.annotation("MSigDB")
    for k,df in AS.mapping.groupby("TermID"):
        elements = list(set(df.ElementID))
        yield k, elements

def metric():
    pass

def permute():
    pass

def reference():
    import wrenlab.enrichment
    import sklearn.decomposition
    import pandas as pd

    # Annotation set
    AS = wrenlab.enrichment.annotation("GO")
    X = AS.matrix()
    model = sklearn.decomposition.NMF(n_components=2)
    L = model.fit_transform(X.T)
    L = pd.DataFrame(L)
    L.index = X.columns
    L.columns = ["C1", "C2"]

    return L

def test():
    return reference()

"""
Distance metrics and nearest neighbors functions.

FIXME: a lot of this module can be replaced by sklearn.metrics.pairwise.pairwise_distances
"""

import tqdm
import pandas as pd
import numpy as np
import scipy.spatial.distance
import scipy.cluster.hierarchy

import wrenlab
import wrenlab.correlation

import multiprocessing as mp
import multiprocessing.sharedctypes

def permutation_test(x, y, metric="euclidean", n_permutations=1000):
    """
    Perform a permutation-tested comparison of x vs y (1D arrays) 
    on a distance metric from :package:`scipy.spatial.distance`.

    Returns
    =======
    distance : the distance metric of x vs non-permuted y

    p : the p-value from permutation testing
    """
    if isinstance(x, pd.Series) and isinstance(y, pd.Series):
        x,y = wrenlab.align_series(x,y)
    else:
        assert len(x) == len(y)
    x,y = np.array(x), np.array(y)

    fn = getattr(scipy.spatial.distance, metric)
    distance = fn(x, y)
    p_distance = np.empty(n_permutations)
    for i in range(n_permutations):
        y_permuted = np.random.permutation(y)
        p_distance[i] = fn(x, y_permuted)
    p = (distance < p_distance).mean()
    return distance, p

def optimize_clustering(X):
    """
    Test combinations of clustering algorithm and distance metric
    against the Cophenetic distance for the given matrix `X` 
    (columns are clustered).

    Returns
    =======
    """
    # hamming, jaccard, several others exist for boolean
    # mahalanobis ?
    METRICS = [
            "euclidean", "minkowski", "cityblock", "seuclidean", "sqeuclidean", 
            "cosine", "correlation", "canberra", "braycurtis", "wminkowski",
            "mahalanobis"
    ]
    METHODS = ["single", "complete", "average", "ward", "median"]

    X = Xs = np.array(X)
    if X.shape[1] > 1000:
        ix = np.arange(X.shape[1])
        np.random.shuffle(ix)
        Xs = X[:,ix[:1000]]
    if X.shape[0] > 1000:
        ix = np.arange(X.shape[0])
        np.random.shuffle(ix)
        Xs = X[ix[:1000],:]

    def fn(metric, method):
        # This is because some metric-method combinations aren't allowed
        try:
            D = scipy.spatial.distance.pdist(Xs, metric)
            Z = scipy.cluster.hierarchy.linkage(D, method=method, metric=metric)
            c, _ = scipy.cluster.hierarchy.cophenet(Z, D)
        except ValueError:
            c = np.nan
        return {"c": c}

    return wrenlab.apply_parameters(fn, metric=METRICS, method=METHODS)\
            .dropna()\
            .sort_values("c", ascending=False)

def optimal_linkage(X):
    """
    Returns the linkage matrix for rows of `X` using the optimal 
    method-metric combination.
    """
    X = np.array(X)
    cm = optimize_clustering(X)
    method, metric = cm.reset_index()\
            .loc[:,["method","metric"]]\
            .to_records(index=False)[0]
    Z = scipy.cluster.hierarchy.linkage(X, method=method, metric=metric)
    return Z
 
def optimal_dendrogram(X, **kwargs):
    """
    Returns the dendrogram for rows of `X` using the optimal method-metric combination.
    """
    labels = list(X.index)
    Z = optimal_linkage(X)
    return scipy.cluster.hierarchy.dendrogram(Z, labels=labels, **kwargs)

class SharedArray(object):
    def __init__(self, X):
        self.size = X.size
        self.shape = X.shape
        self.buffer = multiprocessing.sharedctypes.RawArray("d", X)

    @property
    def data(self):
        X = np.ctypeslib.as_array(self.buffer)
        X.shape = self.shape
        return X

def _nearest_neighbors(X, i, k):
    r = wrenlab.correlation.pearson(X, X[i,:])["r"]
    d = np.array(1 - (r * 2))
    return np.argsort(d)[1:(k+1)]

from joblib import Parallel, delayed

def nearest_neighbors(X, Y, k=50):
    """
    For each column in Y, find the k nearest neighbors in X.

    Arguments
    ---------
    X : :class:`pandas.DataFrame`
        The "database" expression matrix of dimensions (p,n), 
        with probes as columns and samples as rows.

    Y : :class:`pandas.DataFrame`
        The query matrix of dimensions (p,m).

    k : int, default 50
       The number of nearest neighbors. 

    Returns
    -------
    A :class:`pandas.DataFrame` of dimensions, (m,k) containing 
    the nearest neighbors.
    """
    index = Y.columns
    N = np.zeros((Y.shape[1], k), dtype=index.dtype)
    assert k < (X.shape[1] - 1)
    assert X.shape[0] == Y.shape[0]
    n = X.shape[1]
    m = Y.shape[1]
    p = X.shape[0]

    assert X.isnull().sum().sum() == 0
    assert Y.isnull().sum().sum() == 0
    Xa = np.array(wrenlab.statistics.standardize(X, axis=0))
    Ya = np.array(wrenlab.statistics.standardize(Y, axis=0))

    for j in tqdm.tqdm(range(m), total=m):
        r = Ya[:,j] @ Xa / (Xa.shape[0] - 1)
        d = np.array(1 - (r * 2))
        #N[j,:] = index[np.argsort(d)[1:(k+1)]]
        N[j,:] = index[np.argsort(d)[:k]]
    """
        assert (~X.isnull()).sum().min() >= 3
        X = np.array(X.T)
        rs = Parallel(n_jobs=mp.cpu_count() - 2, max_nbytes=20e9)(
                delayed(_nearest_neighbors)(X, i, k) for i in range(X.shape[0]))
        for i,ix in enumerate(rs):
            N[i,:] = index[ix]
    """
    return pd.DataFrame(N, index=index)

class DistanceIndex(object):
    def __init__(self, X): 
        """
        X : :class:`pandas.DataFrame`
            Columns are genes, rows are samples, categories,
            etc; whatever is to be searched for.
        """
        assert X.isnull().sum().sum() == 0
        self._X = X
        self._mu = X.mean()
        self._VI = None

    def _align(self, q):
        ix = list(set(q.index) & set(self._X.columns))
        assert len(ix) > 3
        X = self._X.loc[:,ix]
        q = q.loc[ix]
        return X,q

    def nearest_neighbors(self, X, k=50):
        # This could be done a lot faster w/ impute + PCA probs
        return nearest_neighbors(self._X, X, k=k)

    def spearman(self, q):
        X, q = self._align(q)
        return X.apply(lambda x: scipy.stats.spearmanr(x,q)[0], axis=1)\
                .sort_values(ascending=False)

    def mahalanobis(self, q):
        """
        q : :class:`pandas.Series`
        """
        if self._VI is None:
            self._VI = pd.DataFrame(np.linalg.inv(self._X.cov()), 
                    index=self._X.columns, columns=self._X.columns)

        X,q = self._align(q)
        ix = q.index
        VI = np.array(self._VI.loc[ix,:].loc[:,ix])

        # quantile normalize query to reference
        qn = np.zeros((len(q),))
        qn[np.argsort(q)] = self._mu.loc[ix].sort_values()
        qn = pd.Series(qn, index=q.sort_values().index).loc[ix]

        return X.apply(lambda x: scipy.spatial.distance.mahalanobis(x,qn,VI), axis=1)\
                .sort_values()

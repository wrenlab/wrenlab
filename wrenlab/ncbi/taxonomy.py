import functools
import io
import re
import tarfile
from collections import namedtuple

import networkx as nx
import pandas as pd

from wrenlab.util import memoize, download

Taxon = namedtuple("Taxon", "id,name")

def read_dmp(h, usecols=None):
    FS = "\t\|\t"
    RS = "\t\|\n"
    with io.StringIO("w") as tmp:
        data = re.sub(FS, "\t", re.sub(RS, "\n", h.read().decode("iso-8859-1")))
        tmp.write(data)
        tmp.seek(0)
        return pd.read_csv(tmp, sep="\t", header=None, usecols=usecols)

def _get_handle(fname):
    url = "ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"
    path = str(download(url))
    tar = tarfile.open(path, "r:gz")
    return tar.extractfile(fname)

def nodes():
    with _get_handle("nodes.dmp") as h:
        o = read_dmp(h, usecols=[0,1,2])
        o.columns=["ID", "ParentID", "Rank"]
        return o

def citations():
    o = []
    with _get_handle("citations.dmp") as h:
        df = read_dmp(h, usecols=[3,6]).dropna()
        df = df.ix[df.iloc[:,0] != 0,:]
        for pubmed_id, s in df.to_records(index=False):
            for taxon_id in list(map(int, s.strip().split(" "))):
                o.append((taxon_id, int(pubmed_id)))
    return pd.DataFrame.from_records(o, columns=["TaxonID", "PMID"])

def graph():
    g = nx.DiGraph()
    def edges():
        for child_id, parent_id, rank in nodes().to_records(index=False):
            yield (child_id, parent_id)
    g.add_edges_from(edges())
    for taxon_id, name, _ in names().to_records():
        g.node[taxon_id]["scientific_name"] = name
    for taxon_id, _, rank in nodes().to_records(index=False):
        g.node[taxon_id]["rank"] = rank
    return g

@memoize
def names():
    scientific = {}
    common = {}
    handle = _get_handle("names.dmp")
    for i,line in enumerate(handle):
        fields = [c.strip("\t") for c in
                line.decode("utf-8")\
                        .rstrip("\n").split("|")][:-1]
        taxon_id = int(fields[0])
        name = fields[1]
        unique_name = fields[2]
        name_class = fields[3]
        if name_class == "scientific name":
            scientific[taxon_id] = name
        elif "common name" in name_class:
            common[taxon_id] = name

    records = [(taxon_id, scientific[taxon_id], common.get(taxon_id)) 
            for taxon_id in scientific]
    return pd.DataFrame(records, columns=["Taxon ID", "Scientific Name", "Common Name"])\
            .set_index(["Taxon ID"])

def by_id(taxon_id):
    assert isinstance(taxon_id, int)

    df = names()
    name = df.loc[taxon_id, "Scientific Name"]
    return Taxon(taxon_id, name)

@memoize
def table():
    g = graph()
    columns = ("kingdom", "phylum", "class", "order", "family", "genus", "species")
    def fn(q):
        o = {}
        path = nx.shortest_path(g, q, 1)
        for taxon_id in path:
            attrs = g.node[taxon_id]
            rank = attrs["rank"]
            rank = rank if rank != "superkingdom" else "kingdom"
            if rank in columns:
                name = attrs["scientific_name"]
                name = name if rank != "species" else " ".join(name.split(" ")[1:])
                o[rank] = name
        return o

    o = {}
    for taxon_id in g.nodes():
        if g.node[taxon_id]["rank"] in ("family", "genus", "species"):
            o[taxon_id] = fn(taxon_id)
    o = pd.DataFrame(o).T.loc[:,columns]
    o.index.name = "TaxonID"
    return o

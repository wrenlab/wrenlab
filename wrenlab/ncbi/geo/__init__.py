import os

from .db import GEO, GEOTaxon
from .label import get_labels
from .dataset import ExpressionDataset, MethylationDataset

def augment_labels(taxon_id, synonyms):
    import wrenlab.text.label_extraction.geo
    A = wrenlab.text.label_extraction.geo.annotation(taxon_id).copy()
    corpus = wrenlab.text.corpus.geo.GEOCorpus()
    M = dict(zip(A.index, A.TissueName.values))
     
    for doc in corpus.samples():
        text = "\n".join(doc.text.values())
        for key,ss in synonyms.items():
            for s in ss:
                if s in text.lower():
                    M[doc.id] = key
    A.TissueName = [M[ix] for ix in A.index]
    A.TissueName = A.TissueName.astype("category")
    return A

def available_platforms():
    import wrenlab.util.config
    config = wrenlab.util.config.get_configuration().get("matrixdb")

    assert "root" in config
    root = config["root"]
    o = []
    for path in os.listdir(root):
        accession = int(path.split(".")[0][3:])
        o.append(accession)
    return o

 
def platform(platform_id, taxon_id=9606, level="probe"):
    # FIXME : detect taxon ID
    import wrenlab.util.config
    import wrenlab.text.label_extraction.geo
    import matrixdb2 as matrixdb
    from wrenlab.dataset import Dataset

    assert level in ("probe",)

    config = wrenlab.util.config.get_configuration().get("matrixdb")

    if "root" in config:
        root = config["root"]
        path = os.path.join(root, "GPL{}.matrixdb".format(platform_id))
        X = matrixdb.EmbeddedMatrixDB(path)
    elif "host" in config:
        params = {k:v for k,v in config.items() if k in ("host", "port")}
        cx = matrixdb.Client(**params)
        path = os.path.join("ncbi", "geo", "GPL{}".format(platform_id))
        #path = "ncbi/geo/probe/GPL{}".format(platform_id)
        X = cx[path]

    A = wrenlab.text.label_extraction.geo.annotation(taxon_id)
    #A = augment_labels(taxon_id, {"heart": ["cardiac", "atrium", "heart"]})
    A.index = ["GSM{}".format(ix) for ix in A.index]
    ix = list(set(X.row_index) & set(A.index))
    A = A.loc[ix,:]
    cls = MethylationDataset if platform_id == 13534 else ExpressionDataset
    return cls(X, row_annotation=A, 
            metadata={"PlatformID": platform_id, "TaxonID": taxon_id})\
                    .align()

import errno
import ftplib
import glob
import os.path
import shutil
import subprocess as sp
import tarfile
import tempfile
from tempfile import TemporaryDirectory, NamedTemporaryFile

from joblib import Parallel, delayed

import wrenlab.util
import wrenlab.util.aspera
from wrenlab.util import LOG, ignore_errors

def check_tmpdir(need=100):
    du = wrenlab.util.disk_usage(tempfile.gettempdir())
    GB_FREE = du.free / 2 ** 30
    if GB_FREE < need:
        LOG.warn("Only {}GB free in temporary partition '{}' ({}GB needed). Download may fail.".format(round(GB_FREE, 1), tempfile.gettempdir(), need))
        LOG.warn("If you have a different, non-default temporary partition with more space to use, set the 'tempfile.tempdir' variable and run again.")

def platform_list():
    cx = ftplib.FTP("ftp.ncbi.nlm.nih.gov", user="anonymous")
    cx.cwd("/geo/platforms/")
    o = set()
    for directory in cx.nlst():
        cx.cwd("/geo/platforms/{}/".format(directory))
        for platform_id in cx.nlst():
            o.add(platform_id)
    return list(sorted(o))

def group_prefix(accession):
    if len(accession) <= 6:
        group = "GPLnnn"
    elif len(accession) == 7:
        group = "{}nnn".format(accession[:4])
    else:
        group = "{}nnn".format(accession[:5])
    return group

@ignore_errors(lambda a,t: LOG.error("Error fetching MiniML for accession: {}".format(a)))
def fetch(accession, target):
    """
    Download the MiniML data associated with a GEO platform to the target path
    (creates a .tgz file). 

    Notes
    -----
    MiniML archives come in multiple parts, and the files inside are not
    sorted in any obvious way. To ease later processing stages, this code
    extracts all files from all the archives associated with the accession,
    then rearchives and recompresses them into a single archive. The downside
    is lots of disk space usage during processing.

    - Prepends ${accession}_family.xml and {accession}-tbl-1.txt at the beginning
      of the archive.
    - Only includes sample-level data (that is, deletes GSE-prefixed files) to
      remove duplication.
    """

    check_tmpdir()
 
    with TemporaryDirectory() as dldir, TemporaryDirectory() as dcdir:
        c = wrenlab.util.aspera.Client(host="ftp.ncbi.nlm.nih.gov")
        group = group_prefix(accession)

        LOG.info("Downloading MiniML data for {} ...".format(accession))
        uri = "/geo/platforms/{group}/{accession}/miniml/".format(**locals())
        c.download(uri, dldir)

        LOG.info("Extracting MiniML data for {} to {}...".format(accession, dcdir))
        for path in glob.glob("{}/miniml/*.tgz".format(dldir)):
            with tarfile.open(path, mode="r:gz") as h:
                h.extractall(dcdir)

        LOG.info("Creating output MiniML archive for {} ...".format(accession))
        with NamedTemporaryFile() as tmp:
            with tarfile.open(tmp.name, mode="w:gz") as o:
                # Prepend XML and ptbl
                prepend = [s.format(accession) for s in ["{}_family.xml", "{}-tbl-1.txt"]]
                for fname in prepend:
                    o.add(os.path.join(dcdir, fname), arcname=fname)

                rest = os.listdir(dcdir)
                for fname in prepend:
                    rest.remove(fname)

                for fname in sorted(rest):
                    if fname.startswith("GSM"):
                        path = os.path.join(dcdir, fname)
                        o.add(path, arcname=fname)
            LOG.info("Copying MiniML for {} to final destination.".format(accession))
            shutil.move(tmp.name, target)

def fetch_all(target_dir):
    """
    Fetch and cache all GEO MiniML data locally in the provided directory.
    """
    platforms = platform_list()
    os.makedirs(target_dir, exist_ok=True)

    def jobs():
        for accession in platforms:
            target = os.path.join(target_dir, "{}.tgz".format(accession))
            if os.path.exists(target):
                LOG.info("MiniML archive already cached: {}".format(accession))
                continue
            yield accession, target

    _ = Parallel(n_jobs=4)(delayed(fetch)(accession, target) 
            for accession, target in jobs())
    """
    try:
        fetch(accession, target)
    except Exception as e:
        pass
        LOG.error("Error fetching MiniML for accession: {}".format(accession))
    """

if __name__ == "__main__":
    import sys
    accession, target = sys.argv[1:]
    fetch(accession, target)

    #fetch_all(sys.argv[1])

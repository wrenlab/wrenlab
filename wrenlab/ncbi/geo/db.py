"""
TODO:
- memoize cache per GEO root rather than global
"""
import sys
import functools
import os.path
import itertools
import multiprocessing as mp
import gzip
import glob
import tempfile
import shutil

import numpy as np
import pandas as pd
from joblib import Memory

from wrenlab.experiment import Experiment, ExperimentSet
from wrenlab.util import memoize, LOG
import wrenlab.impute
import wrenlab.lm
import wrenlab.ncbi.geo.fetch
import wrenlab.ncbi.geo.label
import wrenlab.ncbi.geo.metadb
import wrenlab.ncbi.geo.miniml
import wrenlab.normalize
import wrenlab.ontology
import wrenlab.distance

def realign(A,X):
    while X.isnull().all(axis=0).any() or X.isnull().all(axis=1).any():
        X = X.dropna(how="all", axis=0).dropna(how="all", axis=1)
    A, X = A.align(X, join="inner", axis=0)
    return A,X

@functools.lru_cache()
@memoize
def array_describe(path):
    # FIXME: Find a way to regenerate if mtime changes
    X = mmat.MMAT(path)
    return X.describe()

def _fit(args):
    import wrenlab.R
    try:
        gene_id, formula, D = args
        fit = wrenlab.R.lmer(formula, D.Expression, D)
        return gene_id, fit.fixed_effects
    except:
        return

class GEOMatrixCollection(object):
    def __init__(self, directory):
        self._directory = directory

    def __getitem__(self, accession):
        gz_path = os.path.join(self._directory, "{}.gz".format(accession))
        mmat_path = os.path.join(self._directory, "{}.mmat".format(accession))

        if not os.path.exists(gz_path):
            raise KeyError
        elif not os.path.exists(mmat_path):
            with gzip.open(gz_path, "rt") as h:
                LOG.info("Importing {} to MMAT ...".format(accession))
                mmat.MMAT.from_file(h, mmat_path)
        else:
            pass
        return mmat.MMAT(mmat_path)

class GEO(object):
    def __init__(self, root, collapse_method="max_mean"):
        self._collapse_method = collapse_method
        self._metadb = wrenlab.ncbi.geo.metadb.connect()
        assert collapse_method in ("max_mean",)

        self._root = os.path.abspath(os.path.expanduser(root))
        for p in self._dirs.values():
            os.makedirs(p, exist_ok=True)

        #self._labels = wrenlab.ncbi.geo.label.get_labels()

    @property
    def probe(self):
        return GEOMatrixCollection(self._dirs["probe"])

    @property
    def _dirs(self):
        return {
            "miniml": os.path.join(self._root, "miniml"),
            "probe": os.path.join(self._root, "matrix/probe"),
            "gene": os.path.join(self._root, "matrix/gene"),
            "taxon": os.path.join(self._root, "matrix/taxon")
        }

    def _path(self, type, accession):
        dir = self._dirs.get(type)
        if type in ("probe", "gene", "taxon"):
            extension = "gz"
        elif type == "miniml":
            extension = "tgz"
        elif type == "mmat":
            extension = "mmat"
            dir = self._dirs["taxon"]
        return os.path.join(dir, "{}.{}".format(accession, extension))

    def _sync_miniml(self, accession=None):
        if isinstance(accession, str):
            assert accession.startswith("GPL")
            target = os.path.join(self._dirs["miniml"], "{}.tgz".format(accession))
            if not os.path.exists(target):
                wrenlab.ncbi.geo.fetch.fetch(accession, target)
        else:
            wrenlab.ncbi.geo.fetch.fetch_all(self._dirs["miniml"])

    def _sync_probe(self, accession=None):
        if isinstance(accession, str):
            assert accession.startswith("GPL")
            self._sync_miniml(accession)

            archive_path = os.path.join(self._dirs["miniml"], "{}.tgz".format(accession))
            target = os.path.join(self._dirs["probe"], "{}.gz".format(accession))
            if not os.path.exists(target):
                try:
                    with gzip.open(target, mode="wt", compresslevel=5) as o:
                        a = wrenlab.ncbi.geo.miniml.Archive(archive_path, 
                                unpack=False, parallel=False)
                        a.dump(o)
                except Exception as e:
                    print(e)
                    LOG.error("Error dumping values from {}".format(accession))
                    os.unlink(target)
        else:
            for archive_path in glob.glob(os.path.join(self._dirs["miniml"], "*.tgz")):
                accession = os.path.splitext(os.path.basename(archive_path))[0]
                self._sync_probe(accession)

    def _sync_gene(self, accession=None):
        if isinstance(accession, str):
            assert accession.startswith("GPL")
            self._sync_probe(accession)

            archive = wrenlab.ncbi.geo.miniml.Archive(self._path("miniml", accession),
                    unpack=True)
            try:
                j = dict(archive.probe_gene_map.to_records(index=False))
            except AttributeError:
                LOG.info("Probe-gene mapping not found for {}.".format(accession))
                return

            chunks = iter(pd.read_csv(self._path("probe", accession), 
                    compression="gzip", sep="\t", index_col=0, chunksize=100))

            # TODO: here is where multiple collapse methods would be allowed
            head = next(chunks)
            assert self._collapse_method == "max_mean"
            mu = head.mean()
            genes, probes = zip(*mu.groupby(j).apply(
                lambda x: (int(x.name), x.sort_values().index[-1])))

            def collapse(X):
                X = X.loc[:,probes]
                X.columns = genes
                return X

            with gzip.open(self._path("gene", accession), mode="wt", compresslevel=5) as o:
                print("", *genes, sep="\t", file=o)
                for chunk in itertools.chain([head], chunks):
                    Xc = collapse(chunk)
                    for i in range(Xc.shape[0]):
                        print(Xc.index[i], *Xc.iloc[i,:], sep="\t", file=o)
        else:
            raise NotImplementedError

    def sync(self, accession=None, molecule="total RNA"):
        if accession is None:
            rs = list(self._metadb.query("""
                SELECT DISTINCT(gpl)
                FROM gsm
                WHERE
                    molecule_ch1='{}';""".format(molecule)))
            LOG.info("{} accessions found with molecule '{}'".format(len(rs), molecule))
            for accession in rs:
                try:
                    self.sync(accession=accession)
                except Exception as e:
                    LOG.error("Error syncing {} ...".format(accession))
                    print(e)
                    continue
        else:
            self._sync_miniml(accession=accession)
            self._sync_probe(accession=accession)
            self._sync_gene(accession=accession)

    def build_matrix(self, taxon_id):
        """
        """
        import wrenlab.ncbi.taxonomy
        if not os.path.exists(self._path("taxon", taxon_id)):
            taxon_name = wrenlab.ncbi.taxonomy.names().loc[taxon_id,"Scientific Name"]
            df = self._metadb.query("""
                SELECT gsm.gpl, gsm.gsm 
                FROM gsm 
                INNER JOIN gpl
                ON gpl.gpl=gsm.gpl
                WHERE 
                gsm.organism_ch1='{}'
                AND
                gpl.organism='{}'""".format(taxon_name, taxon_name))
            genes = set()
            for platform_id, sdf in df.groupby("gpl"):
                samples = set(sdf["gsm"])
                path = self._path("gene", platform_id)
                if not os.path.exists(path):
                    continue
                with gzip.open(path, "rt") as h:
                    genes |= set(map(int, next(h).split("\t")[1:]))
            genes = list(sorted(genes))
            seen = set()

            with tempfile.NamedTemporaryFile(suffix=".gz") as o:
                with gzip.open(o.name, mode="wt") as h:
                    print("", *genes, sep="\t", file=h)
                    for platform_id, sdf in df.groupby("gpl"):
                        if sdf.shape[0] < 100:
                            continue
                        #if not platform_id == "GPL96":
                        #    continue
                        samples = set(sdf["gsm"])
                        path = self._path("gene", platform_id)
                        if not os.path.exists(path):
                            LOG.info("No gene-level matrix found: {}".format(platform_id))
                            continue
                        else:
                            LOG.info("Exporting {} to matrix for TaxonID: {}"\
                                    .format(platform_id, taxon_id))
                        for chunk in pd.read_csv(path, index_col=0, sep="\t", 
                                compression="gzip", chunksize=100):
                            chunk.columns = list(map(int, chunk.columns))
                            ix = list(sorted((samples & set(chunk.index)) - seen))
                            if len(ix) == 0:
                                continue
                            chunk = chunk.loc[ix,:].loc[:,genes].round(3)
                            chunk.to_csv(h, sep="\t", header=False)
                            seen |= set(chunk.index)
                shutil.copyfile(o.name, self._path("taxon", taxon_id))

        LOG.info("Importing data into MMAT (TaxonID: {})".format(taxon_id))
        with gzip.open(self._path("taxon", taxon_id), mode="rt") as h:
            return mmat.MMAT.from_file(h, self._path("mmat", taxon_id))

    def __getitem__(self, taxon_id):
        assert isinstance(taxon_id, int)

        A = wrenlab.ncbi.geo.label.get_labels(taxon_id)
        path = self._path("mmat", taxon_id)
        if not os.path.exists(path):
            X = self.build_matrix(taxon_id)
        else:
            X = mmat.MMAT(path)
        return GEOTaxon(self._root, taxon_id, A, X)

def limit_platform(A,X,n=100):
    p_count = A.PlatformID.copy().value_counts()
    ix = p_count.index[p_count >= n]
    A = A.ix[A.PlatformID.isin(ix),:]
    return realign(A,X)

@memoize
def data_tissue(root, taxon_id, tissue_id):
    assert taxon_id == 9606

    db = GEO(root)
    GT = db[taxon_id]
    A = GT._A
    A = A.ix[A.TissueID == tissue_id,:]
    A = A.query("Age >= 25 & Age <= 100")
    A = A.ix[A.Molecule.isin(["total RNA","polyA RNA"]),:]
    X = GT._X

    ix = list(sorted(set(["GSM{}".format(ix) for ix in A.index]) & set(X.index)))
    X = X.loc[ix,:].to_frame()
    X.index = [int(x[3:]) for x in X.index]
    A,X = limit_platform(A,X)
    X = X.T.dropna(thresh=int(X.shape[0] * 0.5)).T

    X = wrenlab.statistics.log_transform(X, axis=1)
    X = wrenlab.statistics.standardize(X, axis=1)
    Xi = wrenlab.impute.SVD(X)

    return Xi

@memoize
def data_raw(root, taxon_id):
    """
    Return labels and data matrix for all genes and all samples which
    have an entry in the labels table (i.e., have at least one extracted
    label).
    """
    db = GEO(root)
    GT = db[taxon_id]
    A = GT._A
    A = A.query("TissueID != 0")
    if taxon_id == 9606:
        A = A.query("Age >= 25 & Age <= 100")
    X = GT._X

    ix = list(sorted(set(["GSM{}".format(ix) for ix in A.index]) & set(X.index)))
    X = X.loc[ix,:].to_frame()
    X.index = [int(x[3:]) for x in X.index]
    A,X = limit_platform(A,X)
    X = X.T.dropna(thresh=int(X.shape[0] * 0.5)).T

    return realign(A,X)

# Need outlier detection

@memoize
def data_preprocess(root, taxon_id):
    A,X = data_raw(root, taxon_id)
    X = wrenlab.statistics.log_transform(X, axis=1)
    X = wrenlab.statistics.standardize(X, axis=1)
    return realign(A,X)

def data_impute(root, taxon_id):
    A,X = data_preprocess(root, taxon_id)
    Xi = wrenlab.impute.SVD(X)
    return A,Xi

@memoize
def fit(root, taxon_id, formula):
    A,X = data_impute(root, taxon_id)
    return wrenlab.lm.ols(X,A,formula=formula)

@memoize
def tissue_fits(root, taxon_id, formula):
    GT = GEO(root)[taxon_id]
    o = {}
    for tissue in GT._A.query("TissueID != 0").TissueName.value_counts().head(10):
        try:
            o[tissue] = GT.fit_tissue(tissue, formula)
        except:
            continue
    return o

# possibly this should be done iteratively ....
# standardize? divide my max per sample? what?
#   I guess the point is somehow to control for different SD per gene-experiment

# Assumptions:
# - "Mean" and "SD" of each gene should be same for each platform
# - For each gene, Mean/SD should be zero for each experiment? 
#       (probably not, but SDs should not be same order of magnitude)
# - Distributions of values between samples should be similar/same

@memoize
def control(root, taxon_id):
    A,X = _data(root, taxon_id)
    X = X.iloc[:,:200]
    X = wrenlab.statistics.log_transform(X, axis=1)

    Xi = wrenlab.impute.SVD(X)
    fit = wrenlab.lm.ols(Xi,A,formula="C(ExperimentID) + C(PlatformID)")
    o = np.array(fit._residual)
    o[np.isnan(X)] = np.nan
    return A, pd.DataFrame(o, index=X.index, columns=X.columns)
    
@memoize
def tissue_mean(root, taxon_id, names=False):
    A,X = data(root, taxon_id, impute=True)
    # FIXME: just correct for platform with lm
    ix = A.PlatformID == A.PlatformID.value_counts().index[0]
    #ix = A.PlatformID.isin(A.PlatformID.value_counts().index[:5])
    A = A.ix[ix,:]
    X = X.ix[ix,:]
    mu = X.groupby(A.TissueID).mean()
    return mu

class GEOTaxon(object):
    def __init__(self, root, taxon_id, A, X):
        self._root = root
        cache_path = os.path.join(root, "cache", str(taxon_id))
        os.makedirs(cache_path, exist_ok=True)

        self.taxon_id = taxon_id
        self._A = A
        self._X = X

    def experiment(self, experiment_id):
        #FIXME: account for many-to-many
        A = self._A.ix[self._A.ExperimentID == experiment_id,:]
        assert A.shape[0] > 0
        ix = list(sorted(set(["GSM{}".format(ix) for ix in A.index]) & set(self._X.index)))
        X = self._X.loc[ix,:].to_frame()
        X.index = [int(x[3:]) for x in X.index]
        X = X.T.dropna(thresh=int(X.shape[0] * 0.5)).T
        X = wrenlab.statistics.log_transform(X, axis=1)
        return Experiment(A, X.T)
    
    def tissue(self, tissue_id):
        if isinstance(tissue_id, str):
            o = wrenlab.ontology.fetch("BTO")
            tissue_id = o.name_map(reverse=True)[tissue_id]
        X = data_tissue(self._root, self.taxon_id, tissue_id)
        A = self._A.ix[X.index,:]
        return A,X

    def fit_tissue(self, tissue_id, formula):
        A,X = self.tissue(tissue_id)
        return wrenlab.lm.ols(X,A,formula=formula)

    def data(self, 
            preprocess=False, 
            impute=False, 
            require_labels=None, 
            age_range=None, 
            transpose=False, 
            as_dask=False):
        """
        """
        if impute is True:
            assert preprocess is True
            A,X = data_impute(self._root, self.taxon_id)
        elif preprocess is True:
            A,X = data_preprocess(self._root, self.taxon_id)
        else:
            A,X = data_raw(self._root, self.taxon_id)

        if require_labels is not None:
            A = A.dropna(subset=require_labels, how="any")
        if age_range is not None:
            assert len(age_range) == 2
            assert age_range[1] > age_range[2]
            A = A.ix[(A.Age >= age_range[0]) & (A.Age <= age_range[1]),:]
        A,X = realign(A,X)

        if transpose is True:
            X = X.T
        if as_dask is True:
            X = dd.from_pandas(X, chunksize=1000)
        return A,X

    def fit(self, formula):
        return fit(self._root, self.taxon_id, formula)

    def tissue_mean(self, names=False, min_count=100):
        mu = tissue_mean(self._root, self.taxon_id)

        counts = self._A.TissueID.value_counts()
        ix = list(set(counts.index[counts >= min_count]) & set(mu.index))
        mu = mu.loc[ix,:]

        if names is True:
            M = wrenlab.ontology.fetch("BTO").name_map()
            mu.index = [M[ix] for ix in mu.index]
        return mu

    def distance_index(self, type="tissue"):
        assert type in ("tissue",)
        mu = self.tissue_mean(names=True).iloc[:100,:].iloc[:,:1000]
        return wrenlab.distance.DistanceIndex(mu)

########################
# Command-line interface
########################

import click

@click.group()
def cli():
    pass

@cli.command(help="Download and preprocess GEO MiniML files.")
@click.argument("root")
@click.option("--tmpdir", "-t", help="Temporary directory to for MiniML decompression")
@click.option("--molecule", "-m", default="total RNA", help="Sync GPLs with this molecule type")
@click.option("--accession", "-a", default=None, help="Sync only this GPL. Overrides --molecule.")
def sync(root, tmpdir=None, molecule="total RNA", accession=None):
    if tmpdir is not None:
        import tempfile
        tempfile.tempdir = tmpdir
    db = GEO(root)
    if accession is not None:
        db.sync(accession=accession)
    else:
        db.sync(molecule=molecule)

@cli.command(help="Build taxon-level expression matrix")
@click.argument("root")
@click.argument("taxon_id")
@click.option("--tmpdir", "-t", help="Temporary directory to for MiniML decompression")
def build_matrix(root, taxon_id, tmpdir=None):
    taxon_id = int(taxon_id)
    if tmpdir is not None:
        import tempfile
        tempfile.tempdir = tmpdir
    db = GEO(root)
    db.build_matrix(taxon_id)

if __name__ == "__main__":
    cli()

"""
@memoize
def dataqn(root, taxon_id):
    A,X = _data(root, taxon_id)
    mask = ~np.isnan(np.array(X))
    X = wrenlab.statistics.log_transform(X, axis=1)
    Xi = wrenlab.impute.SVD(X)
    Xn = wrenlab.normalize.quantile(Xi.T).T

    o = np.array(Xn, dtype=np.float32)
    for i in range(o.shape[0]):
        o[i,~mask[i,:]] = np.nan
    o = pd.DataFrame(o, index=Xn.index, columns=Xn.columns)
    print(A.shape, o.shape)
    return realign(A,o)
"""



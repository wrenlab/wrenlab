from tarfile import TarFile
from tempfile import TemporaryDirectory
import collections
import gzip
import io
import itertools
import multiprocessing as mp
import os
import os.path
import pickle
import psutil
import subprocess as sp
import shutil
import sys
import tarfile

import xml.etree.ElementTree as ET

import lxml.etree
import pandas as pd
import numpy as np

import wrenlab.data
import wrenlab.ncbi.geo.fetch
from wrenlab.util import LOG, StreamingDataFrame

ENCODING = "iso-8859-1"

def cleanup(path):
    print("cleaning up", path)
    shutil.rmtree(path, ignore_errors=True)

def as_float(x):
    try:
        return float(x)
    except ValueError:
        return np.nan

def _parse_sample(X, ix, accession):
    """
    Parse an individual sample in a MiniML archive.
    """
    ks = X.iloc[:,0].astype(str)
    try:
        vs = X.iloc[:,1].astype(float).values
    except ValueError:
        try:
            vs = list(map(as_float, X.iloc[:,1]))
        except ValueError:
            LOG.warn("{} : error parsing probe values as float".format(accession))
            return None
    try:
        values = pd.Series(vs, index=ks).loc[ix]
    except:
        LOG.warn("{} : no index values found".format(accession))
        return None
    if values.isnull().all():
        LOG.warn("{} : all probe values NaN".format(accession))
        return None
    values.name = accession
    LOG.debug("{} : OK".format(accession))
    return values

def _parse_sample_from_bytes(args):
    accession, probe_ix, value_ix, ix, data = args
    h = io.BytesIO(data)
    try:
        X = pd.read_csv(h, sep="\t", 
                usecols=(probe_ix, value_ix), header=None, encoding=ENCODING)
        if probe_ix > value_ix:
            X = X.iloc[:,(1,0)]
        return _parse_sample(X, ix, accession)
    except UnicodeDecodeError:
        LOG.warn("{} : UnicodeDecodeError".format(accession))
        return None
    finally:
        h.close()
        del data
        del h

def _parse_sample_from_path(args):
    accession, probe_ix, value_ix, ix, path = args
    with open(path, "rb") as h:
        return _parse_sample_from_bytes((accession, probe_ix, value_ix, ix, h.read()))

class Archive(object):
    """
    Parser for MiniML archives.

    They are assumed to be sorted as done by `hta.data.ncbi.geo.miniml.fetch`.
    1. GPLnnn_family.xml
    2. GPLnnn-tbl-1.txt
    3. Everything else
    """
    NS = {"miniml": "http://www.ncbi.nlm.nih.gov/geo/info/MINiML"}

    def __init__(self, path, parallel=True, unpack=False):
        """
        Arguments
        ---------
        parallel : bool, default True
            Whether to parse in parallel using multiprocessing
        unpack : bool, default False
            Unpack the whole archive to a temporary directory before 
            processing. This is faster, but requires a lot of disk
            space. Generally, this will be inefficient unless the
            caller is planning to iterate over all the data
            vectors in the archive.
        """
        assert isinstance(path, str)
        assert isinstance(parallel, bool)
        assert isinstance(unpack, bool)

        self._path = path
        self._is_open = True

        self._parallel = parallel
        self._unpack = unpack

        if unpack:
            self.unpack()
            xml_path = os.path.join(self._tmpdir.name, 
                    next(fname for fname in os.listdir(self._tmpdir.name) 
                        if fname.endswith("_family.xml")))
            with open(xml_path, encoding=ENCODING) as h:
                self._parse_miniml(h)
            ptbl_path = os.path.join(self._tmpdir.name,
                    "{}-tbl-1.txt".format(self.accession))
            with open(ptbl_path, encoding=ENCODING) as h:
                self._parse_ptbl(h)
        else:
            assert self._path.endswith(".tgz") or self._path.endswith(".tar.gz")
            self._handle = tarfile.open(path, mode="r:gz", encoding=ENCODING)
            self._iter = iter(self._handle)

            ti_xml = next(self._iter)
            with self._handle.extractfile(ti_xml) as h:
                self._parse_miniml(h)

            with self._handle.extractfile(next(self._iter)) as h:
                self._parse_ptbl(h)

        # parse column map
        LOG.debug("Parsing column maps for {}".format(self.accession))
        self._column_map = self._parse_sample_column_map()

    def _parse_miniml(self, handle):
        # parse XML
        LOG.debug("Parsing MiniML XML in archive: {}".format(self._path))
        parser = lxml.etree.XMLParser(recover=True, encoding=ENCODING)
        tree = ET.parse(handle, parser=parser)
        self._root = tree.getroot()

        # determine accession and taxon id
        LOG.debug("Determining accession and taxon ID: {}".format(self._path))
        self.accession = self._root.find("miniml:Platform", self.NS).attrib["iid"]
        try:
            self.taxon_id = self._infer_taxon_id()
        except:
            self.taxon_id = None

    def _parse_ptbl(self, handle):
        # parse platform table
        LOG.debug("Parsing probe table for {}".format(self.accession))
        self._ptbl = pd.read_csv(handle,
                sep="\t", header=None, encoding=ENCODING)
        self._ptbl.columns = [c["Name"] for c in self._platform_table_columns()]
        self._ptbl.columns.description = [c.get("Description") 
                for c in self._platform_table_columns()]

    def unpack(self):
        if self._path.endswith(".tar.lz4"):
            compress = "lz4"
        elif self._path.endswith(".tgz") or self._path.endswith(".tar.gz"):
            compress = "pigz"
        else:
            raise ValueError("Decompress program could not be determined for file: {}".format(self._path))
        self._tmpdir = TemporaryDirectory()
        cmd = ["tar", "--use-compress-program={}".format(compress), "-C", 
                self._tmpdir.name, "-xf", self._path]
        LOG.info("Decompressing {} to temporary directory: {}".format(self._path, 
            self._tmpdir.name))
        sp.check_call(cmd)

    def close(self):
        if self._is_open:
            if hasattr(self, "_pool"):
                self._pool.terminate()

            if hasattr(self, "_handle"):
                self._handle.close()
            
            if hasattr(self, "_tmpdir"):
                self._tmpdir.cleanup()

            self._is_open = False

    def __del__(self):
        self.close()

    def __iter__(self):
        jobs = self._jobs()

        if self._parallel is True:
            # determine memory usage of a single job
            # with a safety factor of 2X of process size (including a job)
            first = next(jobs)
            jobs = itertools.chain([first], jobs)
            #available = psutil.virtual_memory().available
            #usage = psutil.Process(os.getpid()).get_memory_info()[0]
            #ncpu = max(1, min(int(available / (usage * 2)), mp.cpu_count()))
            ncpu = 16

            LOG.info("Parsing {} in parallel with NCPU={}".format(self.accession, ncpu))
            self._pool = mp.Pool(processes=ncpu)
            map_fn = lambda func, iterable: self._pool.imap(func, iterable)
        else:
            LOG.info("Parsing {} in single-threaded mode".format(self.accession))
            map_fn = map

        if self._unpack:
            fn = _parse_sample_from_path
        else:
            fn = _parse_sample_from_bytes

        yield from filter(lambda x: x is not None, map_fn(fn, jobs))

    def dump(self, handle=sys.stdout, round=3, delimiter="\t", collapse=False):
        """
        Dump the data to delimited text on the provided handle.
        """
        assert collapse is False
        it = iter(self)
        first = next(it)
        it = itertools.chain([first], it)
        print("", *first.index, sep=delimiter, file=handle)
        for row in it:
            row = row.round(round)
            print(row.name, *row, sep=delimiter, file=handle)

        #StreamingDataFrame(self.data(collapse=collapse)).dump(handle)

    def data(self, collapse=False):
        """
        Return a generator to the expression vectors in this archive.

        Arguments
        ---------
        collapse : bool
            Whether to collapse probe IDs to gene IDs. Will throw an exception
            if probe->gene mappings are not available.

        Returns
        -------
        A generator of :class:`pandas.Series` with an index of either Probe IDs
        or Entrez Gene IDs, depending on the "collapse" parameter.
        """
        # FIXME: sample to determine max-mean probe mappings
        # FIXME: allow a "method" parameter?

        if not collapse:
            yield from iter(self)
        else:
            pmap = self.probe_gene_map
            assert pmap is not None
            assert pmap.shape[0] > 0

            #genes = Entrez.genes(self.taxon_id)
            genes = list(sorted(set(pmap["Gene ID"])))
            for x in iter(self):
                xc = pmap.join(x, on="Probe ID").groupby("Gene ID")[x.name].mean()
                yield xc.loc[genes]

    @property
    def probe_gene_map(self):
        """
        Map probe IDs from this archive to Entrez Gene IDs using AILUN or
        the platform description table. Try to map using data in the 
        following order:

        1. AILUN
        2. Direct Entrez Gene ID mapping - "ENTREZ_GENE_ID"
        3. Symbol - 
        4. Genbank accession -
        5. Ensembl ID -

        Returns
        -------
        dict of Probe ID -> Entrez Gene ID mappings.
        """
        # TODO: what about multimappings like "123 // 456"
        # should probably return 2-col dataframe with Probe ID, Gene ID

        cmap = {
                #"Probe ID": ["ID", "ProbeID", "ProbeUID", 
                #    "PROBE_ID", "PROBE_NAME", "ProbeName", "Array_Address_Id"],
                "Gene ID":
                    ["GENE", "GeneID", "Entrez Gene", "Entrez_Gene_ID", "LOCUSLINK_ID",
                        "Entrez_Gene", "Entrez Gene ID", "EntrezGeneID", "Gene_ID"],
        }
        # ORF

        """
        try:
            pmap = hta.data.stanford.AILUN(self.accession)
            LOG.info("{} : Found probe-gene mapping in AILUN".format(self.accession))
            return pd.DataFrame.from_records(list(pmap.items()), 
                    columns=["Probe ID", "Gene ID"])\
                            .drop_duplicates()
        except:
            pass
        """

        # 2. direct EG mapping
        for i,c in enumerate(self._ptbl.columns):
            for n in cmap["Gene ID"]:
                if n.lower() == c.lower():
                    ptbl = self._ptbl.loc[:,[self._ptbl.columns[0],c]].copy().dropna()
                    ptbl.iloc[:,1] = ptbl.iloc[:,1].astype(str)
                    o = []
                    for probe_id, gene_txt in ptbl.to_records(index=False):
                        for gene_id in gene_txt.split(" /// "):
                            try:
                                gene_id = int(gene_id)
                            except ValueError:
                                continue
                            o.append((str(probe_id), gene_id))
                    if len(o) > 0:
                        LOG.info("{} : Found probe-gene mapping from MiniML direct mapping"\
                                .format(self.accession))
                        return pd.DataFrame(o, columns=["Probe ID", "Gene ID"])\
                                .drop_duplicates()

        o = wrenlab.data.AILUN(self.accession)
        if len(o) > 0:
            LOG.info("{} : Found probe-gene mapping in AILUN")
            return pd.DataFrame(list(o.items()), columns=["Probe ID", "Gene ID"])\
                    .drop_duplicates()

        if True:
            LOG.debug("FIXME: Use secondary mapping to find probe->gene map (primary mapping not found)")
            return

        # Everything else requires a secondary mapping; each section finds the 
        # mapping, and the final section does the probe -> map -> gene ID conversion

        mapping = None
        column = None

        """
        # 3. Gene symbol
        for c in self._ptbl.columns:
            if "symbol" in c.lower():
                genes = Entrez.genes(self.taxon_id)\
                        .dropna(subset=["Symbol"])
                mapping = dict(zip(genes["Symbol"], map(int, genes.index)))
                column = c
                print("symbol")
                break
        """

        # Perform the secondary mapping

        if (mapping is None) or (column is None):
            print(self._ptbl.head())
            return

        o = {}
        for probe_id, secondary in self._ptbl.loc[:,[self._ptbl.columns[0], column]].dropna():
            gene_id = mapping.get(secondary)
            if gene_id is not None:
                o[probe_id] = gene_id
        return o

    def _jobs(self):
        if self._unpack is True:
            yield from self._jobs_unpacked()
        else:
            yield from self._jobs_packed()

    def _job_info(self, fname):
        if not (fname.startswith("GSM") and fname.endswith("-tbl-1.txt")):
            return
        gsm = fname.split("-")[0]
        if not gsm in self._column_map:
            return
        probe_ix, value_ix = self._column_map[gsm]
        return gsm, probe_ix, value_ix

    def _jobs_packed(self):
        # FIXME: use some name instead of iloc[:,0]?
        index = pd.Index(list(sorted(map(str, self._ptbl.iloc[:,0]))))
        for item in self._iter:
            try:
                gsm, probe_ix, value_ix = self._job_info(item.name)
            except TypeError:
                continue
            with self._handle.extractfile(item) as h:
                data = h.read()
                yield (gsm, probe_ix, value_ix, index, data)

    def _jobs_unpacked(self):
        index = pd.Index(list(sorted(map(str, self._ptbl.iloc[:,0]))))

        for fname in sorted(os.listdir(self._tmpdir.name)):
            try:
                gsm, probe_ix, value_ix = self._job_info(fname)
            except TypeError:
                continue
            path = os.path.join(self._tmpdir.name, fname)
            yield (gsm, probe_ix, value_ix, index, path)

    def _infer_taxon_id(self):
        c = collections.Counter()

        for sample in self._root.findall("miniml:Sample", self.NS):
            for channel in sample.findall("miniml:Channel", self.NS):
                m = channel.find("miniml:Organism", self.NS)
                if m is not None:
                    taxon_id = int(m.attrib["taxid"])
                    c[taxon_id] += 1
        try:
            return c.most_common(1)[0][0]
        except IndexError:
            pass
        raise ValueError("Could not infer taxon ID for {}".format(self.accession))

    def _parse_sample_column_map(self):
        column_map = {}
        for sample in self._root.findall("miniml:Sample", self.NS):
            gsm = sample.attrib["iid"]
            table = sample.find("miniml:Data-Table", self.NS)
            if table is None:
                continue

            cm = {}
            for column in table.findall("miniml:Column", self.NS):
                i = int(column.attrib["position"]) - 1
                name = column.find("miniml:Name", self.NS)
                if name is None:
                    continue
                cm[name.text] = i

            probe_ix = cm.get("ID_REF")
            value_ix = cm.get("VALUE")
            if (probe_ix is not None) and (value_ix is not None):
                column_map[gsm] = (probe_ix, value_ix)
        return column_map

    def _platform_table_columns(self):
        """
        Return a list of dicts containing attributes for each column found in
        GPLnnn-tbl-1.txt, including:
        - Name
        - Description
        - Link-Prefix

        Only "Name" is guaranteed to exist.
        """
        o = []
        for column in self._root.find("miniml:Platform", self.NS)\
                .find("miniml:Data-Table", self.NS)\
                .findall("miniml:Column", self.NS):
            c = {}
            for attr in column:
                tag = attr.tag.split("}")[1]
                c[tag] = attr.text
            o.append(c)
        return o

class Store(object):
    def __init__(self, root, parallel=False, unpack=False, autofetch=True):
        self._root = os.path.abspath(root)
        os.makedirs(self._root, exist_ok=True)

        self._parallel = parallel
        self._unpack = unpack
        self._autofetch = autofetch

    def __getitem__(self, accession):
        if isinstance(accession, int):
            accession = "GPL{}".format(accession)
        assert accession.startswith("GPL")

        path = os.path.join(self._root, "{}.tgz".format(accession))
        if not os.path.exists(path):
            if not self._autofetch:
                raise KeyError
            else:
                wrenlab.ncbi.geo.fetch(accession, path)
        return Archive(path, parallel=self._parallel, unpack=self._unpack)

    def fetch_all(self):
        wrenlab.ncbi.geo.fetch.fetch_all(self._root)

if __name__ == "__main__":
    try:
        path = sys.argv[1]
        archive = Archive(path, parallel=False, unpack=False, tmpdir="/data/tmp")
        try:
            archive.dump()
        except StopIteration:
            pass
        finally:
            archive.close()
    except Exception as e:
        LOG.error("Error opening MiniML archive at: {}".format(path))
        raise

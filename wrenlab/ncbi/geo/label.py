"""
Notes
=====
- a lot of the characteristics fields are prefixed by "cl_"; indicates cell line?
"""

from collections import namedtuple, Counter
import functools
import itertools
import re
import sqlite3
import pkg_resources

import networkx as nx
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib_venn import venn3

import wrenlab.ml
import wrenlab.data.geo
import wrenlab.ontology
from wrenlab.text.ahocorasick import MixedCaseSensitivityTrie
from wrenlab.util import memoize, as_float
import wrenlab.text.ahocorasick

with pkg_resources.resource_stream("wrenlab.ncbi.geo", "labels.yml") as h:
    SCHEMA = yaml.safe_load(h)

def connect():
    url = "https://gbnci-abcc.ncifcrf.gov/geo/GEOmetadb.sqlite.gz"
    path = str(wrenlab.util.download(url, decompress = True))
    return sqlite3.connect(path)

@functools.lru_cache()
@memoize
def experiment_sample_map():
    cx = connect()
    query = """
        SELECT  
            CAST(SUBSTR(gse, 4) AS INT) AS 'ExperimentID',
            CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID'
        FROM gse_gsm;
        """
    o = pd.read_sql(query, cx)
    counts = o.ExperimentID.value_counts()
    ix = list(sorted(o.index, 
        key=lambda i: counts.loc[o.ExperimentID.iloc[i]], 
        reverse=True))
    return o.loc[ix,:].drop_duplicates(subset=["SampleID"])

@functools.lru_cache()
@memoize
def sample_text(taxon_id=None):
    if taxon_id is not None:
        o = sample_text()
        return o.ix[o.TaxonID == taxon_id,:]

    cx = connect()

    query = """
            SELECT
                organism_ch1 AS 'TaxonID',
                CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID',
                CAST(SUBSTR(gpl, 4) AS INT) AS 'PlatformID',
                NULL AS 'ExperimentID',
                molecule_ch1 AS Molecule,
                title as Title,
                description as Description,
                characteristics_ch1 as Characteristics,
                source_name_ch1 as SourceName
            FROM gsm
            WHERE
                channel_count == 1;
            """

    taxon_names = wrenlab.ncbi.taxonomy.names()
    taxon_names = dict(zip(taxon_names["Scientific Name"], taxon_names.index))
    df = pd.read_sql(query, cx)
    df.TaxonID = [taxon_names.get(species) for species in df.TaxonID]
    df.SampleID = df.SampleID.astype(int)

    M = experiment_sample_map()
    M = dict(zip(M.SampleID, M.ExperimentID))
    df.ExperimentID = [M.get(id) for id in df.SampleID]

    TEXT_FIELDS = ["Title", "Description", "Characteristics", "SourceName"]

    o = df\
            .dropna(subset=["TaxonID", "PlatformID", "ExperimentID"])\
            .dropna(subset=TEXT_FIELDS, how="all")\
            .set_index(["SampleID"])
    for field in TEXT_FIELDS:
        o[field] = o[field].fillna("")
    o.TaxonID = o.TaxonID.astype(int)
    o.ExperimentID = o.ExperimentID.astype(int)
    return o

Record = namedtuple("Record", 
        ["SampleID", "TaxonID", "PlatformID", "ExperimentID", "Molecule",
            "Title", "Description", "Characteristics", "SourceName"])

@functools.lru_cache()
def sample_text_records(taxon_id=None):
    df = sample_text(taxon_id=taxon_id)
    return [Record(*r) for r in df.to_records(index=True)]

def matcher(obj, group, case_sensitive=False, postprocess=None):
    def fn(s):
        if not case_sensitive:
            s = s.lower()
        m = obj.search(s)
        if m is not None:
            o = m.group(group)
            if postprocess is not None:
                try:
                    return postprocess(o)
                except:
                    return
    return fn

def trie_matcher(m):
    trie = wrenlab.text.ahocorasick.Trie(case_sensitive=False)
    for k,vs in m.items():
        for v in vs:
            trie.add(v, key=k)
    trie.build()

    def fn(s):
        matches = trie.search(s)
        if len(matches) != 1:
            return
        else:
            return matches[0].key
    return fn

ORDER = {
        "gender": ["Characteristics", "Description", "SourceName", "Title"],
        "age": ["Characteristics", "Description", "SourceName", "Title"],
        "disease": ["Characteristics", "Description", "SourceName", "Title"],
        "tissue": ["SourceName", "Title", "Characteristics", "Description"]
}

def apply_product(*args):
    fks = list(itertools.product(*args))
    def wrap(r):
        for fn, k in fks:
            o = fn(getattr(r,k))
            if o is not None:
                return o
    return wrap

def lstrip_phrase(s, q):
    if q.startswith(s):
        q = q[len(s):]
    return q

def rstrip_phrase(s, q):
    if q.endswith(s):
        q = q[:-len(s)]
    return q

@functools.lru_cache()
@memoize
def characteristics(taxon_id):
    def parse_characteristics(rs):
        def fn(ch):
            o = {}
            for kv in ch.split(";\t"):
                kv = kv.split(":", 1)
                if len(kv) != 2:
                    continue
                k,v = kv
                o[k.strip()] = v.strip()
            return o

        KEY_MAP = {
                "tissue type": "tissue",
                "disease state": "disease",
                "disease status": "disease",
                "phenotype": "disease",
                "condition": "disease",
                "age (years)": "age",
                "age (yrs)": "age",
                "sex": "gender",
                "cell type": "cell line",
                "cell_type": "cell line",
                "ethnicity": "race"
        }

        o = []
        for id,ch in zip(rs.index, rs.Characteristics):
            if ch is None:
                continue
            kvs = fn(ch)
            for k,v in kvs.items():
                k = k.lower()
                k = lstrip_phrase("cl_", k)
                k = lstrip_phrase("donor_", k)
                if k in KEY_MAP:
                    k = KEY_MAP[k]
                o.append((id, k, v))
        return pd.DataFrame.from_records(o, columns=["SampleID", "Key", "Value"])

    rs = sample_text(taxon_id=taxon_id)
    return parse_characteristics(rs)\
            .drop_duplicates(subset=["SampleID", "Key"])

# gender, age: ch, desc, sn, title
# tissue: sn, title, ch, desc

GENDER = matcher(re.compile("(gender|sex)(.+?)(?P<gender>[MFmf])", flags=re.DOTALL), "gender",
        postprocess=str.upper)
AGE = matcher(re.compile("age:\ ?(?P<age>([0-9]+(\.[0-9]+)?))", flags=re.I), "age",
        postprocess=float)
AGE2 = matcher(re.compile("(?P<age>([0-9]+(\.[0-9]+)?)) y(ea)?r", flags=re.I), "age",
        postprocess=float)
NUMBER = matcher(re.compile("(?P<number>([0-9]+(\.[0-9]+)?))"), "number",
        postprocess=float)
DISEASE_STATE = matcher(re.compile(
    "(?P<disease>(tumor|tumour|cancer|sarcoma|glioma|leukem|mesothelioma|metastasis|carcinoma|lymphoma|blastoma|nsclc|cll|ptcl))",
    flags=re.DOTALL | re.I), "disease", postprocess=lambda x: 1)


def _AGE_WITH_UNIT():
    multiplier = {}
    for base,m in SCHEMA["age"].items():
        v = m["value"] * 12
        for s in m["synonyms"]:
            multiplier[s] = v
        for a in m["abbreviations"]:
            multiplier[a] = v
    pattern = re.compile("(?P<age>([0-9]+(\.[0-9]+)?)) (?P<unit>{})\b"\
            .format("|".join(list(multiplier.keys()))), flags=re.I)

    def fn(text):
        m = pattern.search(text) 
        if m is None:
            return
        age = float(m.group("age"))
        unit = m.group("unit")
        return age * multiplier[unit]

    return fn

AGE_WITH_UNIT = _AGE_WITH_UNIT()

GST = trie_matcher(
    {
        "F":[
            "female", "woman", "girl", "ovar", 
            "mammary", "uter", "menstru", "placenta"
        ],
        "M": ["prostate", "testes", "testic", "foreskin", "male", "boy", " man "]
    })

EXCLUDE_TISSUES = [ 
        "ear", "hip", "sputum", "pith", "neck", "tail", "stem",
        "wing", "stoma", "serum", "node", "fin", "finger", "primary cell",
        "root", "adult", "juvenile", "arm", "blast cell", "primary cell"
] 


class TissueExtractor(object):
    def __init__(self):
        BTO = wrenlab.ontology.fetch("BTO")
        name_id = BTO.name_map(reverse=True)

        self._term_ids = list(BTO.terms.index)
        self._trie = BTO.trie(
                exclude_keys=[name_id[n] for n in EXCLUDE_TISSUES],
                allow_overlaps=False, 
                boundary_characters=" ;.\t\n")

        self._fields = Record._fields[4:]
        self._ix = pd.MultiIndex.from_tuples(
                list(itertools.product(
                    BTO.terms.index,
                    self._fields)),
                names=["TermID", "Field"])

    def _search1(self, text):
        c = Counter(m.key for m in self._trie.search(text))
        if len(c) == 0:
            return 0
        else:
            return c.most_common()[0][0]

    def _search(self, r):
        """
        o = pd.Series(index=self._ix, dtype=int).fillna(0)
        for field in self._fields:
            text = getattr(r,field)
            o += self._search1(text)
        # return o.to_sparse(fill_value=0)
        return o
        """
        c = Counter()
        for field in self._fields:
            text = getattr(r, field)
            for m in self._trie.search(text):
                c[m.key] += 1
        if len(c) == 0:
            return 0
        return c.most_common()[0][0]

    def __call__(self, r):
        if isinstance(r, str):
            return self._search1(r)
        else:
            return self._search(r)
        """
        counts = self._search(r)
        if counts.sum() == 0:
            return 0
        return int(counts.groupby(level=0).sum().sort_values().index[-1])
        """

# BEGIN METHOD 1
_extract_gender = apply_product([GENDER, GST], ORDER["gender"])
_extract_age = apply_product([AGE_WITH_UNIT, AGE], ORDER["age"])
_extract_tissue = TissueExtractor()
_extract_disease = apply_product([DISEASE_STATE], ORDER["disease"])

@memoize
def _extract(taxon_id, what):
    rs = sample_text_records(taxon_id=taxon_id)
    fn = globals()["_extract_{}".format(what)]
    o = pd.Series(list(map(fn, rs)), 
            index=[r.SampleID for r in rs])\
                    .dropna()
    o.index.name = "SampleID"
    o.name = {
        "gender": "Gender",
        "age": "Age",
        "tissue": "TissueID",
        "disease": "DiseaseState"
    }[what]
    return o

def M1(taxon_id):
    return [
        _extract(taxon_id, "age"),
        _extract(taxon_id, "gender"),
        _extract(taxon_id, "tissue"),
        _extract(taxon_id, "disease")
    ]

# END METHOD 1

# BEGIN METHOD 2 (high P low R)

@memoize
def extract_all_age(taxon_id):
    # TODO: deal with age ranges (currently will take 1st)
    # TODO: deal with units
    ch = characteristics(taxon_id)
    ch = ch.ix[ch.Key == "age",:]

    def transform(x):
        x = rstrip_phrase(" old", x)
        return x

    return pd.Series([NUMBER(transform(x)) for x in ch.Value], index=ch.SampleID)\
            .dropna()

@memoize
def extract_all_gender(taxon_id):
    def fn(x):
        x = x.lower()
        if x in ("m", "male"):
            return "M"
        elif x in ("f", "female"):
            return "F"

    ch = characteristics(taxon_id)
    ch = ch.ix[ch.Key == "gender",:]
    return pd.Series([fn(x) for x in ch.Value], index=ch.SampleID)\
            .dropna()

@memoize
def extract_all_tissue(taxon_id):
    """
    """
    ch = characteristics(taxon_id)
    ch = ch.ix[ch.Key.isin(["tissue"]),:]
    return pd.Series([_extract_tissue(x) for x in ch.Value], index=ch.SampleID)\
            .dropna()

def extract_all_disease(taxon_id):
    ch = characteristics(taxon_id)
    return pd.Series([])

def M2(taxon_id):
    return [
        extract_all_age(taxon_id),
        extract_all_gender(taxon_id),
        extract_all_tissue(taxon_id),
        extract_all_disease(taxon_id)
    ]


##############
# Entry points
##############

# NB: order from most to least priority
METHODS = [2,1]
FIELDS = ["Age", "Gender", "TissueID", "DiseaseState"]

@memoize
def extract_all(taxon_id, method=None):
    if method is None:
        method = "_ALL"
    df1 = sample_text()
    df1 = df1.ix[df1.TaxonID == taxon_id,:]
    extractions = globals()["M{}".format(method)](taxon_id)
    df2 = pd.concat(extractions,
        axis=1,
        keys=["Age", "Gender", "TissueID", "DiseaseState"])
    o = df1.loc[:,["TaxonID","PlatformID","ExperimentID","Molecule"]]\
            .join(df2, how="inner")
    o.TissueID = o.TissueID.fillna(0).astype(int)
    return o

############
# Evaluation
############

def unmatched_characteristics(taxon_id, key):
    ch = characteristics(taxon_id)
    lkey = "TissueID" if key == "tissue" else key.title()
    A = M2(taxon_id)
    if key == "tissue":
        ix = A[lkey] == 0
    else:
        ix = A[lkey].isnull() 
    ix = A.index[ix]
    return ch.ix[ch.SampleID.isin(ix) & (ch.Key == key),:]


class Evaluation(object):
    def __init__(self, taxon_id):
        assert taxon_id == 9606

        self.A = get_labels(taxon_id)
        self.T = sample_text(taxon_id=taxon_id)
        self.Ag = flatten_tissue(wrenlab.data.geo.labels("jdw"))
        self.ch = characteristics(taxon_id)

    def _binary_report(self, key):
        """
        %All and %Key are estimated percent extracted and correct 
        (based on computed precision) from:
        - "All": all records for this taxon
        - "Key": records which had an "age" key field extracted from characteristics
        """
        y = self.Ag[key.title()].dropna()
        y_hat = self.A[key.title()].loc[y.index]
        ok = ~y_hat.isnull()
        if key == "age":
            dx = (y - y_hat).abs()
            eq = dx < 1
        elif key == "gender":
            eq = y == y_hat

        P = eq[ok].mean()
        R = (eq & ok).mean()

        N = self.A[key.title()].dropna().shape[0]
        pct_all = N / self.T.shape[0]
        pct_key = N / (self.ch.Key == key).sum()

        return pd.Series([N,pct_all,pct_key,P,R], 
                index=["N", "%All", "%Key", "Precision", "Recall"])

    def age(self):
        return self._binary_report("age")

    def gender(self):
        return self._binary_report("gender")

    def _tissue(self):
        pass

    def tissue2(self):
        o = wrenlab.ontology.fetch("BTO")
        M = o.name_map()

        y = self.Ag.TissueID.dropna()
        ix = y.value_counts().index[:10]
        y = y.ix[y.isin(ix)]

        y_hat = self.A.TissueID
        y_hat = y_hat.ix[y_hat != 0]

        y = y.apply(lambda id: M[id]).astype("category")
        y_hat = y_hat.apply(lambda id: M[id]).astype("category")
        return wrenlab.ml.CategoricalResult(y, y_hat)

    def tissue_distance(self, randomize=False):
        g = wrenlab.ontology.fetch("BTO")\
                .to_graph(relations=["is_a", "part_of"])\
                .to_undirected()

        y = self.Ag.TissueID.dropna()
        y_hat = self.A.TissueID
        y_hat = y_hat.ix[y_hat != 0]
        y, y_hat = wrenlab.util.align_series(y, y_hat)

        o = []
        for ix, y_, y_hat_ in zip(y.index, y, y_hat):
            try:
                n = nx.shortest_path_length(g, y_, y_hat_)
            except nx.NetworkXNoPath:
                continue
            o.append((ix, y_, y_hat_, n))
        return pd.DataFrame.from_records(o, 
                    columns=["SampleID", "y", "y_hat", "Distance"])\
                .set_index(["SampleID"])

    def tissue(self, n=None):
        o = wrenlab.ontology.fetch("BTO", int_keys=True)
        y = self.Ag.TissueID
        #y = y.ix[y != 0]
        if n is not None:
            y = y.ix[y.isin(y.value_counts().index[:n])]
        y_hat = self.A.TissueID
        #y_hat = y_hat.ix[y_hat != 0]
        return wrenlab.ml.OntologyResult(o, y, y_hat)

    def venn(self, path=None):
        plt.clf()
        A = self.A.ix[self.A.Molecule.isin(["total RNA", "polyA RNA"]),:]
        return venn3(
                [
                    set(A.Age.dropna().index),
                    set(A.Gender.dropna().index),
                    set(A.TissueID.ix[self.A.TissueID != 0].index)
                ],
                set_labels=["Age", "Gender", "Tissue"])
        if path is not None:
            plt.savefig(path, dpi=360)


class OntologyEvaluation(wrenlab.ml.CategoricalResult):
    def __init__(self, ontology, y, y_hat):
        self._o = ontology

        y = y.dropna()
        y_hat = y_hat.ix[y.index]
        ix = self._o.terms.index
        assert y.isin(ix).all()
        assert (y.isnull() | y.isin(ix)).all()

        super(OntologyEvaluation, self).__init__(
                y.astype("category"), 
                y_hat.astype("category"))
    
    def cr(self):
        M = self._o.name_map()

        y = self.y
        ix = y.value_counts().index[:10]
        y = y.ix[y.isin(ix)]
        y_hat = self.y_hat
        y, y_hat = wrenlab.util.align_series(y, y_hat)

        y = y.apply(lambda id: M[id]).astype("category")
        y_hat = y_hat.apply(lambda id: M[id]).astype("category")
        return wrenlab.ml.CategoricalResult(y, y_hat)


    def distance(self, randomize=False):
        import random
        # NB only calculates where y_hat has a prediction

        g = self._o\
                .to_graph(relations=["is_a", "part_of"])\
                .to_undirected()

        y, y_hat = wrenlab.util.align_series(self.y, self.y_hat)

        o = []
        for ix, y_, y_hat_ in zip(y.index, y, y_hat):
            if randomize:
                y_hat_ = random.choice(self._o.terms.index)
            try:
                n = nx.shortest_path_length(g, y_, y_hat_)
            except nx.NetworkXNoPath:
                continue
            o.append((ix, y_, y_hat_, n))
        return pd.DataFrame.from_records(o, 
                columns=["SampleID", "y", "y_hat", "Distance"])\
                .set_index(["SampleID"])


def M_ALL(taxon_id):
    E = {m:extract_all(taxon_id, method=m) for m in METHODS}
    rs = {}

    # Combine results from various methods
    for field in FIELDS:
        o = {}
        for m in reversed(METHODS):
            x = E[m][field].dropna()
            if field == "TissueID":
                x = x.ix[x != 0]
            for ix,value in zip(x.index, x):
                o[ix] = value
        rs[field] = pd.Series(o)
        rs[field].name = field
    return [rs[field] for field in FIELDS]

def flatten_tissue(A):
    A.ix[A.TissueID.isin([553, 1025]), "TissueID"] = 89
    return A

def add_tissue_name(A):
    BTO = wrenlab.ontology.fetch("BTO")
    id_name = BTO.name_map()
    A["TissueName"] = [id_name[tid] for tid in A.TissueID]
    A.TissueName = A.TissueName.astype("category")
    return A

def get_labels(taxon_id, str_index=False, str_accessions=False, flatten=True):
    o = extract_all(taxon_id)
    if flatten:
        o = flatten_tissue(o)
    o = add_tissue_name(o)
    o.DiseaseState = o.DiseaseState.fillna(0).astype(int)

    if str_index is True or str_accessions is True:
        o.index = ["GSM{}".format(ix) for ix in o.index]
    if str_accessions is True:
        o.ExperimentID = ["GSE{}".format(ix) for ix in o.ExperimentID]
        o.PlatformID = ["GPL{}".format(ix) for ix in o.PlatformID]

    return o

def custom_search(taxon_id, synonyms, name=None):
    """
    Search the text for a custom set of synonyms, returning an
    augmented :class:`pandas.DataFrame` from `get_labels` containing
    an extra column for the custom label.
    """
    ST = wrenlab.ncbi.geo.label.sample_text(taxon_id)
    fn = lambda text: any(s.lower() in text.lower() for s in synonyms)
    ix = ST.Characteristics.apply(fn) \
            | ST.SourceName.apply(fn) \
            | ST.Title.apply(fn) \
            | ST.Description.apply(fn)
    experiments = set(ST["ExperimentID"].ix[ix])

    o = []
    for experiment_id in experiments:
        samples = ST.index[ST["ExperimentID"] == experiment_id]
        ix_subset = ix.loc[samples]
        n = ix_subset.sum()
        if (n > 0) and (n < len(ix_subset)):
            o.append(ix_subset)

    o = pd.concat(o)
    if name is None:
        name = synonyms[0].title()
    o = o.astype(int).to_frame()
    o.columns = [name]

    A = wrenlab.ncbi.geo.label.get_labels(taxon_id)
    o = A.join(o, how="inner")
    o.index.name = A.index.name
    return o

def get_child_IDs(parent_node_name, ontology_abrreviation):
    """
    Get the ID's of the parent node and all of it's children within an ontology. 
    
    Args:
        parent_node_name: String which is the name of the parent node, such as 'brain' in the 'BTO' (Brenda tissue ontology) ontology
        ontology_abrreviation: abbreviation of the ontology given by bioportal ontologies such as BTO for Brenda Tissue Ontology

    Returns:
        a list of IDs (ints) from the ontology

    Raises:
        --

    Example Usage:
        # Get all metadata with label extraction from GEO
        human_metadata_df = label.get_labels(9606)
        
        # Get all IDs in tissue ontology associated as 'brain'
        brain_term_ids = get_child_IDs('brain', 'BTO')

        # Get IDs from metadata asscoiated with brain and anything underneath it's ontology tree
        all_brain_sample_IDs = human_metadata_df[human_metadata_df['TissueID'].isin(brain_term_ids)]\
                                .index.values.tolist()

        # GEO accession numbers for these
        accessions = ['GSM'+str(ID) for ID in all_brain_sample_IDs]
    """
    # Get ontology
    bto = oly.fetch(ontology_abrreviation)

    # Find parent identifier in ontology
    brain_id = int(bto.name_map(reverse=True)[parent_node_name])

    # Get all identifiers underneath parent
    brain_term_ids = nx.descendants(bto.to_graph().reverse(), brain_id)
    # add parent id to the descendents
    brain_term_ids.add(brain_id)
    return brain_term_ids

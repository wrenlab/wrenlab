import contextlib
import gzip
import os.path
import sqlite3
import urllib.parse

import pandas as pd

import wrenlab.db
import wrenlab.util
from wrenlab.util import LOG

HOST = "ftp://ailun.ucsf.edu/"
DIRECTORY = "/ailun/annotation/geo/"

class AILUN(object):
    def __init__(self):
        self._cx = wrenlab.db.connect()

        if not self._is_initialized:
            self.initialize()

    @property
    def _is_initialized(self):
        c = self._cx.cursor()
        c.execute("SELECT * FROM probe_gene LIMIT 1;")
        try:
            next(c)
            return True
        except StopIteration:
            return False

    def initialize(self):
        host = urllib.parse.urlparse(HOST).netloc
        ftp = wrenlab.util.net.FTPClient(host)
        files = [urllib.parse.urljoin(HOST, p) for p in ftp.ls(DIRECTORY)]

        def probe_gene():
            for url in files:
                accession = os.path.basename(urllib.parse.urlparse(url).path).split(".")[0]
                LOG.info("AILUN : Processing {} ...".format(accession))
                if not accession.startswith("GPL"):
                    continue
                path = str(wrenlab.util.download(url))
                data = pd.read_csv(path, compression="gzip", 
                        names=["ProbeID", "GeneID", "Symbol", "Name"], sep="\t")
                data["PlatformID"] = int(accession[3:])
                data = data.loc[:,["PlatformID", "ProbeID", "GeneID"]].drop_duplicates()
                data["ProbeID"] = data["ProbeID"].astype(str)

                o = []
                for platform_id, probe_id, gene_id in data.dropna().to_records(index=False):
                    try:
                        o.append((int(platform_id), str(probe_id), int(gene_id)))
                    except:
                        break
                for item in o:
                    yield item

        self._cx.copy("probe_gene", probe_gene())

    def __call__(self, accession):
        if accession.startswith("GPL"):
            accession = int(accession[3:])


if __name__ == "__main__":
    db = AILUN()

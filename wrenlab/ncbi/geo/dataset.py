import numpy as np

import wrenlab.data
import wrenlab.R.wrap.sva
import wrenlab.statistics.meta_analysis as MA
import wrenlab.collapse
from wrenlab.dataset import Dataset
from wrenlab.util import LOG
from wrenlab.util.pandas import trim_missing

class GEODataset(Dataset):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def meta_analysis(self, formula, term=None):
        if term is None:
            assert len(formula.strip().split(" ")) == 1
            term = formula

        o = []
        for experiment_id, subset in self.has(term).groupby("ExperimentID"):
            try:
                N = subset.row_annotation.shape[0]
                if N < 10:
                    continue
                if len(subset.row_annotation[term].unique()) < 2:
                    continue
                metadata = {
                        "TaxonID": self.metadata["TaxonID"], 
                        "ExperimentID": experiment_id
                }
                summary = subset.preprocess().fit(formula).summary(term)
                o.append(MA.Contrast(summary, N, metadata=metadata))
            except Exception as e:
                print(e)
                continue
        return MA.Result(o)

class ExpressionDataset(GEODataset):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def preprocess(self, normalize=False, batch_correct=["ExperimentID"]):
        assert len(batch_correct) <= 1

        Xo = trim_missing(self.to_frame())
        #Xo = wrenlab.statistics.log_transform(Xo, base=2)
        LOG.info("Imputing missing values...")
        Xo = wrenlab.impute.mean(Xo)

        if batch_correct:
            LOG.info("Quantile normalizing...")
            Xo = wrenlab.normalize.quantile(Xo.T).T

            LOG.info("Batch correcting with ComBat...")
            key = batch_correct[0]
            batch = self.row_annotation[key]
            Xo = wrenlab.R.wrap.sva.ComBat(Xo, batch)

        if normalize or batch_correct:
            Xo = wrenlab.normalize.quantile(Xo.T).T

        return ExpressionDataset(Xo,
                row_annotation=self.row_annotation,
                metadata=self.metadata)\
                        .align()

    def collapse(self):
        LOG.info("Collapsing probes to genes...")
        X = self.to_frame()
        M = wrenlab.data.AILUN(self.metadata["PlatformID"])
        Xc = wrenlab.collapse.collapse(X, M, "max_mean")
        return ExpressionDataset(Xc,
                row_annotation=self.row_annotation,
                metadata=self.metadata)\
                        .align()


    def preprocess_old(self):
        X = self.to_frame()
        X = wrenlab.statistics.log_transform(X, base=2)
        #X = wrenlab.impute.kNN(X)
        X = wrenlab.impute.mean(X)
        X = wrenlab.normalize.quantile(X)
        # FIXME: column annotation
        M = wrenlab.data.AILUN(self.metadata["PlatformID"])
        if M:
            Xc = wrenlab.collapse.collapse(X, M, "max_mean")
        else:
            Xc = X
        return ExpressionDataset(Xc,
                row_annotation=self.row_annotation,
                metadata=self.metadata)

class MethylationDataset(GEODataset):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def preprocess(self, delta=0.001):
        X = self.to_frame()
        ix = (X.min(axis=1) >= 0) & (X.max(axis=1) <= 1)
        X = X.ix[ix,:]

        def fn(x):
            if x == 0:
                x = delta
            elif x == 1:
                x = 1 - delta
            else:
                pass
            return (x / (1 - x))

        A = self.row_annotation.copy()
        M = X.applymap(fn).apply(np.log2)
        A,M = A.align(M, axis=0, join="inner")
        M = wrenlab.impute.mean(M)
        M = wrenlab.normalize.quantile(M.T).T

        batch = A.ExperimentID
        if len(batch.unique()) > 1:
            counts = batch.value_counts()
            ix = counts.index[counts > 3]
            M = M.loc[batch.index[batch.isin(ix)],:]
            M = wrenlab.R.wrap.sva.ComBat(M, batch)

        A,M = A.align(M, axis=0, join="inner")
        return MethylationDataset(M,
                row_annotation=A,
                metadata=self.metadata)


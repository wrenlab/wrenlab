"""
Parser for MEDLINE XML files.

Example:

.. code-block:: python
   
    from wrenlab.ncbi.medline import parse

    path = "http://www.nlm.nih.gov/databases/dtd/medsamp2013.xml.gz"
    with parse(path) as h:
        for article in path:
            print(article)
"""

import contextlib
import datetime
import gzip
import locale
import math
import multiprocessing as mp
import os
import os.path
import pickle
import re
import sqlite3
import xml.etree.ElementTree as ET

from collections import namedtuple

import networkx as nx

import wrenlab.db
import wrenlab.text.nlp
import wrenlab.util
from wrenlab.util import LOG

__all__ = ["parse", "Article", "Journal"]

Article = namedtuple("Article", 
                     "id title abstract publication_date journal xref citations")
Journal = namedtuple("Journal", "id issn title")

class MedlineXMLFile(object):
    # FIXME: Date parsing will probably only work if system
    #   locale is US English

    _months = dict((i, locale.nl_langinfo(getattr(locale, "ABMON_" + str(i))))
                        for i in range(1,13))
    _non_digit_regex = re.compile(r"[^\d]+")

    def __init__(self, path):
        self._is_open = True
        self._handle = gzip.open(path, "rb")
    
    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def close(self):
        if self._is_open:
            if hasattr(self, "_handle"):
                self._handle.close()
            self._is_open = False
    
    def _text(self, xpath):
        try:
            it = self._current_element.findall(xpath)
            return " ".join([x.text for x in it])
        except IndexError:
            return None

    def _strip_non_digit(self, text):
        return self._non_digit_regex.sub('', text)

    def _parse_citation(self):
        # Parse Article information
        pmid = int(self._text(".//PMID"))
        title = self._text(".//Article/ArticleTitle")
        abstract = self._text(".//Article/Abstract/AbstractText")

        publication_date = None
        year = self._text(".//Article/Journal/JournalIssue/PubDate/Year")
        if year:
            month = self._text(".//Article/Journal/JournalIssue/PubDate/Month")
            month = self._months.get(month, "01")
            day = self._text(".//Article/Journal/JournalIssue/PubDate/Day") or "01"
            publication_date = datetime.date(int(year), int(month), int(day))

        # Find PMC ID (if any)
        xref = {}
        pmc_id = self._text(".//OtherID[@Source='NLM']")
        if pmc_id is not None:
            if pmc_id.startswith("PMC"):
                xref["PMC"] = pmc_id

        # Parse Journal information
        journal_id = self._text(".//MedlineJournalInfo/NlmUniqueID")
        journal_id = int(self._strip_non_digit(journal_id))
        journal_issn = self._text(".//MedlineJournalInfo/ISSNLinking")
        journal_name = self._text(".//MedlineJournalInfo/MedlineTA")
        journal = Journal(journal_id, journal_issn, journal_name)

        # Parse Citation information
        # TODO: reftype = "cites"
        citations = set()
        xp = ".//CommentsCorrectionsList/CommentsCorrections"
        for e in self._current_element.findall(xp):
            for c in e.findall("PMID"):
                citations.add(int(c.text))

        return Article(pmid, title, abstract, publication_date, journal, xref, citations)

    def __iter__(self):
        for event, element in ET.iterparse(self._handle):
            if event == "end" and element.tag == "MedlineCitation":
                self._current_element = element
                yield self._parse_citation()

def parse_file(path_or_handle, lazy=False):
    with contextlib.closing(MedlineXMLFile(path_or_handle)) as parser:
        if lazy is True:
            return parser
        else:
            return list(parser)

def parse_directory(medline_dir, cores=16):
    pool = mp.Pool(cores)
    paths = [os.path.join(medline_dir, p) 
            for p in os.listdir(medline_dir) if p.endswith(".xml.gz")]
    seen = set()
    for articles in pool.imap_unordered(parse_file, paths):
        for article in articles:
            if article.id in seen:
                continue
            seen.add(article.id)
            yield article

def parse(x, **kwargs):
    if os.path.isdir(x):
        yield from parse_directory(x, **kwargs)
    else:
        yield from parse_file(x, lazy=True)

##########
# Database
##########

class MEDLINE(object):
    def __init__(self, path="~/.cache/wrenlab/MEDLINE.db"):
        self._path = os.path.expanduser(path)
        create = not os.path.exists(self._path)
        self._cx = sqlite3.connect(self._path)

        if create:
            schema = wrenlab.util.sql_script("medline")
            c = self._cx.cursor()
            c.executescript(schema)
            self._cx.commit()

    def __del__(self):
        self._cx.close()

    def _create_indices(self):
        schema = wrenlab.util.sql_script("medline.index")
        c = self._cx.cursor()
        c.executescript(schema)
        self._cx.commit()

    @property
    def articles(self):
        with contextlib.closing(self._cx.cursor()) as c:
            c.execute("""
                SELECT id, title, abstract, publication_date, NULL, NULL, NULL
                FROM document;""")
            for row in c:
                yield Article(*row)

    @property
    def is_initialized(self):
        with self.cursor() as c:
            c = self._cx.cursor()
            c.execute("SELECT COUNT(*) FROM journal;")
            n = next(c)[0]
            if n == 0:
                return False

            #c.execute("SELECT pagerank FROM document WHERE pagerank IS NOT NULL;")
            #try:
            #    _ = next(c)
            #    return True
            #except StopIteration:
            #    return False

    def cursor(self, commit=False):
        @contextlib.contextmanager
        def ctxmgr():
            c = self._cx.cursor()
            yield c
            if commit:
                self._cx.commit()
            c.close()
        return ctxmgr()

    def _initialize_import_data(self, path, **kwargs):
        # Stage 1: Import journal
        def journal():
            it = parse(path, **kwargs)
            seen = set()
            for article in it:
                if article.journal is None:
                    continue
                j = article.journal
                if not j.id in seen:
                    yield j.id, j.title, j.issn
                    seen.add(j.id)

        LOG.info("Populating journal table ...")
        #self._cx.copy("journal", journal(), columns=["id", "title", "ISSN"])

        with self.cursor(True) as c:
            c.executemany("INSERT INTO journal VALUES (?,?,?);", journal())

        # Stage 2: Import abstracts
        seen_document = set()
        def document():
            it = parse(path)
            for a in it:
                if a.id not in {22944681}:
                    seen_document.add(a.id)
                    yield (a.id, a.publication_date, a.title, a.abstract, a.journal.id)
        
        LOG.info("Populating article table ...")
        #self._cx.copy("document", document(), 
        #        columns=["id", "publication_date", "title", "abstract", "journal_id"])

        with self.cursor(True) as c:
            c.executemany("""
                INSERT INTO document 
                    (id,publication_date,title,abstract,journal_id)
                VALUES 
                (?,?,?,?,?);""", document())

        # Stage 3: Import citations
        def citation():
            it = parse(path)
            for a in it:
                for c in a.citations:
                    if a.id in seen_document and c in seen_document:
                        yield (a.id, c)

        LOG.info("Populating citation table ...")
        with self.cursor(True) as c:
            c.executemany("""INSERT INTO citation VALUES (?,?)""", citation())

        # Stage 4: Import xrefs
        LOG.info("Populating xref table ...")
        def xref():
            it = parse(path)
            for a in it:
                for k,v in a.xref.items():
                    yield a.id, k, v

        #with self.cursor(True):
        #    c.executemany("INSERT INTO xref VALUES (?,?,?)""", xref())

    def _section_map(self):
        with self.cursor() as c:
            c.execute("SELECT COUNT(*) FROM section;")
            if next(c)[0] > 0:
                return
            else:
                c.executemany("INSERT INTO section VALUES (%s,%s);",
                        [(0,"title"), (1, "abstract")])
                self._cx.commit()
            c.execute("SELECT name,id FROM section;")
            return dict(c)

    def _initialize_citation_count(self):
        LOG.info("Setting citation counts ...")
        with self.cursor(True) as c:
            c.execute("""
                SELECT COUNT(source_id), target_id
                FROM citation 
                GROUP BY target_id
                ORDER BY COUNT(source_id) DESC;""")
            counts = list(c)
            c.executemany("""
                UPDATE document
                SET n_citations=?
                WHERE id=?;""", counts)

    def _initialize_pagerank(self):
        LOG.info("Computing PageRank ...")
        g = self.citation_graph()
        pr = [(math.log10(x),id) for id,x in nx.pagerank_scipy(g, tol=1e-10).items()]

        with self.cursor(True) as c:
            c.executemany("""
                UPDATE document
                SET pagerank=?
                WHERE id=?;""", pr)

    def _initialize_populate_sentence(self):
        def sentence():
            section_map = self._section_map()

            fn = wrenlab.text.nlp.punkt_sentence_tokenizer()
            c = self._cx.cursor()
            c.execute("SELECT id, title, abstract FROM document;")
            for id, title, abstract in c:
                if title is not None:
                    for i,s in enumerate(fn(title)):
                        yield id, section_map["title"], i, s
                if abstract is not None:
                    for i,s in enumerate(fn(abstract)):
                        yield id, section_map["abstract"], i, s

        LOG.info("Populating sentence table ...")
        c = self._cx.cursor()
        c.executemany("INSERT INTO sentence VALUES (%s,%s,%s,%s);", sentence())
        self._cx.commit()

    def initialize(self, path, cores=16):
        """
        Import MEDLINE XML data into the local SQLite database.

        Arguments
        ---------
        path : str
            Path to the directory containing MEDLINE XML files.
        """
        if self.is_initialized:
            return

        self._initialize_import_data(path, cores=cores)
        self._initialize_citation_count()
        #self._initialize_pagerank()
        self._initialize_populate_sentence()
        self._create_indices()

    def citation_graph(self):
        with contextlib.closing(self._cx.cursor()) as c:
            c.execute("SELECT source_id, target_id FROM citation;")
            g = nx.DiGraph()
            g.add_edges_from(c)
        return g

#######################
#Command-line interface
#######################

import click

@click.group()
def cli():
    pass

@cli.command()
@click.argument("medline_dir")
def initialize(medline_dir):
    """
    Import data from MEDLINE XML into a local SQLite database.
    """
    db = MEDLINE()
    db.initialize(medline_dir)

@cli.command("parse")
@click.argument("medline_dir")
def cli_parse(medline_dir):
    """
    Parse MEDLINE XML file or directory and output in TSV.
    """
    for article in parse(medline_dir):
        if article.title:
            title = article.title
            text = "\t".join([title, article.abstract or ""])
            text = text.replace("\t", " ").replace("\n", " ")
            print(article.id, text, sep="\t")

if __name__ == "__main__":
    cli()
    #db._initialize_populate_sentence()

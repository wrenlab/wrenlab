import wrenlab.util.remote_table

def mapping(from_taxon_id, to_taxon_id):
    M = wrenlab.util.remote_table.remote_table("NCBI.homologene")
    M_from = M.ix[M.TaxonID == from_taxon_id,:]
    M_to = M.ix[M.TaxonID == to_taxon_id,:]
    o = M_from.join(M_to, on="HID", lsuffix=".F", rsuffix=".T")\
            .loc[:,["GeneID.F", "GeneID.T"]]\
            .dropna()\
            .astype(int)\
            .drop_duplicates()
    o.columns = [from_taxon_id, to_taxon_id]
    o.columns.name = "Taxon ID"
    return o

import multiprocessing as mp
import os
import os.path
import subprocess
import tempfile
import urllib.parse

import biom
import pandas as pd

import wrenlab.util
import wrenlab.util.aspera
import wrenlab.ext
from wrenlab.util import cwd, download, LOG

def _metagenome(args):
    root, accession = args
    LOG.info("Running taxonomy analysis for {} ...".format(accession))
    try:
        db = SRA(root)
        o = db.taxonomy_analysis(accession)
        LOG.info("Taxonomy analysis completed for {}".format(accession))
        return o
    except subprocess.CalledProcessError:
        LOG.error("Taxonomy analysis failed for {}".format(accession))

class SRA(object):
    METADB_URL = "http://gbnci.abcc.ncifcrf.gov/backup/SRAmetadb.sqlite.gz"

    def __init__(self, root):
        self._root = root
        self._ascp = None
        self._metadb = wrenlab.util.SQLiteDB(str(download(self.METADB_URL, decompress=True)))
        self._qiime = None

        self._dirs = {
                "reads": os.path.join(self._root, "reads"),
                "taxonomy": os.path.join(self._root, "taxonomy")
        }
        for path in self._dirs.values():
            os.makedirs(path, exist_ok=True)

    def metagenome(self, spots_range=(10000,100000)):
        q = """
            SELECT run_accession 
                FROM sra
                WHERE 
                    library_construction_protocol LIKE '%16S%'
                AND
                    spots >= {spots_range[0]}
                AND
                    spots <= {spots_range[1]}
                ORDER BY run_accession;""".format(spots_range=spots_range)
        pool = mp.Pool(16)
        rs = self._metadb.query(q)
        for accession in rs:
            self.download_run(accession)
        LOG.info("Starting taxonomy analysis for {} accessions...".format(len(rs)))
        jobs = ((self._root, accession) for accession in rs)
        o = [r for r in pool.map(_metagenome, jobs) if r is not None]
        return pd.concat(o, axis=1)

    def download_run(self, accession):
        if self._ascp is None:
            self._ascp = wrenlab.util.aspera.Client(host="ftp-trace.ncbi.nlm.nih.gov")
        uri = "/".join(["/sra/sra-instant/reads/ByRun/sra", 
            accession[:3], accession[:6], accession, accession]) + ".sra"
        path = os.path.join(self._dirs["reads"], accession + ".sra")
        if os.path.exists(path):
            return

        try:
            LOG.info("Downloading SRA accession: {}".format(accession))
            self._ascp.download(uri, path)
            with cwd(self._dirs["reads"]):
                subprocess.check_call(["fastq-dump", "--split-3", accession + ".sra"], 
                        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except wrenlab.util.aspera.Error:
            return

    def taxonomy_analysis(self, accession):
        # FIXME: add other preprocessing steps from https://www.ebi.ac.uk/metagenomics/pipelines/2.0
        o = os.path.join(self._dirs["taxonomy"], accession + ".biom")
        if not os.path.exists(o):
            if self._qiime is None:
                self._qiime = wrenlab.ext.QIIME()

            fastq_path = os.path.join(self._dirs["reads"], accession + ".fastq")
            # N.B. this will create an empty file for failures so they aren't re-run
            with open(o, mode="wt") as h:
                if not os.path.exists(fastq_path):
                    return
                X = self._qiime.run(fastq_path, fastq=True)
                X.to_json("wrenlab", direct_io=h)
        else:
            if os.stat(o).st_size == 0:
                return
            X = biom.load_table(o)
        o = pd.Series(X.sum(axis="observation"), index=X._observation_ids)
        o.name = accession
        return o

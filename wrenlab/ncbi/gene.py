from collections import namedtuple
import os.path
import urllib.request
import shelve

import yaml
import pandas as pd
import numpy as np

from wrenlab.util.remote_table import remote_table
from wrenlab.util import memoize, download, KVStore, LOG

def gene_taxon():
    if not KVStore.exists("gene_taxon"):
        LOG.debug("Initializing KVStore for gene_taxon ...")
        tbl = remote_table("NCBI.gene.info")
        store = KVStore("gene_taxon", writeback=True)
        for taxon_id, gene_id in tbl.index.values:
            store[int(gene_id)] = int(taxon_id)
        store.sync()
        store.close()
    return KVStore("gene_taxon")

@memoize
def info(taxon_id):
    assert isinstance(taxon_id, int)
    genes = remote_table("NCBI.gene.info")
    o = genes.loc[taxon_id,:]
    o.index.name = "GeneID"
    return o

@memoize
def _synonyms():
    tbl = remote_table("NCBI.gene.synonym")
    o = []
    for (taxon_id, gene_id, synonyms, designations) in tbl.to_records(index=False):
        text = "|".join([field for field in [synonyms, designations] if
            (field is not None) and (field == field)])
        for synonym in text.split("|"):
            synonym = synonym.strip()
            if synonym:
                o.append((taxon_id, gene_id, synonym))
    o = pd.DataFrame.from_records(o, columns=["TaxonID", "GeneID", "Synonym"])
    return o.drop_duplicates()

@memoize
def synonyms(taxon_id):
    tbl = _synonyms()
    return tbl.loc[tbl["TaxonID"] == taxon_id,:].drop("TaxonID", axis=1)

@memoize
def accession(taxon_id):
    assert isinstance(taxon_id, int)
    return remote_table("NCBI.gene.accession")\
            .loc[taxon_id,:]\
            .dropna(how="all")

"""
def as_ontology(taxon_id):
    terms = info(taxon_id).loc[:,["Name"]]
    terms.index.name = "TermID"
    s = synonyms(taxon_id)
    s.columns = ["TermID", "Synonym"]
    return wrenlab.ontology.Ontology(
"""

@memoize
def ensembl(taxon_id):
    assert isinstance(taxon_id, int)
    o = remote_table("NCBI.gene.ensembl")
    return o.loc[o["TaxonID"] == taxon_id,:]\
            .drop("TaxonID", axis=1)

def unigene():
    return remote_table("NCBI.gene.unigene")

Gene = namedtuple("Gene", "id,symbol,name,chromosome")

def by_id(gene_id):
    m = gene_taxon()
    taxon_id = m[gene_id]
    df = info(taxon_id)
    symbol = df.loc[gene_id, "Symbol"]
    name = df.loc[gene_id, "Name"]
    chromosome = df.loc[gene_id, "Chromosome"]
    return Gene(gene_id, symbol, name, chromosome)

def refseq(taxon_id):
    return remote_table("NCBI.gene.refseq")\
            .loc[taxon_id,:]\
            .dropna(how="all")

####################################
# Gene/exon/transcript/promoter loci
####################################

import wrenlab.ensembl
from pyensembl.ensembl_release_versions import MAX_ENSEMBL_RELEASE

@memoize
def gene_loci(taxon_id, ensembl_version=MAX_ENSEMBL_RELEASE):
    L = wrenlab.ensembl.loci(taxon_id, type="gene")
    E = ensembl(9606)
    L = L.merge(E, left_index=True, right_on="Ensembl Gene ID", how="inner")

    def choose(df):
        df = df.drop_duplicates()
        df["length"] = (df.end - df.start).abs()
        df = df.sort_values("length", ascending=False)
        return df.loc[:,["contig", "start", "end", "strand"]].iloc[0,:]
    return L.groupby("GeneID").apply(choose)

def promoter_loci(taxon_id, upstream=3000, downstream=500, 
        ensembl_version=MAX_ENSEMBL_RELEASE):
    """
    """
    L = gene_loci(taxon_id, ensembl_version=ensembl_version)
    L = L.loc[L.strand.isin(["+","-"]),:]
    o = L.copy()

    for strand in ("+","-"):
        ix = L.strand == strand
        if ix.sum() == 0:
            continue
        start = o.start.loc[ix].copy()
        end = o.end.loc[ix].copy()
        if strand == "+":
            o.loc[ix,"start"] = (start - upstream)
            o.loc[ix,"end"] = (start + downstream)
        else:
            o.loc[ix,"start"] = (end - downstream)
            o.loc[ix,"end"] = (end + upstream)
    return o

    """
    o = []
    for gene_id, contig, start, end, strand in L.to_records(index=True):
        if strand == "+":
            p_start, p_end = (start - upstream), (start + downstream)
        elif strand == "-":
            p_start, p_end = (end - downstream), (end + upstream)
        else:
            continue
        o.append((gene_id, contig, start, end, strand))
    return pd.DataFrame.from_records(o, 
            columns=["GeneID", "contig", "start", "end", "strand"])\
                    .set_index(["GeneID"])
    """

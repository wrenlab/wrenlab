"""
Utilities for writing nicer Excel spreadsheets.
"""

import os

import pandas as pd
import openpyxl

class Writer(object):
    def __init__(self, path):
        self._path = os.path.abspath(os.path.expanduser(path))
        self._is_open = True
        self._obj = pd.ExcelWriter(path, engine="xlsxwriter")

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if type is None:
            self.save()
        else:
            os.unlink(self._path)

    def __setitem__(self, key, df):
        assert self._is_open
        df.to_excel(self._obj, sheet_name=key)

    def save(self):
        self._obj.save()
        self._obj.close()

        wb = openpyxl.load_workbook(self._path)
        for sheet_name in wb.sheetnames:
            #i = wb.sheetnames.index(sheet_name)
            sheet = wb[sheet_name]

        #ws = wb['SheetName']
        #ws['A1'] = 'A1'
        #wb.save('names.xlxs')

        self._is_open = False
        return wb

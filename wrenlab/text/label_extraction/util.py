def matcher(obj, group, case_sensitive=False, postprocess=None):
    def fn(s):
        if not case_sensitive:
            s = s.lower()
        m = obj.search(s)
        if m is not None:
            o = m.group(group)
            if postprocess is not None:
                try:
                    return postprocess(o)
                except:
                    return
    return fn



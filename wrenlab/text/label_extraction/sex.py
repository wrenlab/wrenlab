import re

from wrenlab.text.label_extraction.base import LabelExtractor
from wrenlab.text.label_extraction.util import matcher

class SexExtractor(LabelExtractor):
    MATCHERS = { 
        "P": matcher(re.compile("(gender|sex):\ ?(?P<sex>[MFmf])", flags=re.DOTALL), 
            "sex", postprocess=str.upper)
    }

    def key(self):
        return "Sex"

    def _extract_from_string(self, text):
        return self.MATCHERS["P"](text)

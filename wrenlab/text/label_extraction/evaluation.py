import metalearn

from wrenlab.util import defmulti, defmethod

def _geo_data():
    import wrenlab.ncbi.geo.label
    taxon_id = 9606
    A = wrenlab.data.geo.labels("jdw")
    A_hat = wrenlab.ncbi.geo.label.get_labels(taxon_id, flatten=False)
    return A, A_hat

@defmulti
def geo(key):
    return key

@defmethod(geo, "Gender")
def geo(key):
    A, A_hat = _geo_data()
    y = A[key].fillna("NONE")
    y = y.ix[y != "MIXED"].astype("category")
    y_hat = A_hat[key].ix[y.index].fillna("NONE").astype("category")
    return metalearn.result.MulticlassResult.from_predictions(y, y_hat)

@defmethod(geo, "Age")
def geo(key):
    A, A_hat = _geo_data()
    y = A[key].dropna()
    y_hat = A_hat[key].ix[y.index]
    return metalearn.result.RegressionResult(y, y_hat)

@defmethod(geo, "TissueID")
def geo(key):
    A, A_hat = _geo_data()
    y = A[key].dropna()
    counts = y.value_counts()
    y = y.ix[y.isin(counts.index[counts >= 10])].astype("category")
    #y = y.cat.add_categories([0])
    y_hat = A_hat[key].ix[y.index].fillna(0)
    y_hat.ix[~y_hat.isin(set(y))] = 0
    y_hat = y_hat.astype(int).astype("category")
    return metalearn.result.MulticlassResult.from_predictions(y, y_hat)

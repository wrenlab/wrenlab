import functools

import pandas as pd
import numpy as np

import wrenlab.ontology
import wrenlab.ncbi.geo
import wrenlab.ncbi.geo.metadb
import wrenlab.text.corpus.geo
import wrenlab.text.label_extraction.age
import wrenlab.text.label_extraction.sex
import wrenlab.text.label_extraction.ontology
import wrenlab.data.HAGR
from wrenlab.util import memoize

@memoize
def sample_experiment_map():
    cx = wrenlab.ncbi.geo.metadb.connect()
    query = """
        SELECT  
            CAST(SUBSTR(gse, 4) AS INT) AS 'ExperimentID',
            CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID'
        FROM gse_gsm;
        """
    o = {}
    df = pd.read_sql(query,cx)
    counts = df.groupby("ExperimentID").count()
    counts.columns = ["Count"]
    df = df.merge(counts.reset_index(), on="ExperimentID")
    return df.sort_values("Count", ascending=False)\
            .drop_duplicates(subset=["SampleID"])\
            .drop("Count", axis=1)

@memoize
def sample_platform_map():
    cx = wrenlab.ncbi.geo.metadb.connect()
    query = """
        SELECT  
            CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID',
            CAST(SUBSTR(gpl, 4) AS INT) AS 'PlatformID'
        FROM gsm;
        """
    return pd.read_sql(query, cx).drop_duplicates(["SampleID"])
    #o = {}
    #for sample_id, platform_id in pd.read_sql(query, cx).to_records(index=False):
    #    o[int(sample_id)] = int(platform_id)
    #return o

@memoize
def _annotation(taxon_id):
    corpus = wrenlab.text.corpus.geo.GEOCorpus()
    documents = lambda taxon_id: (doc for doc in corpus.samples()
            if doc.metadata.get("TaxonID") == taxon_id)
    extractors = {
        "Age": wrenlab.text.label_extraction.age.AgeExtractor(),
        "Sex": wrenlab.text.label_extraction.sex.SexExtractor(),
        "TissueID": wrenlab.text.label_extraction.ontology.TissueExtractor(),
        #"DiseaseID": wrenlab.text.label_extraction.ontology.DiseaseExtractor(),
    }

    o = {}
    for k, ex in extractors.items():
        o[k] = ex.extract_all(documents(taxon_id))
    o = pd.DataFrame(o)
    o.index.name = "SampleID"
    for k in extractors:
        if "Age" == k:
            continue
        elif "Sex" == k:
            o[k] = [x if x in ("M","F") else "NONE" for x in o[k]]
            o[k] = o[k].astype("category")
        else:
            o[k] = [str(int(x)) if str(x) != "nan" else "NONE" for x in o[k]]
            o[k] = o[k].astype("category")
    return o

def annotation(taxon_id, str_index=False):
    o = _annotation(taxon_id)
    o = o.merge(sample_experiment_map(), how="inner", 
                left_index=True, right_on="SampleID")\
                    .set_index(["SampleID"])\
            .merge(sample_platform_map(), left_index=True, 
                    right_on="SampleID")\
            .set_index(["SampleID"])

    # Bound ages within (maturity, maximum lifespan)
    x = wrenlab.data.HAGR.AnAge().loc[taxon_id,:]
    minimum_age = x.loc[["FemaleMaturity","MaleMaturity"]].mean()
    maximum_age = x.loc["MaximumLongevity"]
    o.Age = [v if ((v >= minimum_age) and (v <= maximum_age)) else np.nan for v in o.Age]

    o = o.loc[:,["PlatformID", "ExperimentID", 
        "Age", "Sex", "TissueID"]]
    o.PlatformID = o.PlatformID.astype("category")
    o.ExperimentID = o.ExperimentID.astype("category")
    M_tissue = wrenlab.ontology.fetch("BTO").name_map()
    #M_disease = wrenlab.ontology.fetch("DOID").name_map()
    def as_int(x):
        try:
            return int(x)
        except:
            pass
    o["TissueName"] = [M_tissue.get(as_int(x)) for x in \
            o.TissueID.cat.categories[o.TissueID.cat.codes]]
    #o["DiseaseName"] = [M_disease.get(as_int(x)) for x in \
    #        o.DiseaseID.cat.categories[o.DiseaseID.cat.codes]]
    o["Sex"] = [{"M": 1.0, "F": 0.0, "NONE": np.nan}[x] for x in o["Sex"]]

    PBMC = ["peripheral blood mononuclear cell", "peripheral blood"]
    for tissue_name in PBMC:
        ix = o.TissueName == tissue_name
        #o.TissueName.loc[ix] = "blood"
        #o.TissueID.loc[ix] = "89"
        o.loc[ix,"TissueName"] = "blood"
        o.loc[ix,"TissueID"] = "89"

    if str_index is True:
        o.index = ["GSM{}".format(ix) for ix in o.index]
    return o

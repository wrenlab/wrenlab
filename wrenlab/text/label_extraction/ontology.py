from collections import Counter

import wrenlab.ontology
from wrenlab.text.label_extraction.base import LabelExtractor
from wrenlab.text.corpus import Document

class OntologyExtractor(LabelExtractor):
    def __init__(self, ontology):
        if isinstance(ontology, str):
            ontology = wrenlab.ontology.fetch(ontology)
        self._ontology = o = ontology

        name_id = o.name_map(reverse=True)

        self._term_ids = list(o.terms.index)
        self._trie = o.trie(
                #exclude_keys=[name_id[n] for n in EXCLUDE_TISSUES],
                allow_overlaps=False, 
                boundary_characters=" ;.\t\n")

    def _extract_from_string(self, text: str):
        c = Counter(m.key for m in self._trie.search(text))
        if len(c) == 0:
            return 0
        else:
            return c.most_common()[0][0]

    def _extract_from_document(self, doc: Document):
        try:
            ch = doc.characteristics()
            if "tissue" in ch:
                ms = self._trie.search(ch["tissue"])
                if ms:
                    return Counter(m.key for m in ms).most_common()[0][0]
        except AttributeError:
            pass

        c = Counter()
        for k,v in doc.text.items():
            for m in self._trie.search(v):
                c[m.key] += 1
        if len(c) == 0:
            return
        return c.most_common()[0][0]

class TissueExtractor(OntologyExtractor):
    def __init__(self):
        super().__init__("BTO")

    def key(self):
        return "TissueID"

class DiseaseExtractor(OntologyExtractor):
    def __init__(self):
        super().__init__("DOID")

    def key(self):
        return "DiseaseID"

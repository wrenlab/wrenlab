import re
import pkg_resources
import collections

import yaml

from wrenlab.text.corpus.base import Document
from wrenlab.text.label_extraction.base import LabelExtractor
from wrenlab.text.label_extraction.util import matcher 
with pkg_resources.resource_stream("wrenlab.text.label_extraction", "schema.yml") as h:
    SCHEMA = yaml.load(h, Loader=yaml.Loader)

def _AGE_WITH_UNIT():
    multiplier = {}
    for base,m in SCHEMA["age"].items():
        v = m["value"]
        for s in m["synonyms"]:
            multiplier[s.lower()] = v
        for a in m["abbreviations"]:
            multiplier[a.lower()] = v
    pattern = re.compile("(?P<age>([0-9]+(\.[0-9]+)?)) (?P<unit>{})"\
            .format("|".join(list(multiplier.keys()))), flags=re.I)

    def fn(text):
        m = pattern.search(text) 
        if m is None:
            return
        age = float(m.group("age"))
        unit = m.group("unit")
        return age * multiplier[unit.lower()]

    return fn

class AgeExtractor(LabelExtractor):
    MATCHERS = collections.OrderedDict([
        ("unit", _AGE_WITH_UNIT()),
        ("P1", matcher(re.compile("age:\ ?(?P<age>([0-9]+(\.[0-9]+)?))", flags=re.I), 
            "age", postprocess=float)),
        ("P2", matcher(re.compile("(?P<age>([0-9]+(\.[0-9]+)?)) y(ea)?r?", flags=re.I), 
            "age", postprocess=float)),
    ])

    def __init__(self):
        with pkg_resources.resource_stream("wrenlab.text.label_extraction", "schema.yml") as h:
            schema = yaml.load(h, Loader=yaml.Loader)["age"]
            self._unit_synonyms = {}
            for k,m in schema.items():
                for s in m["synonyms"]:
                    self._unit_synonyms[s] = m["value"]

    def key(self):
        return "Age"

    def _extract_from_string(self, text):
        raise NotImplementedError

    def _extract_from_document(self, doc: Document):
        # TODO: for nonhuman species (or human too):
        # - build a distribution from the explicitly unit-labeled samples
        # - extract non-unit-labeled and use MLE to infer unit
        # - NOTE will not work if heterogeneous taxon ids in input tho

        taxon_id = doc.metadata.get("TaxonID")

        ch = doc.characteristics()
        if "age" in ch and "age unit" in ch:
            unit = ch["age unit"].lower()
            if unit in self._unit_synonyms:
                try:
                    o = float(ch["age"]) * self._unit_synonyms[unit]
                    if taxon_id == 9606:
                        o /= 12
                    return o
                except ValueError:
                    pass

        if "age" in ch:
            o = self.MATCHERS["unit"](ch["age"])
            if o is not None:
                if taxon_id == 9606:
                    o /= 12
                return o

        for k,v in doc.characteristics().items():
            k = k.replace("(", " ").replace(")", " ")
            k = re.sub("\s+", " ", k)
            tokens = k.lower().split()
            # "age blah (years) : 25"
            if "age" in tokens:
                o = None
                try:
                    o = float(v)
                except ValueError:
                    continue
                for t in tokens:
                    if t in self._unit_synonyms:
                        o *= self._unit_synonyms[t]
                        if taxon_id == 9606:
                            o /= 12
                        return o
                if o is not None:
                    return o


        #if taxon_id == 9606:
        #    for k,v in doc.text.items():
        #        o = self.MATCHERS["P1"](v) or self.MATCHERS["P2"](v)
        #        if o is not None:
        #            return o

    def extract_all(self, documents):
        o = super().extract_all(documents)
        o = o.loc[o > 0]
        return o

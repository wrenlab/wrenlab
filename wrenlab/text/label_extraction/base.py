from abc import ABC, abstractmethod

import pandas as pd

from wrenlab.text.corpus.base import Document

class LabelExtractor(ABC):
    @abstractmethod
    def key(self):
        pass

    @abstractmethod
    def _extract_from_string(self, text : str):
        pass

    def _extract_from_document(self, doc : Document):
        for k,v in doc.text.items():
            o = self._extract_from_string(v)
            if o is not None:
                return o

    def extract(self, x):
        if isinstance(x, Document):
            return self._extract_from_document(x)
        elif isinstance(x, str):
            return self._extract_from_string(x)
        else:
            raise ValueError("Expected str or Document")

    def extract_all(self, documents):
        o = {}
        for doc in documents:
            v = self.extract(doc)
            if v is not None:
                o[doc.id] = v
        o = pd.Series(o)
        o.name = self.key()
        return o

from contextlib import closing as C
from collections import defaultdict
import os
import sqlite3

import tqdm
import pandas as pd
import numpy as np

import wrenlab.ontology
import wrenlab.ncbi.medline
import wrenlab.ncbi.gene
import wrenlab.util

class IRIDESCENT(object):
    #def __init__(self, path="~/.cache/wrenlab/iridescent.db"):
    def __init__(self, path="/data/iridescent.db"):
        self._MEDLINE = wrenlab.ncbi.medline.MEDLINE()
        self._path = os.path.expanduser(path)
        create = not os.path.exists(self._path)
        self._cx = sqlite3.connect(self._path)
        
        if create:
            with C(self._cx.cursor()) as c:
                c.executescript(wrenlab.util.sql_script("iridescent"))
                self._cx.commit()

    def cursor(self):
        return C(self._cx.cursor())

    def synonyms(self, term_id):
        with self.cursor() as c:
            c.execute("SELECT text FROM synonym WHERE term_id=?;", (term_id,))
            return [r[0] for r in c]

    def query(self, term_id, source=None):
        with self.cursor() as c:
            if isinstance(term_id, str):
                c.execute("SELECT term_id FROM synonym WHERE text=?;", (term_id,))
                term_id = next(iter(c))[0]
            document_count = self.metadata("document_count")
            c.execute("""SELECT citation_count FROM term WHERE id=?""", (term_id,))
            count = next(iter(c))[0]
            c.execute("""
                SELECT cocitation.t2, term.name, xref.xref_prefix, xref.xref_id, term.citation_count, cocitation.n FROM (
                    SELECT p2.term_id AS 't2', COUNT(p2.term_id) AS 'n'
                    FROM term_posting p1 
                    INNER JOIN term_posting p2 
                        ON p1.document_id=p2.document_id 
                    WHERE p1.term_id=? 
                    GROUP BY p2.term_id
                ) AS cocitation
                INNER JOIN term
                    ON cocitation.t2=term.id
                LEFT JOIN xref
                    ON term.id=xref.term_id;
                """, (term_id,))
            o = pd.DataFrame.from_records(c, columns=["TermID", "Name", "Source", "SourceID", "Frequency", "Cocitations"])
            pA = count / document_count
            pB = o["Frequency"] / document_count
            pAB = o["Cocitations"] / document_count
            o["MI"] = np.log2(pAB / (pA * pB))
            o = o.set_index("TermID")
        if source is not None:
            o = o.ix[o.Source == source,:].drop("Source", axis=1)
            o.SourceID = o.SourceID.astype(int)
            o = o.set_index("SourceID")
        return o.sort_values("MI", ascending=False)

    def _add_ontology(self, prefix, terms, synonyms):
        # prefix : str
        # terms : dict<term_id, str>
        # synonyms : dict<term_id, set<str>>
        terms = {int(k):v for k,v in terms.items()}
        synonyms = {int(k):set(v) for k,v in synonyms.items()}
        with C(self._cx.cursor()) as c:
            xref_term = {}
            for xref_id, name in terms.items():
                c.execute("INSERT INTO term (name) VALUES (?);", (name,))
                term_id = c.lastrowid
                xref_term[xref_id] = int(term_id)
            c.executemany("INSERT INTO xref (term_id, xref_prefix, xref_id) VALUES (?,?,?);",
                ((term_id, prefix, xref_id) for (xref_id, term_id) in xref_term.items()))
            c.executemany("INSERT INTO synonym (term_id, text) VALUES (?,?);",
                ((term_id, s) for term_id,ss in synonyms.items() for s in ss))
            self._cx.commit()

    def add_ontology(self, o):
        prefix = o._prefix
        terms = dict(zip(o.terms.index, o.terms.Name))
        synonyms = defaultdict(set)
        for term_id, synonym in o.synonyms.to_records(index=False):
            synonyms[term_id].add(synonym)
        self._add_ontology(prefix, terms, synonyms)

    def add_entrez_genes(self, taxon_id):
        genes = wrenlab.ncbi.gene.info(9606).dropna(subset=["Name"])
        ix = set(genes.index)
        symbols = genes["Symbol"].reset_index()
        symbols.columns = ["Gene ID", "Synonym"]
        synonyms = pd.concat([
            wrenlab.ncbi.gene.synonyms(9606),
            symbols])\
                    .drop_duplicates(subset=["Synonym"])\
                    .reset_index()\
                    .drop("index", axis=1)
        terms = dict(zip(genes.index, genes.Name))
        it = synonyms.ix[synonyms["Gene ID"].isin(ix),:].to_records(index=False)
        synonyms = defaultdict(set)
        for term_id, synonym in it:
            synonyms[term_id].add(synonym)
        self._add_ontology("EG", terms, synonyms)

    def trie(self):
        # should allow overlaps?
        trie = wrenlab.text.ahocorasick.Trie(case_sensitive=False, 
                allow_overlaps=False, boundary_characters=" .\t")
        with C(self._cx.cursor()) as c:
            c.execute("SELECT id,text FROM synonym;")
            for key,text in c:
                trie.add(text, key)
        trie.build()
        return trie

    def add_custom_term(self, name, synonyms):
        synonyms = set(synonyms)
        with self.cursor() as c:
            c.execute("INSERT INTO term (name) VALUES (?);", (name,))
            term_id = c.lastrowid
            c.executemany("INSERT INTO synonym (term_id, text) VALUES (?,?);",
                    ((term_id, text) for text in synonyms))
            self._cx.commit()

    def initialize_default(self):
        for ontology_key in ["GO", "BTO"]:
            o = wrenlab.ontology.fetch(ontology_key, int_keys=True)
            self.add_ontology(o)
        self.add_entrez_genes(9606)
        self.add_custom_term("aging", ["age", "aging", "ageing"])
        self.add_custom_term("senescence", 
                ["senescence", "senescent", "cell senescence",
                    "cellular senescence", "senescent cell"])

    def metadata(self, key):
        with self.cursor() as c:
            c.execute("""SELECT value FROM metadata WHERE key=?;""", (key,))
            return next(iter(c))[0]

    def update_postings(self):
        with self.cursor() as c:
            c.execute("DELETE FROM synonym_posting;")
            c.execute("DELETE FROM term_posting;")
            self._cx.commit()

        import itertools
        def get_postings():
            trie = self.trie()
            articles = self._MEDLINE.articles
            articles = itertools.islice(articles, 100000)
            for article in tqdm.tqdm(articles):
                document_id = article.id
                text = "\t".join([article.title or "", article.abstract or ""]).strip()
                if not text:
                    continue
                for synonym_id in set(m.key for m in trie.search(text)):
                    yield synonym_id, document_id

        with self.cursor() as c:
            c.executemany("""
                INSERT INTO synonym_posting (synonym_id, document_id) 
                VALUES (?,?);""", get_postings())
            c.execute("""
                INSERT INTO term_posting (term_id, document_id) 
                    SELECT DISTINCT s.term_id, p.document_id 
                    FROM synonym s 
                    INNER JOIN synonym_posting p 
                        ON p.synonym_id=s.id;""")
            c.execute("""
                UPDATE term 
                SET citation_count=(
                    SELECT COUNT(term_id) 
                    FROM term_posting 
                    WHERE term_posting.term_id=term.id
                );""")
            c.execute("""SELECT COUNT(*) FROM ( SELECT DISTINCT document_id FROM term_posting );""")
            document_count = next(iter(c))[0]
            c.execute("""INSERT INTO metadata VALUES ( 'document_count', ? );""", (document_count,))
            self._cx.commit()

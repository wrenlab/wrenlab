import re
import sqlite3

import wrenlab.util
import wrenlab.ncbi.taxonomy
import wrenlab.ncbi.geo.metadb

from wrenlab.text.corpus.base import *

def lstrip_phrase(s, q):
    if q.startswith(s):
        q = q[len(s):]
    return q

class GEOSampleDocument(Document):
    def __init__(self, id, text_fields, metadata_fields):
        super().__init__(id, text_fields, metadata_fields)

    def title(self):
        return self._text.get("Title")

    def source_name(self):
        return self._text.get("SourceName")

    def description(self):
        return self._text.get("Description")

    def characteristics(self):
        if "Characteristics" not in self.text:
            return {}

        def fn(ch):
            o = {}
            for kv in re.split(";[\t ]", ch):
                kv = kv.split(": ", 1)
                if len(kv) != 2:
                    continue
                k,v = kv
                o[k] = v
            return o

        KEY_MAP = {
                "tissue type": "tissue",
                "disease state": "disease",
                "disease status": "disease",
                "phenotype": "disease",
                "condition": "disease",
                "age (years)": "age",
                "age (yrs)": "age",
                "sex": "gender",
                "cell type": "cell line",
                "cell_type": "cell line",
                "ethnicity": "race"
        }

        o = {}
        kvs = fn(self.text["Characteristics"])
        for k,v in kvs.items():
            k = k.lower()
            k = lstrip_phrase("cl_", k)
            k = lstrip_phrase("donor_", k)
            if k in KEY_MAP:
                k = KEY_MAP[k]
            o[k] = v
        return o

class GEOCorpus(Corpus):
    def __init__(self):
        #url = "https://gbnci-abcc.ncifcrf.gov/geo/GEOmetadb.sqlite.gz"
        #path = str(wrenlab.util.download(url, decompress=True))
        #self._cx = sqlite3.connect(path)
        import wrenlab.ncbi.geo.metadb
        self._cx = wrenlab.ncbi.geo.metadb.connect()

        taxon_names = wrenlab.ncbi.taxonomy.names()
        self._taxon_names = dict(zip(taxon_names["Scientific Name"], 
            taxon_names.index))

    def __del__(self):
        if hasattr(self, "_cx"):
            self._cx.close()

    def _handle_sample(self, row):
        metadata = {}
        taxon_id = self._taxon_names.get(row[0])
        if taxon_id is not None:
            metadata["TaxonID"] = taxon_id
        id = row[1]
        metadata["PlatformID"] = row[2]
        text = dict(zip(
            ["Molecule", "Title", "Description", "Characteristics", "SourceName"],
            row[3:]))
        return GEOSampleDocument(id, text, metadata)

    def experiments(self):
        query = """
        SELECT 
            CAST(SUBSTR(gse, 4) AS INT) AS 'ExperimentID',
            title AS 'Title',
            summary AS 'Summary',
            overall_design AS 'OverallDesign'
        FROM gse;
        """
        c = self._cx.cursor()
        c.execute(query)
        for id, title, summary, design in c:
            text = {
                "Title": title,
                "Summary": summary,
                "OverallDesign": design
            }
            yield Document(id, text, {})

    def samples(self):
        query = """
        SELECT
            organism_ch1 AS 'TaxonID',
            CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID',
            CAST(SUBSTR(gpl, 4) AS INT) AS 'PlatformID',
            molecule_ch1 AS Molecule,
            title as Title,
            description as Description,
            characteristics_ch1 as Characteristics,
            source_name_ch1 as SourceName
        FROM gsm; """
        c = self._cx.cursor()
        c.execute(query)

        taxon_names = wrenlab.ncbi.taxonomy.names()

        for row in c:
            yield self._handle_sample(row)

    ####################
    # non-Corpus methods
    ####################

    def sample(self, sample_id):
        query = """
        SELECT
            organism_ch1 AS 'TaxonID',
            CAST(SUBSTR(gsm, 4) AS INT) AS 'SampleID',
            CAST(SUBSTR(gpl, 4) AS INT) AS 'PlatformID',
            molecule_ch1 AS Molecule,
            title as Title,
            description as Description,
            characteristics_ch1 as Characteristics,
            source_name_ch1 as SourceName
        FROM gsm
        WHERE
            channel_count == 1
            AND 
            gsm == ?
        LIMIT 1;
        """
        c = self._cx.cursor()
        if not isinstance(sample_id, str):
            sample_id = "GSM{}".format(sample_id)
        c.execute(query, (sample_id,))
        row = next(c)
        return self._handle_sample(row)

    def experiment_text(self, experiment_id):
        # FIXME: this should be like a GEOExperimentCorpus
        query = """
            SELECT title, summary
            FROM gse 
            WHERE gse == ?
        """
        c = self._cx.cursor()
        if not isinstance(experiment_id, str):
            experiment_id = "GSE{}".format(experiment_id)
        c.execute(query, (experiment_id,))
        row = next(c)
        return dict(zip(["title", "summary"], row))

    def experiment(self, experiment_id):
        query = """
            SELECT 
                CAST(SUBSTR(gsm,4) AS INT) 
            FROM gse_gsm 
            WHERE gse='GSE{}'""".format(experiment_id)
        for sample_id in self._cx.query(query):
            yield self.sample(sample_id)

    def platform(self, platform_id):
        query = """
            SELECT 
                CAST(SUBSTR(gsm,4) AS INT) 
            FROM gsm 
            WHERE gpl='GPL{}'""".format(platform_id)
        for sample_id in self._cx.query(query):
            yield self.sample(sample_id)

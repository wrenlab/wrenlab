class Document(object):
    def __init__(self, id, text_fields, metadata_fields):
        self.id = id
        self._text = {k:v for k,v in text_fields.items() if v is not None}
        self._metadata = {k:v for k,v in metadata_fields.items() if v is not None}

    @property
    def text(self):
        return self._text

    @property
    def metadata(self):
        return self._metadata

    def apply(self, fn):
        """
        Apply `fn` to each text field, returning a dictionary of
        field key -> fn(text) for each field.
        """
        o = {}
        for k,v in self.text:
            o[k] = fn(v)
        return o


class Corpus(object):
    pass

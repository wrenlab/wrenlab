import functools
import gzip

import gensim
import pandas as pd

import wrenlab.text.ahocorasick
import wrenlab.ncbi.medline

class Vocabulary(object):
    def __init__(self):
        """
        case_sensitive : dict<str, IDType>
        """
        import iridescent
        db = iridescent.Database()
        self.ci = ci

        self.trie = db.trie()
        self.ci = case_insensitive
        self.cs = case_sensitive or {}
        self.trie = wrenlab.text.ahocorasick.MixedCaseSensitivityTrie()

class Model(object):
    def __init__(self, model):
        self._model = model
        import iridescent
        self._db = iridescent.Database()

    def __getitem__(self, key):
        terms = self._db.terms
        query_id = terms.loc[terms.Name == key,:].index[0]
        query = "T{}".format(query_id)

        o = []
        for k,score in self._model.wv.most_similar(query, topn=100):
            if not k.startswith("T"):
                continue
            term_id = int(k[1:])
            name = terms.Name.loc[term_id]
            count = terms.NodeFrequency.loc[term_id]
            o.append((term_id, name, count, score))
        o = pd.DataFrame\
                .from_records(o, columns=["TermID", "Name", "Count", "Score"])\
                .set_index(["TermID"])\
                .query("Count >= 10")
        return o

    def distance(self, key):
        terms = self._db.terms
        query_id = terms.loc[terms.Name == key,:].index[0]
        query = "T{}".format(query_id)
        o = pd.Series(self._model.wv.distances(query))
        o.index = list(self._model.wv.index2word)
        o = o.loc[o.index.str.startswith("T")]
        o.index = [int(x[1:]) for x in o.index]
        return o.sort_values()

    def enrichment(self, query):
        category = "CP"
        terms = self._db.category(category)
        ix = self._terms.loc[terms,:].query("NodeFrequency > 100").index
        for term_id in ix:
            name = self._terms.Name.loc[term_id]
            r, p = scipy.stats.spearmanr()

    def train(self, corpus):
        pass

    def save(self, path):
        pass

    @staticmethod
    def load(path):
        pass

def get_trie():
    import iridescent

    db = iridescent.Database()
    return db.trie()

class MEDLINECorpus(object):
    def __init__(self, path):
        self.path = path
        self.length = 0
        self.trie = get_trie()

    def __len__(self):
        return self.length

    def __iter__(self):
        SET_LENGTH = self.length == 0
        for i,article in enumerate(wrenlab.ncbi.medline.parse(self.path, cores=4)):
            if i % 10000 == 0:
                print("Processing article: ", i)
            if article.title is None:
                continue
            text = article.title
            if article.abstract is not None:
                text += "\n" + article.abstract
            matches = self.trie.search(text)
            terms = set(m.key for m in matches)
            if len(matches) < 2:
                continue
            if SET_LENGTH is True:
                self.length += 1
            yield ["T{}".format(k) for k in terms]
            if self.length == 100000:
                return

class PostingsCorpus():
    def __init__(self, path):
        self.path = path
        self.size = 0
        for _ in self:
            self.size += 1

    def __len__(self):
        return self.size

    def __iter__(self):
        prev = None
        terms = set()

        with gzip.open(self.path, "rt") as h:
            for line in h:
                document_id, term_id = line.strip().split("\t")
                terms.add(str(term_id))
                if (prev is not None) and (document_id != prev):
                    yield list(terms)
                    terms = set()
                prev = document_id
                terms.add(term_id)



def train():
    #path = "/data/ncbi/MEDLINE/current/"
    #corpus = MEDLINECorpus(path)
    path = "data/postings.gz"
    corpus = PostingsCorpus(path)
    model = gensim.models.Word2Vec(corpus, size=1024, window=10, min_count=2, workers=10)
    model.train(corpus, total_examples=len(corpus), epochs=10)
    return model

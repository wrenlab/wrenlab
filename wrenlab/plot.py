"""
Plotting functions, mostly a wrapper layer to seaborn for easier usage.
"""

from math import log10
import collections
import functools

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def plot_fn(fn):
    @functools.wraps(fn)
    def wrap(*args, x_lim=None, y_lim=None, y_label=None, x_label=None, title=None, **kwargs):
        plt.clf()
        figure = fn(*args, **kwargs)
        if x_label is not None:
            plt.xlabel(x_label)
        if y_label is not None:
            plt.ylabel(y_label)
        if title is not None:
            plt.title(title)
        return figure
    return wrap

def rotate_xlabels(degrees=30):
    fig, ax = plt.subplots()
    labels = ax.get_xticklabels()
    plt.setp(labels, rotation=degrees)

@plot_fn
def bar(xs, orient="v", error=None, **kwargs):
    assert isinstance(xs, pd.Series)
    if orient == "v":
        x,y = xs.index, xs
    else:
        x,y = xs, xs.index

    data = pd.DataFrame.from_dict({"x": x, "y": y})
    ax = sns.barplot(x="x", y="y", data=data, orient=orient, order=xs.index)
    if error is not None:
        if isinstance(error, collections.Iterable):
            assert len(error) == len(xs)
            error = list(error)
            for i in range(len(error)):
                ax.errorbar(i, xs.iloc[i], yerr=error[i], fmt="o", color="black")
        else:
            ax.errorbar(np.arange(len(xs)), xs, yerr=error, fmt="o", color="black")
    return plt.gcf()

def coef(fit):
    """
    Plot of statsmodels linear model coefficients.
    """
    return bar(fit.params, error=fit.bse)

def joint_density(x, y, scale=0.25):
    f, ax = plt.subplots(figsize=(6, 6))
    xr = x.max() - x.min()
    yr = y.max() - y.min()
    plt.xlim((x.min() - xr * scale, x.max() + xr * scale))
    plt.ylim((y.min() - yr * scale, y.max() + yr * scale))

    sns.kdeplot(x, y, ax=ax)
    sns.rugplot(x, color="g", ax=ax)
    sns.rugplot(y, vertical=True, ax=ax)
    return ax

def text_histogram(x, bins=25):
    h,b = np.histogram(x, bins)

    for i in range (0, bins-1):
	    print(str(b[i]).rjust(7)[:int(log10(np.amax(b)))+5], '| ', '#'*int(70*h[i-1]/np.amax(h)))
    print(str(b[bins]).rjust(7)[:int(log10(np.amax(b)))+5])

def heatmap(data):
    x_pt = plt.rcParams['xtick.labelsize']
    y_pt = plt.rcParams['ytick.labelsize']
    dpi = 80

    # comput the matrix height in points and inches
    matrix_height_pt = y_pt * data.shape[0]
    matrix_height_in = matrix_height_pt / dpi
    matrix_width_pt = x_pt * data.shape[1]
    matrix_width_in = matrix_width_pt / dpi

    # compute the required figure height 
    margin = 0.04
    figure_height = matrix_height_in / (1 - 2 * margin)
    figure_width = matrix_width_in / (1 - 2 * margin)

    # build the figure instance with the desired height
    fig, ax = plt.subplots(
        figsize=(figure_width,figure_height), 
        gridspec_kw={
            "top": 1 - margin,
            "bottom": margin,
            "left": margin,
            "right": 1 - margin
        })

    # let seaborn do it's thing
    ax = sns.heatmap(data, ax=ax, square=True)
    return ax

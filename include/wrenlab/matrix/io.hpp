#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <cassert>
#include <memory>

#include <wrenlab/util.hpp>

#include <wrenlab/matrix/types.hpp>

namespace wrenlab {

class MatrixReader {
    std::ifstream input;
    std::vector<std::string> _columns;
    char delimiter;
    Row next_row;
    bool _eof = false;

protected:
    void read_next() {
        std::string line;
        if ((!getline(input, line)) | (line.size() == 0)) {
            _eof = true;
            return;
        }

        std::vector<std::string> tokens = split(line, delimiter);
        assert(tokens.size() == (_columns.size() + 1));

        std::vector<double> data(_columns.size());
        for (int i=0; i<_columns.size(); i++) {
            data[i] = atof(tokens[i+1].c_str());
        }
        next_row = Row(tokens[0], data);
    }

public:
    MatrixReader(std::string path, char delimiter='\t') {
        this->delimiter = delimiter;

        input.open(path);
        std::string line;
        getline(input, line);
        std::vector<std::string> tokens = split(line, delimiter);
        for (int i=1; i<tokens.size(); i++) {
            _columns.push_back(tokens[i]);
        }
        read_next();
    }

    bool eof() {
        return _eof;
    }

    const std::vector<std::string>& columns() {
        return _columns;
    }

    Row next() {
        assert(!eof());
        Row o = next_row;
        read_next();
        return o;
    }
};

class SeriesReader {
private:
    bool _eof = false;
    Series current;
    std::ifstream input;

    void read_next();

public:
    std::shared_ptr<SeriesIndex> p_index;

    SeriesReader(std::string path="/dev/stdin");
    ~SeriesReader();

    bool eof();
    Series next();
};

};

#pragma once

#include <iostream>
#include <memory>

#include <wrenlab/matrix/io.hpp>
#include <wrenlab/matrix/types.hpp>
#include <wrenlab/sqlite.hpp>
#include <wrenlab/util.hpp>

namespace wrenlab {

class MatrixDB : public SQLiteDB {
protected:
    bool is_initialized;
    std::vector<std::string> _index, _columns;
    const char* SCHEMA = 
        "CREATE TABLE columns ("
        "   i INTEGER PRIMARY KEY NOT NULL,"
        "   key VARCHAR NOT NULL"
        ");"
        "CREATE TABLE data ("
        "   key VARCHAR UNIQUE,"
        "   data BLOB NOT NULL"
        ");";


    std::shared_ptr<SeriesIndex> c_ix;

    std::string get_schema();

    void insert_rows(std::vector<Series>&);
    void load_indices();

public:
    MatrixDB(const std::string path, bool read_only=true) : SQLiteDB(path, read_only) {
        if (read_only) {
            assert(file_exists(path));
            load_indices();
        }
    }

    virtual void initialize(SeriesReader&);

    virtual const std::vector<std::string>& columns();
    virtual const std::vector<std::string>& index();

    Row*
    row(const std::string key) {
        /* N.B., caller is responsible for deleting the row */
        std::string query = "SELECT key,data FROM data WHERE key='" + key + "';";
        PreparedStatement ps = prepare_statement(query);
        ps.step();
        if (!ps.has_next()) {
            return NULL;
        }
        Row* row = new Row();
        std::vector<char> dc = ps.get_blob(1);
        std::string data_compressed(dc.begin(), dc.end());
        std::string du = decompress_string(data_compressed);
        double* p = (double*) &du[0];
        row->key = key;

        size_t n = du.size() / sizeof(double);
        //std::cerr << n << "\t" << _columns.size() << std::endl;
        assert(n == _columns.size());

        std::vector<double> data;
        data.assign(p, p+n);
        row->data = data;
        return row;
    }
};

};

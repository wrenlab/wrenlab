#pragma once

#include <string>
#include <vector>

namespace wrenlab {

std::vector<std::string> split(const std::string &text, char sep='\t');
std::string lowercase(std::string);
std::string uppercase(std::string);

std::string slurp(const std::string path);

std::string compress_string(const std::string& s);

std::string decompress_string(const std::string& s);

bool file_exists(const std::string& path);

};

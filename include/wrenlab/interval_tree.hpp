#include <vector>
#include <cstdint>

namespace wrenlab {

struct Interval {
    uint64_t start, end;
    void* data;

    bool overlaps(const Interval& o) {
        return (start >= o.end) & (end <= o.start);
    }
};

struct start_lt_key
{
    inline bool operator() (const Interval* iv1, const Interval* iv2)
    {
        return (iv1->start < iv2->start);
    }
};

struct end_lt_key
{
    inline bool operator() (const Interval* iv1, const Interval* iv2)
    {
        return (iv1->end < iv2->end);
    }
};


class IntervalNode;

class IntervalNode {
    uint64_t center;
    IntervalNode *left, *right;
    std::vector<Interval*> v_left, v_right, ov_begin, ov_end;

    std::vector<Interval*> search(uint64_t);
};

class IntervalTree : protected IntervalNode {
    std::vector<Interval*> intervals;

    IntervalTree();
    ~IntervalTree();

    void add(uint64_t, uint64_t, void*);
    void build();
};

};

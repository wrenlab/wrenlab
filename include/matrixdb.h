#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void matrixdb;
typedef void matrixdb_row;
typedef const void matrixdb_index;

matrixdb* matrixdb_open(const char* path, bool read_only=true);
void matrixdb_close(matrixdb*);

matrixdb_row* matrixdb_get_row(matrixdb*, const char* key);
const char* matrixdb_row_key(matrixdb_row*);
size_t matrixdb_row_size(matrixdb_row*);
double* matrixdb_row_data(matrixdb_row*);
void matrixdb_row_delete(matrixdb_row*);

matrixdb_index* matrixdb_get_row_index(matrixdb*);
matrixdb_index* matrixdb_get_column_index(matrixdb*);
size_t matrixdb_index_size(matrixdb_index* ix);
void matrixdb_index_element(matrixdb_index* ix, size_t i, char* result);

#ifdef __cplusplus
}
#endif

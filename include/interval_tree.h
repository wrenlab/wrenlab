#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef void itree;
typedef void itree_match;

typedef struct  {
    uint64_t start, end;
    void* data;
} itree_interval;

itree* itree_new(itree_interval*, size_t);
void itree_delete(itree*);

itree_match* itree_search_overlapping(itree*, uint64_t, uint64_t);
itree_match* itree_search_contained(itree*, uint64_t, uint64_t);

void itree_match_delete(itree_match*);
size_t itree_match_size(itree_match*);
itree_interval itree_match_element(itree_match*, size_t);

#ifdef __cplusplus
}
#endif

TODO
====

- Possibly redo wrenlab.enrichment.AnnotationSet w/ xarray

- Finish implementing xarray-based Experiment

- Move a lot of stuff to `wrenlab.statistics` and organize it better

- Make lme4 bridge (or any GLM) return a MFit. Possibly rework MFit to use
  xarray to be less hairy.

- Convert Connectivity Map and MSIGDB to AnnotationSet. May require subclass
  for a "full" annotation matrix of scores, rather than the
  TermID-ElementID-Score "mapping" mode currently used. Alternatively, GO could
  just be converted to a full (sparse) matrix and AnnotationSet works only on
  that data type. Without sparsity this takes a lot of memory though.

Preprocessing
=============



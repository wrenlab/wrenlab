=================
Developer's Guide
=================

This guide is intended to be an orientation around the **wrenlab** codebase
from a developer's perspective: to explain the general architecture of the
package and its submodules, as well as to present information on how to
contribute. The user-oriented guide (such as it is) is contained in the Sphinx
documentation in the **doc/** subdirectory.

Architecture Overview
=====================

Very broadly, the **wrenlab** package is organized as a standard Python package
in the *wrenlab* namespace. However, it does contain a small amount of R code
organized as a package in the **R/** subdirectory, as well as Sphinx
documentation and other configuration files as standard for a Python package.

The heart of the codebase is in the *wrenlab/* subdirectory, which contains a
variety of subpackages and modules. Following is a summary describing each of
the major subdivisions of the package and their general purpose. In addition,
the "stability level" of each package/module will be indicated:

- S = stable
- U = unstable
- E = experimental
- P = placeholder
- D = deprecated

All modules are subject to change, but stable modules have been heavily used
and tested. Unstable packages are generally packages that have been used and
tested, but have a suboptimal API or some other defect that will require work.
Experimental modules, obviously, are generally poorly tested and are
use-at-your-own risk. Placeholder modules have little content and express an
intent to add this functionality in the future. Deprecated modules probably
need to be removed and generally represent failed experiments.

As a developer, patches are welcomed for all types of module, but generally be
more careful when working with "stable" modules and submit smaller patches.

- **wrenlab.algorithm** (U): Implementations of generic CS algorithms
- **wrenlab.cluster** (P): Clustering algorithms
- **wrenlab.collapse** (U): Various methods for collapsing 2D matrices by an annotation (e.g., probe-gene maps)
- **wrenlab.correlation** (U): Optimized code for computing large amounts of correlation coefficients, etc.
- **wrenlab.data** (S/U): Parsers for various biological datasets
- **wrenlab.db** (E): Experiments with storing a variety of data in a unified SQL database
- **wrenlab.distance** (U): Methods for computing and evaluating distance metrics
- **wrenlab.enrichment** (S/U): Enrichment analyses on generic gene annotations -- this has been heavily used & tested but I feel the API needs a lot of work
- **wrenlab.ensembl** (E): Wrappers for a few **pysensembl** functions. This could be expanded in the future to be more like **wrenlab.ncbi**.
- **wrenlab.experiment** (D): Early experiments for a unified **Dataset**-like datatype representing multiple HT datasets
- **wrenlab.ext** (U): Wrappers for external programs not written in R
- **wrenlab.function_prediction** (D): Self-describing. Mostly superseded by **metalearn**.
- **wrenlab.genome** (E): Core datatypes representing genomic coordinates
- **wrenlab.GO** (D): Stuff for Zoltan. Needs to be moved somewhere more appropriate.
- **wrenlab.graph** (E): A wrapper for **igraph** to make it less painful to work with. **igraph** is a package like **networkx** but written in C++ so it is faster, but more annoying.
- **wrenlab.homology** (U): Methods for determining homologous gene IDs
- **wrenlab.impute** (S/U): Different matrix imputation methods. Largely a wrapper for **fancyimpute**. For now, just use **fancyimpute** directly if you need imputation until the API gets stabilized.
- **wrenlab.lm** (S): Linear model fitting, specifically for the case of fitting the same model to many independent variables. This module's **MFit** class will probably form the foundation of a **DESet**-like datatype (representing a collection of DE genes/loci). I'm not totally happy with the API but this module has been very heavily used/tested.
- **wrenlab.matrix** (D): Different experiments in matrix data storage. Deprecated by **matrixdb**.
- **wrenlab.mixin** (E): Foundation of a "traits" system of having different datatypes with common behavior. Don't worry about it.
- **wrenlab.ml** (D): An early version of **metalearn**, which replaces it.
- **wrenlab.ncbi.gene** (S): Lots of data access methods to get data about NCBI Gene IDs.
- **wrenlab.ncbi.(other)** (U): Data access for other NCBI databases
- **wrenlab.normalize** (S/U): Matrix normalization methods.
- **wrenlab.ontology** (S): Lots of methods for fetching and operating on OBO ontologies.
- **wrenlab.plot** (E): Methods for simplifying common plots.
- **wrenlab.R** (S/U/E): Wrappers for lots of different R packages via rpy2 in various states of completeness.
- **wrenlab.sandbox** (E): Place for anyone to put their junk, err, unfinished stuff.
- **wrenlab.script** (?): CLI scripts
- **wrenlab.signature** (E): Development of a type of directional analysis I'm calling signature analysis. Not ready to be used.
- **wrenlab.sql** (S): Schemas for different specialized SQLite databases used to store intermediate data.
- **wrenlab.statistics** (U): Miscellaneous statistical stuff
- **wrenlab.text** (U): Anything related to text mining
- **wrenlab.ui** (E): An experiment in making a generic web UI toolkit to quickly cook up data web interfaces
- **wrenlab.util** (S): Anything related to generic programming and not a specific biological data type or analysis. **remote_table** is particularly nifty IMO and needs some more documentation.
- **wrenlab.yml** (S): Configuration files for several different downstream functions.

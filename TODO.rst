Better enrichment
=================

- Ensure ancestors computed!!
- Impute annotations?

Distance metrics for DEG lists (etc)
====================================

- Filter by expression level
- Mahalanobis distance?
- https://en.wikipedia.org/wiki/Similarity_learning

By annotation:
- If two DEG sets are different in terms of rank, can they be (more) similar in terms of enriched terms?

Age prediction
==============

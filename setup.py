import os
from os.path import dirname, join as pjoin, exists as pexists
from setuptools import setup, find_packages, Extension
import setuptools.command.install

VERSION = "0.1.3"

DEPENDENCIES = [
    #"cython==0.23",
    "cython",
    "numpy", 
    "scipy", 
    "pandas", 
    "patsy",
    "statsmodels", 
    "joblib", 
    "networkx", 
    "seaborn",
    "pyyaml",
    "scikit-learn",
    "lxml",
    "nltk",
    "click",
    "filemagic",
    "fancyimpute",
    "bottleneck",
    "tqdm",
    "matplotlib_venn",
    "functional",
    "redis",
    "leveldb",
    "zmq",
    "psycopg2",
    "requests",
    "pyamg",
    "lz4",
    "rpy2",
    "unqlite",

    # experimental ML packages
    #"pyrcca",

    # bio-specific packages
    #"scikit-bio",
    "pyensembl",
    "biopython",

    # for wl.genome, see if can be eliminated
    "sortedcontainers", 

    # possibly useful stuff, not used in core yet
    "dask",
    "blaze",
    "cloudpickle",
    "xarray"
]

#requirements = pjoin(dirname(__file__), "requirements.txt")
#assert pexists(requirements)
#requirements = [str(r.req) for r in parse_requirements(requirements, session=PipSession())]


#######################
# Cython/C++ extensions
#######################

from Cython import Compiler

Compiler.language_level = 3

"""
def libwrenlab_extension():
    libraries = ["sqlite3", "z", "c", "armadillo"]
    src = []
    for dirpath, dirnames, fnames in os.walk("src/lib"):
        for fname in fnames:
            if fname.endswith(".cpp"):
                src.append(os.path.join(dirpath, fname))
    return Extension("libwrenlab",
            sources=src,
            language="c++",
            extra_compile_args=["-fopenmp", "-std=c++11", "-Iinclude"],
            libraries=libraries,
            extra_link_args=["-fopenmp"])
"""

extensions = [
        Extension("wrenlab.correlation",
            sources=["wrenlab/correlation.pyx"],
            extra_compile_args=['-fopenmp'],
            extra_link_args=['-fopenmp'],
            language="c++"),
        Extension("wrenlab.text.ahocorasick.types",
            sources=["wrenlab/text/ahocorasick/types.pyx"],
            language="c++"),
        Extension("wrenlab.text.ahocorasick.algorithm",
            sources=["wrenlab/text/ahocorasick/algorithm.pyx"],
            language="c++"),
#        Extension("wrenlab.genome.types",
#            sources=["wrenlab/genome/types.pyx"],
#            language="c++"),
#        Extension("wrenlab.genome.index",
#            sources=["wrenlab/genome/index.pyx"],
#            language="c++"),
#        Extension("wrenlab.genome.BBI",
#            sources=["wrenlab/genome/BBI.pyx"],
#            language="c++"),
]

include_dirs = []
try:
    import numpy
    include_dirs.append(numpy.get_include())
except ImportError:
    pass

##########
# cmdclass
##########

class install(setuptools.command.install.install):
    def run(self):
        self.run_command("build_ext")
        return setuptools.command.install.install.run(self)

cmdclass = {"install": install}

try:
    from Cython.Distutils import build_ext
    cmdclass["build_ext"] = build_ext
except ImportError:
    # FIXME
    pass

####################
# Package definition
####################

setup(
    name="wrenlab",
    version=VERSION,
    description="Wren Lab bioinformatics utilities",
    author="Cory Giles, Chris Reighard, Xiavan Roopnarinesingh, Aleksandra Perz, Chase Brown, Hunter Porter",
    author_email="mail@corygil.es",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.6",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    license="AGPLv3+",

    include_package_data=True,
    packages=find_packages(),
    ext_modules=extensions,
    include_dirs=include_dirs,
    cmdclass=cmdclass,
    
    install_requires=DEPENDENCIES,
    extras_require={
        "r": ["rpy2"]
    },
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)

Wren Lab codebase
=================

This repository contains shared algorithms and data structures used in various
Wren Lab projects.

Contributors
============

- Cory Giles <mail@corygil.es>
- Chris Reighard <Chris-Reighard@ouhsc.edu>
- Xiavan Roopnarinesingh <Xiavan-Roopnarinesingh@ouhsc.edu>
- Aleksandra Perz <Aleksandra-Perz@omrf.org>
- Chase Brown <Chase-Brown@omrf.org>
- Hunter Porter <Hunter-Porter@omrf.org>

PI: Jonathan Wren <Jonathan-Wren@omrf.org>
